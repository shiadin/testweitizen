<?php


 Route::match(['put', 'patch'], '/activate-commision/{commission}','CommisionController@activate')->name('activate-commision');
Route::resource('commissions', 'CommisionController');

  Route::match(['put', 'patch'], '/activate/{campaign}','CampaignController@activate')->name('activate');

Route::resource('campaigns', 'CampaignController');
// https://packagist.org/packages/depsimon/laravel-wallet
Route::get('transactions', 'AdminController@transactions')->name('transactions');
Route::get('transactions/{id}', 'AdminController@ShowTransactions')->name('show.transactions');
Route::get('user-payaments/{id}', 'AdminController@manageUserPayaments')->name('user-payament');
Route::post('completUserPayaments', 'AdminController@CompletUserPayaments')->name('CompletUserPayaments');
Route::get('user-payaments', 'AdminController@userPayaments')->name('user-payaments');

Route::get('dashboard', 'AdminController@dashboard')->name('dashboard');
Route::get('shipping-orders', 'AdminController@shippings')->name('shippings');
Route::get('dealers-management', 'AdminController@members')->name('members');
Route::get('dealers.commissions', 'AdminController@dealersCommissions')->name('dealers.commissions');
Route::get('dealers_commissions_api', 'AdminController@dealers_commissions_api')->name('dealers_commissions_api');

Route::get('withdrawals_api', 'AdminController@withdrawals_api')->name('withdrawals_api');
Route::get('withdrawal-request-history', 'AdminController@withdrawals')->name('withdrawals');
Route::get('workspace', 'AdminController@workspace')->name('workspace');
Route::get('stocks-management', 'AdminController@stocks')->name('stocks');
Route::get('e-wallet', 'AdminController@credits')->name('credits');
Route::get('stock-transfer-history', 'AdminController@requests')->name('requests');
Route::get('company-downlines', 'AdminController@downlines')->name('downlines');

Route::get('users/getupline', 'UserController@getupline')->name('users.getupline');
Route::get('users/index', 'UserController@index')->name('users.index');
Route::get('roles/index', 'RoleController@index')->name('roles.index');
Route::get('orders/index', 'OrderController@index')->name('orders.index');
Route::get('stocks/index', 'StockController@index')->name('stocks.index');
Route::get('products/index', 'ProductController@index')->name('products.index');

Route::get('workspace/deposits&accounts', 'WorkspaceController@depositsAccounts')->name('workspace.deposits&accounts');
Route::get('workspace/stocks', 'WorkspaceController@stocks')->name('workspace.stocks');
Route::get('workspace/credits', 'WorkspaceController@credits')->name('workspace.credits');
Route::get('workspace/wallets', 'WorkspaceController@wallets')->name('workspace.wallets');
Route::get('workspace/shippings', 'WorkspaceController@shippings')->name('workspace.shippings');

Route::get('user/{user}/edit', 'UserController@edit')->name('user.edit');
Route::post('user/{user}/update', 'UserController@update')->name('user.update');
Route::post('user/{user}/approve', 'UserController@approve')->name('user.approve');
Route::post('user/{user}/reject', 'UserController@reject')->name('user.reject');
Route::post('user/{user}/ic/upload', 'UserController@icUpload')->name('user.ic.upload');
Route::post('user/{user}/avatar/upload', 'UserController@uploadUpload')->name('user.avatar.upload');
Route::post('user/{user}/settings/update', 'UserController@updateSettings')->name('user.settings.update');
Route::post('user/{user}/credits/update', 'UserController@updateCredits')->name('user.credits.update');

Route::post('user/{user}/stock/update', 'StockController@update')->name('user.stock.update');

Route::post('payment/{payment}/approve', 'PaymentController@approve')->name('payment.approve');
Route::post('payment/{payment}/reject', 'PaymentController@reject')->name('payment.reject');

Route::post('order/{type}/create', 'OrderController@create')->name('order.create');
Route::get('shipping-orders/new', 'OrderController@new')->name('shipping.new');
Route::get('shipping/{order}/edit', 'OrderController@edit')->name('shipping.edit');
Route::post('shipping/{order}/update', 'OrderController@updateShipping')->name('shipping.update');
Route::post('shipping/{order}/process', 'OrderController@processShipping')->name('shipping.process');
Route::post('shipping/{order}/confirm', 'OrderController@confirmShipping')->name('shipping.confirm');

Route::post('order/{order}/cancel', 'OrderController@cancel')->name('order.cancel');

Route::post('shippings/process', "OrderController@bulkProcess")->name('shippings.process');
Route::post('shippings/confirm', "OrderController@bulkConfirm")->name('shippings.confirm');


Route::middleware(['admin:super'])->group(function () {
    Route::post("user/{user}/upline/update", "UserController@updateUpline")->name("user.upline.update");
    Route::post("user/{user}/role/update", "UserController@updateRole")->name("user.role.update");
   Route::post('UpdateImage/{product}', 'ProductController@UpdateImage')->name('UpdateImage');
    Route::get('products', 'AdminController@products')->name('products');
    Route::get('product/new', 'ProductController@new')->name('product.new');
    Route::post('product/create', 'ProductController@create')->name('product.create');
    Route::get('product/{product}/edit', 'ProductController@edit')->name('product.edit');
    Route::post('product/{product}/update', 'ProductController@update')->name('product.update');
    Route::get('product/{product}/delete', 'ProductController@delete')->name('product.delete');
    Route::resource('settings', 'SettingController');
});


Route::get('/markAsRead', function(){
 auth()->user()->unreadNotifications->markAsRead();
});