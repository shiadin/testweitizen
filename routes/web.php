<?php
use App\Gamify\Points\DownlineCommissionReward;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('all', function(){
  $user = App\User::all();
  foreach ($user as $key => $value) {
    $value->password =  Hash::make('123456');
    $value->save();
  }
});

Route::get('wallet_bav_xd_df', function(){
   // $upline = App\User::find(8);
   // $thisUser = App\User::find(106);
   // $order = App\Order::find(205);
   // $admin = App\User::find(1);

  // $upline = App\User::find(29);
  // $thisUser = App\User::find(47);
  // $order = App\Order::find(39);
  // $admin = App\User::find(1);

  //   $upline = App\User::find(29);
  // $thisUser = App\User::find(62);
  // $order = App\Order::find(52);
  // $admin = App\User::find(1);


  // $upline = App\User::find(29);
  // $thisUser = App\User::find(98);
  // $order = App\Order::find(162);
  // $admin = App\User::find(1);

  $ori_wal =  $upline->balance;
   $wallet = $upline->depositA(50, 'deposit', ['order'=>$order->id, 'downline recruit' => $thisUser, 'description' => 'Deposit of RM 50 from downline('.$thisUser->username.') recruit commision', 'downline'=>$thisUser->name]);
  $wal_new =  $upline->balance;

            $GroupCommission = new App\GroupCommission();
             $GroupCommission->user_id = $thisUser->upline->id;
             $GroupCommission->type = 'RecruitCommission';
             $GroupCommission->amount = 50;
             // $GroupCommission->quantity = $item->quantity;
             $GroupCommission->unit_price = $order->amount;
             $GroupCommission->meta = 'Deposit of RM 50  from downline ('.$thisUser->username.') recruit commision';
              $GroupCommission->downline = $thisUser->username;
             $GroupCommission->downline_level = $thisUser->role_id;
             $GroupCommission->downline_id = $thisUser->id;
             $GroupCommission->current_level = $thisUser->upline->role_id;
             $GroupCommission->save();

              activity()
                ->performedOn($upline)
                ->causedBy($admin)
                ->withProperties($order)
                ->log("RM 50  of downline recruit commission has been added to account from Downlines#$thisUser->username by :causer.username ($ori_wal => $wal_new)");



});

Route::get('qr-code', 'UserController@qrInvitation')->name('user.invitation');

Route::get('check', 'UserController@authenticate_dealer_certificate')->name('authenticate_dealer_certificate');
Route::get('user/{username}/certificate', 'UserController@cert')->name('user.certificate');
Route::get('user/{username}/qrcode', 'UserController@qrCode')->name('user.qrcode');
Route::get('user/activities', 'ActivityController@index')->name('user.activities');

Auth::routes();
Route::get('login-with-phone', 'VerifyOTPController@login')->name('loginPhone');
Route::get('register/{code}', 'Auth\RegisterController@showRegistrationForm')->name('referral');
Route::post('register/{code}', 'Auth\RegisterController@register')->name('register');

Route::get('user/{username}/certificate', 'UserController@cert')->name('user.certificate');
Route::get('user/{username}/qrcode', 'UserController@qrCode')->name('user.qrcode');
Route::get('user/{username}/avatar', 'UserController@avatar')->name('user.avatar');
Route::get('user/activities', 'ActivityController@index')->name('user.activities');

Route::middleware(['auth', 'complete.profile',  'TwoFA'])->group(function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('my-dealers', 'HomeController@downlines')->name('user.downlines');
    Route::get('user/my-dealers/stats', 'UserController@downlineStat')->name('user.stats');
    Route::get('my-shipping-order', 'HomeController@shippings')->name('user.shippings');
    Route::get('my-stock-transfer-history', 'HomeController@requests')->name('user.requests');

    Route::get('product/{product}', 'ProductController@get')->name('product.get');


 Route::post('payment_proof', 'OrderController@payment_proof')
            ->name('user.order.payment_proof');

    Route::post('user/{type}/create', 'OrderController@create')
            ->where('type', '(stocks|shipping|credits)')
            ->name('user.order.create');

    Route::post('user/referral/create', 'ReferralController@create')->name('user.referral.create');
    Route::get('user/referral/{referral}/delete', 'ReferralController@delete')->name('user.referral.delete');

    Route::get('user/shipping/{order}/edit', 'OrderController@edit')->name('user.shipping.edit');
    Route::post('user/shipping/{order}/update', 'OrderController@updateShipping')->name('user.shipping.update');
    Route::get('user/shipping/new', 'OrderController@new')->name('user.shipping.new');

     Route::get('user/postpaid/new', 'OrderController@postpaid')->name('user.postpaid.new');
     Route::post('my-customers/add', 'UserController@AddCustomers')->name('customers.add');

    Route::get('user/my-customers', 'UserController@customers')->name('user.customers');

    Route::post('user/stock/transfer', 'StockController@transfer')->name('user.stock.transfer');

    Route::post('user/order/{order}/cancel', 'OrderController@cancel')
            ->name('user.order.cancel');

    Route::get('user/order/{order}/invoice', 'OrderController@invoice')
            ->name('user.order.invoice');

    Route::get('user/order/index', 'OrderController@index')->name('user.orders.index');
    Route::get('products/index', 'ProductController@index')->name('products.index');
    Route::get('user/my-dealers', 'UserController@downlines')->name('user.downlines.index');


Route::get('campaign', 'HomeController@campaign')->name('user.campaign');
Route::get('user/my-e-wallet', 'HomeController@wallet')->name('user.wallets');

Route::get('my-activity-notifications', 'HomeController@inbox')->name('user.inbox');
Route::get('postpaid', 'HomeController@postpaid')->name('user.postpaid');

Route::get('my-personal-commission', 'HomeController@commission')->name('user.commission');
Route::get('my-group-sales-commission', 'HomeController@Groupcommission')->name('user.group.commission');

Route::get('stocks', 'StockController@stocks')->name('user.stocks');
Route::get('my-stocks', 'StockController@stocks')->name('user.stocks');
Route::post('manageStock', 'StockController@manageStock')->name('user.manageStock');

Route::get('payments', 'HomeController@payment')->name('user.payments');

Route::get('my-customers', 'HomeController@customer')->name('user.customer');
Route::get('activity_info', 'HomeController@activity_info')->name('activity_info');

});

Route::middleware(['auth'])->group(function () {
    Route::get('roles/index', 'RoleController@index')->name('roles.index');

    Route::get('settings', 'HomeController@profile')->name('user.profile');

    Route::post('user/update', 'UserController@update')->name('user.update');
    Route::post('user/settings/update', 'UserController@updateSettings')->name('user.settings.update');
    Route::post('user/ic/upload', 'UserController@icUpload')->name('user.ic.upload');
    Route::post('user/avatar/upload', 'UserController@avatarUpload')->name('user.avatar.upload');

    Route::post('user/deposit/{type}/create', 'OrderController@create')
            ->where('type', '(deposit:\d)')
            ->name('user.deposit.create');

            // marktplace
Route::delete('emptyCart', 'CartController@emptyCart');
Route::post('switchToWishlist/{id}', 'CartController@switchToWishlist');

Route::resource('wishlist', 'WishlistController');
Route::delete('emptyWishlist', 'WishlistController@emptyWishlist');
Route::post('switchToCart/{id}', 'WishlistController@switchToCart');
 Route::resource('cart', 'CartController');
 Route::get('payment-successful/{id}', 'CartController@successfully');
Route::get('payment-failure/{id}', 'CartController@failure');
Route::get('payment-pending/{id}', 'CartController@pending');
Route::resource('bankdetails', 'BankDtailController');
Route::get('requestwithdraw.user', 'withdrawRequestController@user')->name('requestwithdraw.user');
Route::post('requestwithdraw.process', 'withdrawRequestController@process')->name('requestwithdraw.process');
Route::resource('requestwithdraw', 'withdrawRequestController');

Route::post('/callback-stock-add', 'Molpay_Callback_Market_Controller@index');
Route::post('/callback-credit-add', 'Molpay_Callback_Credit_Controller@index');
Route::get('/stockReload', "MolpayController@stockReload")->name('stockReload');
Route::get('/creditReload', "MolpayController@creditReload")->name('creditReload');
Route::resource('/molPays', 'MolpayController');
Route::post('/callback', 'Molpay_Callback_Controller@index');
Route::post('ipn-molpay', 'MolpayNotificationController@index');
Route::post('/push', 'Molpay_Callback_Controller@push');
});


Route::get('/verifyOTP', 'VerifyOTPController@showverifyform')->name('verifyOTP');
Route::post('/verifyOTP', 'VerifyOTPController@verify')->name('verifyOTP');
Route::post('/resendOTP', 'VerifyOTPController@resendOTP')->name('resendOTP');
Route::post('/UpdatePhoneEmailOTP', 'VerifyOTPController@UpdatePhoneEmailOTP')->name('UpdatePhoneEmailOTP');
Route::post('/UpdatePhoneEmail', 'VerifyOTPController@UpdatePhoneEmail')->name('UpdatePhoneEmail');
Route::get('dashboard', 'HomeController@dashboard')->name('user.dashboard');




Route::get('ajax/users', 'AdminController@downline_api')->name('ajax.users.index');
Route::get('ajax/my-dealers', 'HomeController@downline_api')->name('ajax.downlines');
Route::get('ajax/postpaid', 'HomeController@postpaid_api')->name('ajax.postpaid');
Route::get('ajax/customer', 'HomeController@customer_api')->name('ajax.customer');

Route::get('ajax/my-personal-commission', 'HomeController@commission_api')->name('ajax/commission');
Route::get('ajax/my-group-sales-commission_api', 'HomeController@groupcommission_api')->name('ajax/groupcommission_api');
Route::get('ajax/wallet', 'HomeController@wallet_api')->name('ajax/wallet');
Route::get('ajax/viewusers/{id}', 'AdminController@viewusers')->name('ajax.user.viewusers');

Route::get('/markAsRead', function(){
 auth()->user()->unreadNotifications->markAsRead();
});

