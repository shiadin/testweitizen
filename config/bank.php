<?php
return [
    "Affin Islamic Bank Berhad",
    "Al Rajhi Banking & Investment Corporation (Malaysia) Berhad",
    "AmBank Islamic Berhad",
    "Bank Islam Malaysia Berhad",
    "Bank Muamalat Malaysia Berhad",
    "CIMB Islamic Bank Berhad",
    "HSBC Amanah Malaysia Berhad",
    "Hong Leong Islamic Bank Berhad",
    "Kuwait Finance House (Malaysia) Berhad",
    "MBSB Bank Berhad",
    "Maybank Islamic Berhad",
    "RHB Islamic Bank Berhad",
    "Standard Chartered Saadiq Berhad"


];
