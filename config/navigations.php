<?php
return [
    "basic" => [
        // "overview" => "user.dashboard",
        "Dashboard" => "home",
        "My Stocks" => "user.stocks",
        "My Shipping Order" => "user.shippings",
        //"My Sales (Postpaid)" => "user.postpaid",
        "My Dealers" => "user.downlines",
        "My Personal Commission" => "user.commission",
        "My Group Sales Commission"  => "user.group.commission",
        "My Stock Transfer History" => "user.requests",
        // "Campaign" => "user.campaign",
        "My Customers" => "user.customer",
        // "Payment" => "user.payments",
         "My Activity & Notifications" => "user.inbox",
         "My e-Wallet" => "user.wallets",
         "Settings" => "user.profile",
        
        // "requests" => "user.requests",
    ],
    "admin" => [
        "workspace" => "admin.workspace",
        "dashboard" => "admin.dashboard",
        "Company's Downline" => "admin.downlines",
        "Shipping Orders" => "admin.shippings",
        "Stocks Management" => "admin.stocks",
        "e-Wallet" => "admin.credits",
        "Stock Transfer History" => "admin.requests",
        "Dealers Management" => "admin.members",
        "Dealers Commission" => "admin.dealers.commissions",
        'Withdrawal Request History' => "admin.withdrawals"
        // "Campaigns Setting" => "admin.campaigns.index",
        // "Commissions Setting" => "admin.commissions.index",
        // "transactions" => "admin.transactions",
        // "paysments" => "admin.user-payaments",
    ],
    "super" => [
        "products" => "admin.products",
        // "settings" => "admin.settings.index",
    ]
];
