<?php

return array(
	/*
	|--------------------------------------------------------------------------
	| MolPay Merchant ID
	|--------------------------------------------------------------------------
	|
	| Insert your MolPay Merchant ID
	*/
	'MerchantID' => 'SB_weitizen',

	/*
	|--------------------------------------------------------------------------
	| MolPay Merchant Verify Key
	|--------------------------------------------------------------------------
	|
	| Insert your MolPay Merchant Verify Key
	*/
	'verify_key' => '3c9699ce26c4352ca945574040e771c1',

	/*
	|--------------------------------------------------------------------------
	| Molpay Default Payment Method
	|--------------------------------------------------------------------------
	|
	| Select default payment method from one of the list below
	*/
	'default_payment_method' => 'visa',

	/*
	|--------------------------------------------------------------------------
	| Molpay Currency
	|--------------------------------------------------------------------------
	|
	| By default, Molpay will use either MYR or USD
	*/
	'currency' => 'MYR',
	
	/*
	|--------------------------------------------------------------------------
	| Payment Methods
	|--------------------------------------------------------------------------
	*/
	'payment_methods' => array(
		'visa'        => 'index',
		'mastercard'  => 'index',
		'mobilemoney' => 'mobilemoney',
		'maybank2u'   => 'maybank2u',
		'fpx'         => 'fpx',
		'cimb'        => 'cimb',
		'eon'         => 'eon',
		'hongleong'   => 'hlb',
		'mepscash'    => 'mepscash',
		'webcash'     => 'webcash',
	),
);