<?php
return [
    'Malaysia' => [
        [
            "name" => "Kuala Lumpur",
            "code" => "MY-14",
            "subdivision" => "federal territory"
        ],
        [
            "name" => "Labuan",
            "code" => "MY-15",
            "subdivision" => "federal territory"
        ],
        [
            "name" => "Putrajaya",
            "code" => "MY-16",
            "subdivision" => "federal territory"
        ],
        [
            "name" => "Johor",
            "code" => "MY-01",
            "subdivision" => "state"
        ],
        [
            "name" => "Kedah",
            "code" => "MY-02",
            "subdivision" => "state"
        ],
        [
            "name" => "Kelantan",
            "code" => "MY-03",
            "subdivision" => "state"
        ],
        [
            "name" => "Melaka",
            "code" => "MY-04",
            "subdivision" => "state"
        ],
        [
            "name" => "Negeri Sembilan",
            "code" => "MY-05",
            "subdivision" => "state"
        ],
        [
            "name" => "Pahang",
            "code" => "MY-06",
            "subdivision" => "state"
        ],
        [
            "name" => "Perak",
            "code" => "MY-08",
            "subdivision" => "state"
        ],
        [
            "name" => "Perlis",
            "code" => "MY-09",
            "subdivision" => "state"
        ],
        [
            "name" => "Pulau Pinang",
            "code" => "MY-07",
            "subdivision" => "state"
        ],
        [
            "name" => "Sabah",
            "code" => "MY-12",
            "subdivision" => "state"
        ],
        [
            "name" => "Sarawak",
            "code" => "MY-13",
            "subdivision" => "state"
        ],
        [
            "name" => "Selangor",
            "code" => "MY-10",
            "subdivision" => "state"
        ],
        [
            "name" => "Terengganu",
            "code" => "MY-11",
            "subdivision" => "state"
        ]
    ],
    'Singapore' => [
        [
            "name" => "Central Singapore",
            "code" => "SG-01"
        ],
        [
            "name" => "North East",
            "code" => "SG-02"
        ],
        [
            "name" => "North West",
            "code" => "SG-03"
        ],
        [
            "name" => "South East",
            "code" => "SG-04"
        ],
        [
            "name" => "South West",
            "code" => "SG-05"
        ]
    ]
];
