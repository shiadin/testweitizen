<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class StockReload extends Notification
{
    use Queueable;

    public $stockReload;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($stockReload)
    {
        $this->stockReload = $stockReload;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
         return ['database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('Reminder - Please restock to reload the token to supply your downline stock request.')
                    ->action('Notification Action', url('/'))
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
   public function toDatabase($notifiable)
    {
        return [
            'user'=>$this->stockReload->user,
            'url' =>'/notifications/'.$this->stockReload->id,
            'status' => 'Please restock to supply the stock reload request from downline.',
            'downline' =>$this->stockReload->username,
        ];
    }
}
