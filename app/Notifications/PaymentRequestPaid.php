<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class PaymentRequestPaid extends Notification
{
    use Queueable;
     public $withdral;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($withdral)
    {
        $this->withdral = $withdral;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', url('/'))
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toDatabase($notifiable)
    {
         $ac = '';
         if($this->withdral->status==1){
             $ac = 'Approved';
         }else{
             $ac = 'Reject';
         }
        return [
            'user'=>$this->withdral->user,
            'url' =>'/notifications/'.$this->withdral->id,
            'status' => 'Admin have '.$ac. ' your request of withdral RM'.$this->withdral->amount .'  '. $this->withdral->memo,
            'downline' =>auth()->user()->username,
        ];
    }
}
