<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class withdrawRequest extends Model
{
   protected $fillable = ['user_id', 'amount', 'status', 'memo', 'payment_proof'];


   
   public function user(){
   	return $this->belongsTo('App\User')->with(['bank']);
   }


}
