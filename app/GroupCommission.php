<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GroupCommission extends Model
{
    protected $fillable = ['type', 'amount', 'user_id', 'status', 'meta', 'downline_id', 'quantity', 'unit_price', 'current_level', 'downline_level', 'downline'];
    public function user(){
    	return $this->belongsTo('App\User');
    }
     public function downline(){
    	return $this->hasOne('App\User', 'downline_id', 'id');
    }

 

    protected $casts =['meta'=>'array'];


   
}
