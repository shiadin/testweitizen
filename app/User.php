<?php

namespace App;

use LaravelQRCode\Facades\QRCode;
use QCod\Gamify\Gamify;
use Illuminate\Support\Facades\DB;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Mail\OTPCodeVerification;
use Carbon\Carbon;
use Depsimon\Wallet\HasWallet;

class User extends Authenticatable
{

    public static $adminGroup = [999, 950];
    const EXPIRATION_TIME = 2; // minutes
    use Notifiable, Gamify;
     use HasWallet;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'role_id', 'referrer_id', 'initial_referrer_id', 'username', 'avatar', 'memo', 'status',
        'name', 'email', 'password', 'phone_no', 'gender', 'credits', 'ic_no', 'ic_front', 'ic_back', 'isVerified', 'otp_code', 'country_code', 'reputation'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

     // protected $appends = ['uplinename'];

    protected static $situations =  [
        0 => 'processing',
        1 => 'approved',
        -1 => 'blocked'
    ];

    public function isAdmin($access = null)
    {
        // admin type user group
        $group = [
            'super' => 999,
            'admin' => 950,
        ];
        // if no specific access mentioned
        if (is_null($access)) {
            // check user in group admin or not
            return in_array($this->role_id, array_values($group));
        }
        // get group of roles mentioned in access
        $roles = array_filter($group, function ($value, $key) use ($access) {
            return in_array($key, $access);
        }, ARRAY_FILTER_USE_BOTH);
        // if specific admin mentioned, check whether is the admin role or not
        return in_array($this->role_id, $roles);
    }

    public function depositRequired()
    {
        // get reseller
        $reseller = collect(config("resellers"))->where('role_id', $this->role_id)->first();
        return $reseller ? $reseller['deposit'] : 0;
    }

    public function allowToRefer()
    {
        return $this->role_id > 0;
    }

    public static function getSituations()
    {
        return self::$situations;
    }

    public function assignToSuitableUpline($level)
    {
        // set upline
        $upline = $this->upline;
        // break here if upline have higher level than user
        if (!$upline || ($upline && $upline->role_id > $level)) {
            return;
        }
        // loop if:
        // 1. Upline exists
        // 2. Upline level lower or equal to specified level
        while ($upline && $upline->role_id <= $level) {
            // get upline of current upline (ancestor)
            $upline = $upline->upline;
        }
        // update user's upline
        $this->update(['referrer_id' => $upline ? $upline->id : null]);
        // get upline name if exists
        $name = $upline ? $upline->username : 'SYSTEM';
        // log activity
        activity()
            ->performedOn($this)
            ->causedBy(auth()->user())
            ->withProperties([])
            ->log("Upline have been changed to $name since previous upline has same or lower level than $this->username");
    }

    /**
     * Relationship
     */
    public function role()
    {
        return $this->belongsTo('App\Role');
    }

//     public function getUplinenameAttribute()
// {
//     return $this->upline->name;
// }

    public function address()
    {
        return $this->hasOne('App\Address');
    }

    /**
     * Deposit submited in current phase
     */
    public function deposit()
    {
        return $this->hasOne('App\Order')
                    ->select('orders.*')
                    ->whereRaw('type = concat("deposit:", users.role_id)')
                    ->join('users', 'users.id', 'orders.user_id')
                    ->orderByDesc('orders.status');
    }

    /**
     * All the deposits uploaded
     */
    public function deposits()
    {
        return $this->hasMany('App\Order')
                    ->where('type', 'like', 'deposit:_');
    }

    public function upline()
    {
        return $this->hasOne('App\User', 'id', 'referrer_id')
                    ->with(['address', 'role']);
    }


   public function uplineInit()
    {
        return $this->hasOne('App\User', 'id', 'initial_referrer_id')
                    ->with(['address', 'role']);
    }
     public function bank()
    {
        return $this->hasOne('App\BankDtail');
    }
   

   public function orders()
    {
      return $this->hasMany('App\Order')->whereStatus(3);
    }

    public function postpaids(){
      return $this->hasMany('App\Postpaid');
    }

    public function downlines()
    {
      return $this->hasMany('App\User', 'initial_referrer_id', 'id')->whereStatus(1)
                    ->with(['address', 'role']);

        // return $this->hasMany('App\User', 'referrer_id', 'id')->whereStatus(1)
        //             ->with(['address', 'role']);
    }

 public function downlineCount()
    {
        return $this->hasMany('App\User', 'referrer_id', 'id')
                    ->count();
    }
      public function stockToken()
    {
        return $this->hasOne('App\StockToken');
    }
    public function stocks()
    {
        return $this->hasMany('App\Stock');
    }
      public function stocksCount()
    {
        return $this->hasMany('App\Stock')->sum('quantity');
    }
   public function tokenCount()
    {
        return $this->hasMany('App\StockToken')->sum('quantity');
    }
    public function inviteCodes()
    {
        return $this->hasMany('App\Referral');
    }

    public function activities()
    {
        // https://laracasts.com/discuss/channels/eloquent/it-is-a-bug-using-syntax-distinct-with-eloquent-relationship
        return $this->hasMany('App\Activity', 'causer_id')
                    ->distinct()
                    ->select('description', 'created_at', 'causer_id', 'subject_type')
                    ->where('causer_type', 'App\User');
    }

    public function customers()
    {
        return $this->hasManyThrough('App\User', 'App\Customer', 'reseller_id', 'id', 'id', 'user_id')
                    ->with('address');
    }

    public function shippings()
    {
        return $this->hasMany('App\Order')
                    ->where('type', 'shipping');
    }
    public function shippingStat(){
      $processing = 0;
      $confirmed = 0;
      $delivery = 0;
      $completed = 0;
    foreach ($this->shippings as $key => $value) {
       if($value->status==0)
        $processing++;
      if($value->status==1)
        $confirmed++;
      if($value->status==2)
        $delivery++;
      if($value->status==4)
        $completed++;
    }
    return array(
         array('label'=>'Processing Order', 'value' => $processing),
         array('label'=>'Confirmed Order', 'value' => $confirmed),
         array('label'=>'Delivering Order', 'value' => $delivery),
         array('label'=>'Completed Order', 'value' => $completed),
      );
    }

    public function allShippingStat(){
      $processing = 0;
      $confirmed = 0;
      $delivery = 0;
      $completed = 0;
      $shippings = Order::where('type', 'shipping')->get();
    foreach ($shippings as $key => $value) {
       if($value->status==0)
        $processing++;
      if($value->status==1)
        $confirmed++;
      if($value->status==2)
        $delivery++;
      if($value->status==3)
        $completed++;
    }
    return array(
         array('label'=>'Processing Order', 'value' => $processing),
         array('label'=>'Confirmed Order', 'value' => $confirmed),
         array('label'=>'Delivering Order', 'value' => $delivery),
         array('label'=>'Completed Order', 'value' => $completed),
      );
    }

    /**
     * Accessor
     */
    public function getSituationAttribute($value)
    {
        $situation = self::$situations[$this->status];
        if (!$situation) {
            throw new \Exception("Something wrong in evaluate user situation", 500);
        }
        return $situation;
    }

   public function reputations(){
    return  $this->hasmany('QCod\Gamify\Reputation', 'payee_id', 'id')->where('name', 'ShippingOrderReward');
   }
 
   public function reputationsSum(){
    return  $this->hasmany('QCod\Gamify\Reputation', 'payee_id', 'id')->where('name', 'ShippingOrderReward')->sum('point');
   }

public function reward(){
  return $this->hasOne('App\Reward');
}
 public function rewards(){
    return  $this->hasmany('QCod\Gamify\Reputation', 'payee_id', 'id')->where('name', '!=', 'ShippingOrderReward');
   }
       public function sendOTP($via){
      //  dd($via);
        if($this->otp_code){ 
            if($via=='via_sms'){
               
  try {
             $basic  = new \Nexmo\Client\Credentials\Basic(env('NEXMO_API_KEY'), env('NEXMO_API_SECRET'));
            $client = new \Nexmo\Client($basic);
         $message = $client->message()->send([
        'to' => $this->getPhone(),
        'from' => 'Dealership-Reseller',
        'text' => 'Zstore. '.$this->otp_code.' is your verification code'
        ]);
    //dd($message->getResponseData()->message[0].status);
    } catch (\Exception $e) {
        
       auth()->logout();
       return redirect('/home');
        return 0;
    }
            }else{
         \Mail::to($this->email)->send(new OTPCodeVerification ($this->otp_code)); 
            }
      
       } else
       return back();
        
      }

   public function validity(){
    $from = date('d-m-Y', strtotime($this->created_at));
    return 'Valid from '.$from.' to 31-12-2019';
   }
   public function getPhone()
    {
        return $this->country_code.$this->phone_no;
       // return $this->country_code.ltrim($this->phone_no, '0');
    }

    public function getProgress()
    {
        return (($this->totalCampaign())*100)/10000;
    }
     public function getProgress1()
    {
        return (($this->totalCampaign())*100)/20000;
    }
     public function getProgress2()
    {
        return (($this->totalCampaign())*100)/35000;
    }

     public function getNeeded()
    {

        if($this->totalCampaign()>=35000){
              $percent = round(((35000 - $this->totalCampaign())/35000)*100, 0);
              $currentEarning = array('p' => $percent,
                   'progressbar'=>"width:".$percent."%;",
                   'amount' => 'MYR 3000',
                   'nextAmount' =>'MYR 3000',
                   'lastearning'=> 0,
                   'allEarning'=> 0, );
              return $currentEarning;
        }else if($this->totalCampaign()>=20000){
             $percent = (100-round(((35000 - $this->totalCampaign())/35000)*100, 0));
              $currentEarning = array('p' => $percent,
                   'progressbar'=>"width:".$percent."%;",
                   'amount' => 'MYR 1500',
                   'nextAmount' =>'MYR 3000',
                   'lastearning'=> 0,
                   'allEarning'=> 0, );
              return $currentEarning;
        }else if($this->totalCampaign()>=10000){
             $percent = (100 - round(((20000 - $this->totalCampaign())/20000)*100, 0));
              $currentEarning = array('p' => $percent,
                   'progressbar'=>"width:".$percent."%;",
                   'amount' => 'MYR 600',
                   'nextAmount' =>'MYR 1500',
                   'lastearning'=> 0,
                   'allEarning'=> 0, );
              return $currentEarning;
        }else{
        $percent = (100 - round(((10000 - $this->totalCampaign())/10000)*100, 0));
              $currentEarning = array('p' => $percent,
                   'progressbar'=>"width:".$percent."%;",
                   'amount' => 'MYR 0',
                   'nextAmount' =>'MYR 600',
                   'lastearning'=> 0,
                   'allEarning'=> 0, );
              return $currentEarning;
        }
       
    }

public function molpays(){
  return $this->hasMany('App\Molpay');
}
    public function getNeeded0()
    {
        return (10000 - $this->totalCampaign());
    }
     public function getNeeded1()
    {
        return (20000 - $this->totalCampaign());
    }

     public function getNeeded2()
    {
        return (35000 - $this->totalCampaign());
    }

    public function commission(){
     return  $this->hasmany('QCod\Gamify\Reputation', 'payee_id', 'id')->where('name', 'PersonalOrderRewards')->orWhere('name', 'DownlineCommissionReward');  
    }
 
    public function campaigns(){
     return  $this->hasmany('QCod\Gamify\Reputation', 'payee_id', 'id')->where('name', 'ShippingOrderReward');
    }
    public function totalCommission(){
        $sum=0;
        foreach ($this->commission as $key => $value) {
            $sum+=$value->point;
        }
        return $sum;
    }
    public function totalCampaign(){
       $sum=0;
        foreach ($this->campaigns as $key => $value) {
            $sum+=$value->point;
        }
        return $sum;
    }
   public function generateCode($len) {
      $code = '';
   for($i = 0; $i < $len; $i++) {
     $code .= mt_rand(0, 9);
   }
    return $code;
   }

   public function getPhoneNumber()
    {
        return $this->country_code.$this->phone_no;
    }

    public function isExpired()
    {
        return $this->updated_at->diffInMinutes(Carbon::now()) > static::EXPIRATION_TIME;
    }

    public function totalOrder(){
      return $this->hasMany('App\Order')->whereIn('status', [0,1,2,3])->count();
    }

    public function downlineProgress(){
 $user = auth()->user();
 $starOne = 0;
 $starTwo = 0;
 $starThree = 0;
 $starFour = 0;
 $starFive = 0; 
     if(empty($user->downlines)){
        return;
     }else{ 
     foreach ($user->downlines as $key => $value) {
        if($value->role_id == 3){
           $starThree++;
        }
         if($value->role_id == 4){
           $starFour++;
        }
         if($value->role_id == 5){
           $starFive++;  
        }
         if($value->role_id == 1){
           $starOne++;  
        }
         if($value->role_id == 2){
           $starTwo++;  
        }
     }
     }
  

     $p1 = 0;
     $p3 = 0;
     $p5 = 0;
     $p8 = 0;
     $p12 = 0;

     $progressbar1 = 'width: 0%;';
     $progressbar3 = 'width: 0%;';
     $progressbar5 = 'width: 0%;';
     $progressbar8 = 'width: 0%;';
     $progressbar12 = 'width: 0%;';


     $current = 0;
     if($starFive>=10){
      $commission10 = \Config::get('commission.starFive10');
       $p12 = 100;
       $p1 =  100;
       $p3 =  100;
       $p5 =  100;
       $p8 =  100;
       $current = 12;
       
     }else if($starFive>=5){
       $p12 =  round(($starFive/10)*100,0);
       $p1 =  100;
       $p3 =  100;
       $p5 =  100;
       $p8 =  100;
       $current = 8;
     }else if($starFive>=1){
       $p12 =  round(($starFive/10)*100,0);
       $p1 =  100;
       $p3 =  100;
       $p5 =  100;
       $p8 =   round(($starFive/5)*100,0);
      $current =5;
     }
     else if($starFour>=3){
       $p12 =  round(($starFive/10)*100,0);
       $p1 =  100;
       $p3 =  100;
       $p5 =  round(($starFive/1)*100, 0);
       $p8 =   round(($starFive/5)*100, 0);
       $current =3;
     }else if($starThree>=3){
       $p12 =  round(($starFive/10)*100, 0);
       $p1 =  100;
       $p3 =  number_format((float)(($starFour/3)*100), 2, '.', '');
       $p5 =   round(($starFive/1)*100, 0);
       $p8 =   round(($starFive/5)*100, 0);
       $current =1;
     }else if($starThree<3){
      $p12 =  round(($starFive/10)*100, 0);
       $p1 =  round(($starThree/3)*100, 0);;
       $p3 =  number_format((float)(($starFour/3)*100), 0, '.', '');
       $p5 =   round(($starFive/1)*100, 0);
       $p8 =   round(($starFive/5)*100, 0);
       $current =0;
     }

    $arr = array(
    array(
        "customCheck"=>"customCheck1",
        "name"=> '3 Star: '.$starThree.'/3',
        "desc" => "Recruited a 3 x 3 Stars downlines, to receive a 1% commission on your downline's order.",
        "p" => $p1,
        "c" => $p1==100?1:0,
        "progressbar" => 'width: '.$p1.'%;'
    ),
    array(
        "customCheck"=>"customCheck3",
       "name"=> '4 Star: '.$starFour.'/3',
        "desc" => "Recruited a 3 x 4 Stars downlines, to receive a 3% commission on your downline's order.",
        "p" => $p3,
        "c" => $p3==100?1:0,
        "progressbar" => 'width: '.$p3.'%;'
    ),
    array(
        "customCheck"=>"customCheck5",
        "name"=> '5 Star: '.$starFive.'/1',
        "desc" => "Recruited a 1 x 5 Stars downlines, to receive a 5% commission on your downline's order.",
         "p" => $p5,
         "c" => $p5==100?1:0,
        "progressbar" => 'width: '.$p5.'%;'
    ),
    array(
        "customCheck"=>"customCheck8",
         "name"=> '5 Star: '.$starFive.'/5',
        "desc" => "Recruited a 5 x 5 Stars downlines, to receive a 8% commission on your downline's order.",
         "p" => $p8,
         "c" => $p8==100?1:0,
         "progressbar" =>'width: '.$p8.'%;'
    ),
    array(
        "customCheck"=>"customCheck12",
         "name"=> '5 Star: '.$starFive.'/10',
        "desc" => "Recruited a 10 x 5 Stars downlines, to receive a 12% commission on your downline's order.",
         "p" => $p12,
         "c" => $p12==100?1:0,
        "progressbar" => 'width: '.$p12.'%;'
    )
);
    
    $downlineCount = array(
       array('value' => $starOne,),
       array('value' => $starTwo),
       array('value' => $starThree),
       array('value' => $starFour),
       array('value' => $starFive)); 
  $data  = array('current' => $current,
   'data'=>$arr,
   'downData' => $downlineCount,
   "c1" => $p1==100?"timeline-badge success":"timeline-badge primary",
  "c3" => $p3==100?"timeline-badge success":"timeline-badge primary",
  "c5" => $p5==100?"timeline-badge success":"timeline-badge primary",
  "c8" => $p8==100?"timeline-badge success":"timeline-badge primary",
 "c12" => $p12==100?"timeline-badge success":"timeline-badge primary",
    );
 return $data;
}

public function group(){
  return $this->hasMany('App\GroupCommission')->where('type', 'GroupCommission')->where('status', 0); //->sum('amount');
}

public function personal(){
  return $this->hasMany('App\GroupCommission')->where('type', 'RecruitCommission')->where('status', 0); //->sum('amount');
}

}
