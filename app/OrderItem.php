<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model
{
    protected $fillable = [
        "product_id", "order_id", "unit_price", "quantity", "status", "memo", "batch_numbers", "molpay_order_id"
    ];

    /**
     * Relationships
     */

    public function product()
    {
        return $this->belongsTo('App\Product');
    }

       public function Order()
    {
        return $this->belongsTo('App\Order');
    }

    /**
     * Accessor
     */

    public function getSubtotalAttribute($value)
    {
        return intval($this->quantity) * floatval($this->unit_price);
    }
}
