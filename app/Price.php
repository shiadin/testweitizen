<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Price extends Model
{
    protected $fillable = [
        "user_id", "role_id", "product_id", "amount", "is_base", "start_at", "end_at", "status", "min_quantity"
    ];

    public function role()
    {
        return $this->belongsTo('App\Role');
    }

    public function product(){
    	return $this->belongsTo('App\Product');
    }
}
