<?php

namespace App\Gamify\Points;

use QCod\Gamify\PointType;
use App\Gamify\Points\DownLineTransfer;
use App\Gamify\Points\SalesPerQuarter;
use App\User;

class StockTransfered extends PointType
{
    /**
     * Number of points
     *
     * @var double
     */
    public $points = 1;
    public $price = 0;
    public $quantity = 0;
    public $product = 0;
    public $upline = 0;


    /**
     * Point constructor
     *
     * @param $subject
     */
    public function __construct($subject, $price, $quantity, $product, $upline)
    {
        $this->subject = $subject;
        $this->price = $price;
        $this->meta = $price;
        $this->product = $product;
        $this->upline = $upline;
        $this->quantity = $quantity;

        
        
        /*handle giving certaitn % rewards to
         upline for selling the stocks */
        $role_id = $this->price->role_id;
        $commission = \Config::get('commission.'.$role_id);
        $this->points = $commission * $this->quantity;
            $thisUserUpline = auth()->user()->upline;
           if($thisUserUpline){
               $upline = User::find(auth()->user()->referrer_id);
               $upline->givePoint(new DownLineTransfer($product, 
                                  $this->points*0.1, 
                                  $this->subject, auth()->user(), 
                                  $this->product, 
                                  $this->price, 
                                  $this->quantity)); 
            }
    }

    /**
     * User who will be receive points
     *
     * @return mixed
     */
    public function payee()
    {
        return $this->getSubject()->user;
    }
}
