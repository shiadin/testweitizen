<?php

namespace App\Gamify\Points;

use QCod\Gamify\PointType;

class CustomerReward extends PointType
{
    /**
     * Number of points
     *
     * @var int
     */
    public $points = 1;
    public $meta = [];

    /**
     * Point constructor
     *
     * @param $subject
     */
    public function __construct($subject, $customer)
    {
        $this->subject = $subject;
        $this->meta = $customer; //the new customer added
    }

    /**
     * User who will be receive points
     *
     * @return mixed
     */
    public function payee()
    {
        return $this->getSubject()->user;
    }
}
