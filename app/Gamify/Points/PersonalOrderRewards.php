<?php

namespace App\Gamify\Points;

use QCod\Gamify\PointType;
use App\User;
use App\Reward;
use App\Reputed;
use DB;
use App\Gamify\Points\DownlineCommissionReward;
class PersonalOrderRewards extends PointType
{
    /**
     * Number of points
     *
     * @var int
     */
    public $points = 20;
    public $price;
    public $product;
    public $quantity;
    public $order_id;
    public $meta;
    /**
     * Point constructor
     *
     * @param $subject
     */
    public function __construct($subject, $commission)
    {
        //dd(dd);
        $this->subject = $subject;
        //$this->subject = $subject->id;
        $this->points = $commission;
        $this->meta = $subject;


       if(auth()->user()->role_id==5){ 
             $user_id = auth()->user()->initial_referrer_id;
             $upline =User::Find($user_id);
            //$reputed = DB::table('reputations')->insert([
            $reputed = Reputed::create([
            'name'=> 'DownlineCommissionReward',
            'point'=>$commission,
            'price'=>$subject->amount,
            'downline'=>auth()->user()->name,
            'payee_id'=>$user_id,
            'meta'=> $subject,
           ]);
       
         
     }
        
       
    }

    /**
     * User who will be receive points
     *
     * @return mixed
     */
    public function payee()
    {
        return $this->getSubject()->user;
    }
}
