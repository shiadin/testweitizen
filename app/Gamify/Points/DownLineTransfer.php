<?php

namespace App\Gamify\Points;

use QCod\Gamify\PointType;

class DownLineTransfer extends PointType
{
    /**
     * Number of points
     *
     * @var int
     */
    public $points;
    public $meta;
    public $downline;
    public $product;
    public $price;
    public $quantity;

    /**
     * Point constructor
     *
     * @param $subject
     */
    public function __construct($subject, $points, $meta, $downline, $product, $price, $quantity)
    {
        $this->subject = $subject;
        $this->points = $points;
        $this->meta = $meta;
        $this->downline = $downline;
        $this->product = $product;
        $this->price = $price;
        $this->quantity =$quantity;
    }

    /**
     * User who will be receive points
     *
     * @return mixed
     */
    public function payee()
    {
        return $this->getSubject()->user;
    }
}
