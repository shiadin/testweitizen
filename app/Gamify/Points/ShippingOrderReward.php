<?php

namespace App\Gamify\Points;

use QCod\Gamify\PointType;

class ShippingOrderReward extends PointType
{
    /**
     * Number of points
     *
     * @var int
     */
    public $points = 20;
    public $meta = [];
    public $downline;
     public $upline;
    public $product;
    public $price;
    public $quantity;


    /**
     * Point constructor
     *
     * @param $subject
     */
    public function __construct($subject)
    {
        $this->subject = $subject;
        $this->meta = $subject;
        $this->product = $subject->items;
        $this->price = $subject->customer;
        $this->downline = $subject->user->name;
        $this->upline = $subject->customer->name ?? '';
        $this->quantity = $subject->quantity;
        
        
        $totalSale = 0;
        foreach ($subject->items as $key => $value) {
        $totalSale += $value->unit_price*$value->quantity;
        }
        $this->points = $totalSale;
    }

    /**
     * User who will be receive points
     *
     * @return mixed
     */
    public function payee()
    {
        return $this->getSubject()->user;
    }
}
