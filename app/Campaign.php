<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Campaign extends Model
{
    protected $fillable = ['name', 'start', 'end', 'description', 'meta', 'active'. 'eligible_to', 'conditions'];
    protected $cast = ['meta'=>'array', 'conditions'=>'conditions'];
      
}
