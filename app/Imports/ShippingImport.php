<?php

namespace App\Imports;

use App\Shipping;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class ShippingImport implements ToCollection, WithHeadingRow
{
    public $header = [
        'id', 'courier', 'tracking', 'sku', 'batch_numbers'
    ];

    public function collection(Collection $rows)
    {
        // store main row index
        $mainRowIndex = null;
        // loop rows one by one
        foreach ($rows as $index => $row) {
            // if this row is main row
            if (!is_null($row['id'])) {
                $mainRowIndex = $index;
                // if user decide to put in batch numbers
                $row['items'] = [];
                if ($row['items']) {
                    array_push($row['items'], [
                        'sku' => $row['sku'],
                        'batch_numbers' => $row['batch_numbers']
                    ]);
                }
            }
            // no id means this row is a sub row
            // index less than zero means this is first row, first row should not be a sub row
            // sku provided
            if (is_null($row['id']) && $index > 0 && isset($row['sku'])) {
                $main = $rows[$mainRowIndex]->map(function ($val, $attr) use ($row) {
                    if ($attr === 'items') {
                        array_push($val, [
                            'sku' => $row['sku'],
                            'batch_numbers' => $row['batch_numbers']
                        ]);
                    }
                    return $val;
                });
                $rows[$mainRowIndex] = $main;
                unset($rows[$index]);
            }
            // unset unnecessary data
            unset($row['sku']);
            unset($row['batch_numbers']);
        }
        return $rows;
    }
}
