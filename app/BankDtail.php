<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BankDtail extends Model
{
    protected $fillable =['account', 'bank', 'name', 'user_id', 'type_of_withdral'];
    
    public function user()
    {
        return $this->belongsTo('App\User')
                    ->with('address');
    }
}
