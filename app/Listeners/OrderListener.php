<?php

namespace App\Listeners;

use App\Order;
use App\Events\OrderEvent;
use App\Mail\OrderCreated;
use App\Mail\OrderApproved;
use App\Mail\OrderRejected;
use App\Mail\OrderCancelled;
use App\Mail\OrderCompleted;
use App\Mail\OrderConfirmed;
use App\Mail\OrderDelivered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class OrderListener implements ShouldQueue
{
    public function created(Order $order)
    {
        // send email notification
        Mail::to($order->user)->send(new OrderCreated($order));
    }

    public function approved(Order $order)
    {
        // send email notification
        Mail::to($order->user)->send(new OrderApproved($order));
    }

    public function rejected(Order $order)
    {
        // send email notification
        Mail::to($order->user)->send(new OrderRejected($order));
    }

    public function confirmed(Order $order)
    {
        // send email notification
        Mail::to($order->user)->send(new OrderConfirmed($order));
    }

    public function delivered(Order $order)
    {
        // send email notification
        Mail::to($order->user)->send(new OrderDelivered($order));
    }

    public function completed(Order $order)
    {
        // send email notification
        Mail::to($order->user)->send(new OrderCompleted($order));
    }

    public function cancelled(Order $order)
    {
        // send email notification
        Mail::to($order->user)->send(new OrderCancelled($order));
    }
}
