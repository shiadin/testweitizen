<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Reputed extends Model
{

     use SoftDeletes;
    protected $fillable = ['name', 'point', 'subject_id', 'order_id', 'subject_type', 'quantity', 'price', 'product', 'upline', 'downline', 'last_transaction_compagin', 'last_transaction_commission', 'payee_id', 'meta'];
    protected $table = 'reputations';

    protected $cast = [
                 'upline' => 'array',
                 'downline' => 'array',
                 'product' => 'array',
                 'price' => 'array',
                 'meta' => 'array',
                      ];

    public function user(){
    	return $this->belongsTo('App\User', 'payee_id', 'id')->with('bank');
    }

    public function bank(){
    	return $this->hasOne('App\BankDtail', 'user_id', 'payee_id');
    }
}
