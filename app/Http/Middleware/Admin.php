<?php

namespace App\Http\Middleware;

use Closure;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $access = null)
    {
        /**
         * $access: super   => Super Admin
         *          admin      => HQ Admin
         */
        // if user is not admin
        if (!$request->user()->isAdmin($access === null ? null : explode('|', $access))) {
            abort(401);
        }

        return $next($request);
    }
}
