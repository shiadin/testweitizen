<?php

namespace App\Http\Middleware;

use Closure;

class CompleteProfile
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $whitelist = ['user.profile', 'user.update'];
        // get system modules
        $modules = explode(',', env('MODULES'));
        // if deposit modules is exists
        $existsDepositModule = in_array('deposit', $modules);
        // if user is activated admin
        $isActivatedAdmin = auth()->user()->isAdmin() && auth()->user()->status == 1;
        // if user is in the whitelisted route
        $inWhitelistedRoute = in_array($request->route()->getName(), $whitelist);
        // if user didnt provide personal information
        $informationNotProvided = empty(auth()->user()->name) || empty(auth()->user()->ic_front);
        // if user account is inactivated
        $inactiveUser = auth()->user()->status == 0;
        // if user didnt have a deposit
        // no need to check if user is level 1 member
        $depositApproved = !auth()->user()->depositRequired() ? true : auth()->user()->deposit()->where('orders.status', 1)->exists();
        // allow to pass through middleware if user is activated admin or in whitelisted route
        if ($isActivatedAdmin || $inWhitelistedRoute) {
            return $next($request);
        }
        // set a default message
        $message = null;
        // if user still didnt provide personal information
        if ($informationNotProvided) {
            $message = 'Please complete your personal information first';
        // if user deposit not approved yet
        } elseif (!$depositApproved && $existsDepositModule) {
            $message = "Please submit your deposit. If it was submitted, waiting the admin to proceed your request";
        // if user account is inactivated
        } elseif ($inactiveUser) {
            $message = "Be patient, your account is under evaluation now...";
        }
        if (!is_null($message)) {
            if ($request->ajax()) {
                return response()->json([
                    "code" => 500,
                    "message" => $message
                ], 500);
            }
            return redirect()->route('user.profile')
                        ->with('alert-warning', $message);
        };
        return $next($request);
    }
}
