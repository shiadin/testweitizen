<?php

namespace App\Http\Controllers;

use App\Activity;
use Illuminate\Http\Request;

class ActivityController extends Controller
{
    public function index(Request $request)
    {
        // initialize activities data
        $activities = Activity::where('causer_id', $request->user()->id);

        if ($request->has('keyword')) {
            $activities->where('description', 'like', '%'.$request->keyword.'%');
        }

        if ($request->has('start_at')) {
            $activities->where('created_at', '>=', $request->start_at);
        }

        if ($request->has('end_at')) {
            $activities->where('created_at', '<=', $request->end_at);
        }
        
        if ($request->has('subject_type')) {
            $activities->where('subject_type', 'like', '%'.$request->subject_type.'%');
        }

        $activities->orderBy($request->order_by ?: 'id', $request->order_direction ?: 'asc');

        // paginate or get all results
        if ($request->per_page == 'all') {
            $activities = $activities->get();
        } else {
            // paginate results
            $activities = $activities->paginate($request->has('per_page') ? $request->per_page : 10);
        }
        // return success message
        return response()->json([
            "code" => 200,
            "message" => 'User Activities was Successfully Retrieved!',
            "activities" => $activities
        ]);
    }
}
