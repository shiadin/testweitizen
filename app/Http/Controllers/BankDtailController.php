<?php

namespace App\Http\Controllers;

use App\BankDtail;
use App\withdrawRequest;
use Illuminate\Http\Request;

class BankDtailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'user_id' => 'required|integer',
            'name' => 'required',
            'bank' => 'required',
            'account' => 'required',
        ]);

       return BankDtail::updateOrCreate(['user_id'=> $request->user_id], ['name'=>$request->name, 'bank'=>$request->bank, 'account' =>$request->account, 'type_of_withdral' => $request->type_of_withdral]);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\BankDtail  $bankDtail
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         $bankDtail =BankDtail::where('user_id', $id)->first();
        return $bankDtail;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\BankDtail  $bankDtail
     * @return \Illuminate\Http\Response
     */
    public function edit(BankDtail $bankDtail)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\BankDtail  $bankDtail
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BankDtail $bankDtail)
    {
        $this->validate($request, [
            'user_id' => 'required|integer',
            'name' => 'required',
            'bank' => 'required',
            'account' => 'required',
        ]);

         return BankDtail::updateOrCreate(['user_id'=> $request->user_id], ['name'=>$request->name, 'bank'=>$request->bank, 'account' =>$request->account, 'type_of_withdral' => $request->type_of_withdral]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\BankDtail  $bankDtail
     * @return \Illuminate\Http\Response
     */
    public function destroy(BankDtail $bankDtail)
    {
        //
    }



}
