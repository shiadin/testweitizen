<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use QCod\Gamify\Gamify;
use App\Reputed;
use Illuminate\Support\Facades\DB;
use App\User;
use App\Campaign;
use Charts;
use Carbon\Carbon;
class CampaignController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
     
    
        $lastMonth = Reputed::whereMonth(
        'created_at', '=', Carbon::now()->subMonth()->month
       )->where('name',  'ShippingOrderReward')->sum('point');
        $currentMonth = Reputed::
             whereYear('created_at', Carbon::now()->year)
            ->whereMonth('created_at', Carbon::now()->month)->where('name',  'ShippingOrderReward')
            ->sum('point');
        $all = Reputed::where('name',  'ShippingOrderReward')->sum('point');
       if($all==0)
        $all=1;
       $campaigns = Campaign::all();
  
      $sales_performance = Reputed::groupBy('payee_id')->select('payee_id', DB::raw('count(*) as total_restock'),  DB::raw('sum(point) AS total_sales'))
         ->with('user')->where('name', 'ShippingOrderReward')
        ->paginate(12);
     // return $sales_performance;

  $sales = Reputed::select(
            DB::raw('sum(point) as sums'), 
            DB::raw("DATE_FORMAT(created_at,'%M %Y') as months"))
  ->groupBy('months')
  ->where('name', 'ShippingOrderReward')
  ->get();
       
   $chart_sale = Charts::create('bar', 'highcharts')
    ->title('Shippings Order Sales')
    ->elementLabel('Shippings Order RM')
    ->labels($sales->pluck('months'))
    ->values($sales->pluck('sums'))
    ->dimensions(600,500)
    ->responsive(true);     
$stats = array(
         array('label'=>'CURRENT EARNED', 'value' => $currentMonth, 'url' => '/admin/transactions/00',  'color'=>'success', 'progressbar'=>($currentMonth/$all)*100),
         array('label'=>'LAST MONTH EARNED', 'value' => $lastMonth,  'url' => '/admin/transactions/11',  'color'=>'info', 'progressbar'=>($lastMonth/$all)*100),
         array('label'=>'ALL TIME EARNED ', 'value' => $all,  'url' => '/admin/transactions/22', 'color'=>'danger', 'progressbar'=>($all/$all)*100),
      );

        return view('admin.campaign.index',  ["title" => "Camapign management",
            "sidebar" => "Campaigns Setting",
            'sales_performance' => $sales_performance,
            "stats" => $stats,
            'chart_sale' => $chart_sale,
            'sales' => $sales,
            'campaigns' => $campaigns
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       // dd($request->all());
         $this->validate($request, [
            'name' => 'required',
            'start' => 'required',
            'end' => 'required',
            'description' => 'required',
        ]);
        $campaign = Campaign::create($request->all());
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Campaign $campaign)
    {
       return view('admin.campaign.show', [
        'title'=>'View Campaign', 
        'campaign'=>$campaign,
        "sidebar" => "Campaigns Setting",

         ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Campaign $campaign)
    {

              $rules = [ array('label' => 'Rewards','desc' => "shipping order 20000", 'i'=>0, 'value'=>'RM 1500' ), array('label' => 'Rewards','desc' => "shipping order 10000", 'i'=>0, 'value'=>'RM 600' ), array('label' => 'Rewards','desc' => "shipping order 35000", 'i'=>0, 'value'=>'RM 3500'), ];

         return view('admin.campaign.edit', [
        'title'=>'Edit Campaign', 
        'campaign'=>$campaign,
        'rules'=>$rules,
        "sidebar" => "Campaigns Setting",

         ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Campaign $campaign)
    {
       // dd($request->all());
        $this->validate($request, [
            'name' => 'required',
            'start' => 'required',
            'end' => 'required',
            'description' => 'required',
        ]);

        $campaign->update([
            'meta' => json_encode($request->meta),
            'conditions' => json_encode($request->conditions),
            'name' => $request->name,
            'start' => $request->start,
            'end' =>  $request->end,
            'description' => $request->description,
           ]);
    session()->flash('success_message', 'Campaign was updated successfully!');
    return back();
    }


public function activate(Request $request, Campaign $campaign)
    {
    
        
        $campaigns = Campaign::all();
        foreach ($campaigns as $key => $value) {
           $value->active = 0;
           $value->save();
        }
        $campaign->active=$request->active;
        $campaign->save();
       session()->flash('success_message', 'Campaign was activated successfully!');
    return back();
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Campaign $campaign)
    {
        $campaign->delete();
         session()->flash('success_message', 'Campaign was Deleted successfully!');
        return back();
    }
}
