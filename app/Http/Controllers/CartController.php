<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use \Cart as Cart;
use Validator;
use App\Price;
use App\Product;
class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
       $q = array(6, 12, 24, 36, 120);
      // return $q;
        return view('marketplace.cart', [
            'sidebar' => 'Stocks',
            'title' => 'Stock/My Cart',
            'qty' => $q
            
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
        public function store(Request $request)
    {
       // dd($request->all());
          
        $duplicates = Cart::search(function ($cartItem, $rowId) use ($request) {
            return $cartItem->id === $request->id;
        });

        if (!$duplicates->isEmpty()) {
            return redirect('cart')->withSuccessMessage('Item is already in your cart!');
        }



       // get price 
    $price = Price::orderBy('role_id')->where('product_id', $request->id)->where('role_id', auth()->user()->role_id)->first();
   
    
         $request->request->add([
            'price' => $price->amount
        ]);
       // end price
        Cart::add($request->id, $request->name, $request->quantity, $request->price)->associate('App\Product');
        //Cart::store(auth()->user()->id); //add to database
        return redirect('cart')->withSuccessMessage('Item was successfully added to your cart!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Cart  $cart
     * @return \Illuminate\Http\Response
     */
    public function show(Cart $cart)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Cart  $cart
     * @return \Illuminate\Http\Response
     */
    public function edit(Cart $cart)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Cart  $cart
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $validator = Validator::make($request->all(), [
            'quantity' => 'required|numeric|between:1,120'
        ]);

         if ($validator->fails()) {
            session()->flash('error_message', 'Quantity must be between 1 and 120.');
            return response()->json(['success' => false]);
         }
    $cartItem =  Cart::get($id);
    

        // get price 
    $price = Price::orderBy('role_id')->where('product_id', $cartItem->id)->where('role_id', auth()->user()->role_id)->first();
   
    
         $request->request->add([
            'price' => $price->amount
        ]);

         Cart::update($id, $request->quantity);
         Cart::update($id, $request->all());

        session()->flash('success_message', 'Quantity was updated successfully!');
    return back();
        return response()->json(['success' => true]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Cart  $cart
     * @return \Illuminate\Http\Response
     */
     public function destroy($id)
    {
        Cart::remove($id);
        return redirect('cart')->withSuccessMessage('Item has been removed!');
    }

     /**
     * Remove the resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function emptyCart()
    {
        Cart::destroy();
        return redirect('cart')->withSuccessMessage('Your cart has been cleared!');
    }

    /**
     * Switch item from shopping cart to wishlist.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function switchToWishlist($id)
    {
        $item = Cart::get($id);

        Cart::remove($id);

        $duplicates = Cart::instance('wishlist')->search(function ($cartItem, $rowId) use ($id) {
            return $cartItem->id === $id;
        });

        if (!$duplicates->isEmpty()) {
            return redirect('cart')->withSuccessMessage('Item is already in your Wishlist!');
        }

        Cart::instance('wishlist')->add($item->id, $item->name, 1, $item->price)
                                  ->associate('App\Product');

        return redirect('cart')->withSuccessMessage('Item has been moved to your Wishlist!');

    }

    public function successfully($id){
       return redirect('stocks')->withSuccessMessage('Payment was successfully made!');
    }
     public function failure($id){
      return redirect('stocks')->withErrorMessage('There were error connecting to payment api. Try again ot contact us fot the help!');
    }
     public function pending($id){
      dd($id);
    }

    
}
