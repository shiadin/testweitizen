<?php

namespace App\Http\Controllers;

use App\ProductSale;
use App\Purchase;
use App\User;
use Charts;
use App\Wallet;
use App\StockToken;
use App\Transaction;
use App\BankDtail;
use App\Reputed;
use App\Order;
use App\Stock;
use App\Product;
use App\OrderItem;
use App\Referral;
use App\GroupCommission;
use App\withdrawRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use App\Http\Resources\UserResource;
use App\Http\Resources\WithdrawalResource;
use App\Molpay;
use Yajra\Datatables\Datatables;

class AdminController extends Controller
{
    public function members(Request $request)
    {
        // get admin
        $admin = auth()->user()->load(['inviteCodes']);
        // attach system referrals to admin
        $admin->inviteCodes = Referral::with(['role'])->whereNull('user_id')->get();
        // get system invite codes
        return view('admin.users', [
            'title' => 'Dealers Management',
            'sidebar' => "Dealers Management",
            'admin' => new UserResource($admin),
            'routes' => [
                'getUsers' => route('admin.users.index', [
                    'with_situations' => true
                ]),
                'getRoles' => route('admin.roles.index', [
                    'per_page' => 'all',
                    'except_admin' => false,
                     // 'ids' => range(1, 33)
                    'ids' => range(1, env('HIGHEST_RESELLER_LEVEL'))
                ]),
                'getAllRoles' => route('admin.roles.index', [
                    'per_page' => 'all',
                    'except_admin' => false,
                    'ids' => array_merge(range(1, env('HIGHEST_RESELLER_LEVEL')), [
                        999, 950
                    ])
                ]),
            ]
        ]);
    }


    public function dealersCommissions(Request $request){
   
     $commision = User::where('status', 1);

         if ($request->has('keyword')) {
            $activities->where('username', 'like', '%'.$request->keyword.'%');
        }

        if ($request->has('start_at')) {
            $activities->where('created_at', '>=', $request->start_at);
        }

        if ($request->has('end_at')) {
            $activities->where('created_at', '<=', $request->end_at);
        }

           if ($request->per_page == 'all') {
            $commision = $commision->get();
        } else {
            // paginate results
            $commision = $commision->paginate($request->has('per_page') ? $request->per_page : 10);
        }
       

        $data = [];
        foreach ($commision as $key => $value) {
            $g_c = 0;
            if(intval($value->role_id)>4){
            $g_s = $this->GroupCommission($value->group->sum('amount'));
            }
            if(intval($value->role_id<5)){
                $g_s = 0;
            }
            $total = $g_s+$value->personal->sum('amount');
            if($total>1){
                  $user = ['id'=> $value->id,
                    'name'=> $value->name, 
                    'email'=>$value->email, 
                    'phone'=>$value->phone,
                    'level'=>$value->role_id, 
                    'personal'=> $value->personal->sum('amount'),
                    'group'=>$g_s,
                    'total'=>$total];
                 array_push($data, $user);
              }
          
        }

       //return $data;

          $stats = array('current' => 0,
                       'lastMonth' => 0,
                       'all' => 0,
                       );
   // return route('admin.dealers_commissions_api');

     return view('admin.dealer-commission', [
            'title' => 'Dealer Commission',
            'sidebar' => "Dealers Commission",
            'stats' => $stats,
            'commision'=> $commision,
            'data' => $data,
            'routes' => [
                'getCommission' => route('admin.dealers_commissions_api')
            ],
            
         ]);
    }


public function GroupCommission($amount){
   if($amount>=30001){
    return round($amount*0.08, 2);
   }
   else if($amount>=20001){
    return round($amount*0.07, 2);
   }
    else if($amount>=10001){
    return round($amount*0.05, 2);
   }
    else if($amount>=5000){
    return round($amount*0.03, 2);
   }
}
     public function dealers_commissions_api(Request $request){
        $commision = User::where('status', 1);

        //  if ($request->has('keyword')) {
        //     $commision->where('username', 'like', '%'.$request->keyword.'%');
        // }

        if ($request->has('start_at')) {
            $commision->where('created_at', '>=', $request->start_at);
        }

        if ($request->has('end_at')) {
            $commision->where('created_at', '<=', $request->end_at);
        }

           if ($request->per_page == 'all') {
            $commision = $commision->get();
        } else {
            // paginate results
            $commision = $commision->paginate($request->has('per_page') ? $request->per_page : 1000);
        }
       

        $data = [];
        foreach ($commision as $key => $value) {
            $g_c = 0;
            if(intval($value->role_id)>4){
            $g_s = $this->GroupCommission($value->group->sum('amount'));
            }
            if(intval($value->role_id<5)){
                $g_s = 0;
            }
            $total = $g_s+$value->personal->sum('amount');
            if($total>1 && $value->role_id<10){
                  $user = ['id'=> $value->id,
                    'name'=> $value->name, 
                    'email'=>$value->email, 
                    'phone'=>'['.$value->phone_no.']',
                    'level'=>'['.$value->role_id.']', 
                    'personal'=> $value->personal->sum('amount'),
                    'group'=>$g_s,
                    'total'=>$total];
                 array_push($data, $user);
              }
          
        }

        return response()->json([
            "code" => 200,
            "message" => 'Dealers Commision was Successfully Retrieved!',
            "commision" => $commision,
            "data" => $data
        ]);

  // return $commision ;
  //  return Datatables::of($data)
    //    ->make(true);
  }

    public function withdrawals(Request $request)
    {
        // get admin
        $withdraws = withdrawRequest::with('user')->whereStatus(1)->latest()->paginate(20);
     //  return $withdraws;
        // attach system referrals to admin
    
        // get system invite codes
        return view('admin.withdraws', [
            'title' => 'Withdrawal Request History',
            'sidebar' => "Withdrawal Request History",
            'withdraws' => $withdraws,
            'routes' => [
                'getWithdraws' => route('admin.withdrawals_api'
                )]
            
            
        ]);
    }

    
    public function withdrawals_api(Request $request)
    {
        // get admin
       // $withdraws = withdrawRequest::with('user')->whereStatus(1)->latest()->paginate(2);
        //return $withdraws;

        // initialize activities data
        $activities = withdrawRequest::with('user')->whereStatus(1);

        if ($request->has('keyword')) {
            $activities->where('description', 'like', '%'.$request->keyword.'%');
        }

        if ($request->has('start_at')) {
            $activities->where('created_at', '>=', $request->start_at);
        }

        if ($request->has('end_at')) {
            $activities->where('created_at', '<=', $request->end_at);
        }
        
        if ($request->has('subject_type')) {
            $activities->where('subject_type', 'like', '%'.$request->subject_type.'%');
        }

        $activities->orderBy($request->order_by ?: 'id', $request->order_direction ?: 'asc');

        // paginate or get all results
        if ($request->per_page == 'all') {
            $activities = $activities->get();
        } else {
            // paginate results
            $activities = $activities->paginate($request->has('per_page') ? $request->per_page : 20);
        }
        // return success message
        return response()->json([
            "code" => 200,
            "message" => 'User Activities was Successfully Retrieved!',
            "data" => $activities
        ]);
        
    }


    public function workspace(Request $request)
    {
        return view('admin.workspace', [
            'title' => 'Workspace',
            'sidebar' => 'workspace',
            'routes' => [
                'getDepositsAccounts' => route('admin.workspace.deposits&accounts'),
                'getStocks' => route('admin.workspace.stocks'),
                'getCredits' => route('admin.workspace.credits'),
                'getwallets' => route('admin.workspace.wallets'),
                'getuser' => route('requestwithdraw.user'),
                'approvewallets' => route('requestwithdraw.process'),
                'getShippings' => route('admin.workspace.shippings'),
                'bulkProcessShippings' => route('admin.shippings.process'),
                'bulkConfirmShippings' => route('admin.shippings.confirm')
            ]
        ]);
    }

    public function shippings()
    {
        // get shippings
        $user = auth()->user();
        // return view
        return view('user.shippings', [
            'title' => 'Shipping Orders',
            'sidebar' => 'Shipping Orders',
            'user' => new UserResource($user),
            'routes' => [
                'getOrders' => route('admin.orders.index', [
                    'type' => 'shipping',
                    'with_situations' => true,
                    'order_by' => 'updated_at',
                    'order_direction' => 'desc',
                ])
            ]
        ]);
    }

    public function requests()
    {
        // get requests
        $user = auth()->user();
        // return view
        return view('user.requests', [
            'title' => 'Stock Transfer History',
            'sidebar' => 'Stock Transfer History',
            'user' => new UserResource($user),
            'routes' => [
                'getOrders' => route('admin.orders.index', [
                    'order_by' => 'updated_at',
                    'order_direction' => 'desc',
                    'with_situations' => true,
                    'is_requests' => 1
                ])
            ]
        ]);
    }

    public function stocks()
    {
        // return view
        return view('admin.stocks', [
            'title' => 'Stocks Management',
            'sidebar' => 'Stocks Management',
            'routes' => [
                'getStocks' => route('admin.stocks.index', [
                    'order_by' => 'updated_at',
                    'order_direction' => 'desc',
                    'with_products' => 1
                ]),
                'getUsers' => route('admin.users.index', [
                    'roles' => range(1, env('HIGHEST_RESELLER_LEVEL')),
                    'show_admin' => 1,
                    'per_page' => 'all',
                    'with_stock_models' => 1
                ]),
            ]
        ]);
    }

    public function credits()
    {
        // return view
        return view('admin.credits', [
            'title' => 'E-Wallet',
            'sidebar' => 'e-Wallet',
            'routes' => [
                'getUsers' => route('admin.users.index', [
                    'credits_activities' => 1,
                    'show_admin' => 1
                ]),
            ]
        ]);
    }
  public function downline_api(){
    return Datatables::of(User::query()->with('downlines', 'stocks', 'orders')->whereIn('referrer_id', [1, null]))
        ->addColumn('stocks', function($row){
            return $row->stocks->sum();
        })
        ->addColumn('role_id', function($row){
           if($row->role_id<10)
            return $row->role_id;
          else
            return 0;
        })
        ->addColumn('downlines', function($row){
            return $row->downlines->count();
        })
        ->addColumn('total_sales', function($row){
            return $row->orders->sum();
        })
        ->addColumn('edit_url', function($row){
            $rout = route('ajax.user.viewusers', $row->id);
              $btn = '<a href=".$rout." class="edit btn btn-primary btn-sm">View</a>';
  
                            return $btn;
        })
        // >addColumn('edit_url', function($row){
        //  return route('posts.edit', $row->id);
        //   })
        //  ->addColumn('delete_url', function($row){
        //   return route('posts.destroy', $row->id);
        //  })
        ->make(true);
  }
    public function downlines()
    {
        // get users who has no upline
        $users = User::with(['role',
                        'downlines.downlines.downlines.downlines.downlines'
                    ])
                    ->where('role_id', '!=', 700)
                    ->whereIn('referrer_id', [1, null])
                    ->get();
                   // ->OrWhere('referrer_id', 1)->get(); //1 as admin id
            //  return Datatables::of($users)->make(true);
        // return view
        
$month = Carbon::now()->month;
        $year = Carbon::now()->year;
        $firstDayOfMonth = Carbon::parse("first day of this month")->toDateString();
        $lastDayOfMonth = Carbon::parse("last day of this month")->toDateString();
        $firstDayOfLastMonth = Carbon::parse("first day of last month")->toDateString();
        $lastDayOfLastMonth = Carbon::parse("last day of last month")->toDateString();

         $lastMonth = $users->
                 where('created_at', '>=', $firstDayOfLastMonth)
                    ->where('created_at', '<=', $lastDayOfLastMonth)
                            ->count();
          $current = $users->where('created_at', '>=', $firstDayOfMonth)
                            ->where('created_at', '<=', $lastDayOfMonth)
                            ->count();
          $all = $users
                 ->count();

      $stats = array('current' => $current,
                       'lastMonth' => $lastMonth,
                       'all' => $all,
                       );
      
        return view('admin.downlines-company', [
            'title' => 'Company Downlines',
            'sidebar' => "Company's Downline",
            'routes' => [
                'getUsers' => route('admin.users.index', [
                    'credits_activities' => 1,
                    'show_admin' => 1
                ])],
                'stats' => $stats,
            'users' => UserResource::collection($users)
        ]);
    }

    public function dashboard()
    {
 
       //  $product_sales = OrderItem::groupBy('product_id','unit_price', 'quantity')->select('product_id', DB::raw('count(*) as total'), 'unit_price', 'quantity')->with('product')->whereStatus(1)->get();
       // return $product_sales;
       //  ProductSale::truncate();
       //  foreach ($product_sales as $key => $value) {

       //    $sales = ProductSale::create(
        
       //   ['product_id' => $value->product_id, 'unit_price' => $value->unit_price, 'quantity' => $value->total*$value->quantity, 'transaction' => $value->total*$value->quantity*$value->unit_price, 'name' => $value->product->name]
       //  );
       //  }
       // return $product_sales;
        
        // dealer perfomance
$deler_perfomaces = User::paginate(6);

         // dealer perfomance
 $product_sales = Purchase::groupBy('product_id',  'name')->select('product_id', DB::raw('count(*) as total'), 'name', DB::raw('sum(quantity*price) AS total_sales'), DB::raw('sum(quantity) AS quantity'))->paginate(6);

$to_custommer_sales = Order::groupBy('user_id')->select('user_id', DB::raw('count(*) as total'),  DB::raw('sum(amount) AS total_sales'))->whereStatus(0)->paginate(6);



// $to_downlines_sales = Order::groupBy('user_id')->select('user_id', DB::raw('count(*) as total'),  DB::raw('sum(amount) AS total_sales'))->with('user')->get();

$restock_performance = Purchase::groupBy('user_id')->select('user_id', DB::raw('count(*) as total_restock'),  DB::raw('sum(price*quantity) AS total_sales'))->with('user')->paginate(6);

$sales_molpay = StockToken::select(
            DB::raw('sum(quantity) as sums'), 
            DB::raw("DATE_FORMAT(created_at,'%M %Y') as months")
  )
  ->groupBy('months')
  ->get();


$chart_sale = Charts::create('bar', 'highcharts')
    ->title('Token Reload Perfomance')
    ->elementLabel('Unit/Token')
    ->labels($sales_molpay->pluck('months'))
    ->values($sales_molpay->pluck('sums'))
    ->dimensions(600,500)
    ->responsive(true);


   $chart_user = Charts::database(User::all(), 'area', 'highcharts')
                  ->title("Monthly new Register Users")
                  ->elementLabel("Total Users")
                  ->dimensions(600, 500)
                  ->template("material")
                  ->responsive(true)
                  ->groupByMonth(date('Y'), true);

  
//return $restock_performance;
        // $sales = DB::table('purchases')
        // ->join('orders', 'orders.id', '=', 'order_lines.order_id')
        // ->select(DB::raw('sum(order_lines.quantity*order_lines.per_qty) AS total_sales'))
        // ->where('order_lines.product_id', $product->id)
        // ->where('orders.order_status_id', 4)
        // ->first();
       // return $product_sales;
     
        // get date info
        $month = Carbon::now()->month;
        $year = Carbon::now()->year;
        $firstDayOfMonth = Carbon::parse("first day of this month")->toDateString();
        $lastDayOfMonth = Carbon::parse("last day of this month")->toDateString();
        $firstDayOfLastMonth = Carbon::parse("first day of last month")->toDateString();
        $lastDayOfLastMonth = Carbon::parse("last day of last month")->toDateString();
        // get users and group by role then count
        $users = User::with(['role'])->get(['id', 'role_id']);
        // get new regiestered users in current month
        $currentUsers = User::where('role_id', '<=', 5)
                            ->where('created_at', '>=', $firstDayOfMonth)
                            ->where('created_at', '<=', $lastDayOfMonth)
                            ->count('id');
        // get regiestered users in last month
        $lastUsers = User::where('role_id', '<=', 5)
                            ->where('created_at', '>=', $firstDayOfLastMonth)
                            ->where('created_at', '<=', $lastDayOfLastMonth)
                            ->count('id');
        // summarize users
        $users = [
            'total' => $users->count(),
            'currentMonth' => $currentUsers,
            'lastMonth' => $lastUsers,
            'monthsDifference' => $currentUsers - $lastUsers,
            'stuff' => $users->where('role_id', '>=', 900)->groupBy('role.name')
                        ->map(function ($group) {
                            return $group->count();
                        }),
            'member' => $users->where('role_id', '<=', 5)->groupBy('role.name')
                        ->map(function ($group) {
                            return $group->count();
                        }),
            'customer' => $users->where('role_id', 700)->groupBy('role.name')
                        ->map(function ($group) {
                            return $group->count();
                        })
        ];
        // get stocks (hard code product id 1)
        $stocks = Stock::where('product_id', 1)->get(['product_id', 'user_id', 'quantity']);
        // get current stocks approved to upline
        $currentStocks = Order::with([
                                'items' => function ($items) {
                                    return $items->select('id', 'order_id', 'product_id', 'quantity');
                                }
                            ])
                            ->where('type', 'stocks')
                            ->where('status', 1)
                            ->where('created_at', '>=', $firstDayOfMonth)
                            ->where('created_at', '<=', $lastDayOfMonth)
                            ->get(['id'])
                            ->sum(function ($stock) {
                                return $stock->items->sum('quantity');
                            });
        // get last stocks approved to upline
        $lastStocks = Order::with([
                                'items' => function ($items) {
                                    return $items->select('id', 'order_id', 'product_id', 'quantity');
                                }
                            ])
                            ->where('type', 'stocks')
                            ->where('status', 1)
                            ->where('created_at', '>=', $firstDayOfLastMonth)
                            ->where('created_at', '<=', $lastDayOfLastMonth)
                            ->get(['id'])
                            ->sum(function ($stock) {
                                return $stock->items->sum('quantity');
                            });
        // summarize stocks
        $stocks = [
            'total' => $stocks->sum('quantity'),
            'currentMonth' => $currentStocks,
            'lastMonth' => $lastStocks,
            'monthsDifference' => $currentStocks - $lastStocks
        ];
        // get current credits approved to user
        $currentCredits = Order::where('type', 'credits')
                            ->where('status', 1)
                            ->where('created_at', '>=', $firstDayOfMonth)
                            ->where('created_at', '<=', $lastDayOfMonth)
                            ->get(['id', 'amount'])
                            ->sum('amount');
        // get last credits approved to user
        $lastCredits = Order::where('type', 'credits')
                            ->where('status', 1)
                            ->where('created_at', '>=', $firstDayOfLastMonth)
                            ->where('created_at', '<=', $lastDayOfLastMonth)
                            ->get(['id', 'amount'])
                            ->sum('amount');
        // get credits
        $credits = [
            'total' => User::all(['id', 'credits'])->sum('credits'),
            'currentMonth' => $currentCredits,
            'lastMonth' => $lastCredits,
            "monthsDifference" => $currentCredits - $lastCredits
        ];
        // get all shipping orders created
        $shippings = Order::where('type', 'shipping')
                            ->whereIn('status', [1, 2])
                            ->get(['id', 'amount', 'created_at']);
        // group sales order by month,  take last 12
        $shippingsByMonth = $shippings->groupBy(function ($order) {
            $dt = Carbon::parse($order->created_at);
            return "$dt->year/$dt->month";
        })->map(function ($month) {
            return $month->count();
        })->take(-12)->all();
        // get current month shippings
        $currentMonthShippings = $shippings->where('created_at', '>=', $firstDayOfMonth)
                                    ->where('created_at', '<=', $lastDayOfMonth)
                                    ->count();
        // get last month shippings
        $lastMonthShippings = $shippings->where('created_at', '>=', $firstDayOfLastMonth)
                                    ->where('created_at', '<=', $lastDayOfLastMonth)
                                    ->count();
        // summarize shippings
        $shippings = [
            'total' => $shippings->count(),
            'currentMonth' => $currentMonthShippings,
            'lastMonth' => $lastMonthShippings,
            'months' => $shippingsByMonth,
            'monthsDifference' => $currentMonthShippings - $lastMonthShippings
        ];
        // get overall top sellers
        $overallTopSellers = Order::where('orders.type', 'shipping')
                            ->select(['user_id', DB::raw('SUM(order_items.quantity) as total')])
                            ->whereIn('orders.status', [1, 2])
                            ->with(['user'])
                            ->join('order_items', 'order_items.order_id', 'orders.id')
                            ->join('users', 'users.id', 'orders.user_id')
                            ->groupBy('orders.user_id')
                            ->orderByDesc('total')
                            ->take(5)
                            ->get();
        // get current month top sellers
        $monthTopSellers = Order::where('orders.type', 'shipping')
                            ->select(['user_id', DB::raw('SUM(order_items.quantity) as total')])
                            ->whereIn('orders.status', [1, 2])
                            ->where('orders.created_at', '>=', $firstDayOfMonth)
                            ->where('orders.created_at', '<=', $lastDayOfMonth)
                            ->with(['user'])
                            ->join('order_items', 'order_items.order_id', 'orders.id')
                            ->join('users', 'users.id', 'orders.user_id')
                            ->groupBy('orders.user_id')
                            ->orderByDesc('total')
                            ->take(5)
                            ->get();
        $topSellers = [
            'overall' => $overallTopSellers,
            'currentMonth' => $monthTopSellers,
        ];
        $wallet = Wallet::orderBy('balance', 'desc')->paginate(7);
        
        // return view
        return view('admin.dashboard', [
            "title" => "Admin Dashboard",
            "sidebar" => "dashboard",
            "currency" => config('app.currency'),
            "users" => $users,
            "stocks" => $stocks,
            "wallet" => $wallet,
            "credits" => $credits,
            "shippings" => $shippings,
            "topSellers" => $topSellers,
            'restock_performance' => $restock_performance, 
            'to_custommer_sales' => $to_custommer_sales, 
            'product_sales' => $product_sales, 
            'deler_perfomaces' => $deler_perfomaces,
            'chart_user' => $chart_user,
            'chart_sale' => $chart_sale,
        ]);
    }

    public function products()
    {
        // return view
        return view('admin.products', [
            'title' => 'Products Management',
            'sidebar' => 'products',
            'routes' => [
                'getProducts' => route('admin.products.index', [
                    'with_situations' => 1
                ]),
                'createProduct' => route('admin.product.new')
            ]
        ]);
    }

    public function transactions(){
        $transactions =Purchase::with(['user', 'product', 'order', 'molpay'])->paginate(10);
        $completed = Molpay::where('status', '00')->count();
        $failed = Molpay::where('status', '11')->count();
        $pending = Molpay::where('status', '22')->count();
        $all = Molpay::count();
        if($all==0)
            $all =1;
        $progressbarCompleted = round(($completed/$all)*100, 2);
        $progressbarPending = round(($pending/$all)*100, 2);
        $progressbarFail = round(($failed/$all)*100, 2);
     $stats = array(
         array('label'=>'COMPLETED TRANSACTION', 'value' => $completed, 'url' => '/admin/transactions/00',  'color'=>'success', 'progressbar'=>$progressbarCompleted),
         array('label'=>'PENDING TRANSACTION', 'value' => $pending,  'url' => '/admin/transactions/11',  'color'=>'info', 'progressbar'=>$progressbarPending),
         array('label'=>'FAIED TRANSACTION ', 'value' => $failed,  'url' => '/admin/transactions/22', 'color'=>'danger', 'progressbar'=>$progressbarFail),
      );
    // return $stats;
     return view('admin.transactions', 
            ["title" => "transactions",
            "sidebar" => "transactions",
            "transactions" => $transactions,
            "stats" => $stats,
        ]);
    }
 public function ShowTransactions($id){
       $transactions =Purchase::with(['user', 'product', 'order', 'molpay'])->paginate(10);
        $completed = Molpay::where('status', '00')->count();
        $failed = Molpay::where('status', '11')->count();
        $pending = Molpay::where('status', '22')->count();
        $all = Molpay::count();
        $progressbarCompleted = round(($completed/$all)*100, 2);
        $progressbarPending = round(($pending/$all)*100, 2);
        $progressbarFail = round(($failed/$all)*100, 2);
     $stats = array(
         array('label'=>'COMPLETED TRANSACTION', 'value' => $completed, 'url' => '/admin/transactions/00',  'color'=>'success', 'progressbar'=>$progressbarCompleted),
         array('label'=>'PENDING TRANSACTION', 'value' => $pending,  'url' => '/admin/transactions/11',  'color'=>'info', 'progressbar'=>$progressbarPending),
         array('label'=>'FAIED TRANSACTION ', 'value' => $failed,  'url' => '/admin/transactions/22', 'color'=>'danger', 'progressbar'=>$progressbarFail),
      );
    // return $stats;
     return view('admin.transactions', 
            ["title" => "transactions",
            "sidebar" => "transactions",
            "transactions" => $transactions,
            "stats" => $stats,
        ]);
    }

public function CompletUserPayaments(Request $request){
 $wallet = Wallet::findOrFail($request->meta[2]);
 $user = $wallet->user;
//dd($request->all());
 if($user->balance<=0){
    return back()->withErrorMessage('Amount rezo!');;
 }
 try {
                DB::beginTransaction();
$file_name = $user->id.'-'.$request->meta[4];
//return $file_name;
$slip = '';
   if ($request->hasFile('slip')) {
            
                $ext = $request->file('slip')->extension();
                $path = $request->file('slip')->storeAs("public/users/payments/$file_name/", "slip.$ext");
               
                $slip =str_replace('public/', '', $path);
               
            }
    $user->withdraw($request->amount, 'withdraw', ['causer' => $request->meta[1], 'description' => $request->meta[3], 'slip'=>$slip ]);

              activity()
                ->performedOn($wallet)
                ->causedBy($request->user())
                ->withProperties($request->all())
                ->log("RM ".$request->amount." from ". $user->username ."'s EWallet has been transfered to ". $user->username ."'s bank Account by ". auth()->user()->username);
DB::commit();

return back()->withSuccessMessage('Payment saccessfull completed');;
                } catch (\Exception $e) {
            // return error message
             DB::rollback();
             return back()->withErrorMessage('Could not complete transaction');;
        }

}
public function manageUserPayaments($id){ 
   

  $transaction =  Wallet::findOrFail($id);


      $lastDayOfLastMonth = Carbon::parse("last day of last month")->toDateString();
     $firstDayOfLastMonth = Carbon::parse("first day of last month")->toDateString();
   
        return view('admin.manageUserPayaments', 
            ["title" => "Dealer Payments",
            "sidebar" => "payments",
            "trans" => $transaction,
            'lastDayOfLastMonth' => $lastDayOfLastMonth,
            'firstDayOfLastMonth' => $firstDayOfLastMonth
            
        ]);
}
     public function userPayaments(){

         // get date info
        $month = Carbon::now()->month;
        $year = Carbon::now()->year;
        $firstDayOfMonth = Carbon::parse("first day of this month")->toDateString();
        $lastDayOfMonth = Carbon::parse("last day of this month")->toDateString();
        $firstDayOfLastMonth = Carbon::parse("first day of last month")->toDateString();
        $lastDayOfLastMonth = Carbon::parse("last day of last month")->toDateString();
         // $pays_cuts = Reputed::
         //         where('name', '!=', 'ShippingOrderReward')
         //          ->where('created_at', '>=', $firstDayOfLastMonth)
         //            ->where('created_at', '<=', $lastDayOfLastMonth)
         //                    ->get();
        //add mney to wallet for commition for last month
       $pays_cuts = Reputed::groupBy('payee_id')->select('payee_id', DB::raw('count(*) as total_restock'),  DB::raw('sum(point) AS cashback'))
          ->where('created_at', '>=', $firstDayOfLastMonth)
        ->where('created_at', '<=', $lastDayOfLastMonth)
        ->where('name', '!=', 'ShippingOrderReward')
        ->get();
        foreach ($pays_cuts as $key => $value) {
            if($value->cashback>0){
                $value->user->depositA($value->cashback, 'deposit', ['source' => $lastDayOfLastMonth.' Commision', 'description' => 'Deposit of commision  for the '.$lastDayOfLastMonth ]);
            $clear = Reputed::where('payee_id', $value->payee_id)->get();
            foreach ($clear as $key => $individual) {
              $individual->delete();
            }
            }

        }
      

          $user = auth()->user();
          $transactions = Wallet::with(['user'])->paginate(10);
         


        $currentMonth_transactions = Reputed::groupBy('payee_id')->select('payee_id', DB::raw('count(*) as total_restock'),  DB::raw('sum(point) AS total_sales'))
         ->with(['user'])->whereYear('created_at', Carbon::now()->year)
            ->whereMonth('created_at', Carbon::now()->month)->where('name', '!=', 'ShippingOrderReward')
        ->get();
       
    
       

       $currentTransaction = Transaction::where('created_at', '>=', $firstDayOfMonth)
                            ->where('created_at', '<=', $lastDayOfMonth)
                            ->sum('amount');
      $lastMonthTransaction = Transaction::
                 where('created_at', '>=', $firstDayOfLastMonth)
                    ->where('created_at', '<=', $lastDayOfLastMonth)
                            ->sum('amount');
     $allTransaction = Transaction::
                 sum('amount');
                  
        
        $progressbarCurrent = 0;
        $progressbarLast = 100;
        $progressbarAll = 100;
     $stats = array(
         array('label'=>'CURRENT TRANSACTION', 'value' => $currentTransaction, 'url' => '/admin/user-payaments',  'color'=>'success', 'progressbar'=>$progressbarCurrent),
         array('label'=>'lAST MONTH TRANSACTION', 'value' => $lastMonthTransaction,  'url' => '/admin/user-payaments',  'color'=>'info', 'progressbar'=>$progressbarLast),
         array('label'=>'ALL TRANSACTION ', 'value' => $allTransaction,  'url' => '/admin/user-payaments', 'color'=>'danger', 'progressbar'=>$progressbarAll),
      );
        return view('admin.userPayaments', 
            ["title" => "Dealer Payments",
            "sidebar" => "payments",
            "transactions" => $transactions,
            "currentMonth_transactions" => $currentMonth_transactions,
            "stats" => $stats,
        ]);
        
    }
    
}
