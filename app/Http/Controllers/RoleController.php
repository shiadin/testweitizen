<?php

namespace App\Http\Controllers;

use App\Http\Resources\RoleResource;
use App\Role;
use Illuminate\Http\Request;

class RoleController extends Controller
{
    public function index(Request $request)
    {
        // get all roles
        $roles = Role::select('*');

        // filter out admin and customer roles if request came from public
        if (!$request->is('admin/*')) {
            $roles->where('id', '<', 500);
        } else {
            if ($request->has('ids')) {
                $roles->whereIn('id', $request->ids);
            } else {
                $roles->where("id", "<", $request->user()->role_id);
            }
        }

        if ($request->has('status')) {
            $roles->whereStatus($request->status);
        }

        if ($request->has('keyword')) {
            if (is_numeric($request->keyword)) {
                $roles->whereId($request->keyword);
            } else {
                $roles->where(function ($query) use ($request) {
                    $query->where('name', 'like', "%$request->keyword%")
                        ->orWhere('description', 'like', "%$request->keyword%");
                });
            }
        }

        if ($request->has('except_admin') && $request->except_admin) {
            $roles->whereIn('id', [1, 2, 3, 4, 5]);
        }

        $roles->orderBy($request->order_by ?: 'id', $request->order_direction ?: 'asc');

        if ($request->has('per_page') && $request->per_page == 'all') {
            $roles = $roles->get();
        } else {
            $roles = $roles->paginate($request->has('per_page') ? $request->per_page : 10);
        }
        // return in page form
        return RoleResource::collection($roles);
    }
}
