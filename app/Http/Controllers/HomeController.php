<?php

namespace App\Http\Controllers;

use App\User;
use App\Order;
use App\Stock;
use App\Molpay;
use App\StockToken;
use App\Activity;
use App\Postpaid;
use Charts;
use App\GroupCommission;
use App\Mail\OrderCreated;
use App\Mail\OrderApproved;
use App\Mail\OrderRejected;
use App\Mail\OrderCancelled;
use App\Mail\OrderCompleted;
use App\Mail\OrderConfirmed;
use App\Mail\OrderDelivered;
use Illuminate\Http\Request;
use LaravelQRCode\Facades\QRCode;
use Illuminate\Support\Facades\DB;
use App\Http\Resources\UserResource;
use Illuminate\Support\Facades\Mail;
use Carbon\Carbon;
use Yajra\Datatables\Datatables;
class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
             $user = auth()->user()
                ->load([
                    'downlines.role', 'role', 'deposit.payment', 'upline', 'stocks.product',
                    'inviteCodes.role', 'deposits.payment', 'shippings'
                ]);

        // get user's activities
        $activities = $user->activities()->latest()
                            ->paginate(10)
                            ->withPath(route('user.activities'));
        // add activities to user relationship
        $user->setRelation('activities', $activities);
        
    
  

    



     
   
    
   

        $stock_reloads = StockToken::select(
            DB::raw('sum(quantity) as sums'), 
            DB::raw("DATE_FORMAT(created_at,'%M %Y') as months")
  )
  ->groupBy('months')
  ->where('user_id', auth()->user()->id)
  ->get();


          $stocks = Stock::with('product')->where('user_id', $user->id)->paginate(5);
          //return $stocks;

$chart_stock_reloads= Charts::create('bar', 'highcharts')
    ->title('Stocks Reload')
    ->elementLabel('Total Quantity Reloads ')
    ->labels($stock_reloads->pluck('months'))
    ->values($stock_reloads->pluck('sums'))
    ->dimensions(300,350)
    ->responsive(true);


$wallets = $user->wallet->transactions;
//return $wallet;

    //  return $stock_reloads; 
  
   // return $molpays;
        return view('user.dashboard', [ //before home view
            'sidebar' => 'Dashboard',
            'title' => 'Dashboard',
            'routes' => [
                'getProducts' => route('products.index'),
                'getRoles' => route('roles.index', ['per_page' => 'all'])
            ],
            'wallets' => $wallets,
            'chart_stock_reloads' => $chart_stock_reloads,
            'stock_reloads'=>$stock_reloads,
            'stocks' => $stocks,
            'user' => new UserResource($user),
        ]);
    }

    public function profile(Request $request)
    {
        $user = User::withCount(['downlines', 'shippings'])
                    ->with(['role', 'deposit.payment', 'deposits.payment', 'address', 'downlines', 'shippings'])
                    ->find(auth()->id());
        
        
        return view('user.profile', [
            'title' => 'Settings',
            'sidebar' => 'Settings',
            'user' => new UserResource($user),
            'routes' => [
                'getRoles' => route('roles.index', ['per_page' => 'all'])
            ]
        ]);
    }


    public function downlines()
    {
               // get users who has no upline
        
             // get date info
        $month = Carbon::now()->month;
        $year = Carbon::now()->year;
        $firstDayOfMonth = Carbon::parse("first day of this month")->toDateString();
        $lastDayOfMonth = Carbon::parse("last day of this month")->toDateString();
        $firstDayOfLastMonth = Carbon::parse("first day of last month")->toDateString();
        $lastDayOfLastMonth = Carbon::parse("last day of last month")->toDateString();

         $lastMonth = auth()->user()->downlines->
                 where('created_at', '>=', $firstDayOfLastMonth)
                    ->where('created_at', '<=', $lastDayOfLastMonth)
                            ->count();
          $current = auth()->user()->downlines->where('created_at', '>=', $firstDayOfMonth)
                            ->where('created_at', '<=', $lastDayOfMonth)
                            ->count();
          $all = auth()->user()->downlines->
                 count();

      $stats = array('current' => $current,
                       'lastMonth' => $lastMonth,
                       'all' => $all,
                       );
       $user = auth()->user()->load(['role', 'downlines']);
        return view('user.downlines', [
            'title' => 'My Dealers',
            'sidebar' => 'My Dealers',
                'stats' => $stats,
                'user' => new UserResource($user)
           
        ]);
       
    }


  public function downline_api(){

    return Datatables::of(User::query()->with('downlines', 'stocks', 'orders')->where('referrer_id', auth()->user()->id))
        ->addColumn('stocks', function($row){
            return $row->stocks->sum();
        })
        ->addColumn('role_id', function($row){
          if($row->role_id<10)
            return $row->role_id;
          else
            return 0;
        })
        ->addColumn('downlines', function($row){
            return $row->downlines->count();
        })
        ->addColumn('total_sales', function($row){
            return $row->orders->sum();
        })
        ->make(true);
  }
    public function shippings()
    {
        // get shippings
        $user = auth()->user();
        // return view
        return view('user.shippings', [
            'title' => 'My Shipping Order',
            'sidebar' => 'My Shipping Order',
            'user' => new UserResource($user),
            'routes' => [
                'getOrders' => route('user.orders.index', [
                    'type' => 'shipping',
                    'with_situations' => true,
                    'order_by' => 'updated_at',
                    'order_direction' => 'desc',
                ])
            ]
        ]);
    }

    public function requests()
    {
        // get requests
        $user = auth()->user();
        // return view
        return view('user.requests', [
            'title' => 'My Stock Transfer History',
            'sidebar' => 'My Stock Transfer History',
            'user' => new UserResource($user),
            'routes' => [
                'getOrders' => route('user.orders.index', [
                    'order_by' => 'updated_at',
                    'order_direction' => 'desc',
                    'with_situations' => true,
                    'is_requests' => 1
                ])
            ]
        ]);
    }
public function downlineProgress(){
 $user = auth()->user();
 $starThree = 0;
 $starFour = 0;
 $starFive = 0; 
     if(empty($user->downlines)){
    return;
     }else{ 
     foreach ($user->downlines as $key => $value) {
        if($value->role_id == 3){
           $starThree++;
        }
         if($value->role_id == 4){
           $starFour++;
        }
         if($value->role_id == 5){
           $starFive++;  
        }
     }
     }
  

     $p1 = 0;
     $p3 = 0;
     $p5 = 0;
     $p8 = 0;
     $p12 = 0;

     $progressbar1 = 'width: 0%;';
     $progressbar3 = 'width: 0%;';
     $progressbar5 = 'width: 0%;';
     $progressbar8 = 'width: 0%;';
     $progressbar12 = 'width: 0%;';


     
     if($starFive>=10){
      $commission10 = \Config::get('commission.starFive10');
       $p12 = 100;
       $p1 =  100;
       $p3 =  100;
       $p5 =  100;
       $p8 =  100;

     }else if($starFive>=5){
       $p12 = ($starFive/10)*100;
       $p1 =  100;
       $p3 =  100;
       $p5 =  100;
       $p8 =  100;
     }else if($starFive>=1){
       $p12 = ($starFive/10)*100;
       $p1 =  100;
       $p3 =  100;
       $p5 =  100;
       $p8 =  ($starFive/5)*100;
     }
     else if($starFour>=3){
       $p12 = (starFive/10)*100;
       $p1 =  100;
       $p3 =  100;
       $p5 =  ($starFive/1)*100;
       $p8 =  ($starFive/5)*100;
     }else if($starThree>=3){
       $p12 = (starFive/10)*100;
       $p1 =  100;
       $p3 =  ($starFour/3)*100;
       $p5 =  ($starFive/1)*100;
       $p8 =  ($starFive/5)*100;
     }

    $arr = array(
    array(
        "customCheck"=>"customCheck1",
        "name"=> '3:3 stars to earn 1% commision',
        "p" => $p1,
        "progressbar" => 'width: '.$p1.'%; background:linear-gradient(to right, #f39c12 35%, #be7a07 68%);'
    ),
    array(
        "customCheck"=>"customCheck3",
        "name"=> '3:4 stars to earn 1% commision',
        "p" => $p3,
        "progressbar" => 'width: '.$p3.'%; background:linear-gradient(to right, #f39c12 35%, #be7a07 68%);'
    ),
    array(
        "customCheck"=>"customCheck5",
        "name"=> '1:5 stars to earn 1% commision',
         "p" => $p5,
        "progressbar" => 'width: '.$p5.'%; background:linear-gradient(to right, #f39c12 35%, #be7a07 68%);'
    ),
    array(
        "customCheck"=>"customCheck8",
        "name"=> '5:5 stars to earn 1% commision',
         "p" => $p8,
         "progressbar" =>'width: '.$p8.'%; background:linear-gradient(to right, #f39c12 35%, #be7a07 68%);'
    ),
    array(
        "customCheck"=>"customCheck12",
        "name"=> '10:5 stars to earn 1% commision',
         "p" => $p12,
        "progressbar" => 'width: '.$p12.'%; background:linear-gradient(to right, #f39c12 35%, #be7a07 68%);'
    )
);
     
  
 return $arr;
}


public function groupcommission_api(Request $request){
  $commision = GroupCommission::where('type',  'GroupCommission')->where('user_id', auth()->user()->id)->where('current_level', '>=', 5)->get();
   return Datatables::of($commision)->make(true);

  }

public function commission_api(Request $request){
  $commision = GroupCommission::where('type', '!=', 'GroupCommission')->where('user_id', auth()->user()->id)->get();
   return Datatables::of($commision)->make(true);

  }
     public function Groupcommission(Request $request){
   // return $this->downlineProgress();
      $user = auth()->user();

      // $commision = GroupCommission::where('type', '!=', 'GroupCommission')->where('user_id', auth()->user()->id)->get();

      $group_commision = GroupCommission::where('type',  'GroupCommission')->where('user_id', auth()->user()->id)->get();


                
        // get date info
        $month = Carbon::now()->month;
        $year = Carbon::now()->year;
        $firstDayOfMonth = Carbon::parse("first day of this month")->toDateString();
        $lastDayOfMonth = Carbon::parse("last day of this month")->toDateString();
        $firstDayOfLastMonth = Carbon::parse("first day of last month")->toDateString();
        $lastDayOfLastMonth = Carbon::parse("last day of last month")->toDateString();

         $lastMonth = $group_commision->
                 where('created_at', '>=', $firstDayOfLastMonth)
                    ->where('created_at', '<=', $lastDayOfLastMonth)
                            ->sum('amount');
          $current = $group_commision->where('created_at', '>=', $firstDayOfMonth)
                            ->where('created_at', '<=', $lastDayOfMonth)
                            ->sum('amount');
          $all = $group_commision->
                 sum('amount');

      $earning = 0;
      $next = 0;
      $cu =0;
      $needed = 0;
      if($current>30000){
        $earning = $current*0.08;
        $next = 8;
        $cu = 8;
        $needed =0;
      }else if($current>=20001 && $current<=30000 ){
         $earning = $current*0.07;
        $next = 8;
        $cu = 7;
        $needed =30001-$current;
      }else if($current>=10001 && $current<=20000){
        $earning = $current*0.05;
        $next = 7;
        $cu = 5;
        $needed =20001-$current;
      }else if($current>=5000 && $current<=10000){
        $earning = $current*0.03;
        $next = 5;
        $cu = 3;
        $needed =10001-$current;
      }else{
        $earning = 0;
        $next = 3;
        $cu = 0;
        $needed =5000-$current;
      }

     //return $next;

          $earningLast = 0;
      if($lastMonth>=5000 || $lastMonth<=10000){
        $earningLast = $lastMonth*0.03;
      }else if($lastMonth>=10001 || $lastMonth<=20000){
        $earningLast = $lastMonth*0.05;
      }else if($lastMonth>=20001 || $lastMonth<=30000){
        $earningLast = $lastMonth*0.07;
      }else if($lastMonth>=30001){
        $earningLast = $lastMonth*0.08;
      }

      $stats = array('current' => $current,
                       'lastMonth' => $earningLast,
                       'all' => $earning,
                       'cu' =>$cu,
                       'next' =>$next,
                       'needed' => $needed
                       );

     
        return view('user.groupcommission', [
            'sidebar' => 'My Group Sales Commission',
            'title' => 'My Group Sales Commission',
            'stats' => $stats,
            'group_commision' => $group_commision
        ]);
   } 
   public function commission(Request $request){
   // return $this->downlineProgress();
      $user = auth()->user();

      $commision = GroupCommission::where('type', '!=', 'GroupCommission')->where('user_id', auth()->user()->id)->get();

      $group_commision = GroupCommission::where('type',  'GroupCommission')->where('user_id', auth()->user()->id)->paginate(10);
                
        // get date info
        $month = Carbon::now()->month;
        $year = Carbon::now()->year;
        $firstDayOfMonth = Carbon::parse("first day of this month")->toDateString();
        $lastDayOfMonth = Carbon::parse("last day of this month")->toDateString();
        $firstDayOfLastMonth = Carbon::parse("first day of last month")->toDateString();
        $lastDayOfLastMonth = Carbon::parse("last day of last month")->toDateString();

         $lastMonth = $commision->
                 where('created_at', '>=', $firstDayOfLastMonth)
                    ->where('created_at', '<=', $lastDayOfLastMonth)
                            ->sum('amount');
          $current = $commision->where('created_at', '>=', $firstDayOfMonth)
                            ->where('created_at', '<=', $lastDayOfMonth)
                            ->sum('amount');
          $all = $commision->
                 sum('amount');

      $stats = array('current' => $current,
                       'lastMonth' => $lastMonth,
                       'all' => $all,
                       );
     
        return view('user.commission', [
            'sidebar' => 'My Personal Commission',
            'title' => 'My Personal Commission',
            'stats' => $stats,
            'group_commision' => $group_commision
        ]);
   } 

   public function payment(){
      $user = auth()->user();
      $wallets = $user->transactions;
        // get date info
        $month = Carbon::now()->month;
        $year = Carbon::now()->year;
        $firstDayOfMonth = Carbon::parse("first day of this month")->toDateString();
        $lastDayOfMonth = Carbon::parse("last day of this month")->toDateString();
        $firstDayOfLastMonth = Carbon::parse("first day of last month")->toDateString();
        $lastDayOfLastMonth = Carbon::parse("last day of last month")->toDateString();

         $lastMonth = $wallets->
                 where('created_at', '>=', $firstDayOfLastMonth)
                    ->where('created_at', '<=', $lastDayOfLastMonth)
                            ->sum('amount');
          $current = $wallets->where('created_at', '>=', $firstDayOfMonth)
                            ->where('created_at', '<=', $lastDayOfMonth)
                            ->sum('amount');
          $all = $wallets->
                 sum('amount');

      $stats = array('current' => $current,
                       'lastMonth' => $lastMonth,
                       'all' => $all,
                       );
        return view('user.payment', [
            'sidebar' => 'My e-Wallet',
            'title' => 'My e-Wallet',
            'stats' => $stats,
            'wallets' => $wallets,
            'routes' => [
                'getProducts' => route('products.index'),
                'getRoles' => route('roles.index', ['per_page' => 'all'])
            ],
            'user' => new UserResource($user),
        ]);
   }


  public function customer_api(){
  $thisUser = auth()->user();
  $user = $thisUser->customers;

 
    return Datatables::of($thisUser->customers)
        // ->addColumn('address', function($row){
        //     return $row->street.' , '. $row->street_2. ' , '. $row->zipcode;
        // })
        // ->addColumn('place', function($row){
        //     return $row->city.' , '.$row->state.' , '.$row->country;
        // })
        ->make(true);
  }
   public function customer(){
    $user = auth()->user();
   // $user = $thisUser;
    //return $user;
   //   $user = User::find(auth()->user()->id)->with('customers')->first();
      // return $user->customers;
     
 // get date info
        $month = Carbon::now()->month;
        $year = Carbon::now()->year;
        $firstDayOfMonth = Carbon::parse("first day of this month")->toDateString();
        $lastDayOfMonth = Carbon::parse("last day of this month")->toDateString();
        $firstDayOfLastMonth = Carbon::parse("first day of last month")->toDateString();
        $lastDayOfLastMonth = Carbon::parse("last day of last month")->toDateString();
        $currenOrder = $user->customers
            ->where('created_at', '>=', $firstDayOfMonth)
                            ->where('created_at', '<=', $lastDayOfMonth)
                            ->count();
      $lastMonthOrder =$user->customers
                  ->where('created_at', '>=', $firstDayOfLastMonth)
                    ->where('created_at', '<=', $lastDayOfLastMonth)
                            ->count();
     $allOrder = $user->customers->count();
                  
                            
         //  return $allOrder;
      $stats = array(
         array('label'=>'CURRENT MONTH', 'value' => $currenOrder,  'color'=>'success', 'tag'=>'my current  customers'),
         array('label'=>'lAST month customers', 'value' => $lastMonthOrder,   'color'=>'info', 'tag'=>'Previous month customers'),
         array('label'=>'ALL TIME Customer ', 'value' => $allOrder,   'color'=>'danger', 'tag'=>'all time customers '),
      );
         // return $user;    
      //return  new UserResource($user);
        return view('user.customer', [
            'sidebar' => 'My Customers',
            'title' => 'My Customers',
            'stats' => $stats,
            'routes' => [
                'getProducts' => route('products.index'),
                'getRoles' => route('roles.index', ['per_page' => 'all'])
            ],
            'user' => new UserResource($user),
        ]);
   }
   public function dashboard(){
      $user = auth()->user()
                ->load([
                    'downlines.role', 'role', 'deposit.payment', 'upline', 'stocks.product',
                    'inviteCodes.role', 'deposits.payment', 'shippings'
                ]);
                //return $user;
     
        return view('user.dashboard', [
            'sidebar' => 'dashboard',
            'title' => 'Dashboard',
            'routes' => [
                'getProducts' => route('products.index'),
                'getRoles' => route('roles.index', ['per_page' => 'all'])
            ],
            'user' => new UserResource($user),
        ]);
   }

   public function inbox(){
     $user = auth()->user()
                ->load([
                    'downlines.role', 'role', 'deposit.payment', 'upline', 'stocks.product',
                    'inviteCodes.role', 'deposits.payment', 'shippings'
                ]);
              

        // get user's activities
        $activities = $user->activities()->latest()
                            ->paginate(10)
                            ->withPath(route('user.activities'));
        
        // add activities to user relationship
        $user->setRelation('activities', $activities);

        return view('user.inbox', [
            'sidebar' => 'My Activity & Notifications',
            'title' => 'My Activity & Notifications',
            'routes' => [
                'getProducts' => route('products.index'),
                'getRoles' => route('roles.index', ['per_page' => 'all'])
            ],
            'user' => new UserResource($user),
        ]);
   }
   public function activity_info(){
    $activity_info = DB::table('activity_log')
                 ->select('subject_type', DB::raw('count(*) as total'))
                 ->groupBy('subject_type')
                 ->where('causer_id', auth()->user()->id)
                 ->get();
        return $activity_info;
   }

public function wallet_api(){
  return Datatables::of(auth()->user()->transactions)
        ->addColumn('description', function($row){
            return $row->meta['description'];
        })
         ->addColumn('total', function($row){
            return auth()->user()->balance;
        })  
        ->addColumn('amount_in', function($row){
          if($row->type=='deposit')
            return $row->amount;
          else
            return '';
        })
        ->addColumn('amount_out', function($row){
            if($row->type=='withdraw')
            return $row->amount;
          else
            return '';
        })
        ->make(true);
}
   public function wallet(){
    $user = auth()->user();
      $wallets = auth()->user()->transactions;
        // get date info
        $month = Carbon::now()->month;
        $year = Carbon::now()->year;
        $firstDayOfMonth = Carbon::parse("first day of this month")->toDateString();
        $lastDayOfMonth = Carbon::parse("last day of this month")->toDateString();
        $firstDayOfLastMonth = Carbon::parse("first day of last month")->toDateString();
        $lastDayOfLastMonth = Carbon::parse("last day of last month")->toDateString();

         $lastMonth = $wallets->
                 where('created_at', '>=', $firstDayOfLastMonth)
                    ->where('created_at', '<=', $lastDayOfLastMonth)
                            ->sum('amount');
          $current = $wallets->where('created_at', '>=', $firstDayOfMonth)
                            ->where('created_at', '<=', $lastDayOfMonth)
                            ->sum('amount');
          $all = $wallets->
                 sum('amount');

      $stats = array('current' => $current,
                       'lastMonth' => $lastMonth,
                       'all' => $all,
                       );
      return view('user.wallet', [
            'sidebar' => 'My e-Wallet',
            'title' => 'My e-Wallet',
            
            'wallets' => $wallets,
            'stats' => $stats,
            'routes' => [
                'getProducts' => route('products.index'),
                'getRoles' => route('roles.index', ['per_page' => 'all'])
            ],
            'user' => new UserResource($user),
        ]);
       // $wallet =  DB:table('wallet')
   }


  /*public function postpaid_api(){

    return Datatables::of(Postpaid::query()->where('user_id', auth()->user()->id))
        ->make(true); 
  }
   public function postpaid(){

        
             // get date info
        $month = Carbon::now()->month;
        $year = Carbon::now()->year;
        $firstDayOfMonth = Carbon::parse("first day of this month")->toDateString();
        $lastDayOfMonth = Carbon::parse("last day of this month")->toDateString();
        $firstDayOfLastMonth = Carbon::parse("first day of last month")->toDateString();
        $lastDayOfLastMonth = Carbon::parse("last day of last month")->toDateString();

         $lastMonth = auth()->user()->postpaids->
                 where('created_at', '>=', $firstDayOfLastMonth)
                    ->where('created_at', '<=', $lastDayOfLastMonth)
                            ->sum('amount');
          $current = auth()->user()->postpaids->where('created_at', '>=', $firstDayOfMonth)
                            ->where('created_at', '<=', $lastDayOfMonth)
                            ->sum('amount');
          $all = auth()->user()->postpaids->
                 sum('amount');

      $stats = array('current' => $current,
                       'lastMonth' => $lastMonth,
                       'all' => $all,
                       );
      
     return view('user.salesdashboard', [
            'sidebar' => 'My Sales (Postpaid)',
            'title' => 'My Sales (Postpaid)',
            'stats' => $stats
            
            
        ]);
   }*/
   
}
