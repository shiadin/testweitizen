<?php

namespace App\Http\Controllers;

use App\Molpay;
use App\Payment;
use App\OrderItem;
use App\Order;
use Illuminate\Http\Request;
use \Config, \Closure, \Redirect, \URL, Hybrid\Curl;
use PDF;
use Alert;
use App\User;
use Illuminate\Support\Facades\Crypt;
use Cart;
use DB;
class MolpayController extends Controller
{

     protected static $url = 'https://sandbox.molpay.com/MOLPay/pay/%s/%s.php';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Molpay  $molpay
     * @return \Illuminate\Http\Response
     */
    public function show(Molpay $molpay)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Molpay  $molpay
     * @return \Illuminate\Http\Response
     */
    public function edit(Molpay $molpay)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Molpay  $molpay
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Molpay $molpay)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Molpay  $molpay
     * @return \Illuminate\Http\Response
     */
    public function destroy(Molpay $molpay)
    {
        //
    }

    public function stockReload(){
      try {
        DB::beginTransaction();
         $latest = Molpay::orderBy('created_at', 'desc')->first();
           
         
         $n = $latest ? $latest->id+1 : 1;
         
         $l = "WTZ".$n; 
          $orderid = $l . sprintf("%05d", rand(10000, 99999)); 
             
             $user = auth()->user();
             $description = "";
          
            
             $amount = Cart::total();
        foreach(Cart::content() as $row) {

             $params = collect([
            "user_id" => $user->id,
            "type" => "stocks",
            "orderid" => $orderid,
            "amount" => $row->subtotal,
            "molpay_order_id" => $orderid,
            "status" => 0,
        ]);

        $order = Order::create($params->toArray());

           $item = OrderItem::create([
            "product_id" => $row->id,
            "molpay_order_id" => $orderid,
            "order_id" => $order->id,
            "unit_price" => $row->price,
            "quantity" => $row->qty,
            "status" => 0
        ]);

        // create new payment
        $payment = Payment::create([
            "order_id" => $order->id,
            "user_id" => $user->id,
            "gateway" => $orderid,
            "amount" => $row->subtotal,
            "token" => $row->qty,
            "molpay_order_id" => $orderid,
            "status" => 0
        ]);

        }
      

        $description .='  Order: '.$orderid;
        $description .=' Total: '.$amount;
        $merchantID = \Config::get('molpay.MerchantID');

        $verifykey = \Config::get('molpay.verify_key');
        $payment_methods = "index";
        $vcode = md5( $amount.$merchantID.$orderid.$verifykey ); 
        $currency = \Config::get('molpay.currency');

        $url = sprintf(static::$url, $merchantID, $payment_methods).'?'.http_build_query(array(
            'amount'      => $amount,
            'orderid'     => strval($orderid),
            'bill_name'   => auth()->user()->name ?: '',
             // 'bill_email'  => 'mansourabdalla22@gmail.com',
            'bill_email'  => auth()->user()->email ?: '',
            'bill_mobile' => auth()->user()->phone_no ?: '',
            // 'bill_mobile' => '0163809834',
            'bill_desc'   => $description ?: '',
            'cur'         => $currency,
            'returnurl'   => URL::to('/callback-stock-add'),
            'vcode'       => $vcode,
        ));
        DB::commit();
        return Redirect::to($url);
    
    } catch (\Exception $e) {
            DB::rollback();
            // return error message
           return redirect('cart')->withSuccessMessage('There were issue contacting payment gatways!');
        }
    }

     public function creditReload(Request $request){
      try {
        DB::beginTransaction();
         $latest = Molpay::orderBy('created_at', 'desc')->first();
           
         
         $n = $latest ? $latest->id+1 : 1;
         
         $l = "WTZ".$n; 
          $orderid = $l . sprintf("%05d", rand(10000, 99999)); 
             
             $user = auth()->user();
             $description = "";
          
            
             $amount = $request->quantity;
            $params = collect([
            "user_id" => $user->id,
            "type" => "credits",
            "amount" => $request->quantity,
            "status" => 0,
            "path" => $orderid,
            "molpay_order_id" =>$orderid,
        ]);
        // create order
     $order = Order::create($params->except('path')->toArray());
        // create new payment
        $payment = Payment::create([
            "order_id" => $order->id,
            "user_id" => $user->id,
            "gateway" => $orderid,
            "molpay_order_id" => $orderid,
            "amount" => $params['amount'],
            "token" => $params['path'],
            "status" => 0
        ]);
        // return order
       // return $order;
      

        $description .='  Order: '.$orderid;
        $description .=' Total: '.$amount;
        $merchantID = \Config::get('molpay.MerchantID');

        $verifykey = \Config::get('molpay.verify_key');
        $payment_methods = "index";
        $vcode = md5( $amount.$merchantID.$orderid.$verifykey ); 
        $currency = \Config::get('molpay.currency');

        $url = sprintf(static::$url, $merchantID, $payment_methods).'?'.http_build_query(array(
            'amount'      => $amount,
            'orderid'     => strval($orderid),
            'bill_name'   => auth()->user()->name ?: '',
             // 'bill_email'  => 'mansourabdalla22@gmail.com',
            'bill_email'  => auth()->user()->email ?: '',
            'bill_mobile' => auth()->user()->phone_no ?: '',
            // 'bill_mobile' => '0163809834',
            'bill_desc'   => $description ?: '',
            'cur'         => $currency,
            'returnurl'   => URL::to('/callback-credit-add'),
            'vcode'       => $vcode,
        ));
        DB::commit();
        return Redirect::to($url);
    
    } catch (\Exception $e) {
            DB::rollback();
            dd($e);
            // return error message
           return redirect('cart')->withSuccessMessage('There were issue contacting payment gatways!');
        }
    }

 
}
