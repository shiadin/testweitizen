<?php

namespace App\Http\Controllers;
use App\Molpay;
use App\Stock;
use App\Payment;
use App\OrderItem;
use App\Reputed;
use Illuminate\Http\Request;
use  \Event, App\Transaction;
use Illuminate\Support\Facades\Input;
use  App\Events\PaymentWasCompleted;
use Illuminate\Support\Facades\Redirect;
use Cart;
use Alert;
use Illuminate\Support\Facades\Crypt;
use App\Purchase;
use DB;
use App\Gamify\Points\PersonalOrderRewards;
use App\User;
use App\Reward;
use App\Gamify\Points\DownlineCommissionReward;
use App\Mail\StockPurchased;
use Mail;

class Molpay_Callback_Market_Controller extends Controller
{
    protected $input = null;
  /**
   * Initalize the Callback Controller
   *
   * @access  public
   * @return  void
   */
  public function __construct()
  {
    $input = Input::all();
    $input['orderid'] = $input['orderid'];
    $this->input = $input;
  }
  /**
   * Used as Molpay Return URL callback
   *
   * @access  public
   * @return  Response
   */
  public function index(Request $request) 
  { 

    $input = $this->input;

    // verify origin of transaction using the hash
    $this->validation();
    // save the transaction
    $molpay = $this->save();
    switch ($input['status'])
    {
      case '00' :
      $description ='';
      $ids ='';
      $i = 1;
            foreach(Cart::content() as $row) {
               $purchase = new Purchase();
               $purchase->user_id = auth()->user()->id;
               $purchase->order_id = $molpay->order_id;
               $purchase->product_id = $row->id;
               $purchase->name = $row->name;
               $purchase->price = $row->price;
               $purchase->quantity = $row->qty;
               $purchase->status = $molpay->status;
               $purchase->molpay_id = $molpay->id;
               $purchase->save();

            $description .=  '#'.$i++.' ' . $row->name . ' - RM '. $row->price.'<hr>';
           
                       }

        $molpay->description = $description;
      $molpay->content = Cart::content();
      $molpay->tax = Cart::tax();
      $molpay->save();

      Cart::destroy();
       $pay = Payment::where('molpay_order_id', $molpay->order_id)->where('status', 0)->first();
       $thisUser = $pay->user;
         Mail::to($thisUser->email)->send(new StockPurchased($thisUser, $pay));
      
         try{
         DB::beginTransaction();

      
      
         $payments = Payment::where('molpay_order_id', $molpay->order_id)->where('status', 0)->get();
         foreach ($payments as $key => $value) {
          // add more request
        $request->request->add([
            'memo' => $molpay->orderid, 
        ]);
       

        $payment = $this->approveStocks($request, $value);
        
        
         }

         DB::commit();

         return redirect('payment-successful/'.$encrypted = Crypt::encryptString($molpay->id));

         } catch (\Exception $e) {
         DB::rollback();

         }

      
        break;
      case '11' :
      $description ='';
      $i = 1;
            foreach(Cart::content() as $row) {
            $description .=  '#'.$i++.' ' . $row->model->name . ' - RM '. $row->model->price.'<hr>';
                       }
    $molpay->description = $description;
      $molpay->content = Cart::content();
      $molpay->tax = Cart::tax();
      $molpay->total = Cart::subtotal();
      $molpay->save();
      return redirect('payment-failure/'.$encrypted = Crypt::encryptString($molpay->id));
        break;
      case '22' :
      $description ='';
      $i = 1;
       foreach(Cart::content() as $row) {
            $description .=  '#'.$i++.' ' . $row->model->name . ' - RM '. $row->model->price.'<hr>';
                       }
     $molpay->description = $description;
      $molpay->content = Cart::content();
      $molpay->tax = Cart::tax();
      $molpay->save();
      return redirect('payment-pending/'.$encrypted = Crypt::encryptString($molpay->id));
        break;
    }
    if (Event::listeners('molpay.response.return_url'))
    {
      $response = Event::until('molpay.response.return_url', array($molpay));
      if ( ! is_null($response)) return $response;
    }
    return Response::make('', 200);
  }
  /**
   * Used as Molpay callback query beta-1 (PUSH Notification)
   *
   * @access  public
   * @return  Response
   */
  public function push() 
  {
    $input = $this->input;
    // only accept molpay callback query
    if ($input['nbcb'] != '1')
    {
      throw new Exception("Not a valid Molpay Callback Query");
    }

    // verify origin of transaction using the hash
    $this->validation();
    // save the transaction
    $molpay = $this->save();
    switch ($input['status'])
    {
      case '00' :
        // payment is made
         return Redirect::to('/home');
        //Event::fire('molpay.checkout.paid', array($input['orderid']));
        break;
      case '11' :
          return Redirect::to('/home');
        //Event::fire('molpay.checkout.failed', array($input['orderid']));
        break;
      case '22' :
            return Redirect::to('/home');
        //Event::fire('molpay.checkout.pending', array($input['orderid']));
        break;
    }
    if (Event::listeners('molpay.response.callback'))
    {
      $response = Event::until('molpay.response.callback', array($molpay));
      if ( ! is_null($response)) return $response;
    }
    return Response::make('', 200);
  }
  /**
   * Save all payment transaction
   *
   * @access  protected
   * @return  Eloquent
   */
  protected function save()
  {
    $input = $this->input;
    // check for transaction id.
    $molpay = Molpay::where('transaction_id', '=', $input['tranID'])->first();
    if (is_null($molpay))
    {
      $molpay = new Molpay(array(
        'transaction_id' => $input['tranID'],
      ));
    }
    $molpay->amount       = $input['amount'];
    $molpay->domain       = $input['domain'];
    $molpay->app_code     = $input['appcode'];
    $molpay->order_id     = $input['orderid'];
    $molpay->channel      = $input['channel'];
    $molpay->status       = $input['status'];
    $molpay->currency     = $input['currency'];
    $molpay->paid_at      = $input['paydate'];
    $molpay->security_key = $input['skey'];
    $molpay->user_id      = auth()->user()->id;
    if ('00' !== $input['status'])
    {
      $input['error_code'] and $molpay->error_code = $input['error_code'];
      $input['error_desc'] and $molpay->error_description = $input['error_desc'];
    }
    $molpay->save();
    
    return $molpay;
  }
  /**
   * Validate hash to ensure that the request actually came from Molpay
   *
   * @access  protected
   * @return  void
   * @throws  Molpay\Exception
   */
  protected function validation()
  {

    extract($this->input);
    $vkey    = \Config::get('api.verify_key', '');
    $skey =  $this->input['skey'];
    $confirm = md5($paydate.$domain.md5($tranID.$orderid.$status.$domain.$amount.$currency).$appcode.$vkey);
    return $confirm;
    if ($skey !== $confirm)
    {
      throw new \Exception("Invalid Transaction Key");
    }
  }


 // public function rewards($order){
 //     $user = auth()->user();
 //     $starThree = 0;
 //     $starFour = 0;
 //     $starFive = 0;
 //     foreach ($user->downlines as $key => $value) {
 //        if($value->role_id == 3){
 //           $starThree++;
 //        }
 //         if($value->role_id == 4){
 //           $starFour++;
 //        }
 //         if($value->role_id == 5){
 //           $starFive++;  
 //        }
 //     }

     
 //     $commission = 0;
 //     if($starFive>=10){
 //      $commission = $order->amount * \Config::get('commission.starFive10');
 //      //get total amount
 //     }else if($starFive>=5){
 //     $commission = $order->amount * \Config::get('commission.starFive5');
 //     }
 //     else if($starFive>=1){
 //      $commission = $order->amount * \Config::get('commission.starFive'); 
 //     }else if($starFour >= 3){
 //      $commission = $order->amount *
 //        \Config::get('commission.starFour'); 
 //     }else if($starThree >= 3){
 //      $commission = $order->amount * \Config::get('commission.starThree'); 
 //     }else{
 //       $commission = 0; 
 //     }
 //       $order->givePoint(new PersonalOrderRewards($order, $commission, $order_item));
 //     return $user;
 //   }

   
  public function approveStocks(Request $request, Payment $payment)
    {
                 //update order items
         $order_item = orderItem::where('molpay_order_id', $payment->molpay_order_id)->get();
         foreach ($order_item as $key => $i) {
          $i->status = 1;
          $i->save();
         }

        // approve payment
        $payment->update([
            'status' => 1,
            'memo' => $request->memo
        ]);
        // approve payment's order
        $payment->order->update([
            'status' => 1
        ]);
        // get order with related model
        $order = $payment->order->load(['user', 'items']);
        // add stocks to user based on order items
        $order->items->each(function ($item) use ($order, $request) {
            // create or update user stocks based on product id
            $stock = Stock::updateOrCreate([
                'user_id' => $order->user_id,
                'product_id' => $item->product_id
            ], [
                'status' => 1
            ]);
            // get stock amount
            $ori = $stock->quantity;
            // update quantity
            $stock->increment('quantity', $item->quantity);
            // get product
            $product = $stock->product;
            // get new stock amount
            $new = $stock->quantity;
            $downline =auth()->user();
            $this->levelLogic($item->quantity, $downline);

            // log activity
            activity()
                ->performedOn($stock)
                ->causedBy($request->user())
                ->withProperties($request->all())
                ->log("$item->quantity unit of $product->name($product->sku) has been added to account from Request#$order->id by :causer.username ($ori => $new)");
        });

         
     // if($payment->user->role_id == 5){
     // $user = auth()->user();
     // $starThree = 0;
     // $starFour = 0;
     // $starFive = 0;
     // foreach ($user->downlines as $key => $value) {
     //    if($value->role_id == 3){
     //       $starThree++;
     //    }
     //     if($value->role_id == 4){
     //       $starFour++;
     //    }
     //     if($value->role_id == 5){
     //       $starFive++;  
     //    }
     // }

    
     // $commission = 0;
     // if($starFive>=10){
     //  $commission = $order->amount*(\Config::get('commission.starFive10'));
     // }else if($starFive>=5){
     // $commission = $order->amount * \Config::get('commission.starFive5');
     // }
     // else if($starFive>=1){
     //  $commission = $order->amount * \Config::get('commission.starFive'); 
     // }else if($starFour >= 3){
     //  $commission = $order->amount *
     //    \Config::get('commission.starFour'); 
     // }else if($starThree >= 3){
     //  $commission = $order->amount * \Config::get('commission.starThree'); 
     // }else{
     //   $commission = 0; 
     // }
     //  if($commission>0)
     //   $user->givePoint(new PersonalOrderRewards($order, $commission));   
         
    }

    public function levelLogic($quantity, $downline){
    
  if($downline->role_id==5){
     //do nothing
  }else if ($downline->role_id==4){

       if($quantity>=120){
           $this->upgradeLevel($downline, 5, $quantity);
           }  
  }else if ($downline->role_id==3){

            if($quantity==120){
              $this->upgradeLevel($downline, 5, $quantity);
               }else if($quantity==36){
               $this->upgradeLevel($downline, 4, $quantity);
              } 

  }else if ($downline->role_id==2){

          if($quantity==120){
          $this->upgradeLevel($downline, 5, $quantity);
          }else if($quantity==36){
           $this->upgradeLevel($downline, 4, $quantity);
          }else if($quantity==24){
            $this->upgradeLevel($downline, 3, $quantity);
          }

  }else if($downline->role_id==1){
        
         if($quantity==120){
             $this->upgradeLevel($downline, 5, $quantity);
          }else if($quantity==36){
          $roleId = 4;
             $this->upgradeLevel($downline, 4, $quantity);
          }else if($quantity==24){
             $this->upgradeLevel($downline, 3, $quantity);
          }else if($quantity==12){
             $this->upgradeLevel($downline, 2, $quantity);
          }
    }else if($downline->role_id==33){
        
         if($quantity==120){
            $this->upgradeLevel($downline, 5, $quantity);
          }else if($quantity==36){
             $this->upgradeLevel($downline, 4, $quantity);
          }else if($quantity>=24){
             $this->upgradeLevel($downline, 3, $quantity);
          }else if($quantity==12){
             $this->upgradeLevel($downline, 2, $quantity);
          }else if($quantity==6){
             $this->upgradeLevel($downline, 1, $quantity);
          }


  }


}

public function upgradeLevel($downline, $roleID, $quantity){
          $downline->role_id = $roleID;
          $downline->save();
          $downline->assignToSuitableUpline($roleID);
        
}

}
