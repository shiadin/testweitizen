<?php

namespace App\Http\Controllers;

use App\GroupCommission;
use Illuminate\Http\Request;

class GroupCommissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\GroupCommission  $groupCommission
     * @return \Illuminate\Http\Response
     */
    public function show(GroupCommission $groupCommission)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\GroupCommission  $groupCommission
     * @return \Illuminate\Http\Response
     */
    public function edit(GroupCommission $groupCommission)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\GroupCommission  $groupCommission
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, GroupCommission $groupCommission)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\GroupCommission  $groupCommission
     * @return \Illuminate\Http\Response
     */
    public function destroy(GroupCommission $groupCommission)
    {
        //
    }
}
