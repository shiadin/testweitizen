<?php

namespace App\Http\Controllers;

use App\User;
use App\Order;
use App\Payment;
use App\Product;
use App\Customer;
use App\Shipping;
use App\Postpaid;
use App\OrderItem;
use Dompdf\Dompdf;
use App\OrderCustomer;
use Illuminate\Http\Request;
use App\Exports\ShippingExport;
use App\Imports\ShippingImport;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\DB;
use App\Http\Resources\UserResource;
use Illuminate\Support\Facades\View;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Resources\OrderResource;
use App\Http\Resources\ProductResource;
use App\Gamify\Points\CustomerReward;
use App\Gamify\Points\ShippingOrderReward;
use Illuminate\Validation\ValidationException;

class OrderController extends Controller
{
    public function index(Request $request)
    {
        // if it is an export request
        if ($request->export == 1) {
            return (new ShippingExport($request->all()))->download('shippings.xlsx');
        }
        // check if request from admin
        $isAdmin = $request->is('admin/*');

        // initialize orders
        $orders = Order::with(['payment', 'user', 'activities']);

        if ($request->has('type')) {
            $type = is_array($request->type) ? implode('|', $request->type) : $request->type;
            $orders->where('type', is_array($request->type) ? 'regexp' : 'like', $type);
            // load necessary modal if type is shipping
            if ($type === 'shipping') {
                $orders->with(['customer.user', 'items.product', 'shipping']);
            }
        } elseif ($request->has('is_requests') && $request->is_requests == 1) {
            // get system modules
            $modules = explode(',', env('MODULES'));
            // if deposit modules is exists
            $existsDepositModule = in_array('deposit', $modules);
            // set regexp array
            $reg = ['stocks', 'credits'];
            // if module deposit exists
            if ($existsDepositModule) {
                $reg[] = 'deposit';
            }
            $orders->where('type', 'regexp', implode('|', $reg));
        }

        // limit result to owner if not admin request
        if (!$isAdmin) {
            $orders->where('user_id', auth()->id());
        }

        if ($request->has('status')) {
            $orders->whereIn('status', is_array($request->status) ? $request->status : explode(',', $request->status));
        }

        if ($request->has('mode') && sizeof($request->mode) == 1) {
            if ($request->mode[0] === 'delivery') {
                $orders->whereHas('customer');
            } else {
                $orders->whereDoesntHave('customer');
            }
        }

        if ($request->has('start_at')) {
            $orders->where('created_at', '>=', $request->start_at);
        }

        if ($request->has('end_at')) {
            $orders->where('created_at', '<=', $request->end_at);
        }

        if ($request->has('keyword')) {
            $orders->where(function ($query) use ($request) {
                $query->where('id', 'like', "%$request->keyword%")
                    ->orWhereHas('customer.user', function ($query) use ($request) {
                        $query->where('name', 'like', "%$request->keyword%");
                    })
                    ->orWhereHas('customer', function ($query) use ($request) {
                        $query->where('name', 'like', "%$request->keyword%");
                    })
                    ->orWhereHas('user', function ($query) use ($request) {
                        $query->where('name', 'like', "%$request->keyword%")
                            ->orWhere('username', 'like', "%$request->keyword%");
                    })
                    ->orWhereHas('customer.address', function ($query) use ($request) {
                        $query->where('country', 'like', "%$request->keyword%")
                            ->orWhere('state', 'like', "%$request->keyword%");
                    })
                    ->orWhereHas('items', function ($query) use ($request) {
                        $query->where('memo', 'like', "%$request->keyword%");
                        $query->orWhere('batch_numbers', 'like', "%$request->keyword%");
                    });
            });
        }

        if ($request->has('order_by')) {
            switch ($request->order_by) {
                case 'situation':
                    $request->order_by = 'status';
                    break;
                case 'user.id':
                    $request->order_by = 'user_id';
                    break;
                case 'user.username':
                    $orders->join('users', 'users.id', 'orders.user_id');
                    $request->order_by = 'username';
                    break;
            }
        }
        $orders->orderBy($request->order_by ?: 'id', $request->order_direction ?: 'asc');

        // paginate results
        if ($request->per_page == 'all') {
            $orders = $orders->get();
        } else {
            $orders = $orders->paginate($request->has('per_page') ? $request->per_page : 20);
        }
        // form collection
        $collection = OrderResource::collection($orders);
        // if ask for append status list
        if ($request->has('with_situations')) {
            $collection->additional(['situations' => Order::getSituations($request->type)]);
        }
        // return collection
        return $collection;
    }

 public function create(Request $request, $type)
    {
        // remove number in type
        $type = preg_replace('/\:\d/', '', $type);
        // add type into request
        $request->merge(['type' => $type]);
        // validation
        $this->validate($request, [
            'payment_proof' => 'required_unless:type,shipping|image|max:5120',
            'level' => ["required_if:type,deposit", Rule::in(range(1, env('HIGHEST_RESELLER_LEVEL')))],
            'quantity' => 'required_unless:type,deposit,shipping|integer',
            'product' => 'required_if:type,stocks|integer',
            'items' => 'required_if:type,shipping|array',
            'items.*.quantity' => 'numeric|min:1',
            'name' => 'required_with:customer|string|max:255',
            'phone_no' => 'required_with:customer|string|max:255',
            'street' => 'required_with:customer|string|max:255',
            'street_2' => 'nullable|string|max:255',
            'city' => 'required_with:customer|string|max:255',
            'state' => 'required_with:customer|string|max:255',
            'country' => 'required_with:customer|string|max:255',
            'zipcode' => 'required_with:customer|string|max:255',
        ]);
        try {
            DB::beginTransaction();
            // check if request from admin
            $isAdmin = $request->is('admin/*');
            // get user
            $user = ($isAdmin && $request->user_id ? User::find($request->user_id) : auth()->user())->load(['role']);
            // determine order type to get params
            switch ($type) {
                case 'deposit':
                    $order = $this->createDeposit($request, $user, $request->level);
                    $log = "A new deposit(level $request->level) #$order->id has been submited by :causer.username";
                    break;
                case 'stocks':
                    $product = Product::with('price')->findOrFail($request->product);
                    $order = $this->createStocks($request, $user, $product);
                    $log = "A new stock request #$order->id for $product->name($product->sku) has been submited by :causer.username";
                    break;
                case 'credits':
                    $order = $this->createCredits($request, $user);
                    $log = "A new credits request #$order->id has been submited by :causer.username";
                    break;
                case 'shipping':
                    $order = $this->createShipping($request, $user);
                    $log = "A new shipping order #$order->id has been created by :causer.username";
                    break;
                default:
                    throw new \Exception("Something is wrong when processing $type order", 500);
                    break;
            }
            // call order created event
            event('order.created', $order);
            // log activity
            activity()
                ->performedOn($order)
                ->causedBy($request->user())
                ->withProperties($request->all())
                ->log($log);
            DB::commit();
            // return success message
            return response()->json([
                "code" => 200,
                "message" => ucfirst($type)." #$order->id was Successfully Created!",
                'order' => new OrderResource($order->load('payment'))
            ]);
        } catch (\Exception $e) {
            DB::rollback();
            // return error message
            return response()->json([
                "code" => $e->getCode(),
                "message" => $e->getMessage(),
                "trace" => $e->getTrace()
            ], 500);
        }
    }

    public function createDeposit(Request $request, User $user, $level)
    {
        // check if user already have a deposit for specified level or not
        $deposit = Order::with('payment')
                        ->where('user_id', $user->id)
                        ->where('type', "deposit:$level")
                        ->where('status', '>', -1)
                        ->first();
        // if deposit exists
        if ($deposit) {
            // check approved deposit
            if ($deposit->status == 1) {
                throw new \Exception("Submission failed: Your deposit has been approved", 500);
            }
            // set payment status as rejected
            $deposit->payment()->where('status', 0)->update([
                'status' => -1,
                'memo' => 're-submit by user'
            ]);
        }
        // upload payment proof to server
        $path = $request->file('payment_proof')->store("public/users/$user->id/deposit:$level");
        // get reseller
        $reseller = collect(config('resellers'))->where('role_id', $level)->first();
        // if reseller not found
        if (!$reseller) {
            throw new \Exception("Something wrong in retrieving reseller info", 500);
        }
        // set params
        $params = collect([
            "user_id" => $user->id,
            "type" => "deposit:$level",
            "amount" => $reseller["deposit"],
            "status" => 0,
            "path" => str_replace('public/', '', $path),
        ]);
        // create or update order
        $order = Order::updateOrCreate([
            "type" => "deposit:$request->level",
            "user_id" => $user->id
        ], $params->except('path')->toArray());
        // create new payment
        $payment = Payment::create([
            "order_id" => $order->id,
            "user_id" => $user->id,
            "gateway" => "payment_proof",
            "amount" => $params['amount'],
            "token" => $params['path'],
            "status" => 0
        ]);
        // return order
        return $order;
    }

    public function createStocks(Request $request, User $user, Product $product)
    {
        
        
        $p = $request->price;
        // upload payment proof to server
        $path = '';
            if($request->payment_type=='slip' || $request->payment_type=='both'){
          $path = $request->file('payment_proof')->store("public/users/$user->id/stocks");
         
        }
        
    
        // set order total amount
        $amount = intval($request->quantity) * floatval($p);
        // if($amount>($user->balance+$request->both_amount)){
        //     throw new \Exception("You not have enouph ewallet balance to reload using both eWallet and slip", 500);
        // }
        // set params
        $params = collect([
            "user_id" => $user->id,
            "type" => "stocks",
            "amount" => $amount,
            "both_amount" => $request->both_amount,
            "sale_amount" => $amount,
            "payment_type" => $request->payment_type,
            "status" => 0,
            "path" => str_replace('public/', '', $path),
        ]);
        // create order
        $order = Order::create($params->except('path')->toArray());
        // create order item
        $item = OrderItem::create([
            "product_id" => $product->id,
            "order_id" => $order->id,
            "unit_price" => $p,
            "quantity" => intval($request->quantity),
            "status" => 0
        ]);
        // create new payment
        $payment = Payment::create([
            "order_id" => $order->id,
            "user_id" => $user->id,
            "gateway" => $request->payment_type,
            "amount" => $params['amount'],
            "token" => $params['path'],
            "status" => 0
        ]);
        // return order
        return $order;
    }

    public function createCredits(Request $request, User $user)
    {
        // upload payment proof to server
        $path = $request->file('payment_proof')->store("public/users/$user->id/credits");
        // set params
        $params = collect([
            "user_id" => $user->id,
            "type" => "credits",
            "amount" => $request->quantity,
            "status" => 0,
            "path" => str_replace('public/', '', $path),
        ]);
        // create order
        $order = Order::create($params->except('path')->toArray());
        // create new payment
        $payment = Payment::create([
            "order_id" => $order->id,
            "user_id" => $user->id,
            "gateway" => "payment_proof",
            "amount" => $params['amount'],
            "token" => $params['path'],
            "status" => 0
        ]);
        // return order
        return $order;
    }

    public function createPostpaidShipping(Request $request, User $user)
    {
       // if($request->addOn>0 || $request->delivery==1)
        $path = $request->file('payment_proof')->store("public/users/$user->id/shippings");

        // check if request from admin
        $isAdmin = $request->is('admin/*');
        // do if customer exists
        if ($request->customer && $request->delivery==1) {
            $thisCustomer  = json_decode($request->customer, true);
            // get customer
            $customer = collect($thisCustomer);
            // update or create customer
            $customer = User::updateOrCreate([
                'id' => $thisCustomer['id']
            ], [
                'name' => $thisCustomer['name'],
                'phone_no' => $thisCustomer['phone_no'],
                'role_id' => 700
            ]);
            // if this is a new customer
            if ($customer->wasRecentlyCreated) {
                // link to current user
              Customer::create([
                    'user_id' => $customer->id,
                    'reseller_id' => $user->id
                ]);
            
            }
            // update customer's address
            $customer->address()->updateOrCreate([
                'user_id' => $customer->id
            ], [
                "street" => $request->street,
                "street_2" => $request->street_2,
                "city" => $request->city,
                "state" => $request->state,
                "country" => $request->country,
                "zipcode" => $request->zipcode
            ]);
        }
        // create a new order
        $order = Order::create([
            'user_id' => $isAdmin && $request->user_id ? $request->user_id : auth()->id(),
            'type' => 'shipping',
            'amount' => 0,
            'addOn' => $request->addOn,
            'delivery' =>$request->delivery,
            'sale_amount' => $request->sale_amount,
            'shipping_type' => $request->shipping_type,
            'molpay_order_id' => str_replace('public/', '', $path),
        ]);
             //reward user for adding new customer
         // if (!empty($customer) && $customer->wasRecentlyCreated) {   
         //       givePoint(new CustomerReward($order, $customer));
         // }
        // loop array of items
        foreach (json_decode($request->items, true) as $item) {
            // find product with customer price
            $product = Product::with(['customerPrice'])->whereStatus(1)
                            ->whereId($item['product_id'])
                            ->firstOrFail();
            // create an order item

            if($item['quantity'] < 1){
                throw new \Exception("Each product selectected should have min quantity 1 item", 500); 
            }

            OrderItem::create([
                'product_id' => $item['product_id'],
                'order_id' => $order->id,
                'unit_price' => $product->customerPrice->amount,
                'quantity' => $item['quantity'],
                'memo' => isset($item['memo']) ? $item['memo'] : null
            ]);
            // get stock from user
           // $stock = $user->stocks()->where('product_id', $product->id)->first();
            // reduce stock
           // $stock->decrease($item['quantity'], $request);
        }
        // do if customer exists
        if ($request->customer && $request->delivery==1) {
            // create an order customer
            OrderCustomer::create([
                'user_id' => $customer->id,
                'order_id' => $order->id
            ]);
        } else {
            // hack isAdmin variable and fee variable
            $isAdmin = true;
            $request->merge(['fee' => 0]);
        }
        // reload order to calculate total amount
        $order->load(['customer.address', 'items.product']);
        $amount = 0;
        //$amount = $order->getAmount($isAdmin && $request->has('fee') ? $request->fee : null);
        // reduct credits from user
        // if ($user->credits - $amount < 0) {
        //     throw new \Exception("You have no enough credits to create a new order", 500);
        // }
        // $ori = $user->credits;
        // $user->decrement('credits', $amount);
        // $new = $user->credits;
        // log activity
        // activity()
        //     ->performedOn($user)
        //     ->causedBy($request->user())
        //     ->withProperties($request->all())
        //     ->log("$amount unit of credits has been removed from account for Shipping#$order->id by :causer.username ($ori => $new)");
        // update order amount
        $order->update([
            'amount' => $amount
        ]);
        // return order
        return $order;
    }

     public function createShipping(Request $request, User $user)
    {
        // check if request from admin
        $isAdmin = $request->is('admin/*');
        // do if customer exists
        if ($request->customer) {
            // get customer
            $customer = collect($request->customer);
            // update or create customer
            $customer = User::updateOrCreate([
                'id' => $request->customer['id']
            ], [
                'name' => $request->customer['name'],
                'phone_no' => $request->customer['phone_no'],
                'role_id' => 700
            ]);
            // if this is a new customer
            if ($customer->wasRecentlyCreated) {
                // link to current user
                Customer::create([
                    'user_id' => $customer->id,
                    'reseller_id' => $user->id
                ]);
            }
            // update customer's address
            $customer->address()->updateOrCreate([
                'user_id' => $customer->id
            ], [
                "street" => $request->street,
                "street_2" => $request->street_2,
                "city" => $request->city,
                "state" => $request->state,
                "country" => $request->country,
                "zipcode" => $request->zipcode
            ]);
        }
        // create a new order
        $order = Order::create([
            'user_id' => $isAdmin && $request->user_id ? $request->user_id : auth()->id(),
            'type' => 'shipping',
            'amount' => 0
        ]);
        // loop array of items
        foreach ($request->items as $item) {
            // find product with customer price
            $product = Product::with(['customerPrice'])->whereStatus(1)
                            ->whereId($item['product_id'])
                            ->firstOrFail();
            // create an order item
            OrderItem::create([
                'product_id' => $item['product_id'],
                'order_id' => $order->id,
                'unit_price' => $product->customerPrice->amount,
                'quantity' => $item['quantity'],
                'memo' => isset($item['memo']) ? $item['memo'] : null
            ]);
            // get stock from user
            $stock = $user->stocks()->where('product_id', $product->id)->first();
            // reduce stock
            $stock->decrease($item['quantity'], $request);
        }
        // do if customer exists
        if ($request->customer) {
            // create an order customer
            OrderCustomer::create([
                'user_id' => $customer->id,
                'order_id' => $order->id
            ]);
        } else {
            // hack isAdmin variable and fee variable
            $isAdmin = true;
            $request->merge(['fee' => 0]);
        }
        // reload order to calculate total amount
        $order->load(['customer.address', 'items.product']);
        $amount = $order->getAmount($isAdmin && $request->has('fee') ? $request->fee : null);
        // reduct credits from user
        if ($user->balance - $amount < 0) {
            throw new \Exception("You have no enough ewallet to create a new order", 500);
        }
         $desc = 'Shipping Fee of Order #'.$order->id;
         $ori = $user->balance;
         $user->withdraw($amount, 'withdraw', ['description' => $desc, 'order'=>$order]);
         $new = $user->balance;
        // log activity
       activity()
            ->performedOn($user)
            ->causedBy($request->user())
            ->withProperties($request->all())
            ->log("$amount unit of ewaller has been removed from account for Shipping#$order->id by :causer.username ($ori => $new)");
        // update order amount
        $order->update([
            'amount' => $amount
        ]);
        // return order
        return $order;
    }



    

    public function cancel(Order $order, Request $request)
    {
        // validation
        $this->validate($request, [
            'memo' => 'required|max:255',
        ]);
        try {
            DB::beginTransaction();
            // get user
            $user = auth()->user()->load(['role']);
            // load related model
            $order->load('payment');
            // remove number in type
            $type = preg_replace('/\:\d/', '', $order->type);
            // determine order type to get params
            switch ($type) {
                case 'deposit':
                    $order = $this->cancelDeposit($request, $user, $order);
                    $log = "Deposit #$order->id has been cancelled by :causer.username";
                    break;
                case 'shipping':
                       if($order->shipping_type=='postpaid'){
            $this->cancelPostpaidShipping($request, $order);
              $log = "Shipping order #$order->id has been cancelled by :causer.username";

                    }else{
                    $order = $this->cancelShipping($request, $order);
                    $log = "Shipping order #$order->id has been cancelled by :causer.username";
                  }
                    break;
                default:
                    throw new \Exception("Something is wrong when processing $type order", 500);
                    break;
            }
            // call order cancelled event
            event('order.cancelled', $order);

            // log activity
            activity()
                ->performedOn($order)
                ->causedBy($request->user())
                ->withProperties($request->all())
                ->log($log);
            DB::commit();
            // return success message
            return response()->json([
                "code" => 200,
                "message" => ucfirst($type)." #$order->id was Successfully Cancelled!",
                'order' => new OrderResource($order)
            ]);
        } catch (\Exception $e) {
            DB::rollback();
            // return error message
            return response()->json([
                "code" => $e->getCode(),
                "message" => $e->getMessage(),
                "trace" => $e->getTrace()
            ], 500);
        }
    }

    public function cancelDeposit(Request $request, User $user, Order $order)
    {
        // check if order is not cancel by owner or not admin
        if (!$order->allowToModify($request->user())) {
            throw new \Exception("You have no permission to cancel deposit #$order->id", 500);
        }
        // cancel deposit
        $order->update([
            'status' => -1,
            'memo' => $request->memo
        ]);
        // cancel deposit payment
        $order->payment()->where('status', 0)->update([
            'status' => -1,
            'memo' => $request->memo
        ]);
        // return order
        return $order;
    }

        public function postpaid(Request $request)
    {
        // check request is from admin
        $isAdmin = $request->is('admin/*');
        $order = (new Order)->setRelation('items', collect([]));
        $user = $isAdmin ? null : new UserResource(auth()->user()->load(['customers', 'stocks.product.customerPrice', 'shippings']));
        return view('shipping/form-postpaid', [
            'title' => 'Create New Postpaid Order',
            'sidebar' => 'shippings',
            'user' => $user,
            'order' => new OrderResource($order),
            'shipping' => config('shipping'),
            'routes' => [
                'getUsers' => $isAdmin ? route('admin.users.index', [
                    'per_page' => 'all',
                    'with_shipping_models' => true
                ]) : null,
            ]
        ]);
    }


    public function new(Request $request)
    {
        // check request is from admin
        $isAdmin = $request->is('admin/*');
        $order = (new Order)->setRelation('items', collect([]));
        $user = $isAdmin ? null : new UserResource(auth()->user()->load(['customers', 'stocks.product.customerPrice', 'shippings']));
        return view('shipping/form', [
            'title' => 'Create New Shipping Order',
            'sidebar' => 'Shipping Orders',
            'user' => $user,
            'order' => new OrderResource($order),
            'shipping' => config('shipping'),
            'routes' => [
                'getUsers' => $isAdmin ? route('admin.users.index', [
                    'per_page' => 'all',
                    'with_shipping_models' => true
                ]) : null,
            ]
        ]);
    }

    public function edit(Order $order, Request $request)
    {
        // allow owner and admin to edit only
        // and edit page for shipping order only
        if (!$order->allowToModify($request->user()) && $order->type != 'shipping') {
            abort(404);
        }
        // load order related model
        $order->load(['items.product.customerPrice', 'customer', 'shipping']);
        // get user
        $user = $order->user;
        // load user related model
        $user->load(['customers', 'stocks.product.customerPrice', 'shippings']);
        return view('shipping.form', [
            'title' => "Edit Shipping Order #$order->id",
            'sidebar' => 'shippings',
            'user' => new UserResource($user),
            'order' => new OrderResource($order),
            'shipping' => config('shipping'),
            'routes' => [

            ]
        ]);
    }

       public function updateShipping(Request $request, Order $order)
    {
        // allow owner and admin to edit only
        // and edit page for shipping order only
        if (!$order->allowToModify($request->user()) && $order->type != 'shipping') {
            abort(404);
        }
        // validation
        $this->validate($request, [
            'items' => 'required_if:type,shipping|array',
            'items.*.quantity' => 'numeric|min:1',
            'customer' => 'required_if:type,shipping',
            'name' => 'required_with:customer|string|max:255',
            'phone_no' => 'required_with:customer|string|max:255',
            'street' => 'required_with:customer|string|max:255',
            'street_2' => 'nullable|string|max:255',
            'city' => 'required_with:customer|string|max:255',
            'state' => 'required_with:customer|string|max:255',
            'country' => 'required_with:customer|string|max:255',
            'zipcode' => 'required_with:customer|string|max:255',
        ]);
        try {
            DB::beginTransaction();
            // check is admin request
            $isAdmin = $request->is('admin/*');
            // get order user
            $user = $order->user;
            // do if customer exists
            if ($request->customer) {
                // get customer
                $customer = collect($request->customer);
                // update or create customer
                $customer = User::updateOrCreate([
                    'id' => $request->customer['id']
                ], [
                    'name' => $request->customer['name'],
                    'phone_no' => $request->customer['phone_no'],
                    'role_id' => 700
                ]);
                // if this is a new customer
                if ($customer->wasRecentlyCreated) {
                    // link to current user
                    Customer::create([
                        'user_id' => $customer->id,
                        'reseller_id' => $user->id
                    ]);
                }
                // update customer's address
                $customer->address()->updateOrCreate([
                    'user_id' => $customer->id
                ], [
                    "street" => $request->street,
                    "street_2" => $request->street_2,
                    "city" => $request->city,
                    "state" => $request->state,
                    "country" => $request->country,
                    "zipcode" => $request->zipcode
                ]);
                // update or create an order customer
                OrderCustomer::updateOrCreate([
                    'order_id' => $order->id
                ], [
                    'user_id' => $customer->id
                ]);
            } else {
                // delete if order's customer exists
                OrderCustomer::where('order_id', $order->id)->delete();
            }
            // get items id not in request
            $ids = array_filter(collect($request->items)->pluck('id')->toArray());
            if (sizeof($ids)) {
                // loop order items not in request (deleted)
                $order->items->whereNotIn('id', $ids)
                    ->each(function ($item) use ($user, $request) {
                        // get stock from user
                        $stock = $user->stocks()->where('product_id', $item->product_id)->firstOrFail();
                        // increase stock
                        $stock->increase($item->quantity, $request);
                        // delete item from order
                        $item->delete();
                    });
            }
            // loop array of items
            foreach ($request->items as $item) {
                // find product with customer price
                $product = Product::with(['customerPrice'])->whereStatus(1)
                                ->whereId($item['product_id'])
                                ->firstOrFail();
                // create or update order item
                $orderItem = OrderItem::updateOrCreate([
                    'id' => $item['id'],
                    'order_id' => $order->id,
                ], [
                    'product_id' => $item['product_id'],
                    'unit_price' => $product->customerPrice->amount,
                ]);
                // get stock
                $stock = $user->stocks()->where('product_id', $item['product_id'])->firstOrFail();
                // update stock quantity
                $stock->reset($item['quantity'], $orderItem->quantity, $request);
                // update item quantity
                $orderItem->update([
                    'quantity' => $item['quantity'],
                    'memo' => isset($item['memo']) ? $item['memo'] : null
                ]);
            }

            // do if customer not exists
            if (!$request->customer) {
                // hack isAdmin variable and fee variable
                $isAdmin = true;
                $request->merge(['fee' => 0]);
            }
            // reload order to calculate total amount
            $order->load(['customer.address', 'items.product']);
            $oldAmount = $order->amount;
            $amount = $order->getAmount($isAdmin && $request->has('fee') ? $request->fee : null);
            if ($oldAmount + $user->balance - $amount < 0) {
                throw new \Exception("You have no enough credits to update the order: ", 500);
            }
            // reset credits from user
            //$user->update(['credits' => $oldAmount + $user->credits - $amount]);
            $amount_new =  $amount-$oldAmount;
            $desc = 'Shipping Fee of Order updated withdraw #'.$order->id;
         $ori = $user->balance;
         $user->withdraw($amount_new, 'withdraw', ['description' => $desc, 'order'=>$order]);
         $new = $user->balance;
            // update order amount
            $order->update([
                'amount' => $amount
            ]);
            // log activity
            activity()
                ->performedOn($order)
                ->causedBy($request->user())
                ->withProperties($request->all())
                ->log("Shipping Order #$order->id has been updated by :causer.username");
            DB::commit();
            // return success message
            return response()->json([
                "code" => 200,
                "message" => "Shipping Order #$order->id was Successfully Updated!",
                'order' => new OrderResource($order)
            ]);
        } catch (\Exception $e) {
            DB::rollback();
            // return error message
            return response()->json([
                "code" => $e->getCode(),
                "message" => $e->getMessage(),
                "trace" => $e->getTrace()
            ], 500);
        }
    }

    public function updatePostpaidShipping(Request $request, $order)
    {
        // allow owner and admin to edit only
        // and edit page for shipping order only
        if (!$order->allowToModify($request->user()) && $order->type != 'shipping') {
            abort(404);
        }
        // validation
        $this->validate($request, [
            'items' => 'required_if:type,shipping|string',
            'items.*.quantity' => 'numeric|min:1',
            'customer' => 'required_if:type,shipping',
            'name' => 'required_with:customer|string|max:255',
            'phone_no' => 'required_with:customer|string|max:255',
            'street' => 'required_with:customer|string|max:255',
            'street_2' => 'nullable|string|max:255',
            'city' => 'required_with:customer|string|max:255',
            'state' => 'required_with:customer|string|max:255',
            'country' => 'required_with:customer|string|max:255',
            'zipcode' => 'required_with:customer|string|max:255',
        ]);
        try {
            DB::beginTransaction();
            // check is admin request
            $isAdmin = $request->is('admin/*');
            // get order user
            $user = $order->user;
            // do if customer exists
            if ($request->customer) {
                // get customer
                $thisCustomer = json_decode($request, true);
                $customer = collect($$thisCustomer);
                // update or create customer
                $customer = User::updateOrCreate([
                    'id' => $thisCustomer['id']
                ], [
                    'name' => $thisCustomer['name'],
                    'phone_no' => $thisCustomer['phone_no'],
                    'role_id' => 700
                ]);
                // if this is a new customer
                if ($customer->wasRecentlyCreated) {
                    // link to current user
                    Customer::create([
                        'user_id' => $customer->id,
                        'reseller_id' => $user->id
                    ]);
                }
                // update customer's address
                $customer->address()->updateOrCreate([
                    'user_id' => $customer->id
                ], [
                    "street" => $request->street,
                    "street_2" => $request->street_2,
                    "city" => $request->city,
                    "state" => $request->state,
                    "country" => $request->country,
                    "zipcode" => $request->zipcode
                ]);
                // update or create an order customer
                OrderCustomer::updateOrCreate([
                    'order_id' => $order->id
                ], [
                    'user_id' => $customer->id
                ]);
            } else {
                // delete if order's customer exists
                OrderCustomer::where('order_id', $order->id)->delete();
            }
            // get items id not in request
            $ids = array_filter(collect(json_decode($request->items, true))->pluck('id')->toArray());
            if (sizeof($ids)) {
                // loop order items not in request (deleted)
                $order->items->whereNotIn('id', $ids)
                    ->each(function ($item) use ($user, $request) {
                        // get stock from user
                       // $stock = $user->stocks()->where('product_id', $item->product_id)->firstOrFail();
                        // increase stock
                      //  $stock->increase($item->quantity, $request);
                        // delete item from order
                        $item->delete();
                    });
            }
            // loop array of items
            foreach (json_decode($request->items, true) as $item) {
                // find product with customer price
                $product = Product::with(['customerPrice'])->whereStatus(1)
                                ->whereId($item['product_id'])
                                ->firstOrFail();
                // create or update order item
                $orderItem = OrderItem::updateOrCreate([
                    'id' => $item['id'],
                    'order_id' => $order->id,
                ], [
                    'product_id' => $item['product_id'],
                    'unit_price' => $product->customerPrice->amount,
                ]);
                // get stock
                //$stock = $user->stocks()->where('product_id', $item['product_id'])->firstOrFail();
                // update stock quantity
               // $stock->reset($item['quantity'], $orderItem->quantity, $request);
                // update item quantity
                $orderItem->update([
                    'quantity' => $item['quantity'],
                    'memo' => isset($item['memo']) ? $item['memo'] : null
                ]);
            }

            // do if customer not exists
            if (!$request->customer) {
                // hack isAdmin variable and fee variable
                $isAdmin = true;
                $request->merge(['fee' => 0]);
            }
            // reload order to calculate total amount
            // $order->load(['customer.address', 'items.product']);
            // $oldAmount = $order->amount;
            // $amount = $order->getAmount($isAdmin && $request->has('fee') ? $request->fee : null);
            // if ($oldAmount + $user->credits - $amount < 0) {
            //     throw new \Exception("You have no enough credits to update the order: ", 500);
            // }
            // reset credits from user
            // $user->update(['credits' => $oldAmount + $user->credits - $amount]);
            // // update order amount
            $order->update([
                'sale_amount' => $request->sale_amount,
                'addOn' => $request->addOn,
                'delivery' =>$request->delivery,
            ]);
            //log activity
            activity()
                ->performedOn($order)
                ->causedBy($request->user())
                ->withProperties($request->all())
                ->log("Shipping Order #$order->id has been updated by :causer.username");
            DB::commit();
            // return success message
            return response()->json([
                "code" => 200,
                "message" => "Shipping Order #$order->id was Successfully Updated!",
                'order' => new OrderResource($order)
            ]);
        } catch (\Exception $e) {
            DB::rollback();
            // return error message
            return response()->json([
                "code" => $e->getCode(),
                "message" => $e->getMessage(),
                "trace" => $e->getTrace()
            ], 500);
        }
    }

  public function cancelShipping(Request $request, Order $order)
    {

     
        // check if order is not cancel by owner or not admin
        if (!$order->allowToModify($request->user())) {
            throw new \Exception("You have no permission to cancel shipping order #$order->id", 500);
        }
        // check if order is not created
        if ($order->status != 0 && $order->status != 1) {
            throw new \Exception("Only allow to cancel order waiting to process or delivery", 500);
        }
        // get order user
        $user = $order->user;
        // restore stock from order items one by one
        $order->items->each(function ($item) use ($user, $request, $order) {
            $stock = $user->stocks()->where('product_id', $item->product_id)->firstOrFail();
            $ori = $stock->quantity;
            $stock->increment('quantity', $item->quantity);
            $new = $stock->quantity;
            // log activity
            activity()
                ->performedOn($stock)
                ->causedBy($request->user())
                ->withProperties($request->all())
                ->log("$item->quantity unit of stocks has been refunded to account from Shipping#$order->id by :causer.username ($ori => $new)");
        });
        $ori = $user->balance;
        // restore user credits
        //$user->increment('credits', $order->amount);
        $user->depositA($order->amount, 'deposit', ['stripe_source' => 'Refund', 'description' => 'Deposit of '.$order->amount.' credits from Shipping fee cancelled']);
        $new = $user->balance;
        // log activity
        activity()
            ->performedOn($user)
            ->causedBy($request->user())
            ->withProperties($request->all())
            ->log("$order->amount unit of ewallet has been refunded to account from Shipping#$order->id by :causer.username ($ori => $new)");
        // cancel order
        $order->update([
            'status' => -1,
            'memo' => $request->memo
        ]);
        // reload order models
        $order->load(['customer', 'items.product', 'shipping', 'user']);
        return $order;

    }

    public function cancelPostpaidShipping(Request $request,  $order)
    {
        // check if order is not cancel by owner or not admin
        if (!$order->allowToModify($request->user())) {
            throw new \Exception("You have no permission to cancel shipping order #$order->id", 500);
        }
        // check if order is not created
        if ($order->status != 0 && $order->status != 1) {
            throw new \Exception("Only allow to cancel order waiting to process or delivery", 500);
        }
        // get order user
        $user = $order->user;
  
       
        // cancel order
        $order->update([
            'status' => -1,
            'memo' => $request->memo
        ]);
        // reload order models
        $order->load(['customer', 'items.product', 'shipping', 'user']);
        return $order;
    }


    public function processShipping(Request $request, Order $order)
    {
      //  dd($order);

        // allow owner and admin to edit only
        // and edit page for shipping order only
        if (!$order->allowToModify($request->user()) && $order->type != 'shipping') {
            abort(404);
        }
        // load order customer
        $order->load(['customer']);
        // set validation rules
        $rules = [
            'tracking_no' => 'required_unless:force_complete,true|max:255',
            'memo' => 'nullable|string|max:255',
            'courier' => 'required|max:255',
            'items' => 'array|required'
        ];
        // if order is self pickup (no customer found)
        // if (!$order->customer) {
         if (!$order->customer) {
            $rules['tracking_no'] = 'nullable|max:255';
            $rules['courier'] = 'nullable|max:255';
        }
        // validation
        $this->validate($request, $rules);
        try {
            DB::beginTransaction();
            // if tracking number found
            if ($request->tracking_no) {
                // update tracking no of order
                Shipping::updateOrCreate([
                    'order_id' => $order->id
                ], [
                    'tracking_no' => $request->tracking_no,
                    'courier' =>  $request->courier
                ]);
            }
            // update order
            // if no order customer found, turn order to completed state directly
            if($order->delivery==3){
                $order->update([
                    'status' => $order->customer && $request->tracking_no ? 2 : 3,
                    'memo' => $request->memo
                ]);
            }else{
                $order->update([
                    'status' => 2,
                    'memo' => $request->memo
                ]);
            }
           
            // update order items if needed
            if ($request->has('items')) {
                collect($request->items)->each(function ($item) use ($order) {
                    if (isset($item['id']) && isset($item['batch_numbers'])) {
                        $order->items()->where('id', $item['id'])->update([
                            'batch_numbers' => $item['batch_numbers']
                        ]);
                    }
                });
            }
            // if no order customer found, turn order to completed state directly
            if ($order->customer && $request->tracking_no) {
                // call order delivered event
                event('order.delivered', $order);
            } else {
                // call order completed event
                event('order.completed', $order);
            }
            // reload order
            $order->load(['customer', 'shipping', 'items.product', 'user']);
          //  dd($order->amount);
    //accumulate sales for a given time
 

    //sale table
      if($order->shipping_type=='postpaid'){
             
        $qty = 0;
        foreach ($order->items as $key => $value) {
            $qty+=$value->quantity;
        }
       
        Postpaid::create([
        "user_id" => $order->user_id,
        "order_id" => $order->id,
        // "unit_price" => $order->unit_price,
        "amount" => $order->sale_amount,
        "quantity" => $qty,
        // "product_id" => $order->product_id,
        "memo" => $order->shipping_type
     ]);
     //handle admin stock here
        $admin = User::find(1);
        $adminStock = $admin->stockToken;

            // if quantity larger than request
            
     // dd($adminStock);
      //  dd($order->items->count('quantity'));
   

        if ($adminStock->quantity < $qty) {
               //notify
                throw new \Exception("Quantity must be less than Admin stock amount", 500);
            }
            //dd($qty);
             $adminStock->decrement('quantity', $qty);
             
             $admin_ori_wallet = $admin->balance;
             $desc = 'Deposit of RM '.$order->sale_amount.' Postpaid Order by '.$order->user->name.'order # '.$order->id;
             $AdmindebitEwallet = $admin->depositA($order->sale_amount, 'deposit', ['source' => $order->id, 'description' => $desc]);
             $admin_wallet_new = $admin->balance;

   

             $thisUser = $order->user;
              activity()
                ->performedOn($adminStock)
                ->causedBy($request->user())
                ->withProperties($AdmindebitEwallet)
                ->log("RM $order->sale_amount of postpaid shipping order #$order->id has been added to account from seller#$thisUser->username by :causer.username ($admin_ori_wallet => $admin_wallet_new)");

     
      }
            // log activity
            activity()
                ->performedOn($order)
                ->causedBy($request->user())
                ->withProperties($request->all())
                ->log("Shipping Order #$order->id has been processed by :causer.username");
            DB::commit();
            // return success message
            return response()->json([
                "code" => 200,
                "message" => "Shipping Order #$order->id was Successfully Processed!",
                'order' => new OrderResource($order)
            ]);
        } catch (\Exception $e) {
            DB::rollback();
            // return error message
            return response()->json([
                "code" => $e->getCode(),
                "message" => $e->getMessage(),
                "trace" => $e->getTrace()
            ], 500);
        }
    }

    public function confirmShipping(Request $request, Order $order)
    {
        // allow owner and admin to edit only
        // and edit page for shipping order only
        if (!$order->allowToModify($request->user()) && $order->type != 'shipping') {
            abort(404);
        }
        try {
            DB::beginTransaction();
            // load order customer
            $order->load(['customer']);
            // update order
            $order->update([
                'status' => 1
            ]);
            // if order customer found
            if ($order->customer) {
                // update order customer
                OrderCustomer::where('order_id', $order->id)->update([
                    "name" => $order->customer->name,
                    "phone_no" => $order->customer->phone_no,
                    "street" => $order->customer->address->street,
                    "street_2" => $order->customer->address->street_2,
                    "zipcode" => $order->customer->address->zipcode,
                    "city" => $order->customer->address->city,
                    "state" => $order->customer->address->state,
                    "country" => $order->customer->address->country,
                ]);
            }
            // call order confirmed event
            event('order.confirmed', $order);
            // reload order
            $order->load(['customer', 'shipping', 'items.product', 'user']);
            // log activity
            activity()
                ->performedOn($order)
                ->causedBy($request->user())
                ->withProperties($request->all())
                ->log("Shipping Order #$order->id has been confirmed by :causer.username");
            DB::commit();
            // return success message
            return response()->json([
                "code" => 200,
                "message" => "Shipping Order #$order->id was Successfully Confirmed!",
                'order' => new OrderResource($order)
            ]);
        } catch (\Exception $e) {
            DB::rollback();
            // return error message
            return response()->json([
                "code" => $e->getCode(),
                "message" => $e->getMessage(),
                "trace" => $e->getTrace()
            ], 500);
        }
    }

    public function bulkProcess(Request $request)
    {
        // validate filex extension in hacking way since it cant validate with normal metho
        $ext = $request->file('excel') ? $request->file('excel')->getClientOriginalExtension() : null;
        // set extra request params
        $request->merge(['ext' => $ext]);
        // validate
        $this->validate($request, [
            'excel' => 'required',
            'ext' => 'required|in:xls,xlm,xla,xlc,xlt,xlw,xlsx'
        ], [
            'ext.in' => 'Only allow to upload Excel File'
        ]);
        try {
            // parse excel file
            $collection = Excel::toCollection(new ShippingImport, $request->file('excel'))->first();
            $collection = (new ShippingImport)->collection($collection);
            // get orders with items
            $orders = Order::with(['customer', 'shipping', 'items.product'])
                            ->whereIn('id', $collection->pluck('id'))->get();
            // set response container
            $response = [];
            // loop collection
            $collection->each(function ($item) use ($orders, &$response, $request) {
                // set response
                $res = [
                    'id' => $item['id'],
                    'status' => null
                ];
                try {
                    // get order
                    $order = $orders->where('id', $item['id'])->first();
                    // if order not found
                    if (!$order) {
                        throw new \Exception("Order Not Found", 500);
                    }
                    // if order is not shipping order
                    if ($order->type !== 'shipping') {
                        throw new \Exception("Accept Shipping Order Only", 500);
                    }
                    // if dont have order customer
                    if (!$order->customer) {
                        throw new \Exception("Self pickup not supported in bulk process", 500);
                    }
                    $item['items'] = collect($item['items'])->map(function ($product) use ($order) {
                        if (!empty($product['sku'])) {
                            $orderItem = $order->items->where('product.sku', $product['sku'])->first();
                            // if order item not found
                            if (!$orderItem) {
                                throw new \Exception("Product ".$product['sku']." not found in order", 500);
                            }
                            $product['id'] = $orderItem->id;
                        }
                        return $product;
                    })->toArray();
                    // modify request
                    $request->merge([
                        'tracking_no' => $item['tracking'],
                        'courier' => $item['courier'],
                        'memo' => isset($item['memo']) ? $item['memo'] : null,
                        'items' => $item['items']
                    ]);
                    // process order
                    $return = json_decode($this->processShipping($request, $order)->getContent());
                    // if success
                    if ($return->code == 200) {
                        $res['status'] = 'Process successful';
                    } else {
                        $res['status'] = $return->message;
                    }
                } catch (ValidationException $e) {
                    // display first error only
                    $res['status'] = collect($e->errors())->values()->flatten()->first();
                } catch (\Exception $e) {
                    $res['status'] = $e->getMessage();
                }
                array_push($response, $res);
            });
            // return success message
            return response()->json([
                "code" => 200,
                "message" => "Shipping Orders are Successfully Imported!",
                'list' => $response
            ]);
        } catch (\Exception $e) {
            // return error message
            return response()->json([
                "code" => $e->getCode(),
                "message" => $e->getMessage(),
                "trace" => $e->getTrace()
            ], 500);
        }
    }

    public function bulkConfirm(Request $request)
    {
        // validate
        $this->validate($request, [
            'ids' => 'required',
        ]);
        try {
            // get orders
            $orders = Order::with(['customer', 'shipping'])
                            ->whereIn('id', $request->ids)->get();
            // set response container
            $response = [];
            // loop orders
            $orders->each(function ($order) use ($request, $response) {
                // set response
                $res = [
                    'id' => $order->id,
                    'status' => null
                ];
                try {
                    // if order not found
                    if (!$order) {
                        throw new \Exception("Order Not Found", 500);
                    }
                    // if order is not shipping order
                    if ($order->type !== 'shipping') {
                        throw new \Exception("Accept Shipping Order Only", 500);
                    }
                    // if order is not processing
                    if ($order->status != 0) {
                        throw new \Exception("Allow to confirm processing order only (Status: $order->situation)", 500);
                    }
                    // process order
                    $return = json_decode($this->confirmShipping($request, $order)->getContent());
                    // if success
                    if ($return->code == 200) {
                        $res['status'] = 'Confirmed';
                    } else {
                        $res['status'] = $return->message;
                    }
                } catch (\Exception $e) {
                    $res['status'] = $e->getMessage();
                }
                array_push($response, $res);
            });
            // return success message
            return response()->json([
                "code" => 200,
                "message" => "Shipping Orders are Successfully Updated!",
                'list' => $response
            ]);
        } catch (\Exception $e) {
            // return error message
            return response()->json([
                "code" => $e->getCode(),
                "message" => $e->getMessage(),
                "trace" => $e->getTrace()
            ], 500);
        }
    }

    public function invoice(Request $request, Order $order)
    {
        // load related resource
        $order->load(['items.product', 'payment', 'customer', 'user']);
        // only paid order has invoice and payment exists
        // allow owner and admin to view only
        if (!$order->allowToViewInvoice() || !$order->allowToModify($request->user())) {
            abort(404);
        }
        // initialize pdf
        $pdf = new Dompdf;
        $pdf->set_option('isRemoteEnabled', true);
        $contxt = stream_context_create([
            'ssl' => [
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed'=> true
            ]
        ]);
        // dd(json_decode(json_encode(new OrderResource($order))));
        $pdf->setHttpContext($contxt);
        $pdf->loadHtml(
            View::make('shipping.invoice', [
                'title' => "Invoice for Order #$order->id",
                'order' => json_decode(json_encode(new OrderResource($order))),
                'currency' => config('app.currency')
            ])
            ->render()
        );
        $pdf->render();
        $output = $pdf->output();
        $headers = [
            'Content-Type' => 'application/pdf',
            'Content-Disposition' => 'inline; filename="invoice.pdf"'
        ];
        return response($output)->withHeaders($headers);
    }

    public function payment_proof(Request $request){
        dd($request->all());
    }
}
