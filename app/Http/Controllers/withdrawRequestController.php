<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\withdrawRequest;
use DB;
use App\User;
use App\Http\Resources\UserResource;
use App\Notifications\PaymentRequestPaid;
class withdrawRequestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {  
        $this->validate($request, [
            'user_id' => 'required|integer',
            'amount' => 'required',
          ]);
          $balance = auth()->user()->balance;
          if($request->amount> $balance){
            return response()->json([
                "code" => 500,
                "message" => "You requested amount which is greater than your current e-wallet balance",
                "trace" => ""
            ], 500);
          }
       $with =  withdrawRequest::create(['user_id'=> $request->user_id, 'amount'=>$request->amount, 'status'=>0]);
       return response()->json([
        "code" => 200,
        "message" => 'Request was Successfully accepted!',
        'data' => $with
    ]);
       
    }
    public function process(Request $request){


    

        $user = User::find($request->user_id);
        $withdraw = withdrawRequest::find($request->id);


       if($withdraw->status==-1){
        return response()->json([
            "code" => 500,
            "message" => 'you can not approve rejected request wih status on of -1',
        ], 500);
       }

      if($request->action=='reject'){

        $this->validate($request, [
            'memo' => 'required',
          ]);

        $withdraw->status = -1;
        $withdraw->memo = $request->memo;
        $withdraw->save();

        activity()
        ->performedOn($withdraw)
        ->causedBy($request->user())
        ->withProperties($request->all())
        ->log("A withdral request #$withdraw->id  has been rejected by :causer.username");

        $user->notify(new PaymentRequestPaid($withdraw));
        return response()->json([
            "code" => 200,
            "message" => 'Successfull Rejected '.$user->name.' withdral request',
            "request" => $withdraw
        ], 200);

      }else{
        $this->validate($request, [
            'user_id' => 'required|integer',
            'payment_proof' => 'required|image|max:5120',
          ]);
      if(auth()->user()->role_id===999){
        try {
            DB::beginTransaction();
         
          $ori = $user->balance;
          
          if($withdraw->amount>$user->balance){
            return response()->json([
                "code" => 500,
                "message" => 'the user e-walet balance not  enouph',
                "trace" => ''
            ], 500);
          }

          $path = $request->file('payment_proof')->store("public/wallet/$user->id/requestWidraw");
          
          $withdraw->payment_proof = str_replace('public/', '', $path);
          $withdraw->status = 1;
          $withdraw->save();
         $tem = $ori - ($withdraw->amount);
          $user->withdraw($withdraw->amount, 'withdraw', ['ewallet' => 'from RM'.$ori. ' to RM'.$tem, 
          'description' => 'withdraw of  RM '.$withdraw->amount.' from your e-Wallet and transfered to your bank account']);
         $new = $user->balance;

         activity()
         ->performedOn($withdraw)
         ->causedBy($request->user())
         ->withProperties($withdraw)
         ->log("RM $withdraw->amount  has been deducted from your e-wallet and transfered to your bank account  ($ori => $new)");
   
         $user->notify(new PaymentRequestPaid($withdraw));
          DB::commit();
          return response()->json([
            "code" => 200,
            "request" => $withdraw,
            "message" => 'Successfull processed '.$user->name.' withdral request',
        ], 200);

        } catch (\Exception $e) {
            DB::rollback();
            // return error message
            return response()->json([
                "code" => $e->getCode(),
                "message" => $e->getMessage(),
                "trace" => $e->getTrace()
            ], 500);
        }
      }
    }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return withdrawRequest::where('user_id', $id)->get();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        dd($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user= auth()->user();
        $del = withdrawRequest::findOrFail($id);
        $del->delete();
        activity()
         ->performedOn($del)
         ->causedBy($user)
         ->withProperties($del)
         ->log("$user->name have deleted the withdral request #$del->id  RM $del->amount");
   
         return response()->json([
                "code" => 200,
                "message" => 'request deleted successfully',
                
            ], 200);
        
    }

    public function user(Request $request){
        $user = User::find($request->user_id);
        return $user->balance;
    }
}
