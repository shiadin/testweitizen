<?php

namespace App\Http\Controllers;

use App\Role;
use App\User;
use Carbon\Carbon;
use App\Address;
use App\Customer;
use Intervention;
use App\Exports\UserExport;
use Illuminate\Http\Request;
use MathParser\StdMathParser;
use Illuminate\Validation\Rule;
use LaravelQRCode\Facades\QRCode;
use Illuminate\Support\Facades\DB;
use App\Http\Resources\UserResource;
use Illuminate\Support\Facades\Hash;
use MathParser\Interpreting\Evaluator;
use Illuminate\Support\Facades\Storage;
use App\Mail\AccountApproved;
use Illuminate\Support\Facades\Mail;

class UserController extends Controller
{
    public function index(Request $request)
    {
        // if it is an export request
        if ($request->export == 1) {
            return (new UserExport($request->all()))->download('users.xlsx');
        }
        // initialize users
        if ($request->with_shipping_models) {
            $users = User::with(['customers', 'stocks.product.customerPrice', 'shippings']);
        } elseif ($request->with_stock_models) {
            $users = User::with(['stocks.product']);
        } else {
            $users = User::with([
                'role', 'upline', 'address', 'deposits.user.role', 'deposits.payment',
                'activities' => function ($query) use ($request) {
                    if ($request->credits_activities) {
                        return $query->where('description', 'like', '%eWallet%')->latest()->take(20);
                    }
                    return $query->latest()->take(20);
                }
            ]);
        }


        // set role params
        $roles = $request->has('roles') ? $request->roles : range(1, 33);
        // : range(1, env('HIGHEST_RESELLER_LEVEL'));
        // if request to show admin too
        if (filter_var($request->show_admin, FILTER_VALIDATE_BOOLEAN)) {
            $roles = array_merge($roles, User::$adminGroup);
        }
        // filter by user's role
        $users->whereIn('role_id', $roles);


        if ($request->has('status') && !empty($request->status)) {
            $users->whereIn('status', $request->status);
        }

        if ($request->has('keyword')) {
            $users->where(function ($query) use ($request) {
                $query->where("username", "like", "%$request->keyword%")
                    ->orWhere("name", "like", "%$request->keyword%")
                    ->orWhere("email", "like", "%$request->keyword%")
                    ->orWhere("phone_no", "like", "%$request->keyword%")
                    ->orWhere("gender", "like", "%$request->keyword%")
                    ->orWhere("ic_no", "like", "%$request->keyword%");
            });
        }

        if ($request->has('order_by')) {
            switch ($request->order_by) {
                case 'role.name':
                    $request->order_by = 'role_id';
                    break;

                case 'situation':
                    $request->order_by = 'status';
                    break;
            }
        }

        $users->orderBy($request->order_by ?: 'id', $request->order_direction ?: 'asc');

        if ($request->per_page == 'all') {
            $users = $users->get();
        } else {
            // paginate results
            $users = $users->paginate($request->has('per_page') ? $request->per_page : 20);
        }
        // form collection
        $collection = UserResource::collection($users);
        // if ask for append status list
        if ($request->has('with_situations')) {
            $collection->additional(['situations' => User::getSituations()]);
        }
        // return collection
        return $collection;
    }

    public function update(Request $request, User $user = null)
    {
        // get user
        $user = $user ?: $request->user();
        // validate
        $this->validate($request, [
            'email' => "required|string|email|max:255|unique:users,name,$user->id",
            'name' => 'required|string|max:255',
            'gender' => ["required", Rule::in(['male', 'female'])],
            'ic_no' => 'required',
            'phone_no' => 'required|max:50',
            'wechat' => 'max:255',
            'facebook' => 'max:255',
           // 'country_code' => 'required',
            'street' => 'required|string|max:255',
            'street_2' => 'nullable|string|max:255',
            'city' => 'required|string|max:255',
            'state' => 'required|string|max:255',
            'country' => 'required|string|max:255',
            'zipcode' => 'required|numeric',
            'current_password' => 'required_with:password',
            'password' => 'required_with:current_password|string|min:6|confirmed'
        ]);
        try {
            DB::beginTransaction();
            // update password if requested
            if ($request->has('password')) {
                if (!password_verify($request->current_password, $user->password)) {
                    throw new \Exception("Invalid Current Password", 500);
                }
                $user->password = Hash::make($request->password);
            }

            // update user data
            $user->email = $request->email;
            $user->name = $request->name;
            $user->gender = $request->gender;
            $user->ic_no = $request->ic_no;
            $user->phone_no = $request->phone_no;
            // $user->phone_no = ltrim($request->phone_no, '0');
            // $user->country_code =$request->country_code;
            $user->wechat = $request->wechat;
            $user->facebook = $request->facebook;
            $user->save();
            // update or create user address
            Address::updateOrCreate([
                'user_id' => $user->id
            ], [
                "street" => $request->street,
                "street_2" => $request->street_2,
                "city" => $request->city,
                "state" => $request->state,
                "country" => $request->country,
                "zipcode" => $request->zipcode
            ]);
            // log activity
            activity()
                ->performedOn($user)
                ->causedBy($request->user())
                ->withProperties($request->all())
                ->log("The :subject.username's profile has been updated by :causer.username");

            DB::commit();
            // return success message
            return response()->json([
                "code" => 200,
                "message" => 'User was Successfully Updated!',
                'user' => new UserResource($user->load(['role', 'downlines', 'deposits']))
            ]);
        } catch (\Exception $e) {
            DB::rollback();
            // return error message
            return response()->json([
                "code" => $e->getCode(),
                "message" => $e->getMessage(),
                "trace" => $e->getTrace()
            ], 500);
        }
    }

    public function reject(Request $request, User $user)
    {
        $this->validate($request, [
            'memo' => 'required|max:255'
        ]);
        try {
            // set action
            $action = $user->status == 0 ? 'rejected' : 'blocked';
            // block user
            $user->update([
                'status' => -1,
                'memo' => $request->memo
            ]);
            // log activity
            activity()
                ->performedOn($user)
                ->causedBy($request->user())
                ->withProperties($request->all())
                ->log("The :subject.username's account has been $action by :causer.username");

            // return success message
            return response()->json([
                "code" => 200,
                "message" => 'User was Successfully Blocked!',
                'user' => new UserResource($user->load('role'))
            ]);
        } catch (\Exception $e) {
            // return error message
            return response()->json([
                "code" => $e->getCode(),
                "message" => $e->getMessage(),
                "trace" => $e->getTrace()
            ], 500);
        }
    }

    public function approve(Request $request, User $user)
    {
        $this->validate($request, [
            'memo' => 'max:255'
        ]);
        try {
            // set action
            $action = $user->status == 0 ? 'approved' : 'unblocked';
            // approve user
            $user->update([
                'status' => 1,
                'memo' => $request->memo
            ]);
            $thisUser = User::find($user->id);
              Mail::to($thisUser)->send(new AccountApproved($thisUser));
            // log activity
            activity()
                ->performedOn($user)
                ->causedBy($request->user())
                ->withProperties($request->all())
                ->log("The :subject.username's account has been $action by :causer.username");
            // return success message
            return response()->json([
                "code" => 200,
                "message" => 'User was Successfully Approved!',
                'user' => new UserResource($user->load('role'))
            ]);
        } catch (\Exception $e) {
            // return error message
            return response()->json([
                "code" => $e->getCode(),
                "message" => $e->getMessage(),
                "trace" => $e->getTrace()
            ], 500);
        }
    }

    public function edit(Request $request, User $user)
    {
        // load related models
        $user->load(['role', 'deposit.payment', 'deposits.payment', 'address', 'downlines', 'shippings']);
        // return to view
        return view('user.profile', [
            'title' => "Edit User: $user->username",
            'sidebar' => 'members',
            'user' => new UserResource($user),
            'routes' => [
                'getRoles' => route('roles.index', ['per_page' => 'all'])
            ]
        ]);
    }

    public function avatarUpload(Request $request)
    {
  
      //  return $request;
        // // validate
        // $this->validate($request, [
        //     'avatar' => 'required_without:avatar|image',
        // ]);
        try {
        
            // get user
            $user = auth()->user();
            // upload image if field exists
            if ($request->hasFile('avatar')) {
                // get ext
                $ext = $request->file('avatar')->extension();
                $path = $request->file('avatar')->storeAs("public/users/avatar/$user->id/", "avatar.$ext");
                // save to user
                $user->update(['avatar' => str_replace('public/', '', $path)]);
            }
            
            // return success message
            return response()->json([
                "code" => 200,
                "message" => ($request->hasFile('avatar') ? 'Profile' : 'Profile').' Picture  was Successfully Uploaded!',
                'user' => new UserResource($user->load(['role', 'downlines']))
            ]);
        } catch (\Exception $e) {
            // return error message
            return response()->json([
                "code" => $e->getCode(),
                "message" => $e->getMessage(),
                "trace" => $e->getTrace()
            ], 500);
        }
    }

    public function icUpload(Request $request, User $user = null)
    {
        // validate
        $this->validate($request, [
            'ic_front' => 'required_without:ic_back|image',
            'ic_back' => 'required_without:ic_front|image',
        ]);
        try {
            // not allow to re-ipload for user approved or blocked if not request from admin
            // and ic was uploaded
            if (is_null($user) &&
                !empty(auth()->user()->is_front) &&
                !empty(auth()->user()->is_back) &&
                auth()->user()->status != 0) {
                throw new \Exception("You have no permission to re-upload ic/passport", 500);
            }
            // get user
            $user = $user ?: $request->user();
            // upload image if field exists
            if ($request->hasFile('ic_front')) {
                // get ext
                $ext = $request->file('ic_front')->extension();
                $path = $request->file('ic_front')->storeAs("public/users/$user->id/", "ic_front.$ext");
                // save to user
                $user->update(['ic_front' => str_replace('public/', '', $path)]);
            }
            if ($request->hasFile('ic_back')) {
                // get ext
                $ext = $request->file('ic_back')->extension();
                $path = $request->file('ic_back')->storeAs("public/users/$user->id/", "ic_back.$ext");
                // save to user
                $user->update(['ic_back' => str_replace('public/', '', $path)]);
            }
            // return success message
            return response()->json([
                "code" => 200,
                "message" => ($request->hasFile('ic_front') ? 'Front' : 'Back').' View of IC/Passport was Successfully Uploaded!',
                'user' => new UserResource($user->load(['role', 'downlines']))
            ]);
        } catch (\Exception $e) {
            // return error message
            return response()->json([
                "code" => $e->getCode(),
                "message" => $e->getMessage(),
                "trace" => $e->getTrace()
            ], 500);
        }
    }

    public function updateSettings(Request $request, User $user = null)
    {
        // get user
        $user = $user ?: $request->user();
        // validate
        $this->validate($request, [
            'current_password' => 'required_with:password',
            'password' => 'required_with:current_password|string|min:6|confirmed'
        ]);
        try {
            DB::beginTransaction();
            // update password
            if ($request->has('password')) {
                if (!password_verify($request->current_password, $user->password)) {
                    throw new \Exception("Invalid Current Password", 500);
                }
                $user->password = Hash::make($request->password);
            }
            $user->save();
            // log activity
            activity()
                ->performedOn($user)
                ->causedBy($request->user())
                ->withProperties($request->all())
                ->log("The :subject.username's profile settings has been updated by :causer.username");

            DB::commit();
            // return success message
            return response()->json([
                "code" => 200,
                "message" => 'User Settings was Successfully Updated!',
                'user' => new UserResource($user->load(['role', 'downlines']))
            ]);
        } catch (\Exception $e) {
            DB::rollback();
            // return error message
            return response()->json([
                "code" => $e->getCode(),
                "message" => $e->getMessage(),
                "trace" => $e->getTrace()
            ], 500);
        }
    }

    public function updateCredits(Request $request, User $user)
    {
        // get user
        $user = $user ?: $request->user();
        // validate
        $this->validate($request, [
            'quantity' => 'required|numeric'
        ]);
        try {
            DB::beginTransaction();
            // get original amount
            $ori = $user->credits;
            // update credits
            $user->update(['credits' => $request->quantity]);
            // log activity
            activity()
                ->performedOn($user)
                ->causedBy($request->user())
                ->withProperties($request->all())
                ->log("The :subject.username's credits has been updated by :causer.username ($ori => $user->credits)");

            DB::commit();
            // return success message
            return response()->json([
                "code" => 200,
                "message" => 'User Credits was Successfully Updated!',
                'user' => new UserResource($user->load(['activities' => function ($query) {
                    return $query->latest()->take(20);
                }]))
            ]);
        } catch (\Exception $e) {
            DB::rollback();
            // return error message
            return response()->json([
                "code" => $e->getCode(),
                "message" => $e->getMessage(),
                "trace" => $e->getTrace()
            ], 500);
        }
    }

public function AddCustomers(Request $request){
       
        $this->validate($request, [
           'name' => 'required|string|max:255',
           'phone_no' => 'required|max:50',
           'street' => 'required|string|max:255',
           'street_2' => 'nullable|string|max:255',
           'city' => 'required|string|max:255',
           'state' => 'required|string|max:255',
           'country' => 'required|string|max:255',
           'zipcode' => 'required|numeric',
          
       ]);

       
   $reseller= auth()->user();
 try {
 DB::beginTransaction();
 $user = New user();
 $user->name = $request->name;
 $user->phone_no = $request->phone_no;
 $user->gender = 'male';
 $user->role_id = 700;
 $user->save();

 $customer = new Customer();
 $customer->user_id = $user->id;
 $customer->reseller_id = $reseller->id;
 $customer->save();


 $address = new Address();
 $address->street = $request->street;
 $address->street_2 = $request->address_2;
 $address->zipcode = $request->zipcode;
 $address->city = $request->city;
 $address->state = $request->state;
 $address->country = $request->country;
 $address->save();


 DB::commit();
session()->flash('success_message', 'Customer added successfully!');
 return back();
 }catch(\Exception $e){
   
DB::rollback();
 }
}
   public function cert($username)
    {
        // get user
        $user = User::whereUsername($username)
                    ->whereStatus(1)
                    ->with(['role', 'deposit'])
                    ->first();
        // get system modules
        $modules = explode(',', env('MODULES'));
        // if deposit modules is exists
        $existsDepositModule = in_array('deposit', $modules);
        // check deposit approved if user exists
        $depositApproved = $user && (!$user->depositRequired() || ($user->deposit && $user->deposit->status == 1));
        // abort 404 if user not found or deposit required but not exists or approved yet
        if (!$user || ($existsDepositModule && !$depositApproved)) {
            abort(404);
        }
        // if user name not provided
        if (empty($user->name)) {
            return redirect()->route('user.profile')->with('alert-warning', 'You should complete your personal information first');
        }
        // convert user to resource
        $user = json_decode(json_encode(new UserResource($user)));
        // set parser and evaluator for math expression
        $parser = new StdMathParser();
        $evaluator = new Evaluator();
        // set certificate draft
        $certificate = Intervention::make(storage_path('app/public/images/certificate-draft.png'));
        // set certificate width and height as the variables
        $evaluator->setVariables([
            'w' => $certificate->getWidth(),
            'h' => $certificate->getHeight()
        ]);
        // loop config data
        foreach (config('certificate') as $marker) {
            // get type
            $type = isset($marker['type']) ? $marker['type'] : 'text';
            // grab value from $user
            $text = data_get($user, $marker['data']);
            // do something based on type
            switch ($type) {
                case 'date':
                    $text = date('d F Y', strtotime($text));
                    $type = 'text';
                    break;
            }
            // calculate marker x point
            $x = $parser->parse($marker['x'])->accept($evaluator);
            // calculate marker y point
            $y = $parser->parse($marker['y'])->accept($evaluator);
            if ($type === 'text') {
                // insert marker into certificate draft
                $certificate->$type($text, $x, $y, function ($font) use ($marker) {
                    if (!isset($marker['font'])) {
                        return false;
                    }
                    foreach ($marker['font'] as $func => $arg) {
                        $font->$func($arg);
                    }
                });
            } else {
                // set options for curl
                $options = stream_context_create([
                    "ssl" => [
                        "verify_peer"=> false,
                        "verify_peer_name"=> false,
                    ],
                ]);
                // trying to get qrcode
                $res = file_get_contents($text, false, $options);
                // get qrcode as intervention image
                $qrcode = Intervention::make(base64_encode($res));
                // set qrcode size
                $qrcode->resize(optional($marker['image'])['width'], optional($marker['image'])['height']);
                // insert image into certificate draft
                $certificate->insert($qrcode, optional($marker['image'])['align'], $x, $y);
            }
        }

        return $certificate->response('png');
    }

    public function qrCode($username)
    {
        // get user
        $user = User::whereUsername($username)
                    ->whereStatus(1)
                    ->first();
        // if not found
        if (!$user) {
            return false;
        }
        // return qrcode
        return QRCode::url(route('user.certificate', $username))->setMargin(0)->setSize(8)->png();
    }

    public function qrInvitation()
    {
        $user = auth()->user();
        $code = $user->inviteCodes;
        if(empty($code)){
                return false;
        }
        $x = $code[0]->code;
        $url = request()->getHttpHost().'/register/'.$x;
       // return $url;
      return QRCode::url($url)->setMargin(0)->setSize(8)->png();
        // return qrcode
      //  return QRCode::url(route('user.certificate', $username))->setMargin(0)->setSize(8)->png();
    }

        public function avatar($username)
    {
        // get user
        $user = User::whereUsername($username)
                    ->whereStatus(1)
                    ->first();
        // if not found
        if (!$user) {
            return false;
        }
        // return qrcode
        return QRCode::url(route('user.certificate', $username))->setMargin(0)->setSize(8)->png();
    }

    public function updateUpline(Request $request, User $user)
    {
        // validate
        $this->validate($request, [
            'upline_id' => 'nullable|numeric'
        ]);
        try {
            if ($request->upline_id) {
                // get upline if exists
                $upline = User::findOrFail($request->upline_id);
                // if upline id same with user id
                if ($upline->id === $user->id) {
                    throw new \Exception("Not allow to assign user as self upline", 500);
                }
            }
            // get upline username
            $new = $request->upline_id ? $upline->username : 'SYSTEM';
            // load user model
            $user->load(['upline']);
            // get ori upline username
            $ori = $user->upline ? $user->upline->username : 'SYSTEM';
            // update user upline
            $user->update(['referrer_id' => $request->upline_id ? $upline->id : null]);
            // log activity
            activity()
                ->performedOn($user)
                ->causedBy($request->user())
                ->withProperties($request->all())
                ->log("The :subject.username's upline has been updated by :causer.username ($ori => $new)");
            // return success message
            return response()->json([
                "code" => 200,
                "message" => 'User Upline was Successfully Updated!',
            ]);
        } catch (\Exception $e) {
            // return error message
            return response()->json([
                "code" => $e->getCode(),
                "message" => $e->getMessage(),
                "trace" => $e->getTrace()
            ], 500);
        }
    }

    public function updateRole(Request $request, User $user)
    {
        // validate
        $this->validate($request, [
            'role_id' => 'required|numeric'
        ]);
        try {
            // get role if exists
            $role = Role::findOrFail($request->role_id);
            // get role name
            $new = $role->name;
            // load user model
            $user->load(['role']);
            // get ori role name
            $ori = $user->role->name;
            // update user role
            $user->update(['role_id' => $role->id]);
            // log activity
            activity()
                ->performedOn($user)
                ->causedBy($request->user())
                ->withProperties($request->all())
                ->log("The :subject.username's role has been updated by :causer.username ($ori => $new)");
            // return success message
            return response()->json([
                "code" => 200,
                "message" => 'User Role was Successfully Updated!',
            ]);
        } catch (\Exception $e) {
            // return error message
            return response()->json([
                "code" => $e->getCode(),
                "message" => $e->getMessage(),
                "trace" => $e->getTrace()
            ], 500);
        }
    }

    public function downlines(Request $request, User $user = null)
    {
        try {
            // get user
            $user = $user ?: $request->user();
            // load downlines
            $user->load(['downlines']);
            // return success message
            return response()->json([
                "code" => 200,
                "message" => 'User Settings was Successfully Updated!',
                'downlines' => UserResource::collection($user->downlines)
            ]);
        } catch (\Exception $e) {
            // return error message
            return response()->json([
                "code" => $e->getCode(),
                "message" => $e->getMessage(),
                "trace" => $e->getTrace()
            ], 500);
        }
    }

        public function downlineStat(){
    // get date info
        $month = Carbon::now()->month;
        $year = Carbon::now()->year;
        $firstDayOfMonth = Carbon::parse("first day of this month")->toDateString();
        $lastDayOfMonth = Carbon::parse("last day of this month")->toDateString();
        $firstDayOfLastMonth = Carbon::parse("first day of last month")->toDateString();
        $lastDayOfLastMonth = Carbon::parse("last day of last month")->toDateString();
         $user = User::where('id', auth()->user()->id)->first();
         $currentUsers = $user->downlines
                            ->where('created_at', '>=', $firstDayOfMonth)
                            ->where('created_at', '<=', $lastDayOfMonth)
                            ->count('id');
         $lastUsers = $user->downlines
                            ->where('created_at', '>=', $firstDayOfLastMonth)
                            ->where('created_at', '<=', $lastDayOfLastMonth)
                            ->count('id');
         $lifetime = $user->downlines
                            ->count('id');
              $a = array('total'=>$currentUsers,
                              'label'=>'Current Month',
                              'title'=>"downlines this month");
             $b = array('total'=>$lastUsers,
                              'label'=>'Last Month',
                              'title'=>"downlines since last month");
              $c = array('total'=>$lifetime,
                              'label'=>'Lifetime',
                              'title'=>"downlines all time");

          $d = array($a, $b, $c);
        return $d;
}

public function getupline(Request $request){
    return User::findOrFail($request->id);
}
public function authenticate_dealer_certificate(Request $request){
      $message = '';
      $status = 1;
    if(empty($request->q)){
        $message = $request->q;
        $status = 3;
        return view('authenticate_dealer_certificate', compact(['message', 'status']));
    }
    $public = User::where('wechat', $request->q)->orWhere('phone_no', $request->q)->first();
    if(empty($public)){
        $message = $request->q;
        $status = 0;
       // return 0;
        return view('authenticate_dealer_certificate', compact(['message', 'status']));
    }
        $username = $public->username;
        $user = User::whereUsername($username)
                    ->whereStatus(1)
                    ->with(['role', 'deposit'])
                    ->first();
      if(empty($user)){
        $message = $request->q;
        $status = 3;
        return view('authenticate_dealer_certificate', compact(['message', 'status']));
      }
 
          $message = 'authentic dealer';
          $c =route('user.certificate', ['username' => $username]);
           return view('authenticate_dealer_certificate', compact(['c', 'status', 'message']));
}

}


