<?php

namespace App\Http\Controllers;

use App\StockToken;
use Illuminate\Http\Request;

class StockTokenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\StockToken  $stockToken
     * @return \Illuminate\Http\Response
     */
    public function show(StockToken $stockToken)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\StockToken  $stockToken
     * @return \Illuminate\Http\Response
     */
    public function edit(StockToken $stockToken)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\StockToken  $stockToken
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, StockToken $stockToken)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\StockToken  $stockToken
     * @return \Illuminate\Http\Response
     */
    public function destroy(StockToken $stockToken)
    {
        //
    }
}
