<?php

namespace App\Http\Controllers;

use App\Postpaid;
use Illuminate\Http\Request;

class PostpaidController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Postpaid  $postpaid
     * @return \Illuminate\Http\Response
     */
    public function show(Postpaid $postpaid)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Postpaid  $postpaid
     * @return \Illuminate\Http\Response
     */
    public function edit(Postpaid $postpaid)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Postpaid  $postpaid
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Postpaid $postpaid)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Postpaid  $postpaid
     * @return \Illuminate\Http\Response
     */
    public function destroy(Postpaid $postpaid)
    {
        //
    }
}
