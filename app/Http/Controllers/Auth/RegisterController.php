<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Referral;
use App\Mail\UserRegistered;
use App\Mail\WelcomeToWeitizen;
use App\Mail\NewDownlineJoining;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'username' => ['required', 'string', 'regex:/^[a-z0-9_\-\@]+$/', 'max:255', 'min:4', 'unique:users'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
             'phone_no' => ['required', 'string', 'min:9', 'max:11',  'unique:users'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
            'agreement' => 'required'
            
        ], [
            'username.regex' => 'Only accept username mixed with lowercase, numbers, and special characters: @, -, _'
        ]);
    }

    public function register(Request $request, $code)
    {
        // check code from referral record
        $referral = Referral::whereCode($code)->firstOrFail();


        //trim phone leading 00 on phone number
        

        // add more request
        $request->request->add([
            'referrer_id' => $referral->user_id,
            'initial_referrer_id' => $referral->user_id,
            'role_id' => $referral->role_id,
            'country_code' => 60,
        ]);

        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));

        $this->guard()->login($user);

        // delete referral after registered
          //$referral->delete();

        return $this->registered($request, $user)
                        ?: redirect($this->redirectPath());
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $user = User::create([
            'username' => $data['username'],
            'email' => $data['email'],
            'phone_no' => $data['phone_no'],
            'country_code' => 6,
            'password' => Hash::make($data['password']),
            'referrer_id' => $data['referrer_id'],
            'initial_referrer_id' => $data['initial_referrer_id'],
            'role_id' => $data['role_id']
        ]);
        $user->update(['isVerified' => 1]);
         $thisUser = User::findOrFail($user->id);
      //  $this->registered($thisUser);
         Mail::to($thisUser)->send(new WelcomeToWeitizen($thisUser));
         Mail::to($thisUser->upline)->send(new NewDownlineJoining($thisUser->upline));
        return $user;
    }

    public function showRegistrationForm(Request $request, $code)
    {
        // check code from referral record
        $referral = Referral::with(['user', 'role'])->whereCode($code)->firstOrFail();
        return view('auth.register', [
            'code' => $code,
            'referral' => $referral
        ]);
    }

    protected function registered(Request $request, $user)
    {
        // send a welcome email to registered user
        Mail::to($user)->send(new UserRegistered($user));
    }
}
