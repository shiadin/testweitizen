<?php

namespace App\Http\Controllers;

use App\Role;
use App\Price;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Resources\ProductResource;

class ProductController extends Controller
{
    public function index(Request $request)
    {
        // get product with its related modules
        $products = Product::select('products.*')->with(['prices', 'user', 'price']);

        // filter out deleted result by default
        $products->where('products.status', '!=', -1);

        // query search
        if ($request->has('status') && !empty($request->status)) {
            $products->whereIn('products.status', $request->status);
        }

        if ($request->has('keyword')) {
            $products->where(function ($query) use ($request) {
                $query->where("description", "like", "%$request->keyword%")
                    ->orWhere("name", "like", "%$request->keyword%")
                    ->orWhere("sku", "like", "%$request->keyword%")
                    ->orWhere("weight", "like", "%$request->keyword%");
            });
        }

        if ($request->has('order_by')) {
            switch ($request->order_by) {
                case 'situation':
                    $request->order_by = 'status';
                    break;
                case 'user.username':
                    $products->join('users', 'users.id', 'products.user_id');
                    $request->order_by = 'username';
                    break;
            }
        }

        $products->orderBy($request->order_by ?: 'id', $request->order_direction ?: 'asc');

        if ($request->per_page == 'all') {
            $products = $products->get();
        } else {
            // paginate results
            $products = $products->paginate($request->has('per_page') ? $request->per_page : 20);
        }
        // form collection
        $collection = ProductResource::collection($products);
        // if ask for append status list
        if ($request->has('with_situations')) {
            $collection->additional(['situations' => Product::getSituations()]);
        }
        // return collection
        return $collection;
    }

    public function edit(Request $request, Product $product)
    {
        
        // lazy load related model
        $product->load('prices.role');
        // get all roles
        $roles = Role::all();
        // check if product doesnot contains all the roles
        if ($product->prices->count() != $roles->count()) {
            // loop roles not in product prices
            $roles->whereNotIn('id', $product->prices->pluck('role_id'))
                ->each(function ($role) use ($product) {
                    // push a new price to product
                    $product->prices->push(Price::create([
                        'role_id' => $role->id,
                        'user_id' => auth()->id(),
                        'product_id' => $product->id,
                        'amount' => 0
                    ]));
                });
            // reload related model
            $product->load('prices.role');
        }
        // return view
        return view('admin.product', [
            'title' => "Edit Product ($product->name)",
            'sidebar' => 'products',
            'product' => $product
        ]);
    }

    public function new(Request $request)
    {
        // return view
        return view('admin.product', [
            'title' => "Create New Product",
            'sidebar' => 'products',
            'product' => null
        ]);
    }

    public function get(Request $request, Product $product)
    {
        try {
            // load product related model
            $product->load(['price']);
            // return success message
            return response()->json([
                "code" => 200,
                "message" => 'Product was Successfully Retrieved!',
                'product' => new ProductResource($product)
            ]);
        } catch (\Exception $e) {
            // return error message
            return response()->json([
                "code" => $e->getCode(),
                "message" => $e->getMessage(),
                'trace' => $e->getTrace()
            ], 500);
        }
    }

    public function update(Request $request, Product $product)
    {

        //dd($request->all());
        $this->validate($request, [
            'name' => 'required|max:255',
            'sku' => 'required|max:255',
            'weight' => 'required|numeric|min:1',
            'prices.*' => 'required|numeric|min:0',
            'min_quantity.*' => 'required|numeric|min:0',
            'label' => 'max:255'
        ], [
            'prices.*.required' => 'The price field is required',
            'min_quantity.*.required' => 'The min quantity field is required'
        ]);
        try {
            DB::beginTransaction();
            // update product
            $product->update([
                "name" => $request->name,
                "sku" => $request->sku,
                "memo" => $request->label,
                "weight" => $request->weight,
                "stock_amount" =>$request->stock_amount
            ]);

             //product image
          $this->uploadImage($request, $product);

            // loop prices
            foreach ($request->prices as $index => $price) {
                // update price
                Price::where('id', $request->prices_id[$index])->update([
                    'amount' => $price,
                    'min_quantity' => $request->min_quantity[$index]
                ]);
            }
            // log activity
            activity()
                ->performedOn($product)
                ->causedBy($request->user())
                ->withProperties($request->all())
                ->log("Product #$product->id has been updated by :causer.username");
            DB::commit();
            // return success message
            return redirect()->back()->with('alert-success', 'Product was Successfully Updated');
        } catch (\Exception $e) {
            DB::rollback();
            // return danger message
            return redirect()->back()
                ->with('alert-danger', $e->getMessage())
                ->withInputs();
        }
    }

    public function create(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
            'sku' => 'required|max:255',
            'weight' => 'required|numeric|min:1',
            'label' => 'max:255',
            'stock_amount' => 'required'
        ]);
        try {
            DB::beginTransaction();
            // create product
            $product = Product::create([
                "name" => $request->name,
                "sku" => $request->sku,
                "memo" => $request->label,
                "weight" => $request->weight,
                "stock_amount" =>$request->stock_amount,
                'user_id' => auth()->id()
            ]);
            
            //product image
          $this->uploadImage($request, $product);
        
            // log activity
            activity()
                ->performedOn($product)
                ->causedBy($request->user())
                ->withProperties($request->all())
                ->log("Product #$product->id has been created by :causer.username");
            DB::commit();
            // return success message
            return redirect(route('admin.product.edit', $product->id))->with('alert-success', 'Product was Successfully Created');
        } catch (\Exception $e) {
            DB::rollback();
            // return danger message
            return redirect()->back()
                ->with('alert-danger', $e->getMessage())
                ->withInput($request->all());
        }
    }

    public function delete(Request $request, Product $product)
    {
        try {
            // soft delete the product
            $product->update(['status' => -1]);
            // log activity
            activity()
                ->performedOn($product)
                ->causedBy($request->user())
                ->withProperties($request->all())
                ->log("Product #$product->id has been deleted by :causer.username");
            // return success message
            return response()->json([
                "code" => 200,
                "message" => 'Product was Successfully Deleted!'
            ]);
        } catch (\Exception $e) {
            // return error message
            return response()->json([
                "code" => $e->getCode(),
                "message" => $e->getMessage(),
                'trace' => $e->getTrace()
            ], 500);
        }
    }

public function uploadImage($request, $product){
     if ($request->hasFile('image')) {
     $ext = $request->file('image')->extension();
     $path = $request->file('image')->storeAs("public/products/images/$product->id/", "image.$ext");
     $product->update(['image' => str_replace('public/', '', $path)]);

            }
    return $product;
    }
public function UpdateImage(Request $request, Product $product){
     if ($request->hasFile('image')) {
     $ext = $request->file('image')->extension();
     $path = $request->file('image')->storeAs("public/products/images/$product->id/", "image.$ext");
     $product->update(['image' => str_replace('public/', '', $path)]);
     $product->image = str_replace('public/', '', $path);
     $product->save();

            }
   return redirect(route('admin.product.edit', $product->id))->with('alert-success', 'Product Image was Successfully Updated');
}
}
