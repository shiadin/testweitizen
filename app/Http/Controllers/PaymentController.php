<?php

namespace App\Http\Controllers;

use App\Stock;
use App\StockToken;
use App\Payment;
use App\User;
use App\OrderItem;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Resources\PaymentResource;
use App\GroupCommission;
use App\Notifications\StockReload;
use App\Gamify\Points\RecruiteDownlineReward;
class PaymentController extends Controller
{
    public function reject(Request $request, Payment $payment)
    {
        $this->validate($request, [
            'memo' => 'required|max:255'
        ]);
        try {
            // not allow to reject if already rejected
            if ($payment->status != 0) {
                throw new \Exception("Not allow to reject payment processed", 500);
            }
            DB::beginTransaction();
            // get payment's order
            $order = $payment->order;
            // get order type
            $type = preg_replace('/\:\d/', '', $order->type);
            // determine order type to get params
            switch ($type) {
                case 'deposit':
                    $payment = $this->rejectDeposit($request, $payment);
                    break;
                case 'stocks':
                    $payment = $this->rejectStocks($request, $payment);
                    break;
                case 'credits':
                    $payment = $this->rejectCredits($request, $payment);
                    break;
                default:
                    throw new \Exception("Something is wrong when processing $type order", 500);
                    break;
            }
            // call order rejected event
            event('order.rejected', $order);
            // log activity
            activity()
                ->performedOn($order)
                ->causedBy($request->user())
                ->withProperties($request->all())
                ->log("A $type #$order->id payment has been rejected by :causer.username");
            DB::commit();
            // return success message
            return response()->json([
                "code" => 200,
                "message" => 'Payment was Successfully Rejected!',
                'payment' => new PaymentResource($payment)
            ]);
        } catch (\Exception $e) {
            DB::rollback();
            // return error message
            return response()->json([
                "code" => $e->getCode(),
                "message" => $e->getMessage(),
                "trace" => $e->getTrace()
            ], 500);
        }
    }

    public function approve(Request $request, Payment $payment)
    {
        $this->validate($request, [
            'memo' => 'max:255'
        ]);
        try {
            // not allow to approve if already approved
            if ($payment->status != 0) {
                throw new \Exception("Not allow to approve payment processed", 500);
            }
            DB::beginTransaction();
            // get payment's order
            $order = $payment->order;
            // get order type
            $type = preg_replace('/\:\d/', '', $order->type);
            // determine order type to get params
            switch ($type) {
                case 'deposit':
                    $payment = $this->approveDeposit($request, $payment);
                    break;
                case 'stocks':
                     if($payment->user->role_id==999){
                         $payment = $this->approveStocksAdmin($request, $payment);
                     }else{
                         $payment = $this->approveStocks($request, $payment);
                     }
                   
                    break;
                case 'credits':
                    $payment = $this->approveCredits($request, $payment);
                    break;
                default:
                    throw new \Exception("Something is wrong when processing $type order", 500);
                    break;
            }
            // call order approved event
            event('order.approved', $order);
            // log activity
            activity()
                ->performedOn($order)
                ->causedBy($request->user())
                ->withProperties($request->all())
                ->log("A $type #$order->id payment has been approved by :causer.username");
            DB::commit();
            
           
            // return success message
            return response()->json([
                "code" => 200,
                "message" => 'Payment was Successfully Approved!',
                'payment' => new PaymentResource($payment)
            ]);
        } catch (\Exception $e) {
            DB::rollback();
   if($payment->user->upline){
    $payment->user->upline->notify(new StockReload($payment));
   }
           
            // return error message
            return response()->json([
                "code" => $e->getCode(),
                "message" => $e->getMessage(),
                "trace" => $e->getTrace()
            ], 500);
        }
    }

    public function rejectDeposit(Request $request, Payment $payment)
    {
        // reject payment
        $payment->update([
            'status' => -1,
            'memo' => $request->memo
        ]);
        return $payment;
    }

    public function rejectStocks(Request $request, Payment $payment)
    {
        // reject payment
        $payment->update([
            'status' => -1,
            'memo' => $request->memo
        ]);
        // reject order too
        $payment->order->update([
            'status' => -1,
            'memo' => $request->memo
        ]);
        return $payment;
    }

    public function rejectCredits(Request $request, Payment $payment)
    {
        // reject payment
        $payment->update([
            'status' => -1,
            'memo' => $request->memo
        ]);
        // reject order too
        $payment->order->update([
            'status' => -1,
            'memo' => $request->memo
        ]);
        return $payment;
    }

    public function approveDeposit(Request $request, Payment $payment)
    {
        // get related model
        $payment->load(['order.user.upline']);
        // set user and order
        $order = $payment->order;
        $user = $order->user;
        // approve payment
        $payment->update([
            'status' => 1,
            'memo' => $request->memo
        ]);
        // approve payment's order
        $order->update([
            'status' => 1
        ]);
        // get memeber level deposit represent for
        $roleId = str_replace('deposit:', '', $order->type);
        // update user to the role
        $user->update(['role_id' => $roleId]);
        // assign user to suitable upline (ancestor)
        $user->assignToSuitableUpline($roleId);
        return $payment;
    }


public function approveStocksAdmin(Request $request, Payment $payment){
             // approve payment
        $payment->update([
            'status' => 1,
            'memo' => $request->memo
        ]);
        // approve payment's order
        $payment->order->update([
            'status' => 1
        ]);
        // get order with related model
        $order = $payment->order->load(['user', 'items']);
        // add stocks to user based on order items
        $order->items->each(function ($item) use ($order, $request) {
            // create or update user stocks based on product id
            $stock = StockToken::updateOrCreate([
                'user_id' => $order->user_id
            ], [
                'status' => 1
            ]);
            // get stock amount
            $ori = $stock->quantity;
            // update quantity
            $stock->increment('quantity', $item->quantity);
          
            // get new stock amount
            $new = $stock->quantity;
            // log activity
            activity()
                ->performedOn($stock)
                ->causedBy($request->user())
                ->withProperties($request->all())
                ->log("$item->quantity unit/token  has been added to account from Request#$order->id by :causer.username ($ori => $new)");
        });
      $thisUser= $order->user;    
    if($order->payment_type==='eWallet'){
       if($thisUser->balance < $order->amount){
            throw new \Exception("User not have enough e-wallet balance to reload using eWallet", 500);
       }else{
           $or = $payment->user->balance;
        $thisUser->withdraw($payment->order->amount, 'withdraw', ['description' => 'Purchase of Unit/Token #'.$payment->order->id]);
       $aft = $thisUser->balance;
 
        activity()
                ->performedOn($thisUser)
                ->causedBy($request->user())
                ->withProperties($request->all())
                ->log("RM $payment->amount of token reload have been deducted from your account  by :causer.username ($or => $aft)");
       }
      }
      if($order->payment_type==='both'){
        $thisUser= $order->user;
        $user_balance = $thisUser->balance;
        $deduct =  $order->amount - $order->both_amount;

        if($deduct > $user_balance){
            throw new \Exception("User not have enouph ewallet balance to reload using both eWallet and slip", 500);
        }else{
         //we set user eallet to zere
         $or = $payment->user->balance;
         $thisUser->withdraw($deduct, 'withdraw', ['slip'=>$order->both_amount, 'wallet' => $order->amount, 'total'=>$order->amount, 'type' => $order->payment_type, 'description' => 'Purchase of Unit/Token #'.$payment->order->id]);
         $aft = $thisUser->balance;
 
         activity()
                 ->performedOn($thisUser)
                 ->causedBy($request->user())
                 ->withProperties($request->all())
                 ->log("RM $deduct of token reload (using both wallet & slip) have been deducted from your account  by :causer.username ($or => $aft)");
         
        }
      }
      


        
       return $payment;
}

public function approveStocks(Request $request, Payment $payment)
    {
   

         // approve payment
        $payment->update([
            'status' => 1,
            'memo' => $request->memo
        ]);
        
        // approve payment's order
        $payment->order->update([
            'status' => 1
        ]);
        // get order with related model
        $order = $payment->order->load(['user', 'items']);
        // add stocks to user based on order items
        $order->items->each(function ($item) use ($order, $request) {
            // create or update user stocks based on product id
            $stock = StockToken::updateOrCreate([
                'user_id' => $order->user_id
            ], [
                'status' => 1
            ]);
            // get stock amount
            $ori = $stock->quantity;
            // update quantity
            $stock->increment('quantity', $item->quantity);
            
            // get new stock amount
            $new = $stock->quantity;
            // log activity
            activity()
                ->performedOn($stock)
                ->causedBy($request->user())
                ->withProperties($request->all())
                ->log("$item->quantity unit of Unit/Item has been added to account from Request#$order->id by :causer.username ($ori => $new)");

           $deposit = $item->unit_price*$item->quantity;
             $user_in = $order->user;
          
              $user_in = $order->user;
            // if upline token not yet there initialize it upline's stock
            $uplineStock = StockToken::updateOrCreate([
                'user_id' => $order->user->upline->id,
            ], [
                'status' => 1
            ]);
  
            // if quantity larger than request
            if ($uplineStock->quantity < $item->quantity) {
                throw new \Exception("Quantity must be less than upline stock amount, we have notify upline to restock this product, please try again later", 500);
            }
            
              
            $uplineStock->decrement('quantity', $item->quantity);
             
             $upline_ori_wallet = $user_in->upline->balance;
             //dd($upline_ori_wallet);
             $debitEwallet = $user_in->upline->depositA($deposit, 'deposit', ['type'=>$order->payment_type, 'order_no'=>$order->id, 'Stock transfer to downline#'.$user_in->username => $user_in->id, 'description' => $item->quantity.' Unit/token have transfered to you downline ('.$user_in->username.' )', 'downline'=>$user_in->name]);
             $upline_wallet_new = $user_in->upline->balance;

             $GroupCommission = new GroupCommission();
             $GroupCommission->user_id = $user_in->upline->id;
             $GroupCommission->type = 'GroupCommission';
             $GroupCommission->amount = $deposit;
             $GroupCommission->quantity = $item->quantity;
             $GroupCommission->unit_price = $item->unit_price;
             $GroupCommission->meta = 'GroupCommission';
             $GroupCommission->downline_id = $user_in->id;
             $GroupCommission->downline = $user_in->name;
             $GroupCommission->downline_level = $user_in->role_id;
             $GroupCommission->current_level = $user_in->upline->role_id;
             $GroupCommission->save();

              activity()
                ->performedOn($uplineStock)
                ->causedBy($request->user())
                ->withProperties($debitEwallet)
                ->log("RM $deposit of downline reload has been added to account from Downlines#($user_in->username) by :causer.username ($upline_ori_wallet => $upline_wallet_new)");
          
             //upgrade user level
            
          

        });
        
          
          $thisUser = $order->user;
          if($thisUser->role_id==33){
          $upline = $thisUser->upline;  
          $ori_wal = $upline->balance;
          $wallet = $upline->depositA(50, 'deposit', ['order'=>$order->id, 'downline recruit' => $thisUser, 'description' => 'Deposit of RM 50 from downline('.$thisUser->username.') recruit commision', 'downline'=>$thisUser->name]);
          $wal_new = $upline->balance;

             $GroupCommission = new GroupCommission();
             $GroupCommission->user_id = $thisUser->upline->id;
             $GroupCommission->type = 'RecruitCommission';
             $GroupCommission->amount = 50;
             // $GroupCommission->quantity = $item->quantity;
             $GroupCommission->unit_price = $order->amount;
             $GroupCommission->meta = 'Deposit of RM 50  from downline ('.$thisUser->username.') recruit commision';
              $GroupCommission->downline = $thisUser->username;
             $GroupCommission->downline_level = $thisUser->role_id;
             $GroupCommission->downline_id = $thisUser->id;
             $GroupCommission->current_level = $thisUser->upline->role_id;
             $GroupCommission->save();
         

           activity()
                ->performedOn($upline)
                ->causedBy($request->user())
                ->withProperties($request->all())
                ->log("RM 50  of downline recruit commission has been added to account from Downlines#$thisUser->username by :causer.username ($ori_wal => $wal_new)");
      }
 

      if($order->payment_type==='eWallet'){
       if($thisUser->balance < $order->amount){
            throw new \Exception("User not have enouph ewallet balance to reload using eWallet", 500);
       }else{
           $or = $payment->user->balance;
        $thisUser->withdraw($payment->order->amount, 'withdraw', ['description' => 'Purchase of Unit/Token #'.$payment->order->id]);
       $aft = $thisUser->balance;
      
                activity()
                ->performedOn($order)
                ->causedBy($request->user())
                ->withProperties($request->all())
                ->log("RM $payment->amount of token reload have been deducted from your account  by :causer.username ($or => $aft)");
       }
      }
      if($order->payment_type==='both'){
        $thisUser= $order->user;
        $user_balance = $thisUser->balance;
        $deduct =  $order->amount - $order->both_amount;

        if($deduct > $user_balance){
            throw new \Exception("User not have enouph ewallet balance to reload using both eWallet and slip", 500);
        }else{
         //we set user eallet to zere
         $or = $payment->user->balance;
         $thisUser->withdraw($deduct, 'withdraw', ['slip'=>$order->both_amount, 'wallet' => $order->amount, 'total'=>$order->amount, 'type' => $order->payment_type, 'description' => 'Purchase of Unit/Token ewallet RM '.$deduct.' & slip RM'.$order->both_amount.' (using both wallet and payment proof)#'.$payment->order->id]);
         $aft = $thisUser->balance;
 
                 activity()
                 ->performedOn($thisUser)
                 ->causedBy($request->user())
                 ->withProperties($request->all())
                 ->log("RM $deduct of token reload (using both wallet & slip) have been deducted from  account ($thisUser->username)  by :causer.username ($or => $aft)");
         
        }
      }
      
       $quantity_reload  = OrderItem::where('order_id', $order->id)->first()->quantity;
          //dd($quantity_reload);

    $this->levelLogic($quantity_reload, $order->user);
        return $payment;
    }

public function levelLogic($quantity, $downline){
    
  if($downline->role_id==7){
     //do nothing
  }else if($downline->role_id==6){
  if($quantity==500){
           $this->upgradeLevel($downline, 7, $quantity);
           }
  }else if($downline->role_id==5){
    if($quantity==500){
           $this->upgradeLevel($downline, 7, $quantity);
           }
    else if($quantity==300){
           $this->upgradeLevel($downline, 6, $quantity);
      }
  }
   else if ($downline->role_id==4){
  if($quantity==500){
           $this->upgradeLevel($downline, 7, $quantity);
           }
  else if($quantity==300){
           $this->upgradeLevel($downline, 6, $quantity);
      }
  else if($quantity==100){
           $this->upgradeLevel($downline, 5, $quantity);
  } 
  }else if ($downline->role_id==3){

  if($quantity==500){
           $this->upgradeLevel($downline, 7, $quantity);
           }
  else if($quantity==300){
           $this->upgradeLevel($downline, 6, $quantity);
      }
  else if($quantity==100){
           $this->upgradeLevel($downline, 5, $quantity);
  }
  else if($quantity==50) {
           $this->upgradeLevel($downline, 4, $quantity);
  }

     }else if ($downline->role_id==2){

 if($quantity==500){
           $this->upgradeLevel($downline, 7, $quantity);
           }
  else if($quantity==300){
           $this->upgradeLevel($downline, 6, $quantity);
      }
  else if($quantity==100){
           $this->upgradeLevel($downline, 5, $quantity);
  }
  else if($quantity==50) {
           $this->upgradeLevel($downline, 4, $quantity);
  }
  else if($quantity==30) {
           $this->upgradeLevel($downline, 3, $quantity);
  }

    }else if($downline->role_id==1){
        
  if($quantity==500){
           $this->upgradeLevel($downline, 7, $quantity);
           }
  else if($quantity==300){
           $this->upgradeLevel($downline, 6, $quantity);
      }
  else if($quantity==100){
           $this->upgradeLevel($downline, 5, $quantity);
  }
  else if($quantity==50) {
           $this->upgradeLevel($downline, 4, $quantity);
  }
  else if($quantity==30) {
           $this->upgradeLevel($downline, 3, $quantity);
  }
   else if($quantity==10) {
           $this->upgradeLevel($downline, 2, $quantity);
  }

      }else if($downline->role_id==33){
        
  if($quantity==500){
           $this->upgradeLevel($downline, 7, $quantity);
           }
  else if($quantity==300){
           $this->upgradeLevel($downline, 6, $quantity);
      }
  else if($quantity==100){
           $this->upgradeLevel($downline, 5, $quantity);
  }
  else if($quantity==50) {
           $this->upgradeLevel($downline, 4, $quantity);
  }
  else if($quantity==30) {
           $this->upgradeLevel($downline, 3, $quantity);
  }
   else if($quantity==10) {
           $this->upgradeLevel($downline, 2, $quantity);
  }
  else if($quantity==4 || $quantity==1) {
           $this->upgradeLevel($downline, 1, $quantity);
  }

}


}

public function upgradeLevel($downline, $roleID, $quantity){
          $downline->role_id = $roleID;
          $downline->save();
         // $downline->assignToSuitableUpline($roleID);
        
}

    public function approveCredits(Request $request, Payment $payment)
    {
        // approve payment
        $payment->update([
            'status' => 1,
            'memo' => $request->memo
        ]);
        // get order
        $order = $payment->order;
        // approve payment's order
        $order->update([
            'status' => 1
        ]);
        // get payment user
        $user = $payment->user;
        // get ori amount
        $ori = $user->credits;
        // get  order amount
        $qty = $payment->order->amount;
        // increse credits in payment's user
        $user->increment('credits', $qty);
        // get new amount
        $new = $user->credits;
        // get currency
        $currency = config('app.currency');
        // log activity
        activity()
            ->performedOn($user)
            ->causedBy($request->user())
            ->withProperties($request->all())
            ->log("$qty unit of credits has been added to account from Request#$order->id by :causer.username ($ori => $new)");
        return $payment;
    }
}