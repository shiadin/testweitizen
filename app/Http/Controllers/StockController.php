<?php

namespace App\Http\Controllers;

use App\User;
use App\Stock;
use App\Order;
use App\StockToken;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Resources\UserResource;
use App\Http\Resources\StockResource;
use App\Http\Resources\ProductResource;



class StockController extends Controller
{
    public function index(Request $request)
    {
        // check if request from admin
        $isAdmin = $request->is('admin/*');

        // initialize stocks
        $stocks = Stock::with(['product', 'user.stocks.product',
            'activities' => function ($query) {
                return $query->latest()->take(20);
            }
        ]);

        // limit result to owner if not admin request
        if (!$isAdmin) {
            $stocks->where('user_id', auth()->id());
        }

        if ($request->has('status')) {
            $stocks->whereIn('status', is_array($request->status) ? $request->status : explode(',', $request->status));
        }

        if ($request->has('product_ids')) {
            $stocks->whereIn('product_id', $request->product_ids);
        }

        if ($request->has('keyword')) {
            $stocks->where(function ($query) use ($request) {
                $query->where('id', 'like', "%$request->keyword%")
                    ->orWhereHas('user', function ($query) use ($request) {
                        $query->where('name', 'like', "%$request->keyword%")
                            ->orWhere('username', 'like', "%$request->keyword%");
                    });
            });
        }

        if ($request->has('username')) {
            $stocks->where(function ($query) use ($request) {
                $query->WhereHas('user', function ($query) use ($request) {
                    $query->where('name', 'like', "%$request->username%")
                        ->orWhere('username', 'like', "%$request->username%");
                });
            });
        }

        if ($request->has('order_by')) {
            switch ($request->order_by) {
                case 'user.id':
                    $request->order_by = 'user_id';
                    break;
                case 'user.username':
                    $stocks->join('users', 'users.id', 'stocks.user_id');
                    $request->order_by = 'username';
                    break;
                case 'product.name':
                    $stocks->join('products', 'products.id', 'stocks.product_id');
                    $request->order_by = 'products.name';
                    break;
            }
        }
        $stocks->orderBy($request->order_by ?: 'id', $request->order_direction ?: 'asc');
        // paginate results
        if ($request->per_page == 'all') {
            $stocks = $stocks->get();
        } else {
            $stocks = $stocks->paginate($request->has('per_page') ? $request->per_page : 20);
        }
        // form collection
        $collection = StockResource::collection($stocks);
        // if ask for append products list
        if ($request->has('with_products') && $request->with_products) {
            $collection->additional([
                'products' => ProductResource::collection(Product::where('status', '!=', -1)->get())
            ]);
        }
        // return collection
        return $collection;
    }

    public function transfer(Request $request)
    {

        $this->validate($request, [
            'downline' => 'required|integer|min:1',
           // 'product_id' => 'required|integer|min:1',
            'quantity' => 'required|integer|min:1'
        ]);
        try {
            DB::beginTransaction();
            // get upline
            $upline = auth()->user();
            // get upline's stock
            $uplineStock = $upline->stockToken;
            // if quantity larger than request
            if ($uplineStock->quantity < $request->quantity) {
                throw new \Exception("Quantity must be less than stock amount", 500);
            }
            // get downline and check downline is exists or not
            $downline = $upline->downlines()
                        ->with(['stocks', 'deposit', 'downlines'])
                        ->where('id', $request->downline)
                        ->firstOrFail();
            // if downline have and need deposit
            $downlineHaveDeposit = $downline->depositRequired() > 0 && $downline->deposit && $downline->deposit->status == 1;
             if ($downline->status == -1 || $downline->status == 0) {
                throw new \Exception("You are not allow to transfer stocks to unapproved/blocked user: $downline->username", 500);
            }
            // if downline is not approved
            if ($downline->status != 1 && ($downline->depositRequired() > 0 && $downline->deposit)) {
                throw new \Exception("You are not allow to transfer stocks to unapproved user: $downline->username", 500);
            }
            // get product
         //   $product = Product::with('prices')->whereId($request->product_id)->firstOrFail();
            // get price
            // $roleID = 0;
            // if($request->quantity==500){
            //     $roleID = 7;
            // }else if($request->quantity==300){
            //      $roleID = 6;
            // }else if($request->quantity==100){
            //       $roleID = 5;
            // }else if($request->quantity==50){
            //        $roleID = 4;
            // }else if($request->quantity==30){
            //         $roleID = 3;
            // }else if($request->quantity==10){
            //         $roleID = 2;
            
            // }else if($request->quantity==4){
            //         $roleID = 1;
            // }
      
            // $price = $product->prices->where('role_id', $roleID)->first();
            //   \Log::info($price);

            // if not price found
            // if (!$price) {
            //     throw new \Exception("No price rule for user: $downline->username", 500);
            // }
            // check if quantity is less than min stock purchase of product
            // if ($request->quantity < $price->min_quantity) {
            //     throw new \Exception("At least $price->min_quantity units of stocks need to transfer to level $downline->role_id user", 500);
            // }
            // check downline has stock or not
            $downlineStock = $downline->stockToken;
            // create stock if not exists
            if (!$downlineStock) {
                $downlineStock = StockToken::create([
                    "user_id" => $downline->id,
                    // "product_id" => $request->product_id
                ]);
            }
            // increase downline stock
            $downlineStock->increment('quantity', $request->quantity);

           //give level to downlone as per quntity 
          //  $this->levelLogic($request->quantity, $downline);

            // descrease upline stock
            $uplineStock->decrement('quantity', $request->quantity);


            // log activity
            activity()
                ->performedOn($downlineStock)
                ->causedBy($request->user())
                ->withProperties($request->all())
                ->log("$request->quantity unit/token  has been transferred to $downline->username by :causer.username");
            // log activity
            activity()
                ->performedOn($uplineStock)
                ->causedBy($request->user())
                ->withProperties($request->all())
                ->log("$request->quantity unit/token  has been transferred to $downline->username by :causer.username");
            DB::commit();
            // reload user stocks
          //  $upline->load(['stocks.product']);
            // return success message
            return response()->json([
                "code" => 200,
                "message" => 'Stock was Successfully Transferred!',
                'user' => new UserResource($upline)
            ]);
        } catch (\Exception $e) {
            DB::rollback();
            // return error message
            return response()->json([
                "code" => $e->getCode(),
                "message" => $e->getMessage(),
                "trace" => $e->getTrace()
            ], 500);
        }
    }


    public function update(Request $request, User $user)
    {
        // validate
        $this->validate($request, [
            'product_id' => 'required|integer|min:1',
            'quantity' => 'required|integer'
        ]);
        try {
            DB::beginTransaction();
            // check product is exists
            $product = Product::findOrFail($request->product_id);
            // check user has stock or not
            $stock = $user->stocks()->where('product_id', $request->product_id)->first();
            // create stock if not exists
            if (!$stock) {
                $stock = Stock::create([
                    "user_id" => $user->id,
                    "product_id" => $request->product_id
                ]);
            }
            // get ori stock amount
            $ori = $stock->quantity;
            // update user stock
            $stock->update(['quantity' => $request->quantity]);
            // log activity
            activity()
                ->performedOn($stock)
                ->causedBy($request->user())
                ->withProperties($request->all())
                ->log("$request->quantity unit of $product->name($product->sku) has been updated to $user->username by :causer.username ($ori => $stock->quantity)");
            DB::commit();
            // reload user stocks and its activities
            $user->load(['stocks.activities']);
            // return success message
            return response()->json([
                "code" => 200,
                "message" => 'Stock was Successfully Updated!',
                'user' => new UserResource($user)
            ]);
        } catch (\Exception $e) {
            DB::rollback();
            // return error message
            return response()->json([
                "code" => $e->getCode(),
                "message" => $e->getMessage(),
                "trace" => $e->getTrace()
            ], 500);
        }
    }

public function levelLogic($quantity, $downline){
    
  if($downline->role_id==7){
     //do nothing
  }else if($downline->role_id==6){
  if($quantity==500){
           $this->upgradeLevel($downline, 7, $quantity);
           }
  }else if($downline->role_id==5){
    if($quantity==500){
           $this->upgradeLevel($downline, 7, $quantity);
           }
    else if($quantity==300){
           $this->upgradeLevel($downline, 6, $quantity);
      }
  }
   else if ($downline->role_id==4){
  if($quantity==500){
           $this->upgradeLevel($downline, 7, $quantity);
           }
  else if($quantity==300){
           $this->upgradeLevel($downline, 6, $quantity);
      }
  else if($quantity==100){
           $this->upgradeLevel($downline, 5, $quantity);
  } 
  }else if ($downline->role_id==3){

  if($quantity==500){
           $this->upgradeLevel($downline, 7, $quantity);
           }
  else if($quantity==300){
           $this->upgradeLevel($downline, 6, $quantity);
      }
  else if($quantity==100){
           $this->upgradeLevel($downline, 5, $quantity);
  }
  else if($quantity==50) {
           $this->upgradeLevel($downline, 4, $quantity);
  }

     }else if ($downline->role_id==2){

 if($quantity==500){
           $this->upgradeLevel($downline, 7, $quantity);
           }
  else if($quantity==300){
           $this->upgradeLevel($downline, 6, $quantity);
      }
  else if($quantity==100){
           $this->upgradeLevel($downline, 5, $quantity);
  }
  else if($quantity==50) {
           $this->upgradeLevel($downline, 4, $quantity);
  }
  else if($quantity==30) {
           $this->upgradeLevel($downline, 3, $quantity);
  }

    }else if($downline->role_id==1){
        
  if($quantity==500){
           $this->upgradeLevel($downline, 7, $quantity);
           }
  else if($quantity==300){
           $this->upgradeLevel($downline, 6, $quantity);
      }
  else if($quantity==100){
           $this->upgradeLevel($downline, 5, $quantity);
  }
  else if($quantity==50) {
           $this->upgradeLevel($downline, 4, $quantity);
  }
  else if($quantity==30) {
           $this->upgradeLevel($downline, 3, $quantity);
  }
   else if($quantity==10) {
           $this->upgradeLevel($downline, 2, $quantity);
  }

      }else if($downline->role_id==33){
        
  if($quantity==500){
           $this->upgradeLevel($downline, 7, $quantity);
           }
  else if($quantity==300){
           $this->upgradeLevel($downline, 6, $quantity);
      }
  else if($quantity==100){
           $this->upgradeLevel($downline, 5, $quantity);
  }
  else if($quantity==50) {
           $this->upgradeLevel($downline, 4, $quantity);
  }
  else if($quantity==30) {
           $this->upgradeLevel($downline, 3, $quantity);
  }
   else if($quantity==10) {
           $this->upgradeLevel($downline, 2, $quantity);
  }
  else if($quantity==4) {
           $this->upgradeLevel($downline, 1, $quantity);
  }

}


}

public function upgradeLevel($downline, $roleID, $quantity){
          $downline->role_id = $roleID;
          $downline->save();
          $downline->assignToSuitableUpline($roleID);
        
}






 public function stocks(Request $request){
          $user = auth()->user();
          // $order = Order::with('items')->where('user_id', $user->id)->where('status', 0)->get();
          // $pending = $order->items;
        //  return $pending;
          $stocks = Stock::with('product')->where('user_id', $user->id)->get();
         $stockQuantity = 0;
         $pendingStock = 0;
         $token = StockToken::updateOrCreate([
                'user_id' => $user->id,
            ], [
                'status' => 1
            ]);
          if(!empty($stocks)){
             $stockQuantity =  $stocks->sum('quantity');
             $a=array();
             foreach ($stocks as $key => $client) {
              array_push($a, $client['product_id']);   
                }
              }

    
     $user = auth()->user()
                ->load([
                    'downlines.role', 'role', 'deposit.payment', 'upline', 'stocks.product',
                    'inviteCodes.role', 'deposits.payment', 'shippings'
                ]);
        // get user's activities
        $activities = $user->activities()->latest()
                            ->paginate(10)
                            ->withPath(route('user.activities'));
        // add activities to user relationship
        $user->setRelation('activities', $activities);
     
     //$prod = Product::all();
     $products = Product::with(['prices'])->whereNotIn('id', $a)->get();
     foreach ($products as $key => $value) {
         Stock::updateOrCreate(['user_id'=>auth()->user()->id, 'product_id'=>$value->id],
         ['status', 1]);
     }
   //  return $p;
        return view('user.stocks', [
            'sidebar' => 'My Stocks',
            'title' => 'My Stocks',
            'stocks' => $stocks,
           'routes' => [
                'getProducts' => route('products.index'),
                'getRoles' => route('roles.index', ['per_page' => 'all'])
            ],
            'user' => new UserResource($user),
           
            'stockQuantity' => $stockQuantity,
            'token' => $token,
            
            
        ]);
   
   }
   public function manageStock(Request $request){
    $user = auth()->user();
    if($request->type=='add'){
      try {
         DB::beginTransaction();
        
          $stock = Stock::updateOrCreate([
                'user_id' => $user->id,
                'product_id' =>$request->product_id
            ], [
                'status' => 1
            ]);
          $token = StockToken::updateOrCreate([
                'user_id' => $user->id,
            ], [
                'status' => 1
            ]);
          if($token->quantity<$request->quantity){

           return redirect('stocks')->withErrorMessage('Not have enough token to add stocks!');
          }
            // get stock amount
            $ori = $stock->quantity;
            // update quantity
            $stock->increment('quantity', $request->quantity);
            // get new stock amount
            $new = $stock->quantity;
           
            $token->decrement('quantity', $request->quantity);
            // log activity
            activity()
                ->performedOn($stock)
                ->causedBy($request->user())
                ->withProperties($request->all())
                ->log("$request->quantity unit of stocks has been added to my stock from token  by $user->name ($ori => $new)");
         DB::commit();
          return redirect('stocks')->withSuccessMessage('Stocks Addedd successfully!'); //change to api
         DB::commit();
         } catch (\Exception $e) {
           return redirect('stocks')->withErrorMessage($e->getMessage());
            // DB::rollback();
            //  return response()->json([
            //     "code" => $e->getCode(),
            //     "message" => $e->getMessage(),
            //     "trace" => $e->getTrace()
            // ], 500);
        }
    }else if($request->type=='remove'){
        try {
         DB::beginTransaction();
          // return $request->product_id;
          $stock = Stock::updateOrCreate([
                'user_id' => $user->id,
                'product_id' =>$request->product_id
            ], [
                'status' => 1
            ]);
          
          if($stock->quantity < $request->quantity){
           return redirect('stocks')->withErrorMessage('Stocks Qauntity must be less than current stock!'); 
          }
            // get stock amount
            $ori = $stock->quantity;
            // update quantity
            $stock->decrement('quantity', $request->quantity);
            // get new stock amount
            $new = $stock->quantity;
           $token = StockToken::updateOrCreate([
                'user_id' => $user->id,
            ], [
                'status' => 1
            ]);
            $token->increment('quantity', $request->quantity);
            // log activity
            activity()
                ->performedOn($stock)
                ->causedBy($request->user())
                ->withProperties($request->all())
                ->log("$request->quantity unit of stocks has been removed from my stock to token  by $user->name ($ori => $new)");
         DB::commit();
         return redirect('stocks')->withSuccessMessage('Stocks Removed successfully!'); //change to api
         } catch (\Exception $e) {
            DB::rollback();
            return redirect('stocks')->withErrorMessage($e->getMessage());
            //  return response()->json([
            //     "code" => $e->getCode(),
            //     "message" => $e->getMessage(),
            //     "trace" => $e->getTrace()
            // ], 500);
        }

    }

   }

}
