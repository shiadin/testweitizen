<?php

namespace App\Http\Controllers;

use App\User;
use App\Order;
use App\withdrawRequest;
use Illuminate\Http\Request;
use App\Http\Resources\UserResource;
use App\Http\Resources\OrderResource;

class WorkspaceController extends Controller
{
    public function depositsAccounts(Request $request)
    {
        try {
            // get user deposits need to approve
            $deposits = Order::with(['user.role', 'payment'])
                            ->where('type', 'like', 'deposit:%')
                            ->whereStatus(0)
                            ->whereHas('payment', function ($payment) {
                                $payment->where('status', 0);
                            })
                            ->get();
            // get user account havent approved yet
            $users = User::with('role')->where('role_id', '!=', 700)->whereStatus(0)->get();
            // return success message
            return response()->json([
                "code" => 200,
                'users' => UserResource::collection($users),
                'deposits' => OrderResource::collection($deposits)
            ]);
        } catch (\Exception $e) {
            // return error message
            return response()->json([
                "code" => $e->getCode(),
                "message" => $e->getMessage()
            ], 500);
        }
    }

    public function stocks(Request $request)
    {
        try {
            // get user stocks need to approve
            $stocks = Order::with(['user.role', 'payment', 'items.product'])
                            ->whereType('stocks')
                            ->whereStatus(0)
                            ->whereHas('payment', function ($payment) {
                                $payment->where('status', 0);
                            })
                            ->get();
            // return success message
            return response()->json([
                "code" => 200,
                'stocks' => OrderResource::collection($stocks)
            ]);
        } catch (\Exception $e) {
            // return error message
            return response()->json([
                "code" => $e->getCode(),
                "message" => $e->getMessage(),
                "trace" => $e->getTrace()
            ], 500);
        }
    }


    public function wallets(Request $request)
    {
        try {
            // get user credits need to approve
            $wallets = withdrawRequest::with(['user'])
                            ->whereStatus(0)
                            ->get();
            // return success message
            return response()->json([
                "code" => 200,
                'wallets' => $wallets
            ]);
        } catch (\Exception $e) {
            // return error message
            return response()->json([
                "code" => $e->getCode(),
                "message" => $e->getMessage(),
                "trace" => $e->getTrace()
            ], 500);
        }
    }
    public function credits(Request $request)
    {
        try {
            // get user credits need to approve
            $credits = Order::with(['user.role', 'payment'])
                            ->whereType('credits')
                            ->whereStatus(0)
                            ->whereHas('payment', function ($payment) {
                                $payment->where('status', 0);
                            })
                            ->get();
            // return success message
            return response()->json([
                "code" => 200,
                'credits' => OrderResource::collection($credits)
            ]);
        } catch (\Exception $e) {
            // return error message
            return response()->json([
                "code" => $e->getCode(),
                "message" => $e->getMessage(),
                "trace" => $e->getTrace()
            ], 500);
        }
    }

    public function shippings(Request $request)
    { 
        try {
            // get user shippings need to approve
            $shippings = Order::with(['customer.address', 'items.product', 'user.role', 'shipping'])
                            ->whereType('shipping')
                            ->whereIn('status', [0, 1])
                            ->get();
            // return success message
            return response()->json([
                "code" => 200,
                'shippings' => OrderResource::collection($shippings)
            ]);
        } catch (\Exception $e) {
            // return error message
            return response()->json([
                "code" => $e->getCode(),
                "message" => $e->getMessage(),
                "trace" => $e->getTrace()
            ], 500);
        }
    }
}
