<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail\OTPCodeVerification;

class VerifyOTPController extends Controller
{
    
  public function __construct(){
    $this->middleware('auth')->except(['showverifyform', 'login']);
}

  public function login(){
      return view('auth.login-with-phone');
   }


public function verify(Request $request){
       
        $validatedData = $request->validate([
        'OTP' => 'required',
        ]);

       if($request->input('OTP')==auth()->user()->otp_code){
       	auth()->user()->update(['isVerified' => true, 'otp_code' => null]);
       	  return redirect('/'); 
       }

         return redirect()->route('verifyOTP')
                    ->withInput()
                    ->with('alert-danger', "The verification Code you entered is Invalid or Expired");
       //return back()->withErrors('the Otp is Invalid or Expired');
    }

    public function showverifyform(Request $request){
     // return  $request->session()->pull('otp', 'default');
    	return view('verify-otp');
    }

    public function resendOTP(Request $request){

               $OTP = auth()->user()->generateCode(6);
               
                
                session()->put("user_id", auth()->user()->id);
                session()->put("otp", $OTP);
                auth()->user()->update(['otp_code' => $OTP]);
                auth()->user()->sendOTP($request->get('otp_via'));
                return redirect('verifyOTP');
         }


  public function UpdatePhoneEmailOTP(Request $request){
     $OTP = auth()->user()->generateCode(6);
     if($request->has('email')){          
                auth()->user()->update(['otp_code' => $OTP]);
               \Mail::to($request->email)->send(new OTPCodeVerification ($OTP));
                return response()->json([
                "code" => 200,
                "email" => $request->email,
                "message" => 'verification code was Successfully sent!', 
                "trace" => 'verification code was Successfully sent!'
               
            ]);
      } else if($request->has('phone')){
           try {
             $basic  = new \Nexmo\Client\Credentials\Basic(env('NEXMO_API_KEY'), env('NEXMO_API_SECRET'));
            $client = new \Nexmo\Client($basic);
         $message = $client->message()->send([
        'to' => '60'.ltrim($request->phone, '0'), 
        'from' => 'Dealership-Reseller',
        'text' => 'Zstore. '.$OTP.' is your verification code'
        ]);
       auth()->user()->update(['otp_code' => $OTP]);
    } catch (\Exception $e) {
        
        return response()->json([
                "code" => $e->getCode(),
                "message" => $e->getMessage(),
                "trace" => $e->getTrace()
            ], 500);
    }
         
}
}
   public function UpdatePhoneEmail(Request $request){
      $user = auth()->user();
      if($request->has('email')){ 
      $this->validate($request, [
            'email' => "required|string|email|max:255|unique:users",
        ]);
      
       if($user->otp_code==$request->otp){
         $user->email = $request->email;
         $user->otp_code=NULL;
         $user->save();
         return response()->json([
                "code" => 200,
                "message" => 'Email Successfully updated!', 
            ]);
       }else{
        return response()->json([
                "code" => 500,
                "message" => 'Invalid verification code',
                "trace" => 'Invalid verification code'
            ], 500);
       }
     }
     else if($request->has('phone')){
           $this->validate($request, [
            'phone' => "required|phone_no|max:255|unique:users",
        ]);
        if($user->otp_code==$request->otp){
         $user->phone_no = $request->phone;
         $user->otp_code=NULL;
         $user->save();
         return response()->json([
                "code" => 200,
                "message" => 'Phone Successfully updated!', 
            ]);
       }else{
        return response()->json([
                "code" => 500,
                "message" => 'Invalid verification code',
                "trace" => 'Invalid verification code'
            ], 500);
       }
     }
   }
}
