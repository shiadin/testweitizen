<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use QCod\Gamify\Gamify;
use App\Reputed;
use Illuminate\Support\Facades\DB;
use App\User;
use App\Commision;
use Charts;
use Carbon\Carbon;

class CommisionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    
   
       
       $commisions = Commision::all();
  
      $sales_performance = Reputed::groupBy('payee_id')->select('payee_id', DB::raw('count(*) as total_restock'),  DB::raw('sum(point) AS total_sales'))
         ->with('user')->where('name', '!=', 'ShippingOrderReward')
        ->paginate(12);

        $lastMonth = Reputed::whereMonth(
        'created_at', '=', Carbon::now()->subMonth()->month
       )->where('name', '!=', 'ShippingOrderReward')->sum('point');
        $currentMonth = Reputed::
             whereYear('created_at', Carbon::now()->year)
            ->whereMonth('created_at', Carbon::now()->month)->where('name', '!=', 'ShippingOrderReward')
            ->sum('point');
        $all = Reputed::where('name', '!=', 'ShippingOrderReward')->sum('point');
       if($all==0)
        $all =1;
     // return $sales_performance;

  $sales = Reputed::select(
            DB::raw('sum(point) as sums'), 
            DB::raw("DATE_FORMAT(created_at,'%M %Y') as months"))
  ->groupBy('months')
  ->where('name', '!=', 'ShippingOrderReward')
  ->get();
       
   $chart_sale = Charts::create('bar', 'highcharts')
    ->title('Commision  Rewards')
    ->elementLabel('Accumulated Commision RM')
    ->labels($sales->pluck('months'))
    ->values($sales->pluck('sums'))
    ->dimensions(600,500)
    ->responsive(true);     

        $stats = array(
         array('label'=>'CURRENT TRANSACTION', 'value' => $currentMonth, 'url' => '/admin/transactions/00',  'color'=>'success', 'progressbar'=>($currentMonth/$all)*100),
         array('label'=>'LAST MONTH TRANSACTION', 'value' => $lastMonth,  'url' => '/admin/transactions/11',  'color'=>'info', 'progressbar'=>($lastMonth/$all)*100),
         array('label'=>'ALL TRANSACTION ', 'value' => $all,  'url' => '/admin/transactions/22', 'color'=>'danger', 'progressbar'=>($all/$all)*100),
      );

        return view('admin.reward.index',  ["title" => "Commissions management",
            "sidebar" => "Commissions Setting",
            'sales_performance' => $sales_performance,
            "stats" => $stats,
            'chart_sale' => $chart_sale,
            'sales' => $sales,
            'commisions' => $commisions
        ]);

      
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
         $this->validate($request, [
            'name' => 'required',
            'start' => 'required',
            'end' => 'required',
            'description' => 'required',
        ]);
        $commission = Commision::create($request->all());
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Commision $commission)
    {
       return view('admin.reward.show', [
        'title'=>'View Commision', 
        'campaign'=>$commission,
        "sidebar" => "Commissions Setting",

         ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Commision $commission)
    {
         $rules = [ array('label' => 'Rewards','desc' => "Recruited a 3 x 3 Stars downlines, to receive a 1% commission on your downline's order.", 'i'=>0, 'value'=>'1%' ), array('label' => 'Rewards','desc' => "Recruited a 3 x 4 Stars downlines, to receive a 3% commission on your downline's order.
100%
", 'i'=>1,  'value'=>'3%' ), array('label' => 'Rewards','desc' => "Recruited a 1 x 5 Stars downlines, to receive a 5% commission on your downline's order.", 'i'=>2,  'value'=>'5%' ), array('label' => 'Rewards','desc' => "Recruited a 5 x 5 Stars downlines, to receive a 8% commission on your downline's order.", 'i'=>3 , 'value'=>'8%'), array('label' => 'Rewards','desc' => "
Recruited a 10 x 5 Stars downlines, to receive a 12% commission on your downline's order.", 'i'=>4 , 'value'=>'12%')];
//return $rules;
         return view('admin.reward.edit', [
        'title'=>'Edit Commision', 
        'commission'=>$commission,
        'rules'=>$rules,
        "sidebar" => "Commissions Setting",

         ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,  Commision $commission)
    {
         // dd($request->all());
        $this->validate($request, [
            'name' => 'required',
            'start' => 'required',
            'end' => 'required',
            'description' => 'required',
        ]);

        $commission->update([
            'meta' => json_encode($request->meta),
            'conditions' => json_encode($request->conditions),
            'name' => $request->name,
            'start' => $request->start,
            'end' =>  $request->end,
            'description' => $request->description,
           ]);
    session()->flash('success_message', 'Commission was updated successfully!');
    return back();
    }

    public function activate(Request $request, Commision $commission)
    {
    
        
        $commissions = Commision::all();
        foreach ($commissions as $key => $value) {
           $value->active = 0;
           $value->save();
        }
        $commission->active=$request->active;
        $commission->save();
       session()->flash('success_message', 'Commision was activated successfully!');
    return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Commision $commission)
    {
        $commission->delete();
         session()->flash('success_message', 'Commision was Deleted successfully!');
        return back();
    }
}
