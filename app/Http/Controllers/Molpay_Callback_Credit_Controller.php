<?php

namespace App\Http\Controllers;
use App\Molpay;
use App\Stock;
use App\Payment;
use App\OrderItem;
use Illuminate\Http\Request;
use  \Event, App\Transaction;
use Illuminate\Support\Facades\Input;
use  App\Events\PaymentWasCompleted;
use Illuminate\Support\Facades\Redirect;
use Cart;
use Alert;
use Illuminate\Support\Facades\Crypt;
use App\Purchase;
use DB;
use App\Gamify\Points\PersonalOrderRewards;
use App\Mail\EwalletSuccessfullTopUp;
use Mail;

class Molpay_Callback_Credit_Controller extends Controller
{
    protected $input = null;
	/**
	 * Initalize the Callback Controller
	 *
	 * @access  public
	 * @return  void
	 */
	public function __construct()
	{
		$input = Input::all();
		$input['orderid'] = $input['orderid'];
		$this->input = $input;
	}
	/**
	 * Used as Molpay Return URL callback
	 *
	 * @access  public
	 * @return  Response
	 */
	public function index(Request $request) 
	{ 

		$input = $this->input;
       // dd($input);
		// verify origin of transaction using the hash
		$this->validation();
		// save the transaction
		$molpay = $this->save();
		switch ($input['status'])
		{
			case '00' :
			$description ='';
			$ids ='';
			$i = 1;
			     

        $molpay->description = $description;
     	$molpay->content = $molpay->orderid;
     	$molpay->tax = 'credit';
     	$molpay->save();

     	 $pay = Payment::where('molpay_order_id', $molpay->order_id)->where('status', 0)->first();
     	 if(!empty($pay)){
     	 	  $thisUser = $pay->user;
              Mail::to($thisUser->email)->send(new EwalletSuccessfullTopUp($thisUser, $pay));
     	 }
       
     	
         try{
         DB::beginTransaction();

      
      
         $payments = Payment::where('molpay_order_id', $molpay->order_id)->where('status', 0)->first();
      //  return $payments;
          // add more request
        $request->request->add([
            'memo' => $molpay->orderid, 
        ]);
       

        $payment = $this->approveCredits($request, $payments);
       
         DB::commit();

         return redirect('/')->withSuccessMessage('Ewalet was successfull Reloaded!');

         } catch (\Exception $e) {
         DB::rollback();

         }

	    
				break;
			case '11' :
			$description ='';
			$i = 1;
			      foreach(Cart::content() as $row) {
            $description .=  '#'.$i++.' ' . $row->model->name . ' - RM '. $row->model->price.'<hr>';
                       }
		$molpay->description = $description;
     	// $molpay->content = Cart::content();
     	// $molpay->tax = Cart::tax();
     	// $molpay->total = Cart::subtotal();
     	$molpay->save();
	    return redirect('payment-failure/'.$encrypted = Crypt::encryptString($molpay->id));
				break;
			case '22' :
			$description ='';
			$i = 1;
			
     	
	    return redirect('payment-pending/'.$encrypted = Crypt::encryptString($molpay->id));
				break;
		}
		if (Event::listeners('molpay.response.return_url'))
		{
			$response = Event::until('molpay.response.return_url', array($molpay));
			if ( ! is_null($response)) return $response;
		}
		return Response::make('', 200);
	}
	/**
	 * Used as Molpay callback query beta-1 (PUSH Notification)
	 *
	 * @access  public
	 * @return  Response
	 */
	public function push() 
	{
		$input = $this->input;
		// only accept molpay callback query
		if ($input['nbcb'] != '1')
		{
			throw new Exception("Not a valid Molpay Callback Query");
		}

		// verify origin of transaction using the hash
		$this->validation();
		// save the transaction
		$molpay = $this->save();
		switch ($input['status'])
		{
			case '00' :
				// payment is made
			   return Redirect::to('/home');
				//Event::fire('molpay.checkout.paid', array($input['orderid']));
				break;
			case '11' :
			    return Redirect::to('/home');
				//Event::fire('molpay.checkout.failed', array($input['orderid']));
				break;
			case '22' :
			      return Redirect::to('/home');
				//Event::fire('molpay.checkout.pending', array($input['orderid']));
				break;
		}
		if (Event::listeners('molpay.response.callback'))
		{
			$response = Event::until('molpay.response.callback', array($molpay));
			if ( ! is_null($response)) return $response;
		}
		return Response::make('', 200);
	}
	/**
	 * Save all payment transaction
	 *
	 * @access  protected
	 * @return  Eloquent
	 */
	protected function save()
	{
		$input = $this->input;
		// check for transaction id.
		$molpay = Molpay::where('transaction_id', '=', $input['tranID'])->first();
		if (is_null($molpay))
		{
			$molpay = new Molpay(array(
				'transaction_id' => $input['tranID'],
			));
		}
		$molpay->amount       = $input['amount'];
		$molpay->domain       = $input['domain'];
		$molpay->app_code     = $input['appcode'];
		$molpay->order_id     = $input['orderid'];
		$molpay->channel      = $input['channel'];
		$molpay->status       = $input['status'];
		$molpay->currency     = $input['currency'];
		$molpay->paid_at      = $input['paydate'];
		$molpay->security_key = $input['skey'];
		$molpay->user_id      = auth()->user()->id;
		if ('00' !== $input['status'])
		{
			$input['error_code'] and $molpay->error_code = $input['error_code'];
			$input['error_desc'] and $molpay->error_description = $input['error_desc'];
		}
		$molpay->save();
		
		return $molpay;
	}
	/**
	 * Validate hash to ensure that the request actually came from Molpay
	 *
	 * @access  protected
	 * @return  void
	 * @throws  Molpay\Exception
	 */
	protected function validation()
	{

		extract($this->input);
		$vkey    = \Config::get('api.verify_key', '');
		$skey =  $this->input['skey'];
		$confirm = md5($paydate.$domain.md5($tranID.$orderid.$status.$domain.$amount.$currency).$appcode.$vkey);
		return $confirm;
		if ($skey !== $confirm)
		{
			throw new \Exception("Invalid Transaction Key");
		}
	}




   
	

    public function approveCredits(Request $request, Payment $payment)
    {
    
        // approve payment
        $payment->update([
            'status' => 1,
            'memo' => $request->memo
        ]);
        // get order
        $order = $payment->order;
        // approve payment's order
        $order->update([
            'status' => 1
        ]);
        // get payment user
        $user = $payment->user;
        // get ori amount
        $ori = $user->balance;
        // get  order amount
        $qty = $payment->order->amount;
        // increse credits in payment's user
      //  $user->increment('credits', $qty);
        
        $new = $ori+$qty;
        $wallet = $user->depositA($qty, 'deposit', ['source' => json_encode($payment->order), 'description' => 'Deposit of RM '.$qty .'  from eWallet payment reload.  Approved by '.auth()->user()->username.' ( RM'.$ori.' => RM'.$new.' )', 'amount' => $qty, 'user_id' => $user->id]);
        
        // get new amount
        
        // get currency
        $currency = config('app.currency');
        // log activity
        activity()
            ->performedOn($user)
            ->causedBy($request->user())
            ->withProperties($request->all())
            ->log("$qty unit of ewallet has been added to account from Request#$order->id by :causer.username ($ori => $new)");

        
        return $payment;
    }

}
