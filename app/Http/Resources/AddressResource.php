<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class AddressResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "user_id" => $this->user_id,
            "street" => $this->street,
            "street_2" => $this->street_2,
            "zipcode" => $this->zipcode,
            "city" => $this->city,
            "state" => $this->state,
            "country" => $this->country,
            "full" => $this->full,
            "user" => $this->whenLoaded('user', function () {
                return new UserResource($this->user);
            })
        ];
    }
}
