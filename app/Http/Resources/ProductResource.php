<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // check if request from admin
        $isAdmin = $request->is('admin/*');

        // get reseller info
        $reseller = collect(config('resellers'))
                            ->where('role_id', $request->user()->role_id)
                            ->first();

        // try to get price
        $price = $this->relationLoaded('price') ? $this->price :
                ($this->relationLoaded('customerPrice') ? $this->customerPrice : false);

        return [
            "id" => $this->id,
            "user_id" => $this->user_id,
            "name" => $this->name,
            "sku" => $this->sku,
            "description" => $this->description,
            "stock_amount" => $this->stock_amount,
            "status" => $this->status,
            "weight" => $this->weight,
            "image" => !empty($this->image) ? asset("storage/$this->image?id=".time()) : null,
            "memo" => $this->memo,
            "situation" => $this->situation,
            "min_stock_purchase" => $this->price->min_quantity ?? '',
            "created_at" => !$this->created_at ? $this->created_at : $this->created_at->toDateTimeString(),
            "updated_at" => !$this->updated_at ? $this->updated_at : $this->updated_at->toDateTimeString(),
            "price" => $this->when($price, function () use ($price) {
                return $price->amount;
            }),
            "user" => $this->whenLoaded('user', function () {
                return new UserResource($this->user);
            }),
            "links" => [
                'edit' => $this->when($isAdmin, route('admin.product.edit', $this->id)),
                'update' => $this->when($isAdmin, route('admin.product.update', $this->id)),
                'delete' => $this->when($isAdmin, route('admin.product.delete', $this->id))
            ]
        ];
    }
}
