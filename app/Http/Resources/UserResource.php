<?php

namespace App\Http\Resources;

use App\Address;
use App\Http\Resources\RoleResource;
use App\Order;
use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $isAdmin = $request->is('admin/*');

        $depositRequired = $this->depositRequired();

        return [
            "id" => $this->id,
            "role_id" => $this->role_id,
            "referrer_id" => $this->referrer_id,
            "username" => $this->username,
            "eWallet" => $this->balance,
            "wallet_trans" => $this->wallet->transactions,
            "memo" => $this->memo,
            "status" => $this->status,
            "situation" => $this->situation,
            "name" => $this->name,
            "email" => $this->email,
            "phone_no" => $this->phone_no,
            "country_code" => $this->country_code,
            "wechat" => $this->wechat,
            "facebook" => $this->facebook,
            "gender" => $this->gender,
            "notification" => $this->notifications->take(10),
            'total_order' => $this->totalOrder(),
            "token" => $this->tokenCount()+$this->stocksCount(),
            "credits" => $this->balance ? $this->balance : 0, //credites changed to ewwalet
            "ic_no" => $this->ic_no,
            "ic_front" => !empty($this->ic_front) ? asset("storage/$this->ic_front?id=".time()) : null,
            "ic_back" => !empty($this->ic_back) ? asset("storage/$this->ic_back?id=".time()) : null,
            "avatar" => !empty($this->avatar) ? asset("storage/$this->avatar?id=".time()) : null,
            "created_at" => !$this->created_at ? $this->created_at : $this->created_at->toDateTimeString(),
            "updated_at" => !$this->updated_at ? $this->updated_at : $this->updated_at->toDateTimeString(),
            "deposit_required" => $depositRequired,
            "role" => $this->whenLoaded('role', function () {
                return new RoleResource($this->role);
            }),
            "upline" => $this->whenLoaded('upline', function () {
                return new UserResource($this->upline);
            }),
            "downlines_count" => $this->when($this->downlines_count !== null || $this->relationLoaded('downlines'), function () {
                return $this->downlines_count !== null ? $this->downlines_count : $this->downlines->count();
            }),
            "downlines" => $this->whenLoaded('downlines', function () {
                return UserResource::collection($this->downlines);
            }),
            "customers" => $this->whenLoaded('customers', function () {
                return UserResource::collection($this->customers);
            }),
            "deposit" => $this->whenLoaded('deposit', function () {
                return new OrderResource($this->deposit);
            }),
            "deposits" => $this->whenLoaded('deposits', function () {
                return OrderResource::collection($this->deposits);
            }),
            "shippings_count" => $this->when($this->shippings_count !== null || $this->relationLoaded('shippings'), function () {
                return $this->shippings_count !== null ? $this->shippings_count : $this->shippings->count();
            }),
            "shippings" => $this->whenLoaded('shippings', function () {
                return OrderResource::collection($this->shippings);
            }),
            "stocks" => $this->whenLoaded('stocks', function () {
                return StockResource::collection($this->stocks);
            }),
            "invite_codes" => ReferralResource::collection($this->whenLoaded('inviteCodes')),
            "address" => new AddressResource($this->address ?: new Address),
            "activities" => $this->whenLoaded('activities'),
            "points" => round($this->totalCampaign(), 2),
            "commission" => round($this->totalCommission(), 2),
            "progress" => 'width: '.$this->getProgress().'%;',
            "progress1" => 'width: '.$this->getProgress1().'%;',
            "progress2" => 'width: '.$this->getProgress2().'%;',
            "progressP" => round($this->getProgress(),2),
            "progressP1" => round($this->getProgress1(),2),
            "progressP2" => round($this->getProgress2(),2),
            "needed" => $this->getNeeded(),
            "needed1" => $this->getNeeded1(),
            "needed2" => $this->getNeeded2(),
            'needed0' => $this->getNeeded0(),
            "reputations" => $this->reputations,
            "rewards" => $this->rewards,
            "commissionStatus" =>$this->downlineProgress(),
            "shippingStat" => $this->shippingStat(),
            "allShippingStat" => $this->allShippingStat(),
            "links" => [
                'get_upline' => route('admin.users.getupline'),
                'stats' => route('user.stats'),
                'update' => $isAdmin ? route('admin.user.update', $this->id) : route('user.update'),
                'approve' => $this->when($isAdmin, route('admin.user.approve', $this->id)),
                'reject' => $this->when($isAdmin, route('admin.user.reject', $this->id)),
                'edit' => $this->when($isAdmin, route('admin.user.edit', $this->id)),
                "qrcode" => $this->when($this->id, route('user.qrcode', ['username' => $this->username])),
                'certificate' => $this->when($this->id, route('user.certificate', ['username' => $this->username])),
                'create_stocks' => route('user.order.create', 'stocks'),
                'create_shipping' => route($isAdmin ?  'admin.order.create' : 'user.order.create', 'shipping'),
                'create_deposit' => route('user.deposit.create', "deposit:$this->role_id"),
                'create_credits' => route('user.order.create', 'credits'),
                'create_referral' => route('user.referral.create', ['is_system' => $isAdmin ? true : false]),
                'transfer_stock' => route('user.stock.transfer'),
                'invite_link' => route('user.invitation'),
                'upload_ic' => $isAdmin ? route('admin.user.ic.upload', $this->id) : route('user.ic.upload'),
                'upload_avatar' => $isAdmin ? route('admin.user.avatar.upload', $this->id) : route('user.avatar.upload'),
                'update_settings' => $isAdmin ? route('admin.user.settings.update', $this->id) : route('user.settings.update'),
                'update_credits' => $this->when($isAdmin, route('admin.user.credits.update', $this->id)),
                'update_stock' => $this->when($isAdmin, function () {
                    return route('admin.user.stock.update', $this->id);
                }),
                'update_upline' => $this->when($isAdmin && auth()->user()->isAdmin(['super']), function () {
                    return route('admin.user.upline.update', $this->id);
                }),
                'update_role' => $this->when($isAdmin && auth()->user()->isAdmin(['super']), function () {
                    return route('admin.user.role.update', $this->id);
                }),
                'get_downlines' => $this->when($this->id, route('user.downlines.index', $this->id)),
            ],
            "deposit_instructions" => config('deposits.instructions'),
        ];
    }
}
