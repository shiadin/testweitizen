<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // check if request from admin
        $isAdmin = $request->is('admin/*');

        return [
            "id" => $this->id,
            "user_id" => $this->user_id,
            "type" => $this->type,
            "delivery" => $this->delivery,
            "strictType" => $this->strictType,
            "purpose" => $this->purpose,
            "molpay_order_id" =>$this->molpay_order_id,
            'both_amount' => $this->both_amount,
            "amount" => floatval($this->amount),
            "order_category" =>$this->order_category,
            "payment_type" =>$this->payment_type,
            "shipping_type" => $this->shipping_type,
            "addOn" => $this->addOn,
            "sale_amount" => floatval($this->sale_amount),
            "status" => $this->status,
            "situation" => $this->situation,
            "memo" => $this->memo,
            "created_at" => !$this->created_at ? $this->created_at : $this->created_at->toDateTimeString(),
            "updated_at" => !$this->updated_at ? $this->updated_at : $this->updated_at->toDateTimeString(),
            "payment" => $this->whenLoaded('payment', function () {
                return new PaymentResource($this->payment);
            }),
            "user" => $this->whenLoaded('user', function () {
                return new UserResource($this->user);
            }),
            "customer" => $this->whenLoaded('customer', function () {
                return new CustomerResource($this->customer);
            }),
            "items" => $this->whenLoaded('items', function () {
                return OrderItemResource::collection($this->items);
            }),
            "tracking_no" => $this->whenLoaded('shipping', function () {
                return $this->shipping->tracking_no;
            }),
            "courier" => $this->whenLoaded('shipping', function () {
                return $this->shipping->courier;
            }),
            "activities" => $this->whenLoaded('activities', function () {
                return $this->activities;
            }),
            "links" => [
                'cancel' => $this->when(($this->status == 0 || $this->status == 1) && $this->id, function () use ($isAdmin) {
                    return $isAdmin ? route('admin.order.cancel', $this->id) : route('user.order.cancel', $this->id);
                }),
                'edit' => $this->when($this->id, function () use ($isAdmin) {
                    return $isAdmin ? route('admin.shipping.edit', $this->id) : route('user.shipping.edit', $this->id);
                }),
                'update' => $this->when($this->id, function () use ($isAdmin) {
                    return $isAdmin ? route('admin.shipping.update', $this->id) : route('user.shipping.update', $this->id);
                }),
                'process' => $this->when($isAdmin && $this->id, function () {
                    return route('admin.shipping.process', $this->id);
                }),
                'confirm' => $this->when($isAdmin && $this->id, function () {
                    return route('admin.shipping.confirm', $this->id);
                }),
                'invoice' => $this->when($this->id && ($this->allowToViewInvoice()), function () {
                    return route('user.order.invoice', $this->id);
                })
            ]
        ];
    }
}
