<?php

namespace App\Http\Resources;

use App\Address;
use Illuminate\Http\Resources\Json\JsonResource;

class CustomerResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->user_id,
            "name" => $this->name ?: $this->user->name,
            "phone_no" => $this->phone_no ?: $this->user->phone_no,
            "address" => new AddressResource($this->street ? new Address([
                "street" => $this->street,
                "street_2" => $this->street_2,
                "zipcode" => $this->zipcode,
                "city" => $this->city,
                "state" => $this->state,
                "country" => $this->country
            ]) : $this->address),
        ];
    }
}
