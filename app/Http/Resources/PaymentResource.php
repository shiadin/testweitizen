<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PaymentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $isAdmin = $request->is('admin/*');

        return [
            "id" => $this->id,
            "order_id" => $this->order_id,
            "user_id" => $this->user_id,
            "approver_id" => $this->approver_id,
            "gateway" => $this->gateway,
            "amount" => $this->amount,
            "token" => $this->token,
            "url" => asset("storage/$this->token"),
            "status" => $this->status,
            "memo" => $this->memo,
            "order" => $this->whenLoaded('order', function () {
                return new OrderResource($this->order);
            }),
            "links" => [
                'approve' => $this->when($isAdmin, route('admin.payment.approve', $this->id)),
                'reject' => $this->when($isAdmin, route('admin.payment.reject', $this->id)),
            ]
        ];
    }
}
