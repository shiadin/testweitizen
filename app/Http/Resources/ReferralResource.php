<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ReferralResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "user_id" => $this->user_id,
            "role_id" => $this->role_id,
            "code" => $this->code,
            "user" => $this->whenLoaded('user', function () {
                return new UserResource($this->user);
            }),
            "role" => $this->whenLoaded('role', function () {
                return new RoleResource($this->role);
            }),
            "links" => [
                'register' => route('referral', ['code' => $this->code]),
                'delete' => route('user.referral.delete', $this->id)
            ]
        ];
    }
}
