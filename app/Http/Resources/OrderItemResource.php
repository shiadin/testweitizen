<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderItemResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "product_id" => $this->product_id,
            "order_id" => $this->order_id,
            "unit_price" => $this->unit_price,
            "quantity" => $this->quantity,
            "status" => $this->status,
            "memo" => $this->memo,
            "subtotal" => $this->subtotal,
            "batch_numbers" => $this->batch_numbers,
            "product" => $this->whenLoaded('product', function () {
                return new ProductResource($this->product);
            })
        ];
    }
}
