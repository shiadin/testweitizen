<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class RoleResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $reseller = collect(config("resellers"))->where('role_id', $this->id)->first();
        return [
            "id" => $this->id,
            "name" => ucwords($this->name),
            "description" => $this->description,
            "status" => $this->status,
            "deposit" => $reseller ? $reseller['deposit'] : null
        ];
    }
}
