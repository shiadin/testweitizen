<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class StockResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // check if request is from admin
        $isAdmin = $request->is('admin/*');

        return [
            "id" => $this->id,
            "user_id" => $this->user_id,
            "product_id" => $this->product_id,
            "quantity" => $this->quantity,
            "status" => $this->status,
            "status" => $this->payment_type,
            "memo" => $this->memo,
            "product" => $this->whenLoaded('product', function () {
                return $this->product ? new ProductResource($this->product) : null;
            }),
            "user" => $this->whenLoaded('user', function () {
                return $this->user ? new UserResource($this->user) : null;
            }),
            "activities" => $this->whenLoaded('activities', function () {
                return $this->activities;
            }),
            "links" => [
            ]
        ];
    }
}
