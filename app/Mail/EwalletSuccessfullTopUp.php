<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\User;
use App\Payment;
class EwalletSuccessfullTopUp extends Mailable
{
    use Queueable, SerializesModels;
    public $payment;
    public $user;

    /**
     * Create a new message instance.
     *
     * @return void
     */
   public function __construct(User $user, Payment $payment)
    {
        $this->user = $user;
        $this->payment = $payment;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.user.wallet-success-top-up')->with(['user'=>$this->user, 'payment'=>$this->payment]);
    }
}
