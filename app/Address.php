<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    protected $fillable = [
        "user_id", "street", "street_2", "zipcode", "city", "state", "country"
    ];

    /**
     * Relationships
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function getFullAttribute($value)
    {
        return implode(',<br>', array_filter([
            $this->street,
            $this->street_2,
            "$this->zipcode $this->city",
            "$this->state, $this->country"
        ]));
    }
}
