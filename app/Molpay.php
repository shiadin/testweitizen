<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Database\Eloquent\SoftDeletes;

class Molpay extends Model
{
      use SoftDeletes;

         protected $fillable = ['order_id', 'transaction_id', 'user_id'];

   public function user(){
   	return $this->belongsTo('App\User')->withDefault();
   }

    public function order(){
   	return $this->belongsTo('App\Order', 'molpay_order_id', 'order_id')->withDefault();
   }
}
