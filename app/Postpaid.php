<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Postpaid extends Model
{
     protected $fillable = ['user_id', 'order_id', 'amount', 'product_id', 'name', 'quantity', 'unit_price', 'status', 'memo'];
}
