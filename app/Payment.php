<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $fillable = [
        "order_id", "user_id", "approver_id", "gateway", "amount", "token", "status", "memo", "molpay_order_id"
    ];

    /**
     * Relationships
     */
    public function order()
    {
        return $this->belongsTo('App\Order');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
