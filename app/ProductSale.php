<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductSale extends Model
{
    protected $fillable = ['transaction', 'product_id', 'name', 'quantity', 'unit_price'];
}
