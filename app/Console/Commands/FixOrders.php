<?php

namespace App\Console\Commands;

use App\Order;
use App\Activity;
use App\Shipping;
use GuzzleHttp\Client;
use App\Mail\OrderCompleted;
use Illuminate\Console\Command;
use Symfony\Component\DomCrawler\Crawler;

class FixOrders extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'orders:fix';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fix the invalid order type caused by the changes on type name';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $this->info('Update orders with type c-credit');

            $orders = Order::whereType('c-credit')->update(['type' => 'credits']);

            $this->info($orders." orders fixed");

            $this->info('Update activity logs contains words c-credit');

            $activities = Activity::where('description', 'like', '%c-credit%')
                                ->get()
                                ->each(function ($activity) {
                                    $activity->update([
                                        'description' => str_replace('c-credit', 'credits', $activity->description)
                                    ]);
                                })
                                ->count();

            $this->info($activities." activities fixed");

            $this->info('Update shipping courier code poslaju');

            $shippings = Shipping::whereCourier('poslaju')->update(['courier' => 'malaysia-post']);

            $this->info($shippings." shippings fixed");

            $this->info("Finish fixing orders.");
        } catch (\Exception $e) {
            $this->error("Something wrong!");
            $this->error($e->getMessage());
        }
    }
}
