<?php

namespace App\Console\Commands;

use App\Order;
use App\Shipping;
use GuzzleHttp\Client;
use App\Mail\OrderCompleted;
use Illuminate\Console\Command;
use Symfony\Component\DomCrawler\Crawler;

class TrackOrders extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'orders:track';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Track the order shipped and turn into completed state if delivered';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Get shipped orders...');

        $orders = Order::where('status', 2)
                ->where('type', 'shipping')
                ->with('shipping')
                ->whereHas('shipping', function ($query) {
                    return $query->whereNotNull('tracking_no');
                })
                ->get();

        $this->info('Start tracking orders:');
        // set need proxy flag
        $needProxy = false;
        // set proxy list container
        $proxys = [];
        // set a proxy contaner
        $proxy = null;
        foreach ($orders as $order) {
            $this->info("Track order #$order->id...");
            if (strtotime($order->updated_at) < strtotime('-20 days')) {
                $this->info("Order not updated more than 20 days. Turn order into completed state");
                // turn order into completed state
                $order->update([
                    'status' => 3
                ]);
                // notify user
                event('order.completed', $order);
            } else {
                // set attempt flag
                $attempt = false;
                do {
                    try {
                        // if this turn need to request with proxy
                        if ($needProxy) {
                            // get a proxy from list if not available
                            $proxy = $proxy ?: $this->getProxy();
                            $this->info("Proxy: $proxy->protocol://$proxy->ip:$proxy->port");
                            // initialize client with proxy
                            $client = new Client([
                                'proxy' => !$proxy ? null : "$proxy->protocol://$proxy->ip:$proxy->port",
                                'verify' => public_path('cacert.pem')
                            ]);
                        } else {
                            // initialize client
                            $client = new Client;
                        }
                        // check tracking no on aftership tracking page
                        $response = $client->request('GET', "https://track.aftership.com/".$order->shipping->courier."/".$order->shipping->tracking_no);
                        // parsing response
                        $crawler = new Crawler((string)$response->getBody());
                        // check if the parcel is delivered
                        $isArrived = $crawler->filter('.checkpoint__icon.delivered')->count() > 0;
                        // blocked by crawler blocker
                        $isBlocked = $crawler->filter('.sr-only')->text() === "I'm not a robot";
                        // return true if parcel was delivered
                        if ($isArrived) {
                            $this->info("Parcel arrived (".$order->shipping->tracking_no."). Turn order into completed state");
                            // turn order into completed state
                            $order->update([
                                'status' => 3
                            ]);
                            // notify user
                            event('order.completed', $order);
                            // set attempt flag to false
                            $attempt = false;
                        } elseif ($isBlocked) {
                            $this->info('Blocked by crawler blocker');
                            // turn flag to proxy
                            $needProxy = true;
                            // set attempt flag, attempt 2 times only
                            $attempt = !$attempt;
                            // clear proxy if cant use anymore
                            $proxy = null;
                        } else {
                            $this->info("Parcel haven't arrived. (".$order->shipping->tracking_no.")");
                            // set attempt flag to false
                            $attempt = false;
                        }
                    } catch (\Exception $e) {
                        // set attempt flag to false
                        $attempt = false;
                        // clear proxy if cant use anymore
                        $proxy = null;
                        $this->error("Something wrong in tracking order #$order->id");
                        $this->error($e->getMessage());
                    }
                } while ($needProxy && $attempt);
            }
        }
        $this->info("Finish tracking orders.");
    }

    public function getProxy()
    {
        try {
            $this->info('Get proxy...');
            // initialize client
            $client = new Client;
            // check tracking no on aftership tracking page
            $response = $client->request('GET', "https://gimmeproxy.com/api/getProxy?protocol=http");
            // parsing response
            return json_decode((string)$response->getBody());
        } catch (\Exception $e) {
            $this->error('Error occurs when retrieving proxy');
            $this->error($e->getMessage());
            $this->error($e->getCode());
            // exit from too many requests
            if ($e->getCode() == 429) {
                exit;
            }
        }
    }
}
