<?php

namespace App\Console\Commands;

use App\Order;
use Illuminate\Console\Command;

class FixBatchNumbers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fix:batch_numbers';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description =
        'modify the order\'s batch numbers due to the requirements changed in commits 56207bc and de4a4a9';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $this->info('Get orders need to fix...');
            // get orders which is not cancelled and not empty in column memo
            $orders = Order::with('items')
                            ->where('status', '!=', -1)
                            ->whereNotNull('memo')
                            ->get()
                            ->each(function ($order) {
                                $this->info('Start to fix order #'.$order->id);
                                // copy the memo content to batch numbers of first item
                                $item = $order->items->first();
                                if ($item) {
                                    $item->update([
                                        'batch_numbers' => $order->memo
                                    ]);
                                }
                                // remove memo content in order
                                $order->update([
                                    'memo' => null
                                ]);
                            });
            $this->info("Finish fixing orders.");
        } catch (\Exception $e) {
            $this->error($e->getMessage());
        }
    }
}
