<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
        "user_id", "name", "sku", "description", "stock_amount", "status", "memo", "weight", "image"
    ];

    protected static $situations =  [
        0 => 'inactive',
        1 => 'active',
        -1 => 'deleted'
    ];

    public static function getSituations()
    {
        return collect(self::$situations)->filter(function ($desc, $id) {
            return $id !== -1;
        });
    }
    /**
     * Relationships
     */

    public function price()
    {
        return $this->hasOne('App\Price')
                    ->where('role_id', auth()->user()->role_id);
    }

    public function customerPrice()
    {
        return $this->hasOne('App\Price')
                    ->where('role_id', 700);
    }

    public function prices()
    {
        return $this->hasMany('App\Price')
                    ->orderBy('prices.role_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User')
                    ->with(['address', 'role']);
    }

    /**
     * Accessor
     */
    public function getSituationAttribute($value)
    {
        return self::$situations[$this->status];
    }
}
