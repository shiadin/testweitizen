<?php

namespace App;

use App\Shipping;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = ['user_id', 'type', 'both_amount', 'delivery', 'amount', 'status', 'memo', 'molpay_order_id', 'order_category', 'sale_amount', 'addOn', 'payment_type', 'shipping_type'];

    protected static $situations =  [
        0 => 'processing',
        1 => 'approved',
        -1 => 'rejected'
    ];

    protected static $shippingSituations =  [
        0 => 'processing',
        1 => 'confirmed',
        2 => 'delivering',
        3 => 'completed',
        -1 => 'cancelled'
    ];

    public static function getSituations($type = null)
    {
        if ($type === 'shipping') {
            return self::$shippingSituations;
        }
        return self::$situations;
    }

    public function getAmount($fee = null)
    {
        // this operation is specified for shipping order
        if ($this->type !== 'shipping') {
            return false;
        }
        // if amount is provided
        if (!is_null($fee)) {
            // update order amount
            $this->update([
                'amount' => $fee
            ]);
            return $fee;
        }
        // preset total
        $total = 0;
        $weight = 0;
        // get address
        $address = $this->customer->address;
        $state = collect(config("countries.".$address->country))
        // get state from countries
                        ->where('name', $address->state)
                        ->first();
        // something wrong if state not found
        if (!$state) {
            throw new \Exception("Undefined State Code: ".$address->state, 500);
        }
        // get shipping fee list by state code
        $list = collect(config('shipping'))->filter(function ($region) use ($state) {
            return in_array($state['code'], $region['includes']);
        })->first();
        // something wrong if list not found
        if (!$list) {
            throw new \Exception("Undefined Shipping Fee List: ".$state['code'], 500);
        }
        // loop items
        $this->items->each(function ($item) use ($list, &$weight) {
            // calculate total weight of items
            $weight += $item->quantity * $item->product->weight;
        });
        // loop shipping fee list to get correct fee
        $fee = collect($list['fees'])->filter(function ($price, $key) use ($weight) {
            // parse the operator from key
            preg_match('/^(.*[^\d])(\d+)$/', $key, $matches);
            return $this->compareResult($weight, $matches[1], $matches[2]);
        })->first();
        // something wrong if fee not found
        if (!$fee) {
            throw new \Exception("Undefined Shipping Fee", 500);
        }
        // add to total amount
        $total += $fee;
        return $total;
    }

    public function compareResult($a, $operator, $b)
    {
        switch ($operator) {
            case '<':
                return $a < $b;
            case '<=':
                return $a <= $b;
            case '>':
                return $a > $b;
            case '>=':
                return $a >= $b;
            default:
                return false;
        }
    }

    public function allowToModify($user)
    {
        if ($this->user_id === $user->id) {
            return true;
        } elseif ($user->isAdmin()) {
            return true;
        }
        return false;
    }

    public function trackingLink()
    {
        if (!$this->shipping) {
            return '';
        }
        return "https://track.aftership.com/".$this->shipping->courier."/".$this->shipping->tracking_no;
    }

    public function allowToViewInvoice()
    {
        if ($this->type === 'shipping') {
            return $this->status > 1;
        }
        return $this->status > 0;
    }


    /**
     * Relationships
     */
    public function payment()
    {
        return $this->hasOne('App\Payment')
                    ->orderByDesc('status')
                    ->latest();
    }
     public function postpaid()
    {
        return $this->hasOne('App\Postpaid');
                    
    }

    public function user()
    {
        return $this->belongsTo('App\User')
                    ->with('address');
    }

    public function items()
    {
        return $this->hasMany('App\OrderItem');
    }

    public function customer()
    {
        return $this->hasOne('App\OrderCustomer')
                    ->with('address');
    }

    public function shipping()
    {
        return $this->hasOne('App\Shipping');
    }

    public function activities()
    {
        return $this->hasMany('App\Activity', 'subject_id')
                    ->where('subject_type', 'App\Order');
    }
    /**
     * Accessor
     */
    public function getSituationAttribute($value)
    {
        $situation = $this->status !== null ?
            self::getSituations($this->type)[$this->status] :
            'draft';
        if (!$situation) {
            throw new \Exception("Something wrong in evaluate user situation", 500);
        }
        return $situation;
    }

    public function getPurposeAttribute($value)
    {
        // check if type is deposit
        if (preg_match("/deposit:(\d+)/", $this->type, $matches)) {
            // get role
            $role = collect(config('resellers'))->where('role_id', $matches[1])->first();
            return "Upgrade to ".$role['name']." (".$role['description'].")";
        } elseif ($this->type == 'credits') {
            return "Top up Credits ".config('app.currency')." $this->amount";
        } elseif ($this->type == 'stocks') {
            return  "Request stocks from system worth ".config('app.currency')." $this->amount";
        } elseif ($this->type == 'shipping') {
            return "Create shipping order worth ".config('app.currency')." $this->amount";
        }
        return null;
    }

    public function getStrictTypeAttribute($value)
    {
        if (preg_match("/deposit:(\d+)/", $this->type, $matches)) {
            return 'deposit';
        }
        return $this->type;
    }
}
