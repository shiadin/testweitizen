<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Purchase extends Model
{
        protected $fillable = ['user_id', 'order_id', 'product_id', 'description', 'name', 'paid', 'molpay_id', 'status', 'quantity'];

     public function product(){
      return $this->belongsTo('App\Product')->withDefault();
      }

       public function user(){
      return $this->belongsTo('App\User')->withDefault();
      }

       public function molpay(){
      return $this->belongsTo('App\Molpay')->withDefault();
      }

      public function order(){
      return $this->belongsTo('App\Order', 'order_id', 'molpay_order_id')->withDefault();
      }
   
      public function item(){
      return $this->belongsTo('App\OrderItem', 'order_id', 'molpay_order_id')->withDefault();
      }
   
   
}
