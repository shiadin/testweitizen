<?php

namespace App;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;

class Stock extends Model
{
    protected $fillable = [
        "user_id", "product_id", "quantity", "status", "memo", 'molpay_order_id'
    ];

    public function decrease($quantity, Request $request)
    {
        // check stock balance before reduce
        if (intval($this->quantity) - intval($quantity) < 0) {
            throw new \Exception("You have no enough stocks to reduce", 500);
        }
        // set original quantity
        $ori = $this->quantity;
        // reduce
        $this->decrement('quantity', intval($quantity));
        // set updated quantity
        $new = $this->quantity;
        // log activity
        activity()
            ->performedOn($this)
            ->causedBy($request->user())
            ->withProperties($request->all())
            ->log("$quantity unit of stocks has been reduced from account by :causer.username ($ori => $new)");
    }

    public function increase($quantity, Request $request)
    {
        // set original quantity
        $ori = $this->quantity;
        // reduce
        $this->increment('quantity', intval($quantity));
        // set updated quantity
        $new = $this->quantity;
        // log activity
        activity()
            ->performedOn($this)
            ->causedBy($request->user())
            ->withProperties($request->all())
            ->log("$quantity unit of stocks has been added to account by :causer.username ($ori => $new)");
    }

    public function reset($quantity, $previous, Request $request)
    {
        // check stock balance before reset
        if (intval($this->quantity) + intval($previous) - intval($quantity) < 0) {
            throw new \Exception("You have no enough stocks to update", 500);
        }
        // set original quantity
        $ori = $this->quantity;
        // reset
        $this->update(['quantity' => intval($this->quantity) + intval($previous) - intval($quantity)]);
        // set updated quantity
        $new = $this->quantity;
        // log activity
        activity()
            ->performedOn($this)
            ->causedBy($request->user())
            ->withProperties($request->all())
            ->log("$quantity unit of stocks has been reset to account by :causer.username ($ori => $new)");
    }

    /**
     * Relationships
     */

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function product()
    {
        return $this->belongsTo('App\Product')->with('price');
    }

    public function activities()
    {
        return $this->hasMany('App\Activity', 'subject_id')
                    ->where('subject_type', 'App\Stock');
    }
}
