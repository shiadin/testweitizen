<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Models\Activity as ActivityLog;
use Carbon\Carbon;
use App\User;
class Activity extends ActivityLog
{


public function getCreatedAtAttribute()
    {
        return  Carbon::parse($this->attributes['created_at'])->diffForHumans();
    }

public function getCauserIdAttribute()
    {
        return  User::find($this->attributes['causer_id'])->name ?? 'NA';
    }
}
