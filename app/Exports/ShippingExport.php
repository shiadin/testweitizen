<?php

namespace App\Exports;

use App\Order;
use App\Http\Resources\OrderResource;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ShippingExport implements FromQuery, WithHeadings, WithMapping
{
    use Exportable;

    public function __construct($filters)
    {
        $this->filters = $filters;
    }

    public function headings(): array
    {
        return [
            'No',
            'Parcel Content',
            'Value of goods (RM)',
            'Total Weight (kg)',
            'Collection Date',
            'Sender Name',
            'Sender Company',
            'Sender Contact',
            'Sender Alt Contact',
            'Sender Email',
            'Sender Address',
            'Sender Postcode',
            'Sender City',
            'Sender State',
            'Sender Country',
            'Receiver Name',
            'Receiver Company',
            'Receiver Contact',
            'Receiver Alt Contact',
            'Receiver Email',
            'Receiver Address',
            'Receiver Postcode',
            'Receiver City',
            'Receiver State',
            'Receiver Country',
            'Courier Service SMS (RM0.20)',
            'Drop Off At Courier Branch',
            'Reference (optional)',
            'Tracking No',
            'Courier'
        ];
    }

    public function map($order): array
    {
        // parse order
        $order = json_decode(json_encode(new OrderResource($order)));
        return [
            $order->id,
            collect($order->items)->map(function ($item) {
                return "$item->quantity * ".$item->product->name;
            })->implode(', '),
            $order->amount,
            collect($order->items)->sum(function ($item) {
                return $item->quantity * $item->product->weight/1000;
            }),
            null,
            $order->user->name,
            'CANALE ENTERPRISE',
            $order->user->phone_no,
            null,
            $order->user->email,
            trim($order->user->address->street." ".$order->user->address->street_2),
            $order->user->address->zipcode,
            $order->user->address->city,
            $order->user->address->state,
            $order->user->address->country,
            !$order->customer ? null : $order->customer->name,
            null,
            !$order->customer ? null : $order->customer->phone_no,
            null,
            null,
            !$order->customer ? null : trim($order->customer->address->street." ".$order->customer->address->street_2),
            !$order->customer ? null : $order->customer->address->zipcode,
            !$order->customer ? null : $order->customer->address->city,
            !$order->customer ? null : $order->customer->address->state,
            !$order->customer ? null : $order->customer->address->country,
            'no',
            null,
            $order->memo,
            $order->tracking_no ?: null,
            $order->courier ?: null,
        ];
    }

    public function query()
    {
        $request = (Object)array_filter($this->filters);

        $orders = Order::query()->where('type', 'shipping');

        $orders->with(['customer.address', 'items.product', 'user', 'shipping']);

        if (isset($request->ids)) {
            $orders->whereIn('id', $request->ids);
        }

        if (isset($request->keyword)) {
            $orders->where(function ($query) use ($request) {
                $query->where('id', 'like', "%$request->keyword%")
                    ->orWhereHas('customer.user', function ($query) use ($request) {
                        $query->where('name', 'like', "%$request->keyword%");
                    })
                    ->orWhereHas('customer', function ($query) use ($request) {
                        $query->where('name', 'like', "%$request->keyword%");
                    })
                    ->orWhereHas('user', function ($query) use ($request) {
                        $query->where('name', 'like', "%$request->keyword%")
                            ->orWhere('username', 'like', "%$request->keyword%");
                    })
                    ->orWhereHas('customer.address', function ($query) use ($request) {
                        $query->where('country', 'like', "%$request->keyword%")
                            ->orWhere('state', 'like', "%$request->keyword%");
                    })
                    ->orWhereHas('items', function ($query) use ($request) {
                        $query->where('memo', 'like', "%$request->keyword%");
                    });
            });
        }

        if (isset($request->status)) {
            $orders->whereIn('status', is_array($request->status) ? $request->status : explode(',', $request->status));
        }

        if (isset($request->start_at)) {
            $orders->where('created_at', '>=', $request->start_at);
        }

        if (isset($request->end_at)) {
            $orders->where('created_at', '<=', $request->end_at);
        }

        if ($request->mode && sizeof($request->mode) == 1) {
            if ($request->mode[0] === 'delivery') {
                $orders->whereHas('customer');
            } else {
                $orders->whereDoesntHave('customer');
            }
        }

        return $orders;
    }
}
