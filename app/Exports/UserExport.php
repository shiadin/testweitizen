<?php

namespace App\Exports;

use App\User;
use App\Http\Resources\UserResource;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;

class UserExport implements FromQuery, WithHeadings, WithMapping
{
    use Exportable;

    public function __construct($filters)
    {
        $this->filters = array_filter($filters);
    }

    public function headings(): array
    {
        return [
            'ID', 'username', 'name', 'email', 'phone_no', 'credits', 'gender', 'status', 'memo',
            'upline',
            'street', 'city', 'zipcode', 'state', 'country'
        ];
    }

    public function map($user): array
    {
        // parse user
        $user = json_decode(json_encode(new UserResource($user)));
        return [
            $user->id,
            $user->username,
            $user->name,
            $user->email,
            $user->phone_no,
            $user->credits,
            $user->gender,
            $user->situation,
            $user->memo,
            $user->upline ? $user->upline->username : null,
            trim($user->address->street." ".$user->address->street_2),
            $user->address->city,
            $user->address->zipcode,
            $user->address->state,
            $user->address->country
        ];
    }

    public function query()
    {
        $request = (Object)$this->filters;
        $users = User::with(['address', 'upline']);

        if (isset($request->roles)) {
            $users->whereIn('role_id', $request->roles);
        } else {
            $users->whereIn('role_id', [1, 2, 3, 4, 5]);
        }

        if (isset($request->show_admin) && filter_var($request->show_admin, FILTER_VALIDATE_BOOLEAN)) {
            $users->orWhere('role_id', '>=', 900);
        }

        if (isset($request->ids)) {
            $users->whereIn('id', $request->ids);
        }

        if (isset($request->status)) {
            $users->whereIn('status', $request->status);
        }

        if (isset($request->keyword)) {
            $users->where(function ($query) use ($request) {
                $query->where("username", "like", "%$request->keyword%")
                    ->orWhere("name", "like", "%$request->keyword%")
                    ->orWhere("email", "like", "%$request->keyword%")
                    ->orWhere("phone_no", "like", "%$request->keyword%")
                    ->orWhere("gender", "like", "%$request->keyword%")
                    ->orWhere("ic_no", "like", "%$request->keyword%");
            });
        }
        return $users;
    }
}
