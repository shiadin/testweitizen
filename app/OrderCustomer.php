<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderCustomer extends Model
{
    protected $fillable = [
        "user_id", "order_id", "street", "street_2", "zipcode", "city", "state", "country", "name", "phone_no"
    ];

    /**
     * Relationships
     */
    public function address()
    {
        return $this->hasOne('App\Address', 'user_id', 'user_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    
}
