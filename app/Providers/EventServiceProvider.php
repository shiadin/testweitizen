<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        'order.created' => [
            'App\Listeners\OrderListener@created',
        ],
        'order.approved' => [
            'App\Listeners\OrderListener@approved',
        ],
        'order.rejected' => [
            'App\Listeners\OrderListener@rejected',
        ],
        'order.confirmed' => [
            'App\Listeners\OrderListener@confirmed',
        ],
        'order.delivered' => [
            'App\Listeners\OrderListener@delivered',
        ],
        'order.completed' => [
            'App\Listeners\OrderListener@completed',
        ],
        'order.cancelled' => [
            'App\Listeners\OrderListener@cancelled',
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
