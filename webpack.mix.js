const mix = require('laravel-mix');

mix.webpackConfig({
    resolve: {
        alias: {
            components: path.resolve(__dirname, 'resources/js/components'),
            js: path.resolve(__dirname, 'resources/js')
        }
    }
});

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .js('resources/js/user-dashboard.js', 'public/js')
    .js('resources/js/user-inbox.js', 'public/js')
    .js('resources/js/admin-dealer-commision.js', 'public/js')
    .js('resources/js/admin-users-downline.js', 'public/js')
    .js('resources/js/user-overview.js', 'public/js')
    .js('resources/js/user-profile.js', 'public/js')
    .js('resources/js/user-rewards.js', 'public/js')
    .js('resources/js/user-stocks.js', 'public/js')
    .js('resources/js/user-payment.js', 'public/js')
    .js('resources/js/user-customer.js', 'public/js')
    .js('resources/js/user-downlines.js', 'public/js')
    .js('resources/js/user-shippings.js', 'public/js')
    .js('resources/js/user-requests.js', 'public/js')
    .js('resources/js/user-shipping-form.js', 'public/js')
    .js('resources/js/user-postpaid-form.js', 'public/js')
    .js('resources/js/admin-dashboard.js', 'public/js')
    .js('resources/js/admin-workspace.js', 'public/js')
    .js('resources/js/admin-users.js', 'public/js')
    .js('resources/js/admin-stocks.js', 'public/js')
    .js('resources/js/admin-credits.js', 'public/js')
    .js('resources/js/admin-products.js', 'public/js')
    .js('resources/js/admin-withdrawal.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css')
    .version();
