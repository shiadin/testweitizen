@if(session("alert-warning"))
    <p id="notice-alert" class="alert alert-warning alert-dismissible sticky-top m-0 rounded-0 fade show" role="alert">
        <span class="alert-inner--text">{{ session("alert-warning") }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </p>
@endif
@if(session("alert-success"))
    <p id="notice-alert" class="alert alert-success alert-dismissible sticky-top m-0 rounded-0 fade show" role="alert">
        <span class="alert-inner--text">{{ session("alert-success") }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </p>
@endif
@if(session("alert-danger"))
    <p id="notice-alert" class="alert alert-danger alert-dismissible sticky-top m-0 rounded-0 fade show" role="alert">
        <span class="alert-inner--text">{{ session("alert-danger") }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </p>
@endif
