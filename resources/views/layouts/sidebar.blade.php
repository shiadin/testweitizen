<nav class="navbar navbar-vertical fixed-left navbar-expand-md navbar-light bg-white" id="sidenav-main">
    <div class="container-fluid">
        <!-- Toggler -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#sidenav-collapse-main" aria-controls="sidenav-main" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <!-- Brand -->
        <a class="navbar-brand pt-0" href="{{ route('home') }}">
            <img src="{{ asset(config('app.logo')) }}" class="navbar-brand-img" alt="logo">
        </a>
        <!-- User -->
        <ul class="nav align-items-center d-md-none">
            @guest
                <li class="nav-item">
                    <a class="nav-link p-0" href="{{ route('login') }}">{{ __('Login') }}</a>
                </li>
            @else
            <li class="nav-item dropdown">
                <a class="nav-link" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    {{ Auth::user()->username }}
                </a>
                <div class="dropdown-menu dropdown-menu-arrow dropdown-menu-right">
                    <a class="dropdown-item"
                        href="{{ route('logout') }}"
                       onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                        <span>{{ __('Logout') }}</span>
                    </a>

                    <form id="logout-form" class="d-none" action="{{ route('logout') }}" method="POST">
                        @csrf
                    </form>
                </div>
            </li>
            @endguest
        </ul>
        <!-- Collapse -->
        <div class="collapse navbar-collapse" id="sidenav-collapse-main">
            <!-- Collapse header -->
            <div class="navbar-collapse-header d-md-none">
                <div class="row">
                    <div class="col-6 collapse-brand">
                        <a href="{{ route('home') }}">
                            <img src="{{ asset(config('app.logo')) }}">
                        </a>
                    </div>
                    <div class="col-6 collapse-close">
                        <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#sidenav-collapse-main" aria-controls="sidenav-main" aria-expanded="false" aria-label="Toggle sidenav">
                            <span></span>
                            <span></span>
                        </button>
                    </div>
                </div>
            </div>
            @if(isset($sidebar))
            <!-- Navigation -->
            <ul class="navbar-nav">
                <!--  <li class="nav-item">
              <a class="nav-link active" href="#navbar-dashboards" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="navbar-dashboards">
               
                <span class="nav-link-text">Dashboards</span>
              </a>
              <div class="collapse show" id="navbar-dashboards">
                <ul class="nav nav-sm flex-column">
                  <li class="nav-item">
                    <a href="../../pages/dashboards/dashboard.html" class="nav-link">Dashboard</a>
                  </li>
                  <li class="nav-item">
                    <a href="../../pages/dashboards/alternative.html" class="nav-link">Alternative</a>
                  </li>
                </ul>
              </div>
            </li> -->
                @foreach(config('navigations.basic') as $key => $route)
               
                <li class="nav-item">
                    <a href="{{ route($route) }}" class="nav-link {{ $sidebar == $key && !request()->is('admin/*')  ? 'active' : '' }}">{{ ucwords($key) }}</a>
                </li>
        
                @endforeach
            </ul>
            @if(auth()->check() && auth()->user()->isAdmin())
            <!-- Divider -->
            <hr class="my-3">
            <!-- Heading -->
            <h6 class="navbar-heading text-muted">Executive Control Panel</h6>
            <ul class="navbar-nav mb-md-3">
            @foreach(config('navigations.admin') as $key => $route)
                <li class="nav-item">
                    <a href="{{ route($route) }}" class="nav-link {{ $sidebar == $key && request()->is('admin/*') ? 'active' : '' }}">{{ ucwords($key) }}</a>
                </li>
            @endforeach
            </ul>
            @endif
            @if(auth()->check() && auth()->user()->isAdmin(['super']))
            <!-- Divider -->
            <hr class="my-3">
            <!-- Heading -->
            <h6 class="navbar-heading text-muted">Admin Panel</h6>
            <ul class="navbar-nav mb-md-3">
            @foreach(config('navigations.super') as $key => $route)
                <li class="nav-item">
                    <a href="{{ route($route) }}" class="nav-link {{ $sidebar == $key && request()->is('admin/*') ? 'active' : '' }}">{{ ucwords($key) }}</a>
                </li>
            @endforeach
            </ul>
            @endif
            @endif
        </div>
    </div>
</nav>
