<footer class="py-3">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="copyright text-right text-muted">
                    Copyright © {{ date('Y') }} {{ env('COMPANY_NAME') }}. All rights reserved | Dealership eCommerce System Powered By <a class="text-muted" href="http://wrointernational.com/">WRO International Sdn Bhd</a>
                </div>
            </div>
        </div>
    </div>
</footer>
