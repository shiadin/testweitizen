<nav class="navbar navbar-top  @if(!isset($sidebar)) navbar-horizontal @else d-none d-md-block @endif navbar-expand-md navbar-dark" id="navbar-main" style="background: linear-gradient(87deg, rgb(0, 0, 0) 0px, rgb(75, 75, 75) 100%)!important;">
    <div class="container-fluid">
        <!-- Brand -->
        @if(isset($sidebar))
        <a class="h4 mb-0 text-white text-uppercase d-none d-lg-inline-block">{{ $title ?? config('app.name', 'Viinz') }}</a>
        @else
        <a class="navbar-brand ml-3" href="{{ route('home') }}">
            <img src="{{ asset(config('app.logo')) }}">
        </a>
        <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbar-collapse-main" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        @endif
        <!-- User -->
        @if(!isset($sidebar))
        <div class="navbar-collapse collapse" id="navbar-collapse-main">
            <div class="navbar-collapse-header d-md-none">
                <div class="row">
                    <div class="col-6 collapse-brand">
                        <a href="{{ route('home') }}">
                            <img src="{{ asset(config('app.logo')) }}">
                        </a>
                    </div>
                    <div class="col-6 collapse-close">
                        <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbar-collapse-main" aria-controls="sidenav-main" aria-expanded="true" aria-label="Toggle sidenav">
                            <span></span>
                            <span></span>
                        </button>
                    </div>
                </div>
            </div>
        @endif
            <ul class="navbar-nav align-items-center @if(isset($sidebar)) d-none d-md-flex @else ml-auto @endif">
                @guest
                    <li class="nav-item">
                        <a class="nav-link p-0" href="{{ route('login') }}">
                            <span class="nav-link-inner--text">{{ __('Login') }}</span>
                        </a>
                    </li>
                @else

        
               <!--  notification here -->
   <ul class="navbar-nav align-items-center ml-md-auto">
            <li class="nav-item d-xl-none">
              <!-- Sidenav toggler -->
              <div class="pr-3 sidenav-toggler sidenav-toggler-dark" data-action="sidenav-pin" data-target="#sidenav-main">
                <div class="sidenav-toggler-inner">
                  <i class="sidenav-toggler-line"></i>
                  <i class="sidenav-toggler-line"></i>
                  <i class="sidenav-toggler-line"></i>
                </div>
              </div>
            </li>
            <li class="nav-item d-sm-none">
              <a class="nav-link" href="#" data-action="search-show" data-target="#navbar-search-main">
                <i class="ni ni-zoom-split-in"></i>
              </a>
            </li>
            <li class="nav-item dropdown">
              <a   id="demo" class="nav-link" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="ni ni-bell-55" ></i>
              </a>
              <div class="dropdown-menu dropdown-menu-xl dropdown-menu-right py-0 overflow-hidden">
                <!-- Dropdown header -->
                <div class="px-3 py-3">
                  <h6 class="text-sm text-muted m-0">You have <strong class="text-primary">{{auth()->user()->unreadNotifications->count()}}</strong> notifications.</h6>
                </div>
                <!-- List group -->
                <div class="list-group list-group-flush">
              
                    @forelse(auth()->user()->unreadNotifications as $not)
                  <a href="#!" class="list-group-item list-group-item-action">
                    <div class="row align-items-center">
                      <div class="col-auto">
                       
                        <img alt="Image placeholder" src="/storage/{{ $not->data['user']['avatar'] }}" class="avatar rounded-circle">
                      </div>
                      <div class="col ml--2">
                        <div class="d-flex justify-content-between align-items-center">
                          <div>
                            <h4 class="mb-0 text-sm">{{$not->data['user']['name']}}</h4>
                          </div>
                          <div class="text-right text-muted">
                            <small>{{$not->created_at}}</small>
                          </div>
                        </div>
                        <p class="text-sm mb-0">{{$not->data['status']}}</p>
                      </div>
                    </div>
                  </a>
                  @empty

                  @endforelse
                </div>
                <!-- View all -->
                <a href="/inbox" class="dropdown-item text-center text-primary font-weight-bold py-3">View all</a>
              </div>
            </li>
            <li class="nav-item dropdown">
              <a class="nav-link" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="ni ni-ungroup"></i>
              </a>
              <div class="dropdown-menu dropdown-menu-lg dropdown-menu-dark bg-default dropdown-menu-right">
                <div class="row shortcuts px-4">
                  <a href="/inbox" class="col-4 shortcut-item">
                    <span class="shortcut-media avatar rounded-circle bg-gradient-red">
                      <i class="ni ni-bell-55"></i>
                    </span>
                    <small>Inbox</small>
                  </a>
                  <a href="/commission" class="col-4 shortcut-item">
                    <span class="shortcut-media avatar rounded-circle bg-gradient-orange">
                      <i class="ni ni-email-83"></i>
                    </span>
                    <small>Commision</small>
                  </a>
                  <a href="/payments" class="col-4 shortcut-item">
                    <span class="shortcut-media avatar rounded-circle bg-gradient-info">
                      <i class="ni ni-credit-card"></i>
                    </span>
                    <small>Payments</small>
                  </a>
                  <a href="/dashboard" class="col-4 shortcut-item">
                    <span class="shortcut-media avatar rounded-circle bg-gradient-green">
                      <i class="ni ni-books"></i>
                    </span>
                    <small>Reports</small>
                  </a>
                  <a href="/campaign" class="col-4 shortcut-item">
                    <span class="shortcut-media avatar rounded-circle bg-gradient-purple">
                      <i class="ni ni-pin-3"></i>
                    </span>
                    <small>Campagin</small>
                  </a>
                  <a href="/stocks" class="col-4 shortcut-item">
                    <span class="shortcut-media avatar rounded-circle bg-gradient-yellow">
                      <i class="ni ni-basket"></i>
                    </span>
                    <small>Stocks</small>
                  </a>
                </div>
              </div>
            </li>
          </ul>
                <!-- end here -->
                <!-- profile -->
 <ul class="navbar-nav align-items-center ml-auto ml-md-0">
            <li class="nav-item dropdown">
              <a class="nav-link pr-0" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <div class="media align-items-center">
                  <span class="avatar avatar-sm rounded-circle">
                    <img alt="Image placeholder" src="/storage/{{auth()->user()->avatar}}">
                  </span>
                  <div class="media-body ml-2 d-none d-lg-block">
                    <span class="mb-0 text-sm  font-weight-bold">{{ Auth::user()->username }}</span>
                  </div>
                </div>
              </a>
              <div class="dropdown-menu dropdown-menu-right">
                <div class="dropdown-header noti-title">
                  <h6 class="text-overflow m-0">Welcome! </h6>
                </div>
                <a href="/profile" class="dropdown-item">
                  <i class="ni ni-single-02"></i>
                  <span>My profile</span>
                </a>
               <!--  <a href="#!" class="dropdown-item">
                  <i class="ni ni-settings-gear-65"></i>
                  <span>Settings</span>
                </a> -->
                <a href="/inbox" class="dropdown-item">
                  <i class="ni ni-calendar-grid-58"></i>
                  <span>Activity</span>
                </a>
               <!--  <a href="#!" class="dropdown-item">
                  <i class="ni ni-support-16"></i>
                  <span>Support</span>
                </a> -->
                <div class="dropdown-divider"></div>
                   <a class="dropdown-item"
                            href="{{ route('logout') }}"
                           onclick="event.preventDefault(); document.getElementById('logout-form-nav').submit();">
                            <span>{{ __('Logout') }}</span>
                        </a>

                        <form id="logout-form-nav" class="d-none" action="{{ route('logout') }}" method="POST">
                            @csrf
                        </form>
              </div>
            </li>
          </ul>
                <!-- end -->
    
                @endguest
            </ul>
        @if(!isset($sidebar))
        </div>
        @endif
    </div>
</nav>


