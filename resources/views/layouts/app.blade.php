<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ $title ?? config('app.name', 'Weitizen') }}</title>

    <link rel="icon" href="{{ asset('storage/images/favicon.png') }}" sizes="32x32">

    <!-- Fonts -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.2/css/all.css" integrity="sha384-/rXc/GQVaYpyDdyxK+ecHPVYJSN9bmVFBvjA/9eOB+pb3F2w2N6fc5qB9Ew5yIns" crossorigin="anonymous">    <!-- Styles -->
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">
    @yield('style')
    <style>
        .float{
    position:fixed;
    width:60px;
    height:60px;
    bottom:40px;
    right:40px;
    background-color:#25d366;
    color:#FFF;
    border-radius:50px;
    text-align:center;
  font-size:30px;
    box-shadow: 2px 2px 3px #999;
  z-index:100;
}

.my-float{
    margin-top:16px;
}
    </style>
</head>
<body >
    @includeWhen(isset($sidebar), 'layouts.sidebar')
    <div class="main-content">
    
        @includeWhen(isset($sidebar), 'layouts.navbar')
        @include('layouts.alert')
        @yield('content')
    </div>
    {{-- @includeWhen(isset($sidebar), 'layouts.footer') --}}
  
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
<!-- <a href="https://wa.me/601111110018" class="float" target="_blank">
<i class="fa fa-whatsapp my-float"></i>
</a> -->
    <!-- Scripts -->
    <script src="{{ mix('js/app.js') }}"></script>
    <script>
document.getElementById("demo").addEventListener("mouseover", mouseOver);
document.getElementById("demo").addEventListener("mouseout", mouseOut);

function mouseOver() {
  $.ajax({
            'type': 'GET',
            'url': 'markAsRead',
            'dataType': 'json',
            'success': function (data) {
               alert('asasas')
            }
  });
}

function mouseOut() {
  document.getElementById("demo").style.color = "black";
}
</script>
    @yield('script')

    <!--  <footer class="py-5">
    <div class="container">
      <div class="row align-items-center justify-content-xl-between">
        <div class="col-xl-6">
          <div class="copyright text-center text-xl-left text-muted">
            &copy; {{date('Y')}} <a href="" class="font-weight-bold ml-1" target="_blank">Weitizen</a>
          </div>
        </div>
        <div class="col-xl-6">
          <ul class="nav nav-footer justify-content-center justify-content-xl-end">
            <li class="nav-item">
              <a href="https://app.weitizen.com" class="nav-link" target="_blank">Contact Us</a>
            </li>
            <li class="nav-item">
              <a href="https://app.weitizen.com" class="nav-link" target="_blank">Privacy Policy</a>
            </li>
            <li class="nav-item">
              <a href="https://app.weitizen.com" class="nav-link" target="_blank">Vendor Agreements</a>
            </li>
            <li class="nav-item">
              <a href="https://app.weitizen.com" class="nav-link" target="_blank">Weitizen Marketplace</a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </footer> -->
</body>
</html>
