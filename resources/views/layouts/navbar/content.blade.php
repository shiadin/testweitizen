<!-- Brand -->
<a class="navbar-brand ml-3 pt-0 {{ isset($isSidebar) && $isSidebar ? '' : 'd-none d-lg-inline-block' }}">
    <img src="{{ asset(config('app.logo')) }}" width="40" height="40" alt="logo">
</a>

<!-- User -->
<ul class="nav align-items-center {{ isset($isSidebar) && $isSidebar ? 'd-md-none' : 'd-none d-lg-inline-block' }}">
    @guest
        <li class="nav-item dropdown">
            <a class="nav-link py-1" href="{{ route('login') }}">{{ __('Login') }}</a>
        </li>
    @else
    <li class="nav-item dropdown">
        <a class="nav-link" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
           <span class="d-md-none">Hello, </span>{{ Auth::user()->username }} <span class="caret"></span>
        </a>

        <div class="dropdown-menu dropdown-menu-arrow dropdown-menu-right">
            <a class="dropdown-item" href="{{ route('logout') }}"
               onclick="event.preventDefault();
                             document.getElementById('logout-form').submit();">
                {{ __('Logout') }}
            </a>

            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
        </div>
    </li>
    @endguest
</ul>
