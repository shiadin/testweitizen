<div class="col-12 col-md-3 col-xl-2 ct-sidebar">
    <nav class="collapse ct-links" id="ct-docs-nav">
        <div class="ct-toc-item active">
            <ul class="nav ct-sidenav">
                @foreach(config('navigations.basic') as $key => $route)
                <li>
                    <a href="{{ route($route) }}" class="{{ $sidebar == $key ? 'active' : '' }}">{{ ucwords($key) }}</a>
                </li>
                @endforeach
                @if(auth()->check() && auth()->user()->isAdmin())
                @foreach(config('navigations.admin') as $key => $route)
                <li>
                    <a href="{{ route($route) }}" class="{{ $sidebar == $key ? 'active' : '' }}">{{ ucwords($key) }}</a>
                </li>
                @endforeach
                @endif
            </ul>
        </div>
    </nav>
</div>
