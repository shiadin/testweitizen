@extends('layouts.app')

@section('content')

<div class="container-fluid d-flex justify-content-center align-items-center" style="min-height: calc(100vh); background: url({{asset(env('APP_BACK'))}})">
    <div class="row justify-content-center">
        <div class="col-lg-8 col-md-12 mt-lg-3">
            <div class="card bg-secondary border-0 rounded-0" style="box-shadow: 0 14px 28px rgba(0,0,0,0.25), 0 10px 10px rgba(0,0,0,0.22);">
                {{-- <div class="card-header">{{ __('OTP Verification') }}</div> --}}

                <div class="card-body p-0">


                    <div class="row">
                        <div class="col-sm-12 col-md-4 px-5 py-7" style="background-color: #3f51b5;">
                            <h1 class="text-white">{{ env('AUTH_TITLE') }}</h1>
                            <p style="color: #99b2c5">{{ env('AUTH_BODY') }}</p>
                            <div class="text-center">
                                <a target="_blank" href="{{ env('AUTH_LINK') }}" class="btn btn-pill btn-warning">{{ env("AUTH_BUTTON") }}</a>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-8 px-5 pt-7 pb-3 bg-white d-flex flex-column justify-content-between">

        
                
                            <div class="text-center mb-3">
                                <img width="80" src="{{ config('app.logo') }}" alt="logo" class="img-fluid">
                            </div>
    <form method="POST" action="{{ route('verifyOTP') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Enter OTP Code') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="OTP" required autofocus>

                                
                            </div>
                        </div>

             

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Verify') }}
                                </button>
                            </div>
                        </div>
                    </form>

  <div class="container">
                  <form method="POST" action="{{ route('resendOTP') }}">
                        @csrf

                   <div class="row">
                     <div class="col-md-3">
                     </div>
                    <div class="col-md-3">
 <button type="submit" class="btn btn-dark btn-sm">
                                    {{ __('Resend Code Via') }}
                                </button>
                    </div>
                    <div class="col-md-3"> 
                       <div class="orm-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="otp_via" id="sms" value="via_sms">

                                    <label class="form-check-label" for="sms">
                                        {{ __('Via SMS') }}
                                    </label>
                                </div></div>

                                <div class="col-md-3"> 
                               <div class="form-check form-check-inline">
                                    <input class="form-check-input" checked="" type="radio" name="otp_via" id="otpemail" value="via_email">

                                    <label class="form-check-label" for="otpemail">
                                        {{ __('Via Email') }}
                                    </label>
                                </div>
                                </div>

                       
                   </div>
                       
                    



                      
                     </form>
                     </div>
                            <div class="copyright text-center text-muted">
                                Copyright © {{ date('Y') }} {{ env('COMPANY_NAME') }}. All rights reserved <br> Web System Powered By <a class="text-muted" href="http://wrointernational.com/">WRO International</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


       
@stop



