@extends('layouts.app')

@section('content')

<div class="container-fluid d-flex justify-content-center align-items-center" style="min-height: calc(100vh); background: url({{asset(env('APP_BACK'))}})">
    <div class="row justify-content-center">
        <div class="col-lg-8 col-md-12 mt-lg-3">
            <div class="card bg-secondary border-0 rounded-0" style="box-shadow: 0 14px 28px rgba(0,0,0,0.25), 0 10px 10px rgba(0,0,0,0.22);">
                {{-- <div class="card-header">{{ __('Login') }}</div> --}}

                <div class="card-body p-0">
                    <div class="row">
                        <div class="col-sm-12 col-md-4 px-5 py-7" style="background-color: #3f51b5;">
                            <h1 class="text-white">{{ env('AUTH_TITLE') }}</h1>
                            <p style="color: #99b2c5">{{ env('AUTH_BODY') }}</p>
                            <div class="text-center">
                                <a target="_blank" href="{{ env('AUTH_LINK') }}" class="btn btn-pill btn-warning">{{ env("AUTH_BUTTON") }}</a>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-8 px-5 pt-7 pb-3 bg-white d-flex flex-column justify-content-between">
                            <div class="text-center mb-3">
                                <img width="80" src="{{ config('app.logo') }}" alt="logo" class="img-fluid">
                            </div>
  
                

                            <form method="POST" action="{{ route('login') }}">
                                @csrf
                                <input type="hidden" name="sign_via" value="via_sms">
                                <div class="form-group">
                                    <div class="input-group input-group-alternative">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="ni ni-circle-08"></i></span>
                                        </div>
                     <input class="form-control" placeholder="{{ __('Phone Number i.e 0163345434') }}" name="phone_no"   type="text" value="1" min="1" id="number"  required autofocus>
                                    </div>
                                    
                                </div>
                         
                       

                             
                               
                                  


                                

                                <div class="text-center">
                                    <button type="submit" class="btn btn-primary btn-pill my-4">{{ __('Send Verification Code') }}</button>
                                </div>
                              
                            </form>
          <center>--OR--</center>
          <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="btn btn-dark btn-sm btn-block">
    Login With Email
</a>

                       
                           

    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
    {{ csrf_field() }}
</form>

                            <div class="copyright text-center text-muted">
                                Copyright © {{ date('Y') }} {{ env('COMPANY_NAME') }}. All rights reserved <br> Web System Powered By <a class="text-muted" href="http://wrointernational.com/">WRO International</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
