@extends('layouts.app')

@section('content')
<div class="container-fluid d-flex justify-content-center align-items-center" style="min-height: calc(100vh);background: url({{asset(env('APP_BACK'))}})">
    <div class="row justify-content-center">
        <div class="col-lg-8 col-md-12 mt-lg-3">
            <div class="card bg-secondary border-0 rounded-0" style="box-shadow: 0 14px 28px rgba(0,0,0,0.25), 0 10px 10px rgba(0,0,0,0.22);">
                <div class="card-body p-0">
                    <div class="row">
                        <div class="col-sm-12 col-md-4 px-5 py-7" style="background-color: #3f51b5;">
                            <h1 class="text-white">{{ env('AUTH_TITLE') }}</h1>
                            <p style="color: #99b2c5">{{ env('AUTH_BODY') }}</p>
                            <div class="text-center">
                                <a target="_blank" href="{{ env('AUTH_LINK') }}" class="btn btn-pill btn-warning">{{ env("AUTH_BUTTON") }}</a>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-8 px-5 pt-3 pb-3 bg-white d-flex flex-column justify-content-between">
                            <center><h3 class="card-title mb-5">{{ __('Register') }} As {{ $referral->role->name }} </h3></center>
                            <div class="text-center mb-3">
                                <img width="80" src="{{ asset(config('app.logo')) }}" alt="logo" class="img-fluid">
                            </div>
                            @if($referral->user)
                            <div class="mb-1">
                               <center> <h4 >
                                    Upline: {{ $referral->user->name }} ({{ $referral->user->username }})
                                </h4></center>
                            </div>
                            @endif
                            <form method="POST" action="{{ route('register', $code) }}">
                                @csrf
                     
                                <div class="form-group">
                                    <div class="input-group input-group-alternative">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="ni ni-circle-08"></i></span>
                                        </div>
                                        <input class="form-control {{ $errors->has('username') ? ' is-invalid' : '' }}" placeholder="{{ __('Username') }}" name="username" type="text" value="{{ old('username') }}" required autofocus>
                                    </div>
                                    @if ($errors->has('username'))
                                        <span class="invalid-feedback d-block" role="alert">
                                            <strong>{{ $errors->first('username') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <!-- phone number -->
                                    

                                              <div class="form-group">
                                    <div class="input-group input-group-alternative">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="ni ni-mobile-button"></i></span>
                                        </div>
            <input class="form-control" placeholder="{{ __('Phone Number ') }}" name="phone_no" value="{{ old('phone_no') }}"   type="text"   id="number"  required autofocus>
                                    </div>
                                    @if ($errors->has('phone_no'))
                                        <span class="invalid-feedback d-block" role="alert">
                                            <strong>{{ $errors->first('phone_no') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <!-- end phone number -->

                                <div class="form-group">
                                    <div class="input-group input-group-alternative">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="ni ni-email-83"></i></span>
                                        </div>
                                        <input class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="{{ __('E-Mail Address') }}" name="email" type="email" value="{{ old('email') }}" required>
                                    </div>
                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback d-block" role="alert">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div class="form-group">
                                    <div class="input-group input-group-alternative">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="ni ni-lock-circle-open"></i></span>
                                        </div>
                                        <input class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="{{ __('Password') }}" name="password" type="password" value="{{ old('password') }}" required>
                                    </div>
                                    @if ($errors->has('password'))
                                        <span class="invalid-feedback d-block" role="alert">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div class="form-group">
                                    <div class="input-group input-group-alternative">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="ni ni-key-25"></i></span>
                                        </div>
                                        <input class="form-control {{ $errors->has('password_confirmation') ? ' is-invalid' : '' }}" placeholder="{{ __('Confirm Password') }}" name="password_confirmation" type="password" value="{{ old('password_confirmation') }}" required>
                                    </div>
                                    @if ($errors->has('password_confirmation'))
                                        <span class="invalid-feedback d-block" role="alert">
                                            <strong>{{ $errors->first('password_confirmation') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <div class="custom-control custom-checkbox mb-3">
                                        <input name="agreement" class="custom-control-input {{ $errors->has('agreement') ? ' is-invalid' : '' }}" id="agreement" type="checkbox" {{ !old('agreement') ?: 'checked' }}>
                                        <label class="custom-control-label" for="agreement">I agree to the <a target="_blank" href="{{ env('TERMS_CONDITIONS') }}">Terms of Service</a></label>
                                    </div>
                                    @if ($errors->has('agreement'))
                                    <div class="invalid-feedback d-block" role="alert">
                                        <strong>You must agree before register.</strong>
                                    </div>
                                    @endif
                                </div>
                                <div class="text-center">
                                    <button type="submit" class="btn btn-primary btn-pill my-4">{{ __('Register') }}</button>
                                </div>
                            </form>
                            <div class="copyright text-center text-muted">
                                Copyright © {{ date('Y') }} {{ env('COMPANY_NAME') }}. All rights reserved <br> Web System Powered By <a class="text-muted" href="http://wrointernational.com/">WRO International</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
