@extends('layouts.app')

@section('content')

<div class="container-fluid d-flex justify-content-center align-items-center" style="min-height: calc(100vh); background: url({{asset(env('APP_BACK'))}})">
    <div class="row justify-content-center">
        <div class="col-lg-8 col-md-12 mt-lg-3">
            <div class="card bg-secondary border-0 rounded-0" style="box-shadow: 0 14px 28px rgba(0,0,0,0.25), 0 10px 10px rgba(0,0,0,0.22);">
                {{-- <div class="card-header">{{ __('Login') }}</div> --}}

                <div class="card-body p-0">
                    <div class="row">
                        <div class="col-sm-12 col-md-4 px-5 py-7" style="background-color: #3f51b5;">
                            <h3 class="text-white">{{ env('AUTH_TITLE') }}</h3>
                            <p style="color: #99b2c5; text-align: left!important">{{ env('AUTH_BODY') }}</p>
                            <div class="text-center">
                                <a target="_blank" href="{{ env('AUTH_LINK') }}" class="btn btn-pill btn-warning">{{ env("AUTH_BUTTON") }}</a>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-8 px-5 pt-7 pb-3 bg-white d-flex flex-column justify-content-between">
                            <div class="text-center mb-3">
                                <img width="200px" src="{{ config('app.logo') }}" alt="logo" class="img-fluid">
                            </div>
                            <form method="POST" action="{{ route('login') }}">
                                @csrf
            <input type="hidden" name="sign_via" value="email">
                                <div class="form-group">
                                    <div class="input-group input-group-alternative">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="ni ni-circle-08"></i></span>
                                        </div>
                                        <input class="form-control {{ $errors->has('username') ? ' is-invalid' : '' }}" placeholder="{{ __('Username') }}" name="username" type="text" value="{{ old('username') }}" required autofocus>
                                    </div>
                                    @if ($errors->has('username'))
                                        <span class="invalid-feedback d-block" role="alert">
                                            <strong>{{ $errors->first('username') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <div class="input-group input-group-alternative">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="ni ni-lock-circle-open"></i></span>
                                        </div>
                                        <input class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="{{ __('Password') }}" name="password" type="password" value="{{ old('password') }}" required>
                                    </div>
                                    @if ($errors->has('password'))
                                        <span class="invalid-feedback d-block" role="alert">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="custom-control custom-control-alternative custom-checkbox">
                                    <input class="custom-control-input" name="terms_and_condition" required="" id="terms" type="checkbox" {{ old('terms') ? 'checked' : '' }}>
                                    <label class="custom-control-label" for="terms">
                                        <span class="text-muted">
                                            {{ __('I agree to the terms of ') }}
                                            <a href="" target="_blank">{{ __('Services and Rules of Conduct.') }}</a>
                                        </span>
                                    </label>
                                </div>
                                <div class="custom-control custom-control-alternative custom-checkbox">
                                    <input class="custom-control-input" name="remember" id="remember" type="checkbox" {{ old('remember') ? 'checked' : '' }}>
                                    <label class="custom-control-label" for="remember">
                                        <span class="text-muted">{{ __('Remember Me') }}</span>
                                    </label>
                                </div>

                       
                               
                                  


                                

                                <div class="text-center">
                    <button type="submit" class="btn btn-primary my-4">{{ __('Login') }}</button>
                                </div>
                                <div class="text-center">
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        <small>{{ __('Forgot Your Password?') }}</small>
                                    </a>
                                </div>
                            </form>
 <!--  <center>--OR--</center>
                       <a href="/login-with-phone" class="btn btn-dark btn-sm btn-block">Login With Phone</a> -->
                           

                            <div class="copyright text-center text-muted">
                                Copyright © {{ date('Y') }} <a href="http://weitizen.com/">weitizen.com</a>. All rights reserved <br> Dealership eCommerce System Powered <a class="text-muted" href="http://wrointernational.com/">WRO International Sdn Bhd.</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
