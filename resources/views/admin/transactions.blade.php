@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
         <div class="col-12 pb-7" style="background: linear-gradient(87deg, rgb(0, 0, 0) 0px, rgb(75, 75, 75) 100%)!important;">
         <div class="row">
         	@foreach($stats as $s)
        <div class="col-xl-4 col-md-6">
          <div class="card bg-gradient-{{$s['color']}} border-0">
            <!-- Card body -->
            <div class="card-body">
              <div class="row">
                <div class="col">
                  <h5 class="card-title text-uppercase text-muted mb-0 text-white">{{$s['label']}}</h5>
                  <span class="h2 font-weight-bold mb-0 text-white">{{$s['value']}}</span>
                  <div class="progress progress-xs mt-3 mb-0">
                    <div class="progress-bar bg-success" role="progressbar" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100" style="width: {{$s['progressbar']}}%;"></div>
                  </div>
                </div>
                <div class="col-auto">
                  <button type="button" class="btn btn-sm btn-neutral mr-0" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fas fa-ellipsis-h"></i>
                  </button>
                  <div class="dropdown-menu dropdown-menu-right">
                    <a class="dropdown-item" href="{{$s['url']}}">See All</a>
                   
                  </div>
                </div>
              </div>
              <p class="mt-3 mb-0 text-sm">
                <a href="{{$s['url']}}" class="text-nowrap text-white font-weight-600">See details</a>
              </p>
            </div>
          </div>
        </div>
       @endforeach
      
      </div>
         </div>
        <div class="col-12 mb-3" style="margin-top: -4em">
           <!-- start -->
<div class="row">
        <div class="col">
          <div class="card">
            <!-- Card header -->
            <div class="card-header border-0">
              <h3 class="mb-0">All transactions</h3>
            </div>
            <!-- Light table -->
            <div class="table-responsive">
              <table class="table align-items-center table-flush">
                <thead class="thead-light">
                  <tr>
                    <th scope="col">Product</th>
                    <th scope="col">Order ID</th>
                     <th scope="col">Transaction ID</th>
                    <th scope="col">Status</th>
                    <th scope="col">User</th>
                    <th scope="col">Unit Price</th>
                     <th scope="col">Quantity</th>
                    <th scope="col">Amount</th>
                   
                    <th scope="col">Paid at</th>
                  </tr>
                </thead>
                <tbody class="list">
                	@forelse($transactions as $trans)
                  <tr>
                    <th scope="row">
                      <div class="media align-items-center">
                        <a href="#" class="avatar rounded-circle mr-3">
                          <img alt="Image placeholder" src="/storage/{{$trans->product->image ?? ''}}">
                        </a>
                        <div class="media-body">
                          <span class="name mb-0 text-sm">{{$trans->product->name ?? ''}}</span>
                        </div>
                      </div>
                    </th>
                    <td class="budget">
                     {{$trans->order_id}}
                    </td>
                    <td>
                      {{$trans->molpay->transaction_id ?? ''}}
                    </td>
                    <td>
                    @if($trans)
                     Completed
                    @else
                     Failed
                    @endif
                    </td>
                    <td>
                  {{$trans->user->name ?? ''}}
                    </td>
                    <td >
                    
                    {{$trans->item->unit_price ?? ''}}
                    	 </td>
                    	 <td >
                    {{$trans->item->quantity ?? ''}}
                    </td>
                    	 <td> 
                    {{$trans->item->unit_price*$trans->item->quantity}}
                    </td>
                     
                      <td >
                    {{$trans->created_at}}
                    </td>
                  </tr>
               @empty
            
               @endforelse

                </tbody>
              </table>
            </div>
            <!-- Card footer -->
            <div class="card-footer py-4">
              <nav aria-label="...">
                <ul class="pagination justify-content-end mb-0">
                 {{$transactions}}
                </ul>
              </nav>
            </div>
          </div>
        </div>
      </div>
           <!-- end -->
            </div>
        </div>
    </div>
@endsection

@section('script')
<!-- <script src="{{ mix('js/admin-products.js') }}"></script> -->
@endsection
