@extends('layouts.app')

@section('content')
<input type="hidden" id="routes-data" value='@json($routes)'>
<div id="admin-products">
    <container :routes='routes'></container>
</div>
@endsection

@section('script')
<script src="{{ mix('js/admin-products.js') }}"></script>
@endsection
