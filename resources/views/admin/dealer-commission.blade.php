@extends('layouts.app')

@section('content')
<input type="hidden" id="routes-data" value='@json($routes)'>
<input type="hidden" id="commission-data" value='@json($commision)'>
<div id="commission-users">
    <container></container>
</div>
@stop

@section('script')
<script src="{{ mix('js/admin-dealer-commision.js') }}"></script>
@stop
