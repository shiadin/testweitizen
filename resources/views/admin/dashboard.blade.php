@extends('layouts.app')

@section('content')
<div id="admin-dashbaord">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 pb-7" style="background: linear-gradient(87deg, rgb(0, 0, 0) 0px, rgb(75, 75, 75) 100%)!important;">
                <div class="row">
                    <div class="col-md-3 col-sm-12 mb-3">
                        <div class="card card-stats">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col">
                                        <h5 class="card-title text-uppercase text-muted mb-0">{{ env('CREDIT_NAME') }}</h5>
                                        <span class="h2 font-weight-bold mb-0" data-toggle="tooltip" data-placement="top" title="Credits in Network">{{ $currency." ".$credits['total'] }}</span>
                                    </div>
                                    <div class="col-auto">
                                        <div class="icon icon-shape bg-primary text-white rounded-circle shadow">
                                            <i class="fas fa-coins"></i>
                                        </div>
                                    </div>
                                </div>
                                <p class="mt-3 mb-0 text-muted text-sm">
                                    <span class="@if($credits['monthsDifference'] > 0) text-success @else text-danger  @endif">
                                        <i class="fa @if($credits['monthsDifference'] > 0) text-success fa-arrow-up @else text-danger fa-arrow-down @endif"></i>
                                        {{ $currency." ".$credits['currentMonth'] }}
                                    </span>
                                    <span class="text-nowrap">approved this month</span>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-12 mb-3">
                        <div class="card card-stats">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col">
                                        <h5 class="card-title text-uppercase text-muted mb-0">Shipping Orders</h5>
                                        <span class="h2 font-weight-bold mb-0" data-toggle="tooltip" data-placement="top" title="Total Orders Processed">{{ $shippings['total'] }}</span>
                                    </div>
                                    <div class="col-auto">
                                        <div class="icon icon-shape bg-success text-white rounded-circle shadow">
                                            <i class="fas fa-shipping-fast"></i>
                                        </div>
                                    </div>
                                </div>
                                <p class="mt-3 mb-0 text-muted text-sm">
                                    <span class="@if($shippings['monthsDifference'] > 0) text-success @else text-danger  @endif">
                                        <i class="fa @if($shippings['monthsDifference'] > 0) text-success fa-arrow-up @else text-danger fa-arrow-down @endif"></i>
                                        {{ $shippings['currentMonth'] }}
                                    </span>
                                    <span class="text-nowrap">created this month</span>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-12 mb-3">
                        <div class="card card-stats">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col">
                                        <h5 class="card-title text-uppercase text-muted mb-0">Stocks</h5>
                                        <span class="h2 font-weight-bold mb-0" data-toggle="tooltip" data-placement="top" title="Stocks in Network">{{ $stocks['total'] }}</span>
                                    </div>
                                    <div class="col-auto">
                                        <div class="icon icon-shape bg-info text-white rounded-circle shadow">
                                            <i class="fas fa-boxes"></i>
                                        </div>
                                    </div>
                                </div>
                                <p class="mt-3 mb-0 text-muted text-sm">
                                    <span class="@if($stocks['monthsDifference'] > 0) text-success @else text-danger  @endif">
                                        <i class="fa @if($stocks['monthsDifference'] > 0) text-success fa-arrow-up @else text-danger fa-arrow-down @endif"></i>
                                        {{ $stocks['currentMonth'] }} units
                                    </span>
                                    <span class="text-nowrap">approved this month</span>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-12 mb-3">
                        <div class="card card-stats">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col">
                                        <h5 class="card-title text-uppercase text-muted mb-0">Users</h5>
                                        <span class="h2 font-weight-bold mb-0" data-toggle="tooltip" data-placement="top" title="Total Users Registered">{{ $users['total'] }}</span>
                                    </div>
                                    <div class="col-auto">
                                        <div class="icon icon-shape bg-warning text-white rounded-circle shadow">
                                            <i class="fas fa-users"></i>
                                        </div>
                                    </div>
                                </div>
                                <p class="mt-3 mb-0 text-muted text-sm">
                                    <span class="@if($users['monthsDifference'] > 0) text-success @else text-danger  @endif">
                                        <i class="fa @if($users['monthsDifference'] > 0) text-success fa-arrow-up @else text-danger fa-arrow-down @endif"></i>
                                        {{ $users['currentMonth'] }} person
                                    </span>
                                    <span class="text-nowrap">registered this month</span>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 mb-3" style="margin-top: -4em">
                <div class="row">
                    <div class="col-md-6 col-sm-12">
                        <div class="card">
                         
                            <div class="card-body">
                                
                                    
                                     {!! $chart_user->html() !!}
                               
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12">
                     
                        <div class="card">
                            <div class="card-body">
                               
                                 {!! $chart_sale->html() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- start here -->

       <!--  end here -->

       <!-- start -->
 <div class="row" style="margin-top: 2em">
        <div class="col-xl-6">
          <div class="card">
            <div class="card-header border-0">
              <div class="row align-items-center">
                <div class="col">
                  <h3 class="mb-0">E-wallet  </h3>
                </div>
                <div class="col text-right">
                  <a href="/admin/credits" class="btn btn-sm btn-primary">See all</a>
                </div>
              </div>
            </div>
            <div class="table-responsive">
              <!-- Projects table -->
              <table class="table align-items-center table-flush">
                <thead class="thead-light">
                  <tr>
                    <th scope="col">Dealer name</th>
                    <th scope="col">Level</th>
                    <th scope="col">Total Balance</th>
                  
                  </tr>
                </thead>
                <tbody>
                    @forelse($wallet as $item)
                  <tr>
                    <th scope="row">
                     {{$item->user->name ?? ''}}
                    </th>
                    <td>
                     {{$item->user->role->name ?? ''}}
                    </td>
                     <td>
                     RM {{$item->balance ?? ''}}
                    </td>
                   
                   
                  </tr>
              @empty
              No Sales to customer data yet
              @endforelse
                  
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <div class="col-xl-6">
          <div class="card">
            <div class="card-header border-0">
              <div class="row align-items-center">
                <div class="col">
                  <h3 class="mb-0">Dealer’s Recruit Performance </h3>
                </div>
                <div class="col text-right">
                  <a href="/downlines" class="btn btn-sm btn-primary">See all</a>
                </div>
              </div>
            </div>
            <div class="table-responsive"> 
              <!-- Projects table -->
              <table class="table align-items-center table-flush">
                <thead class="thead-light">
                  <tr>
                    <th scope="col">Dealer</th>
                    <th scope="col">Total Recruit</th>
                  
                  </tr>
                </thead>
                <tbody>
                         @forelse($deler_perfomaces as $item)
                  <tr>
                    <th scope="row">
                     {{$item->name }}
                    </th>
                    <td>
                     {{$item->downlineCount()}}
                    </td>
                   
                   
                  </tr>
              @empty
              No downline data  yet
              @endforelse
              
                 
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
       <!-- end -->
    </div>
</div>
@stop

@section('script')
<script src="{{ mix('js/admin-dashboard.js') }}"></script>
{!! Charts::scripts() !!}
{!! $chart_user->script() !!}
{!! $chart_sale->script() !!}
@stop
