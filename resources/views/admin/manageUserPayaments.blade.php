@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
          <div class="col-12 pb-7 bg-gradient-danger">
         <div class="row">
         	
      
      </div>
         </div>
        <div class="col-12 mb-3" style="margin-top: -4em">
           <!-- start -->
  <div class="row">
        <div class="col-lg-6">
          <div class="card-wrapper">
            <!-- Input groups -->
           <div class="card">
            <div class="card-header">
              <h3 class="mb-0">User Details</h3>
            </div>
            <div class="card-body">
            
              <div class="row py-3 align-items-center">
             <div class="col-sm-3">
                  <small class="text-uppercase text-muted font-weight-bold">Name</small>
                </div>
                <div class="col-sm-9">
                  <h5 class="mb-0">{{$trans->user->name ?? ''}}  </h5>
                </div>
              </div>

                  <div class="row py-3 align-items-center">
             <div class="col-sm-3">
                  <small class="text-uppercase text-muted font-weight-bold">Bank</small>
                </div>
                <div class="col-sm-9">
                  <h5 class="mb-0">{{$trans->user->bank->bank ?? ''}}  </h5>
                </div>
              </div>
             

                 <div class="row py-3 align-items-center">
             <div class="col-sm-3">
                  <small class="text-uppercase text-muted font-weight-bold">Account Number</small>
                </div>
                <div class="col-sm-9">
                  <h5 class="mb-0">{{$trans->user->bank->account ?? ''}}  </h5>
                </div>
              </div>

                  <div class="row py-3 align-items-center">
             <div class="col-sm-3">
                  <small class="text-uppercase text-muted font-weight-bold">Account Name</small>
                </div>
                <div class="col-sm-9">
                  <h5 class="mb-0">{{$trans->user->bank->name ?? ''}}  </h5>
                </div>
              </div>

        <div class="row py-3 align-items-center">
             <div class="col-sm-3">
                  <small class="text-uppercase text-muted font-weight-bold">User want</small>
                </div>
                <div class="col-sm-9">
                  <h5 class="mb-0">{{$trans->user->bank->type_of_withdral ?? ''}}  </h5>
                </div>
              </div>

               <div class="row py-3 align-items-center">
             <div class="col-sm-3">
                  <small class="text-uppercase text-muted font-weight-bold">Earned Ammount</small>
                </div>
                <div class="col-sm-9">
                  <h5 class="mb-0">MYR {{$trans->balance }}  </h5>
                </div>
              </div>

                      <div class="row py-3 align-items-center">
             <div class="col-sm-3">
                  <small class="text-uppercase text-muted font-weight-bold">Earned For</small>
                </div>
                <div class="col-sm-9">
                  <h5 class="mb-0"> {{$firstDayOfLastMonth}} to  {{$lastDayOfLastMonth }}  </h5>
                </div>
              </div>

            </div>
          </div>
          
          
          
          </div>
        </div>
        <div class="col-lg-6">
          <div class="card-wrapper">
            
        
         
            <!-- Dropzone -->
            <div class="card">
              <!-- Card header -->
              <div class="card-header">
                <h3 class="mb-0">Action Section</h3>
                <small>completed by {{auth()->user()->name}}</small>
              </div>
              <!-- Card body -->
              <div class="card-body">

           <form action="/admin/completUserPayaments" method="POST" enctype="multipart/form-data">
            @csrf
                <div class="form-group">
                <label class="form-control-label" for="example3cols1Input">Amount</label>
                <input name="amount" type="text" class="form-control" id="example3cols1Input" value="{{$trans->balance}}" readonly="">
              </div>

                <div class="form-group">
                <label class="form-control-label" for="example3cols1Input">Description</label>
                <input name="meta[3]" type="text" class="form-control" id="example3cols1Input" placeholder="Last Month withdrawal" value="Last Month withdrawal {{$firstDayOfLastMonth}} -  {{$lastDayOfLastMonth }}">
              </div>

                <!-- Single -->
                <div class="dropzone dropzone-single mb-3" data-toggle="dropzone" data-dropzone-url="http://">
                  <div class="fallback">
                    <div class="custom-file">
                      <input name="slip" type="file" class="custom-file-input" id="projectCoverUploads">
                      <label class="custom-file-label" for="projectCoverUploads">Choose payment slip file</label>
                    </div>
                  </div> 
                </div>
                <input type="hidden" name="meta[1]" value="{{auth()->user()->id}}">
                <input type="hidden" name="meta[2]" value="{{$trans->id}}">
                <input type="hidden" name="meta[4]" value="{{$lastDayOfLastMonth}}">
                 <div class="form-group">
                  @if($trans->balance>0 && $trans->user->bank)
                  @if($trans->user->bank->type_of_withdral ==='Withdrawal the money by monthly basic')
              <button type="submit" class="btn btn-block btn-success">Complete Transaction</button>
              @else
              {{$trans->user->bank->type_of_withdral}}
              @endif
              @endif
              @if (session()->has('success_message'))
    <br><br><br>
        <div class="alert alert-success">
    <h5>{{ session()->get('success_message') }}</h5>
                    </div>
    @endif
                
 @if (session()->has('error_message'))
 <br><br><br>
 <div class="alert alert-danger">
     <h5>{{ session()->get('error_message') }}</h5>
                    </div>
@endif

                 </div>

                </form>
               
              </div>
            </div>
          </div>
        </div>
      </div>
           <!-- end -->
            </div>
        </div>
    </div>
@endsection

@section('script')
<!-- <script src="{{ mix('js/admin-products.js') }}"></script> -->
@endsection
