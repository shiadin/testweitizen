@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
          <div class="col-12 pb-7" style="background: linear-gradient(87deg, rgb(0, 0, 0) 0px, rgb(75, 75, 75) 100%)!important;">
         <div class="row">
         	@foreach($stats as $s)
        <div class="col-xl-4 col-md-6">
          <div class="card bg-gradient-{{$s['color']}} border-0">
            <!-- Card body -->
            <div class="card-body">
              <div class="row">
                <div class="col">
                  <h5 class="card-title text-uppercase text-muted mb-0 text-white">{{$s['label']}}</h5>
                  <span class="h2 font-weight-bold mb-0 text-white">{{$s['value']}}</span>
                  <div class="progress progress-xs mt-3 mb-0">
                    <div class="progress-bar bg-success" role="progressbar" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100" style="width: {{$s['progressbar']}}%;"></div>
                  </div>
                </div>
                <div class="col-auto">
                  <button type="button" class="btn btn-sm btn-neutral mr-0" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fas fa-ellipsis-h"></i>
                  </button>
                  <div class="dropdown-menu dropdown-menu-right">
                    <a class="dropdown-item" href="{{$s['url']}}">See All</a>
                   
                  </div>
                </div>
              </div>
              <p class="mt-3 mb-0 text-sm">
                <a href="{{$s['url']}}" class="text-nowrap text-white font-weight-600">See details</a>
              </p>
            </div>
          </div>
        </div>
       @endforeach
      
      </div>
         </div>
        <div class="col-12 mb-3" style="margin-top: -4em">
           <!-- start -->
<div class="row">
        <div class="col">
          <div class="card">
            <!-- Card header -->
            <div class="card-header border-0">
              <h3 class="mb-0">Transactions</h3>
            </div>
            <!-- Light table -->
            <div class="table-responsive">
              <table class="table align-items-center table-flush">
                <thead class="thead-light">
                  <tr>
                    <th scope="col">Name</th>
                    <th scope="col">Email</th>
                     <th scope="col">Phone</th>
                     <th>Amount</th>
                       <th>User prefference</th>
                    <th scope="col">Action</th>
                    
                   
                    
                  </tr>
                </thead>
                <tbody class="list">
                	@forelse($transactions as $trans)
              @if($trans->balance>100)
                  <tr>
                    <th scope="row">
                      <div class="media align-items-center">
                        <a href="#" class="avatar rounded-circle mr-3">
                          <img alt="Image placeholder" src="/storage/{{$trans->user->avatar ?? ''}}">
                        </a>
                        <div class="media-body">
                          <span class="name mb-0 text-sm">{{$trans->user->name ?? ''}}</span>
                        </div>
                      </div>
                    </th>
                     <td class="budget">
                     {{$trans->user->email ?? '' }}
                    </td>
                    <td class="budget">
                    {{$trans->user->phone_no ?? '' }}
                    </td>
                    <td>
                      RM {{$trans->balance }}
                    </td>
                    <td>
                      {{$trans->user->bank->type_of_withdral ?? ''}}
                    </td>
             
                     
                    <td class="text-right">
                    <div class="dropdown">
                        <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          <i class="fas fa-ellipsis-v"></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                    <a class="dropdown-item" href="/admin/user-payaments/{{$trans->id}}">manage</a>
                           
                        </div>
                    </div>
                </td>
                  </tr>
                  @else
                
                  @endif
               @empty
            <center>  No Transctions currently</center>
               @endforelse

                </tbody>
              </table>
            </div>
            <!-- Card footer -->
            <div class="card-footer py-4">
              <nav aria-label="...">
                <ul class="pagination justify-content-end mb-0">
                 {{$transactions}}
                </ul>
              </nav>
            </div>
          </div>
        </div>
      </div>
           <!-- end -->
            </div>
        </div>
    </div>
@endsection

@section('script')
<!-- <script src="{{ mix('js/admin-products.js') }}"></script> -->
@endsection
