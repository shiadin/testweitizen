@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
          <div class="col-12 pb-7 bg-gradient-danger">
         <div class="row">
      
      
      </div>
         </div>
        <div class="col-12 mb-3" style="margin-top: -4em">
           <!-- start -->

 
      <div class="row justify-content-center">
        <div class="col-lg-8 card-wrapper">

  
     
          <!-- Paragraphs -->
          <div class="card">
            <div class="card-header">
              <h3 class="mb-0">Campaign Details</h3>
            </div>
            <div class="card-body">
         <form action="/admin/campaigns/{{$campaign->id}}" method="POST">

              @if (session()->has('success_message'))
    <br><br><br>
        <div class="alert alert-success">
    <h5>{{ session()->get('success_message') }}</h5>
                    </div>
    @endif
                
 @if (session()->has('error_message'))
 <br><br><br>
 <div class="alert alert-danger">
     <h5>{{ session()->get('error_message') }}</h5>
                    </div>
@endif
        @csrf
        @method('PUT')
    <div class="form-group">
        <label for="example-text-input" class="form-control-label">Campaign Name</label>
        <input class="form-control" type="text" value="{{$campaign->name}}" id="example-text-input" name="name">
    </div>
  

  <div class="form-group">
    <label for="exampleFormControlTextarea1">Campaign Description </label>
    <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name="description">{{$campaign->description}}</textarea>
  </div>
   <div class="form-group">
        <label for="example-date-input" class="form-control-label">Start Date</label>
        <input class="form-control" type="date" value="{{$campaign->start}}" id="example-date-input" name="start">
    </div>
 
    <div class="form-group">
        <label for="example-date-input" class="form-control-label">End Date</label>
        <input class="form-control" type="date" value="{{$campaign->end}}" id="example-date-input" name="end">
    </div>

     <div class="card">
            <div class="card-header">
              <h3 class="mb-0">Campaign rules</h3>
            </div>
            <div class="card-body">
                       @foreach($rules as $rule)
            
              <div class="row py-3 align-items-center">
                <div class="col-sm-3">
                     <div class="form-group">
        <label for="example-date-input" class="form-control-label">Rewards</label>
        <input class="form-control" type="text"  id="example-date-input" name="conditions[{{$rule['i']}}]" value="{{$rule['value']}}">
    </div>
                </div>
                <div class="col-sm-9">
                  <div class="form-group">
    <label for="exampleFormControlTextarea1"> Description </label>
    <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name="meta[$rule['i']]">{{$rule['desc']}}</textarea>
  </div>
                </div>
              </div>
    
@endforeach

            </div>
          </div>
          <br>
<div class="form-group">
    <center> <button type="submit" class="btn btn-primary">Save changes</button></center>
   </div>
</form>

            </div>
          </div>
        </div>
     



           <!-- end -->
            </div>
        </div>
    </div>
@endsection
@section('script')
<script>
  $(document).ready(function() {
    
    var navListItems = $('ul.setup-panel li a'),
        allWells = $('.setup-content');

    allWells.hide();

    navListItems.click(function(e)
    {
        e.preventDefault();
        var $target = $($(this).attr('href')),
            $item = $(this).closest('li');
        
        if (!$item.hasClass('disabled')) {
            navListItems.closest('li').removeClass('active');
            $item.addClass('active');
            allWells.hide();
            $target.show();
        }
    });
    
    $('ul.setup-panel li.active a').trigger('click');
    
    // DEMO ONLY //
    $('#activate-step-2').on('click', function(e) {
        $('ul.setup-panel li:eq(1)').removeClass('disabled');
        $('ul.setup-panel li a[href="#step-2"]').trigger('click');
        $(this).remove();
    })    
});


</script>
@endsection

