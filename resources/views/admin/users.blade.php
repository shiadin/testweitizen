@extends('layouts.app')

@section('content')
<input type="hidden" id="routes-data" value='@json($routes)'>
<input type="hidden" id="admin-data" value='@json($admin)'>
<div id="admin-users">
    <container></container>
</div>
@stop

@section('script')
<script src="{{ mix('js/admin-users.js') }}"></script>
@stop
