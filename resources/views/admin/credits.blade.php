@extends('layouts.app')

@section('content')
<input type="hidden" id="routes-data" value='@json($routes)'>
<div id="admin-credits">
    <container :users="users" :loading="loading" :filters="filters" :meta="meta"></container>
</div>
@stop

@section('script')
<script src="{{ mix('js/admin-credits.js') }}"></script>
@stop
