@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
          <div class="col-12 pb-7 bg-gradient-danger">
         <div class="row">
      
      
      </div>
         </div>
        <div class="col-12 mb-3" style="margin-top: -4em">
           <!-- start -->

 
      <div class="row justify-content-center">
        <div class="col-lg-8 card-wrapper">
     
          <!-- Paragraphs -->
          <div class="card">
            <div class="card-header">
              <h3 class="mb-0">Campaign Details</h3>
            </div>
            <div class="card-body">
         <form action="/admin/campaigns" method="POST">
        @csrf
    <div class="form-group">
        <label for="example-text-input" class="form-control-label">Campaign Name</label>
        <input class="form-control" type="text" value="John Snow" id="example-text-input" name="name">
    </div>
  

  <div class="form-group">
    <label for="exampleFormControlTextarea1">Campaign Description </label>
    <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name="description"></textarea>
  </div>
   <div class="form-group">
        <label for="example-date-input" class="form-control-label">Start Date</label>
        <input class="form-control" type="date" value="2019-11-23" id="example-date-input" name="start">
    </div>
 
    <div class="form-group">
        <label for="example-date-input" class="form-control-label">End Date</label>
        <input class="form-control" type="date" value="2019-11-23" id="example-date-input" name="end">
    </div>
</form>

            </div>
          </div>
        </div>
     



           <!-- end -->
            </div>
        </div>
    </div>
@endsection

