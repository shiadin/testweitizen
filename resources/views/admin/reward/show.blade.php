@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
          <div class="col-12 pb-7 bg-gradient-danger">
         <div class="row">
      
      
      </div>
         </div>
        <div class="col-12 mb-3" style="margin-top: -4em">
           <!-- start -->

 
      <div class="row justify-content-center">
        <div class="col-lg-8 card-wrapper">
     
          <!-- Paragraphs -->
          <div class="card">
            <div class="card-header">
              <h3 class="mb-0">Commision Details</h3>
              <a href="/admin/commissions" class="btn btn-info btn-sm pull-right">back</a>
            </div>
            <div class="card-body">
              <div class="row py-3 align-items-center">
                <div class="col-sm-3">
                  <small class="text-uppercase text-muted font-weight-bold">Name</small>
                </div>
                <div class="col-sm-9">
                  <p>{{$campaign->name}}</p>
                </div>
              </div>
              <div class="row py-3 align-items-center">
                <div class="col-sm-3">
                  <small class="text-uppercase text-muted font-weight-bold">Description</small>
                </div>
                <div class="col-sm-9">
                  <p class="lead">{{$campaign->description}}</p>
                </div>
              </div>
              <div class="row py-3 align-items-center">
                <div class="col-sm-3">
                  <small class="text-uppercase text-muted font-weight-bold">Start</small>
                </div>
                <div class="col-sm-9">
                  <blockquote class="blockquote">
                    <p class="mb-0">{{$campaign->start}}</p>
                    <footer class="blockquote-footer">the date the commission
                      <cite title="Source Title">resume</cite>
                    </footer>
                  </blockquote>
                </div>
              </div>
              <div class="row py-3 align-items-center">
                <div class="col-sm-3">
                  <small class="text-uppercase text-muted font-weight-bold">End</small>
                </div>
                <div class="col-sm-9">
                  <blockquote class="blockquote">
                    <p class="mb-0">{{$campaign->start}}</p>
                    <footer class="blockquote-footer">the date the commission
                      <cite title="Source Title">End</cite>
                    </footer>
                  </blockquote>
                </div>


              </div>
            </div>
          </div>
        </div>
       
       

           <!-- end -->
            </div>
        </div>
    </div>
@endsection

