@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
          <div class="col-12 pb-7 bg-gradient-pink">
         <div class="row">
          @foreach($stats as $s)
        <div class="col-xl-4 col-md-6">
          <div class="card bg-gradient-{{$s['color']}} border-0">
            <!-- Card body -->
            <div class="card-body">
              <div class="row">
                <div class="col">
                  <h5 class="card-title text-uppercase text-muted mb-0 text-white">{{$s['label']}}</h5>
                  <span class="h2 font-weight-bold mb-0 text-white">{{$s['value']}}</span>
                  <div class="progress progress-xs mt-3 mb-0">
                    <div class="progress-bar bg-success" role="progressbar" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100" style="width: {{$s['progressbar']}}%;"></div>
                  </div>
                </div>
                <div class="col-auto">
                  <button type="button" class="btn btn-sm btn-neutral mr-0" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fas fa-ellipsis-h"></i>
                  </button>
                  <div class="dropdown-menu dropdown-menu-right">
                    <a class="dropdown-item" href="{{$s['url']}}">See All</a>
                   
                  </div>
                </div>
              </div>
              <p class="mt-3 mb-0 text-sm">
                <a href="{{$s['url']}}" class="text-nowrap text-white font-weight-600">See details</a>
              </p>
            </div>
          </div>
        </div>
       @endforeach
      
      </div>
         </div>
        <div class="col-12 mb-3" style="margin-top: -4em">
           <!-- start -->
<div class="row">
    <div class="col-6 mb-3">
        <div class="col">
          <div class="card">
            <!-- Card header -->
            <div class="card-header border-0">
              <h3 class="mb-0">Earning by Dearers</h3>
            </div>
            <!-- Light table -->
            <div class="table-responsive">
              <table class="table align-items-center table-flush">
                <thead class="thead-light">
                  <tr>

                    <th scope="col">User</th>
                    <th scope="col">amount</th>
                   
                   
                  </tr>
                </thead>
                <tbody class="list">
                  @forelse($sales_performance as $trans)
                 @if($trans->total_sales>0)
                  <tr>
                    <th scope="row">
                      <div class="media align-items-center">
                    
                        <div class="media-body">
                          <span class="name mb-0 text-sm">{{$trans->user->name ?? '' }}</span>
                        </div>
                      </div>
                    </th>
                    <td class="budget">
                    RM {{$trans->total_sales}}
                    </td>
                   
                 
                  </tr>
                @endif
               @empty
            
               @endforelse

                </tbody>
              </table>
            </div>
            <!-- Card footer -->
            <div class="card-footer py-4">
              <nav aria-label="...">
                <ul class="pagination justify-content-end mb-0">
                 {{$sales_performance}}
                </ul>
              </nav>
            </div>
          </div>
        </div>
      </div>
        <div class="col-6 mb-3">
           <div class="card">
            <!-- Card header -->
            <div class="card-header border-0">
              <h3 class="mb-0">Earning Perfomances</h3>
            </div>
          {!! $chart_sale->html() !!}

        </div>
      </div>
    </div>
    

           <!-- end -->

           <!-- start -->
  <div class="row">
        <div class="col-xl-12">
          <div class="card">
            <div class="card-header border-0">
              <div class="row align-items-center">
                <div class="col">
                  <h3 class="mb-0">Commission Details</h3>
                </div>
                <div class="col text-right">
                <button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target=  "#exampleModal">
                Add Commision
               </button>
                </div>
              </div>
            </div>
            <div class="table-responsive">

                            @if (session()->has('success_message'))
    <br><br><br>
        <div class="alert alert-success">
    <h5>{{ session()->get('success_message') }}</h5>
                    </div>
    @endif
                
 @if (session()->has('error_message'))
 <br><br><br>
 <div class="alert alert-danger">
     <h5>{{ session()->get('error_message') }}</h5>
                    </div>
@endif
              <!-- Projects table -->
              <table class="table align-items-center table-flush">
                @if(count($commisions)>0)
                <thead class="thead-light">
                  <tr>
                    <th>#</th>
                    <th scope="col">Commision name</th>
                   
                    <th scope="col">Start Date</th>
                    <th scope="col">End Date</th>
                     <th scope="col">Status</th>
                    <th></th>
                  </tr>
                </thead>
                @endif
                <tbody>
                  @forelse($commisions as $camp)
                  <tr>
                    <td>  {{$camp->id}}</td>
                    <th scope="row">
                    {{$camp->name}}
                    </th>
                    
                    <td>
                      {{$camp->start}}
                    </td>
                    <td>
                        {{$camp->end}}
                    </td>
                    <td>
                       {{$camp->active}}
                    </td>
                    <td class="text-right">
                    <div class="dropdown">
                        <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          <i class="fas fa-ellipsis-v"></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                            <a class="dropdown-item" href="/admin/commissions/{{$camp->id}}/edit">Edit</a>
                            <a class="dropdown-item" href="/admin/commissions/{{$camp->id}}">View</a>
                            @if(!$camp->active)
                            <form action="/admin/commissions/{{$camp->id}}" method="POST">
                              @csrf
                              @method('DELETE')
                              <button  type="submit" class="dropdown-item">Delete</button>
                            </form>
                            @endif

                             @if(!$camp->active)
                            <form action="/admin/activate-commision/{{$camp->id}}" method="POST">
                              @csrf
                              @method('PUT')
                          <input type="hidden" name="active" value="1">
                              <button  type="submit" class="dropdown-item">Activate</button>
                            </form>
                            @endif
                        </div>
                    </div>
                </td>
                  </tr>
              @empty
             <center> No commission created yet</center><br>
              @endforelse
                </tbody>
              </table>
            </div>
          </div>
        </div>
      
      </div>

      <!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add Commision</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       <form action="/admin/commissions" method="POST">
        @csrf
    <div class="form-group">
        <label for="example-text-input" class="form-control-label">Commission Name</label>
        <input class="form-control" type="text"  id="example-text-input" name="name" required="">
    </div>
  

  <div class="form-group">
    <label for="exampleFormControlTextarea1">Commission Description </label>
    <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name="description" required=""></textarea>
  </div>
   <div class="form-group">
        <label for="example-date-input" class="form-control-label">Start Date</label>
        <input class="form-control" type="date"  id="example-date-input" name="start" required="">
    </div>
 
    <div class="form-group">
        <label for="example-date-input" class="form-control-label">End Date</label>
        <input class="form-control" type="date"  id="example-date-input" name="end" required="">
    </div>
     
  
   
   
  

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Create Commission</button>
      </div>
    </div></form>
  </div>
</div>
<div style="margin-top: 5em">
  
</div>
           <!-- end -->
            </div>
        </div>
    </div>
@endsection

@section('script')
<script src="{{ mix('js/admin-dashboard.js') }}"></script>
{!! Charts::scripts() !!}

{!! $chart_sale->script() !!}
@stop
