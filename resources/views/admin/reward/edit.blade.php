@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
          <div class="col-12 pb-7 bg-gradient-danger">
         <div class="row">
      
      
      </div>
         </div>
        <div class="col-12 mb-3" style="margin-top: -4em">
           <!-- start -->

 
      <div class="row justify-content-center">
        <div class="col-lg-8 card-wrapper">
     
          <!-- Paragraphs -->
          <div class="card">
            <div class="card-header">
              <h3 class="mb-0">Commission Details</h3>
            </div>
            <div class="card-body">
         <form action="/admin/commissions/{{$commission->id}}" method="POST">

              @if (session()->has('success_message'))
    <br><br><br>
        <div class="alert alert-success">
    <h5>{{ session()->get('success_message') }}</h5>
                    </div>
    @endif
                
 @if (session()->has('error_message'))
 <br><br><br>
 <div class="alert alert-danger">
     <h5>{{ session()->get('error_message') }}</h5>
                    </div>
@endif
        @csrf
        @method('PUT')
    <div class="form-group">
        <label for="example-text-input" class="form-control-label">Commission Name</label>
        <input class="form-control" type="text" value="{{$commission->name}}" id="example-text-input" name="name">
    </div>
  

  <div class="form-group">
    <label for="exampleFormControlTextarea1">Commission Description </label>
    <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name="description">{{$commission->description}}</textarea>
  </div>
   <div class="form-group">
        <label for="example-date-input" class="form-control-label">Start Date</label>
        <input class="form-control" type="date" value="{{$commission->start}}" id="example-date-input" name="start">
    </div>
 
    <div class="form-group">
        <label for="example-date-input" class="form-control-label">End Date</label>
        <input class="form-control" type="date" value="{{$commission->end}}" id="example-date-input" name="end">
    </div>

     <div class="card">
            <div class="card-header">
              <h3 class="mb-0">Commission Rules</h3>
            </div>
            <div class="card-body">
              @foreach($rules as $rule)
            
              <div class="row py-3 align-items-center">
                <div class="col-sm-3">
                     <div class="form-group">
        <label for="example-date-input" class="form-control-label">Rewards</label>
        <input class="form-control" type="text"  id="example-date-input" name="conditions[{{$rule['i']}}]" value="{{$rule['value']}}">
    </div>
                </div>
                <div class="col-sm-9">
                  <div class="form-group">
    <label for="exampleFormControlTextarea1"> Description </label>
    <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name="meta[$rule['i']]">{{$rule['desc']}}</textarea>
  </div>
                </div>
              </div>
    
@endforeach
 
            </div>
          </div>
          <br>
<div class="form-group">
    <center> <button type="submit" class="btn btn-primary">Save changes</button></center>
   </div>
</form>

            </div>
          </div>
        </div>
     



           <!-- end -->
            </div>
        </div>
    </div>
@endsection

