@extends('layouts.app')

@section('content')
<input type="hidden" id="routes-data" value='@json($routes)'>
<input type="hidden" id="withdraws-data" value='@json($withdraws)'>
<div id="withdraws-users">
    <container></container>
</div>
@stop

@section('script')
<script src="{{ mix('js/admin-withdrawal.js') }}"></script>
@stop
