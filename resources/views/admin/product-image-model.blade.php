 <form action="{{route('admin.UpdateImage', $product->id ?? '') }}" method="post"  enctype="multipart/form-data">
                @csrf
                <div class="modal fade" id="modal-default" tabindex="-1" role="dialog" aria-labelledby="modal-default" aria-hidden="true">
    <div class="modal-dialog modal- modal-dialog-centered modal-" role="document">
        <div class="modal-content">
        	
            <div class="modal-header">
                <h6 class="modal-title" id="modal-title-default">Product Image</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            
            <div class="modal-body">
            	
                
                  
                @if($product)
                  <img src="{{  asset('storage/products/images/'.$product->id.'/image.png') }}" height="200px" width="200px%">
                  <br>
                  <center>Or update</center>
                  <br>
                  @endif
                <div class="form-group {{ $errors->has('label') ? 'has-danger' : '' }}">
                    <input id="label" name="image" type="file" placeholder="product option label" class="form-control form-control-alternative {{ $errors->has('label') ? ' is-invalid' : '' }}" >
                   
                  

                   
                                <div class="invalid-feedback">{{ $errors->first('label') }}</div>
                            </div>
                
            </div>
            
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Save changes</button>
                <button type="button" class="btn btn-link  ml-auto" data-dismiss="modal">Close</button> 
            </div>
            
        </div>
    </div>
</div>
</form>