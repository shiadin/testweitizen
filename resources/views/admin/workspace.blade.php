@extends('layouts.app')

@section('content')
<input type="hidden" id="routes-data" value='@json($routes)'>
<input type="hidden" id="couriers-data" value='@json(config('aftership'))'>

<div id="admin-workspace">
    <container></container>
</div>
@stop

@section('script')
<script src="{{ mix('js/admin-workspace.js') }}"></script>
@stop
