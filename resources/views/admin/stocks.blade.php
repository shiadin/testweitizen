@extends('layouts.app')

@section('content')
<input type="hidden" id="routes-data" value='@json($routes)'>
<div id="admin-stocks">
    <container :stocks="stocks" :loading="loading" :filters="filters" :meta="meta" :products="products"></container>
</div>
@stop

@section('script')
<script src="{{ mix('js/admin-stocks.js') }}"></script>
@stop
