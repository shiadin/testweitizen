@extends('layouts.app')

@section('content')

<input type="hidden" id="product-data" value='@json($product)'>
<div class="container-fluid">
<div class="row mt-3">
    <div class="col-12" style="margin-top: 100px!important;">
        <div class="card">
            <form action="{{ $product ? route('admin.product.update', $product->id) : route('admin.product.create') }}" method="post"  enctype="multipart/form-data">
                @csrf
                <div class="card-body">
                    <h2 class="card-title">{{ $title }}</h2>
                    <div class="row">
                        <div class="col-sm-12 col-md-6">
                            <label for="name">Product Name</label>
                            <div class="form-group {{ $errors->has('name') ? 'has-danger' : '' }}">
                                <input id="name" name="name" type="text" placeholder="product name" class="form-control form-control-alternative {{ $errors->has('name') ? ' is-invalid' : '' }}" value="{{ old('name', optional($product)->name) }}">
                                <div class="invalid-feedback">{{ $errors->first('name') }}</div>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-6">
                            <label for="sku">Product SKU</label>
                            <div class="form-group {{ $errors->has('sku') ? 'has-danger' : '' }}">
                                <input id="sku" name="sku" type="text" placeholder="product sku" class="form-control form-control-alternative {{ $errors->has('sku') ? ' is-invalid' : '' }}" value="{{ old('sku', optional($product)->sku) }}">
                                <div class="invalid-feedback">{{ $errors->first('sku') }}</div>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-4">
                            <label for="weight">Product stock</label>
                            <div class="form-group {{ $errors->has('stock_amount') ? 'has-danger' : '' }}">
     <select name="stock_amount"  class="form-control" placeholder="">
    <option value="1" selected="">In Stocks</option>
    <option value="0">Out Of Stocks</option>
     </select>
    <!--  <input id="stock_amount" min="0" name="stock_amount" type="number" placeholder="product Stock" class="form-control form-control-alternative {{ $errors->has('stock_amount') ? ' is-invalid' : '' }}" value="{{ old('stock_amount', optional($product)->stock_amount) }}"> -->
                                <div class="invalid-feedback">{{ $errors->first('stock_amount') }}</div>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-6">
                            <label for="weight">Product Weight (gram)</label>
                            <div class="form-group {{ $errors->has('weight') ? 'has-danger' : '' }}">
                                <input id="weight" min="1" name="weight" type="number" placeholder="product weight" class="form-control form-control-alternative {{ $errors->has('weight') ? ' is-invalid' : '' }}" value="{{ old('weight', optional($product)->weight) }}">
                                <div class="invalid-feedback">{{ $errors->first('weight') }}</div>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-6">
                            
                @if($product)
                <br>
                  <button type="button" class="btn  btn-default btn-sm mb-3" data-toggle="modal" data-target="#modal-default">change product image</button>
                  <br>
                  

    <container :product="product"></container>

                @else 
                <label for="label">Product Image</label> 
                <div class="form-group {{ $errors->has('label') ? 'has-danger' : '' }}">
                    <input id="label" name="image" type="file" placeholder="product option label" class="form-control form-control-alternative {{ $errors->has('label') ? ' is-invalid' : '' }}" value="{{ old('label', optional($product)->memo) }}">
                   
                  

                   
                                <div class="invalid-feedback">{{ $errors->first('label') }}</div>
                            </div>
                        </div>
                        @endif
                    </div>
                    <h3 class="card-title text-muted">Manage Prices</h3>
                    @if($product)
                    <div class="row">
                        @foreach($product->prices as $price)
                       @if($price->role->id==33 || $price->role->id==1 || $price->role->id==2 || $price->role->id==3 || $price->role->id==4  )
                        <div class="col-sm-12 col-md-4 d-flex align-items-center justify-content-end">
                            {{ $price->role->name }} @if($price->role->description) ({{ $price->role->description }}) @endif
                        </div>
                        <div class="col-sm-12 col-md-4">
                            <div class="mb-3 form-group {{ $errors->has("prices.$loop->index") ? 'has-danger' : '' }}">
                                <div class="input-group input-group-alternative">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">Price ({{ config('app.currency') }})</span>
                                        <input type="hidden" name="prices_id[]" value="{{ $price->id }}">
                                    </div>
        <input step=".01" name="prices[]" class="form-control form-control-alternative {{ $errors->has("prices.$loop->index") ? 'is-invalid' : '' }}" value="{{ old("prices.$loop->index", $price->amount) }}" type="number">
                                </div>
        <div class="invalid-feedback  {{ $errors->has("prices.$loop->index") ? 'd-block' : '' }}">{{ $errors->first("prices.$loop->index") }}</div>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-4">
                            <div class="mb-3 form-group {{ $errors->has("min_quantity.$loop->index") ? 'has-danger' : '' }}">
                                <div class="input-group input-group-alternative">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">Min Purchase Quantity</span>
                                    </div>
                                    <input name="min_quantity[]" class="form-control form-control-alternative {{ $errors->has("min_quantity.$loop->index") ? 'is-invalid' : '' }}" value="{{ old("min_quantity.$loop->index", $price->min_quantity) }}" type="number">
                                </div>
                                <div class="invalid-feedback  {{ $errors->has("min_quantity.$loop->index") ? 'd-block' : '' }}">{{ $errors->first("min_quantity.$loop->index") }}</div>
                            </div>
                        </div>
@endif
                        @endforeach
                    </div>
                    @else
                    <div class="row">
                        <div class="col-12 text-center">
                            <h3>This Section Will Be Opened After Product Created</h3>
                        </div>
                    </div>
                    @endif
                    <div class="row">
                        <div class="col-12 text-right">
                            <button class="btn btn-info">{{ $product ? 'Update' : 'Create' }} Product</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
</div>
@include('admin.product-image-model')
@stop
@section('script')
@stop
