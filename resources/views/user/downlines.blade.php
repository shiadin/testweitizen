
@extends('layouts.app')

@section('content')
<input type="hidden" id="user-data" value='@json($user)'>

  <link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">

 <div class="main-content">
  <div id="downlines">
    <!-- Topnav -->
    <div class="container-fluid ">
      <div class="row">
        <div class="col-12 pb-7" style="background: linear-gradient(87deg, rgb(0, 0, 0) 0px, rgb(75, 75, 75) 100%) !important;">
          <div class="row">
            <div class="container-fluid" style="margin-top:60px!important;">
              <div class="header-body">
                <div class="row align-items-center py-4">
                  <div class="col-lg-4 col-md-10">
                    <h1 class="display-2 text-white">Hello {{auth()->user()->username}},</h1>
                    <p class="text-white mt-0 mb-5"style="margin-bottom: 0px !important;">This is your stocks page. You can manages the restock or buy new product from this page </p>
                    @if (session()->has('success_message'))
                    <div class="alert alert-success">
                      <h5>{{ session()->get('success_message') }}</h5>
                    </div>
                    @endif
                  </div>
                  <div id="user-downlines" class="col text-right"  style="margin-top: 3rem !important;">
                   
                    <container :user="user"> </container>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--6">
    	<!-- stats -->
         <div class="row">
          <div class="col-lg-4">
              <div class="card bg-gradient-default border-0">
                
                <div class="card-body">
                  <div class="row">
                    <div class="col">
                      <h5 class="card-title text-uppercase text-muted mb-0 text-white">NEW DEALERS ADDED THIS MONTH</h5>
                      <span class="h2 font-weight-bold mb-0 text-white">{{$stats['current']}}</span>
                    </div>
                    <div class="col-auto">
                    
                    </div>
                  </div>
                  <p class="mt-3 mb-0 text-sm">
                  
                    <span class="text-nowrap text-light">This Month Dealers</span>
                  </p>
                </div>
              </div>
            </div>

                <div class="col-lg-4">
              <div class="card bg-gradient-default border-0">
                
                <div class="card-body">
                  <div class="row">
                    <div class="col">
                      <h5 class="card-title text-uppercase text-muted mb-0 text-white">NEW DEALERS ADDED LAST MONTH</h5>
                      <span class="h2 font-weight-bold mb-0 text-white">{{$stats['lastMonth']}}</span>
                    </div>
                    <div class="col-auto">
                    
                    </div>
                  </div>
                  <p class="mt-3 mb-0 text-sm">
                  
                    <span class="text-nowrap text-light">Last Month Dealers</span>
                  </p>
                </div>
              </div>
            </div>


                <div class="col-lg-4">
              <div class="card bg-gradient-default border-0">
                
                <div class="card-body">
                  <div class="row">
                    <div class="col">
                      <h5 class="card-title text-uppercase text-muted mb-0 text-white">TOTAL DEALERS</h5>
                      <span class="h2 font-weight-bold mb-0 text-white">{{$stats['all']}}</span>
                    </div>
                    <div class="col-auto">
                    
                    </div>
                  </div>
                  <p class="mt-3 mb-0 text-sm">
                  
                    <span class="text-nowrap text-light"> All Dealers</span>
                  </p>
                </div>
              </div>
            </div>

           
           
         
          </div>
      <!-- end stats -->
      <!-- Table -->
      <div class="row">
        <div class="col">
          <div class="card">
            <!-- Card header -->
            <div class="card-header border-0">
            
            </div>
            <!-- Light table -->
            <div class="table-responsive" data-toggle="list" data-list-values='["name", "budget", "status", "completion"]'>
              <table class="table align-items-center table-flush" id="users-table">
                <thead class="thead-light">
                  <tr>
                  	<th>ID</th>
                    <th scope="col" class="sort" data-sort="name">Name</th>
                    <th scope="col" class="sort" data-sort="budget">Email</th>
                    <th scope="col" class="sort" data-sort="status">Phone</th>
                     <th scope="col" class="sort" data-sort="status">Level</th>
                    <th scope="col">Downlines</th>
                    <th scope="col" class="sort" data-sort="completion">Stocks</th>
                   <th scope="col" class="sort" data-sort="completion">Total Sales</th>
                      <th scope="col" class="sort" data-sort="completion">Created at</th>
                  
                        
                   
                  </tr>
                </thead>
                <tbody class="list">

                </tbody>
              </table>
            </div>
            <!-- Card footer -->
          
          </div>
        </div>
      </div>
 
    <br><br><br>
   
    </div>
 </div>
    

      
    

   

         

            

       



        @stack('scripts')

  
@stop

@section('script')
        

        <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>


        <script>

        $(function() {

            $('#users-table').DataTable({

                processing: true,

                serverSide: true,

              "ajax": '{{ route('ajax.downlines') }}',

                columns: [

                    { data: 'id', name: 'id' },

                    { data: 'name', name: 'name' },

                    { data: 'email', name: 'email' },

                    { data: 'phone_no', name: 'phone_no' },

                    { data: 'role_id', name: 'level', searchable: false  },

                    { data: 'downlines', name: 'downlines',  searchable: false },

                    { data: 'stocks', name: 'stocks',  searchable: false },

                     { data: 'total_sales', name: 'total_sales',  searchable: false },

                    { data: 'created_at', name: 'created_at' },

                      // { data: "edit_url", name: 'action', render:      function(data){
                      //        return htmlDecode(data) } }

                    
           
                ]

            });

        });

        </script>
 <script src="{{ mix('js/user-downlines.js') }}"></script>
@stop