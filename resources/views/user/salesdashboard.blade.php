
@extends('layouts.app')

@section('content')
  <link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">

 <div class="main-content" id="panel">
    <!-- Topnav -->
  
   <div class="header pb-6 d-flex align-items-center" style="min-height: 300px;  background-size: cover; background-position: center top;">
      <!-- Mask -->
      <span class="mask bg-gradient-pink" style="background: linear-gradient(87deg, rgb(0, 0, 0) 0px, rgb(75, 75, 75) 100%)!important;"></span>
      <!-- Header container -->
     
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--6">
      <!-- stats -->
         <div class="row">
          <div class="col-lg-4">
              <div class="card bg-gradient-default border-0">
                
                <div class="card-body">
                  <div class="row">
                    <div class="col">
                      <h5 class="card-title text-uppercase text-muted mb-0 text-white">Current Month</h5>
                      <span class="h2 font-weight-bold mb-0 text-white">RM {{$stats['current']}}</span>
                    </div>
                    <div class="col-auto">
                    
                    </div>
                  </div>
                  <p class="mt-3 mb-0 text-sm">
                  
                    <span class="text-nowrap text-light">This month postpaid shipping</span>
                  </p>
                </div>
              </div>
            </div>

                <div class="col-lg-4">
              <div class="card bg-gradient-default border-0">
                
                <div class="card-body">
                  <div class="row">
                    <div class="col">
                      <h5 class="card-title text-uppercase text-muted mb-0 text-white">Last Month</h5>
                      <span class="h2 font-weight-bold mb-0 text-white">RM {{$stats['lastMonth']}}</span>
                    </div>
                    <div class="col-auto">
                    
                    </div>
                  </div>
                  <p class="mt-3 mb-0 text-sm">
                  
                    <span class="text-nowrap text-light">Last month postpaid shipping</span>
                  </p>
                </div>
              </div>
            </div>


                <div class="col-lg-4">
              <div class="card bg-gradient-default border-0">
                
                <div class="card-body">
                  <div class="row">
                    <div class="col">
                      <h5 class="card-title text-uppercase text-muted mb-0 text-white">Life Time </h5>
                      <span class="h2 font-weight-bold mb-0 text-white">RM{{$stats['all']}}</span>
                    </div>
                    <div class="col-auto">
                    
                    </div>
                  </div>
                  <p class="mt-3 mb-0 text-sm">
                  
                    <span class="text-nowrap text-light"> All time postpaid shipping</span>
                  </p>
                </div>
              </div>
            </div>

           
           
         
          </div>
      <!-- end stats -->
      <!-- Table -->
      <div class="row">
        <div class="col">
          <div class="card">
            <!-- Card header -->
            <div class="card-header border-0">
              <h3 class="mb-0">Downlines Management</h3>
            </div>
            <!-- Light table -->
            <div class="table-responsive" data-toggle="list" data-list-values='["name", "budget", "status", "completion"]'>
              <table class="table align-items-center table-flush" id="users-table">
                <thead class="thead-light">
                  <tr>
                   
                    <th scope="col" class="sort" data-sort="order_id">Order ID</th>
                      <th scope="col" class="sort" data-sort="quantity">Quantity</th>
                    <th scope="col" class="sort" data-sort="amount">Amount</th>

                     <th scope="col" class="sort" data-sort="type">Type</th>
                   
                      <th scope="col" class="sort" data-sort="created_at">Created at</th>
                  
                        
                   
                  </tr>
                </thead>
                <tbody class="list">

                </tbody>
              </table>
            </div>
            <!-- Card footer -->
          
          </div>
        </div>
      </div>
 
    <br><br><br>
   
    </div>
 </div>
    

      
    

   

         

            

       



        @stack('scripts')

  
@stop

@section('script')
         

        <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>


        <script>

        $(function() {

            $('#users-table').DataTable({

                processing: true,

                serverSide: true,

              "ajax": '{{ route('ajax.postpaid') }}',

                columns: [

                   

                    { data: 'order_id', name: 'order_id' },
                    { data: 'quantity', name: 'quantity' },

                    { data: 'amount', name: 'amount' },

                    { data: 'memo', name: 'memo' },


                    { data: 'created_at', name: 'created_at' },

          

                    
           
                ]

            });

        });

        </script>

@stop

