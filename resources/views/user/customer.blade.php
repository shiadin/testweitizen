@extends('layouts.app')

@section('content')
<input type="hidden" id="routes-data" value='@json($routes)'>
<input type="hidden" id="user-data" value='@json($user)'>
<input type="hidden" id="stats-data" value='@json($stats)'>
<div id="user-customer">
  <!--div class="header pb-6 d-flex align-items-center" style="min-height: 300px; background-size: cover; background-position: center top;">
    <!- Mask >
    <span class="mask bg-gradient-pink" style="background: linear-gradient(87deg, rgb(0, 0, 0) 0px, rgb(75, 75, 75) 100%)!important;"></span-->
    <!-- Header container -->
    <div class="container-fluid ">
      <div class="row">
        <div class="col-12 pb-7" style="background: linear-gradient(87deg, rgb(0, 0, 0) 0px, rgb(75, 75, 75) 100%) !important;">
          <div class="row">
            <div class="container-fluid">
              <div class="header-body">
                <div class="row align-items-center py-4">
                  <div class="col-lg-4 col-md-10">
                    <h1 class="display-2 text-white">Hello {{auth()->user()->username}},</h1>
                    <p class="text-white mt-0 mb-5"style="margin-bottom: 0px !important;">This is your customer page. You can see all customer you've made shipping with</p>
                    @if (session()->has('success_message'))
                    <div class="alert alert-success">
                      <h5>{{ session()->get('success_message') }}</h5>
                    </div>
                    @endif
                  </div>
                  <div class="col text-right"  style="margin-top: 3rem !important;">
                    <a href="#!" class="btn btn-sm btn-neutral" data-toggle="modal" data-target="#modal-default"><i class="fa fa-plus"></i> Add New Customer</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
  <div class="container-fluid mt--6">
    <div class="row">
       
        <div class="col-xl-12 order-xl-1">
          <div class="row">
            <!-- stats -->
  @foreach($stats as $stat)
                  <div class="col-lg-4">
              <div class="card bg-gradient-default border-0">
                <!-- Card body -->
                <div class="card-body">
                  <div class="row">
                    <div class="col">
                      <h5 class="card-title text-uppercase text-muted mb-0 text-white">{{$stat['label']}}</h5>
                      <span class="h2 font-weight-bold mb-0 text-white">{{$stat['value']}}</span>
                    </div>
                    <div class="col-auto">
                      <div class="icon icon-shape bg-white text-dark rounded-circle shadow">
                        <i class="ni ni-spaceship"></i>
                      </div>
                    </div>
                  </div>
                  <p class="mt-3 mb-0 text-sm">
                    <span class="text-white mr-2"><i class="fa fa-arrow-right"></i></span>
                    <span class="text-nowrap text-light">{{$stat['tag']}}</span>
                  </p>
                </div>
              </div>
            </div>

         
            @endforeach
            <!-- end stats -->
          </div>
          <div class="card">
          
            <div class="card-body">
              <container :user="user"></container>
            </div>
          </div>
        </div>
      </div>

    </div>

<!-- model -->
    <div class="col-md-4">
      
      <div class="modal fade" id="modal-default" tabindex="-1" role="dialog" aria-labelledby="modal-default" aria-hidden="true">
      
      <form method="post" action="/customers/add">
                    @csrf
    <div class="modal-dialog modal- modal-dialog-centered modal-" role="document">
        <div class="modal-content">
          
            <div class="modal-header">
                <h2 class="modal-title" id="modal-title-default">Customer Details</h2>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            
            <div class="modal-body">

            @if ($errors->any())
     @foreach ($errors->all() as $error)
         <div class="error">{{$error}}</div>
     @endforeach
 @endif
               <!-- START -->
                  <h3>Customer Information</h3>
                  user/add
                 
                 <div class="row">
      
            <div class="col-md-6">
              <div class="form-group">
                <label class="form-control-label" for="example3cols1Input">Customer Name</label>
                <input name="name" type="text" class="form-control" id="example3cols1Input" placeholder="Customer Name">
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label class="form-control-label" for="example3cols2Input">Phone Number</label>
                <input name="phone_no" type="text" class="form-control" id="example3cols2Input" placeholder="Phone Number">
              </div>
            </div></div>
  <h3>Shipping Address</h3>
   <div class="row">
             <div class="col-md-6">
              <div class="form-group">
                <label class="form-control-label" for="example3cols2Input">Street</label>
                <input name="street" type="text" class="form-control" id="example3cols2Input" placeholder="Street">
              </div>
            </div>

             <div class="col-md-6">
              <div class="form-group">
                <label class="form-control-label" for="example3cols2Input">Street 2 (optional)</label>
                <input name="street_2" type="text" class="form-control" id="example3cols2Input" placeholder="Street 2">
              </div>
            </div>

             <div class="col-md-6">
              <div class="form-group">
                <label class="form-control-label" for="example3cols2Input">City</label>
                <input name="city" type="text" class="form-control" id="example3cols2Input" placeholder="City">
              </div>
            </div>

             <div class="col-md-6">
              <div class="form-group">
                <label class="form-control-label" for="example3cols2Input">Postal code</label>
                <input name="zipcode" type="text" class="form-control" id="example3cols2Input" placeholder="Postal code">
              </div>
            </div>

             <div class="col-md-6">
              <div class="form-group">
                <label class="form-control-label" for="example3cols2Input">Countryr</label>
                <input name="country" type="text" class="form-control" id="example3cols2Input" placeholder="Country">
              </div>
            </div>

             <div class="col-md-6">
              <div class="form-group">
                <label class="form-control-label" for="example3cols2Input">State</label>
                <input name="state" type="text" class="form-control" id="example3cols2Input" placeholder="State">
              </div>
            </div>
            
          </div>
        
               <!-- END -->
               
                
            </div>
            
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Add Customer</button>
                <button type="button" class="btn btn-link  ml-auto" data-dismiss="modal">Cancel</button>
            </div>
            
        </div>
    </div>
     
</div>
</form>
  </div>
<!-- end model -->

    
</div>
@endsection

@section('script')
<script src="{{ mix('js/user-customer.js') }}"></script>
@stop
