@extends('layouts.app')

@section('content')
<link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
<input type="hidden" id="routes-data" value='@json($routes)'>
<input type="hidden" id="user-data" value='@json($user)'>
<div id="user-payment">
    <container :user="user"></container>
</div>
@endsection

@section('script')
<script src="{{ mix('js/user-payment.js') }}"></script>
  <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>


        <script>

        $(function() {

            $('#users-table').DataTable({

                processing: true,

                serverSide: true,

              "ajax": '{{ route('ajax/wallet') }}',

                columns: [

                    { data: 'id', name: 'id' },

                    { data: 'type', name: 'type' },

                    { data: 'description', name: 'description' },

                    { data: 'amount', name: 'amount' },


              

                    { data: 'created_at', name: 'created_at' },

                      // { data: "edit_url", name: 'action', render:      function(data){
                      //        return htmlDecode(data) } }

                    
           
                ]

            });

        });

        </script>

@stop
