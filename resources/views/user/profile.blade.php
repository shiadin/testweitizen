@extends('layouts.app')

@section('content')
<input type="hidden" id="user-data" value='@json($user)'>
<input type="hidden" id="routes-data" value='@json($routes)'>
<div id="user-profile">
	
    <container :user='user'></container>
	
</div>
@stop

@section('script')
<script src="{{ mix('js/user-profile.js') }}"></script>
@stop
