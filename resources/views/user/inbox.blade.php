@extends('layouts.app')

@section('content')
<input type="hidden" id="routes-data" value='@json($routes)'>
<input type="hidden" id="user-data" value='@json($user)'>
<div id="user-inbox">
    <container :user="user"></container>
</div>
@endsection

@section('script')
<script src="{{ mix('js/user-inbox.js') }}"></script>
@stop
