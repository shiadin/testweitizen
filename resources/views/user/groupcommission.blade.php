
@extends('layouts.app')

@section('content')
  <link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">

 <div class="main-content" id="panel">
    <!-- Topnav -->
  
   <div class="header pb-6 d-flex align-items-center" style="min-height: 300px;  background-size: cover; background-position: center top;">
      <!-- Mask -->
      <span class="mask bg-gradient-pink" style="background: linear-gradient(87deg, rgb(0, 0, 0) 0px, rgb(75, 75, 75) 100%)!important;"></span>
      <!-- Header container -->
     
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--6">
    	<!-- stats -->
         <div class="row">
          <div class="col-lg-4">

              <div class="card bg-gradient-default border-0">
                
                <div class="card-body">
                  <div class="row">
                    <div class="col">
                      <h6 class="card-title text-uppercase text-muted mb-0 text-white">Current Group Sales Commission </h6>
                      <span class="h2 font-weight-bold mb-0 text-white">MYR{{$stats['all']}}</span>
                    </div>
                    <div class="col-auto">
                    
                    </div>
                  </div>
                  <p class="mt-3 mb-0 text-sm">
                  
                    <span class="text-nowrap text-light">This month commission {{$stats['cu']}}%</span>
                  </p>
                </div>
              </div>
            </div>

                <div class="col-lg-4">
              <div class="card bg-gradient-default border-0">
                
                <div class="card-body">
                  <div class="row">
                    <div class="col">
                      <h6 class="card-title text-uppercase text-muted mb-0 text-white">Current Total Group Sales Amount</h6>
                      <span class="h2 font-weight-bold mb-0 text-white">MYR{{$stats['current']}}</span>
                    </div>
                    <div class="col-auto">
                    
                    </div>
                  </div>
                  <p class="mt-3 mb-0 text-sm">
                  
                    <span class="text-nowrap text-light">You're entitled for {{$stats['cu']}}% group commission under <br> your current group sales performance. </span>
                  </p>
                </div>
              </div>
            </div>


                <div class="col-lg-4">
              <div class="card bg-gradient-default border-0">
                
                <div class="card-body">
                  <div class="row">
                    <div class="col">
                      <h6 class="card-title text-uppercase text-muted mb-0 text-white">Group Sales Next Challenge  </h6>
                      <!-- <span class="h2 font-weight-bold mb-0 text-white">MYR{{$stats['needed']}}</span> -->
                    </div>
                    <div class="col-auto">
                    
                    </div>
                  </div>
                  <p class="mt-3 mb-0 text-sm">
                  
  <div class="progress-info">
    <div class="progress-label">
      <span>sell  MYR{{$stats['needed']}} more to earn</span>
    </div>
    <div class="progress-percentage">
      <span>{{$stats['next']}}%</span>
    </div>
  </div>
  <div class="progress">
    <div class="progress-bar bg-default" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;"></div>
  </div>

                    
                </div>
              </div>
            </div>

           
           
         
          </div>
      <!-- end stats -->
        


      <!-- Table -->
    
      <div class="row">
        <div class="col">
         <!-- @if(auth()->user()->role_id<5)
             Level 5 to Level 7 Dealer are entitled for this group commission only
          @endif-->

          <div class="card" style="margin-top: 2em">
            <!-- Card header -->
            <div class="card-header border-0">
              <h3 class="mb-0">Commission History</h3>
                 <div class="col text-right">
                 <button type="button" class="btn btn-sm btn-primary mb-3" data-toggle="modal" data-target="#modal-group"><i class="fas fa-question-circle"></i></button>
                </div>

                
            </div>
            <div class="table-responsive" data-toggle="list" data-list-values='["name", "budget", "status", "completion"]'>
              <table class="table align-items-center table-flush" id="commission-table">
                <thead class="thead-light">
                  <tr>
                    <!-- <th>ID</th> -->
                    <th scope="col" class="sort" data-sort="type">Type</th>
                   <!--  <th scope="col" class="sort" data-sort="description">Description</th> -->
                     <th scope="col" class="sort" data-sort="quantity">Quantity</th>
                    <th scope="col" class="sort" data-sort="amount">Amount</th>
                     <th scope="col" class="downline" data-sort="downline">Downline</th>
                 
                      <th scope="col" class="sort" data-sort="completion">Created at</th>
                  
                        
                   
                  </tr>
                </thead>
                <tbody class="list">

                </tbody>
              </table>
            </div>
          
          </div>
        </div>
      </div>

    <br><br><br>
   
    </div>
 </div>
    

      


    
<!-- 
   model -->
  
                  <div class="modal fade" id="modal-default" tabindex="-1" role="dialog" aria-labelledby="modal-default" aria-hidden="true">
                    <div class="modal-dialog modal- modal-dialog-centered modal-" role="document">
                      <div class="modal-content">
                        <div class="modal-header">
                          <h6 class="modal-title" id="modal-title-default"> DIRECT COMMISSION</h6>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                          </button>
                        </div>
                        <div class="modal-body">
                          <p> 
             

               RM50/downline (When you invited one dealer, he/she will earn RM 50)
              </p>
        
                        
                        </div>
                        <div class="modal-footer">
                          
                          <button type="button" class="btn btn-link  ml-auto" data-dismiss="modal">Close</button>
                        </div>
                      </div>
                    </div>
                  </div>
<!-- 
   end -->    
<!-- 
   model -->
   
                  <div class="modal fade" id="modal-group" tabindex="-1" role="dialog" aria-labelledby="modal-default" aria-hidden="true">
                    <div class="modal-dialog modal- modal-dialog-centered modal-" role="document">
                      <div class="modal-content">
                        <div class="modal-header">
                          <h6 class="modal-title" id="modal-title-default">Group commision</h6>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                          </button>
                        </div>
                        <div class="modal-body">
                          <p>               Commission: Level 5 to Level 7 Dealer are entitled for this group commission only
<p>
Example: A Level 6 Dealer, he has 20 level 2 dealers purchase items (20x10xRM100/item) this Level 6 dealer will accumulated RM20,000 monthly group sales.So,this Level 6 dealer will awarded RM1,000(RM20,000x5%)as group sales commission.
              </p></p>
                          <p>If Group Sales achieve RM 5000 -RM 10,000 per month=3% commission from total groupsales.
If Group Sales achieve RM10,001–RM20,000 per month =5% commission from total group sales.
If Group Sales achieve RM20,001–RM30,000 per month =7% commission from total group sales.
If Group Sales achieve RM30,001 and above perm onth =8% commission from totalgroup sales.
***Remar Group Sales is based on thedownline’s personal item purchase.</p>
                        </div>
                        <div class="modal-footer">
                        
                          <button type="button" class="btn btn-link  ml-auto" data-dismiss="modal">Close</button>
                        </div>
                      </div>
                    </div>
                  </div>
<!-- 
   end -->

         

            

       



        @stack('scripts')

  
@stop

@section('script')
         

        <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>


        <script>






 $(function() {

            $('#commission-table').DataTable({

                processing: true,

                serverSide: true,

              "ajax": '{{ route('ajax/groupcommission_api') }}',

                columns: [

                    // { data: 'id', name: 'id' },

                    { data: 'type', name: 'type' },

                   
                    // { data: 'meta', name: 'description' },
                     { data: 'quantity', name: 'quantity' },

                    { data: 'amount', name: 'amount' },

                   { data: 'downline', name: 'downline' },
              

                    { data: 'created_at', name: 'created_at' },

                      // { data: "edit_url", name: 'action', render:      function(data){
                      //        return htmlDecode(data) } }

                    
           
                ]

            });

        });
        </script>

@stop

