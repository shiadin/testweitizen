@extends('layouts.app')

@section('content')
<input type="hidden" id="user-data" value='@json($user)'>
<input type="hidden" id="routes-data" value='@json($routes)'>
<div id="user-requests">
    <container :user='user'></container>
</div>
@endsection

@section('script')
<script src="{{ mix('js/user-requests.js') }}"></script>
@endsection
