@extends('layouts.app')
<style>
  :root{ --main-color: #f368e0; }
.product-grid{
    border:1px solid transparent;
    font-family: 'Barlow Condensed', sans-serif;
    padding:10px 10px;
    transition: all 0.5s;
}
.product-grid:hover{ border:1px solid #c1c1c1; }
.product-grid .product-image{
    position: relative;
    overflow: hidden;
}
.product-grid .product-image a.image{ display: block; }
.product-grid .product-image img{
    width: 100%;
    height: 250px;
}
.product-grid .add-to-cart{
    color: #fff;
    background: #FF5733;
    font-size: 17px;
    font-weight: 500;
    text-align: center;
    padding: 10px 0;
    display: block;
    position: absolute;
    left: 0;
    bottom: -50px;
    right: 0;
    transition: all ease-in-out 0.35s;
}
.product-grid:hover .add-to-cart{ bottom: 0; }
.product-grid .add-to-cart:after{
    content: "\f101";
    color: #fff;
    font-family: 'Font Awesome 5 Free';
    font-weight: 900;
    display: inline-block;
    opacity: 0;
    transition: all 0.5s;
}
.product-grid .add-to-cart:hover:after{
    padding-left: 10px;
    opacity: 1;
}
.product-grid .product-content{ position: relative; }
.product-grid .product-stock{
    color: #999;
    font-size: 16px;
    font-weight: 600;
    line-height: 35px;
    border-bottom: 1px solid #c1c1c1;
    display: block;
    margin: 0 0 15px;
}
.product-grid .icon{
    padding: 0;
    margin: 0;
    list-style: none;
    opacity: 0;
    position: absolute;
    top: -15px;
    right: 0;
    transition: all ease-in-out 0.35s;
}
.product-grid:hover .icon{
    opacity: 1;
    top: 8px;
}
.product-grid .icon li{ display: inline-block; }
.product-grid .icon li a{
    color: #FF5733;
    font-size: 16px;
    margin: 0 4px;
}
.product-grid .icon li a:hover{ color: var(--main-color); }
.product-grid .title{
    font-size: 12px;
    font-weight: 400;
    text-transform: capitalize;
    margin: 0 0 5px;
}
.product-grid .title a{
    color: #FF5733;
    transition: all 0.5s ease-out 0s;
}
.product-grid .title a:hover{ color: var(--main-color); }
.product-grid .category{
    font-size: 12px;
    margin: 0 0 3px;
    display: block;
}
.product-grid .category a{
    color: #FF5733;
    transition: all 0.3s ease 0s;
}
.product-grid .category a:hover{ color: var(--main-color); }
.product-grid .price{
    color: #FF5733;
    font-size: 14px;
    font-weight: 500;
    display: inline-block;
}
.product-grid .rating{
    padding: 0;
    margin: 0;
    list-style: none;
    float: right;
}
.product-grid .rating li{
    color: #FFB14B;
    font-size: 10px;
    display: inline-block;
}
.product-grid .rating li.disable{ color: #c1c1c1; }
@media only screen and (max-width:990px){
    .product-grid{ margin-bottom: 30px; }
}
a:hover, a:focus{
    outline: none;
    text-decoration: none;
}
#accordion .panel{
    border:0px none;
    box-shadow:none;
}
#accordion .panel-heading{
    padding:0;
    background: #fff;
}
#accordion .panel-title a{
    display: block;
    position: relative;
    background:transparent;
    color:#de4d4e;
    font-size:14px;
    font-weight: bolder;
    text-transform:uppercase;
    margin-bottom:15px;
    padding:15px 50px;
    border-bottom:1px solid #de4d4e;
    border-radius: 0 15px 0 15px;
    transition:all 0.10s linear 0s;
}

</style>
@section('content')
<input type="hidden" id="routes-data" value='@json($routes)'>
<input type="hidden" id="user-data" value='@json($user)'>



<div id="user-stocks">

     <div class="container-fluid ">
      <div class="row">
        <div class="col-12 pb-7" style="background: linear-gradient(87deg, rgb(0, 0, 0) 0px, rgb(75, 75, 75) 100%) !important;">
          <div class="row">
            <div class="container-fluid">
              <div class="header-body">
                <div class="row align-items-center py-4">
                  <div class="col-lg-4 col-md-10">
                    <h1 class="display-2 text-white">Hello {{auth()->user()->username}},</h1>
                    <p class="text-white mt-0 mb-5"style="margin-bottom: 0px !important;">This is your stocks page. You can manages the restock or buy new product from this page </p>
                    @if (session()->has('success_message'))
                    <div class="alert alert-success">
                      <h5>{{ session()->get('success_message') }}</h5>
                    </div>
                    @endif
                  </div>
                  <div class="col text-right"  style="margin-top: 3rem !important;">
                    <container :user="user"> </container>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    <div class="container-fluid mt--6" style="margin-top: -4em">
      <!-- starts -->
      <div class="row">
        <div class="col-lg-4">
          <div class="card bg-gradient-default border-0">
            <!-- Card body -->
            <div class="card-body">
              <div class="row">
                <div class="col">
                  <h5 class="card-title text-uppercase text-muted mb-0 text-white">MY E-WALLET</h5>
                  <span class="h2 font-weight-bold mb-0 text-white">MYR{{auth()->user()->balance}}</span>
                </div>
                <div class="col-auto">
                  <div class="icon icon-shape bg-white text-dark rounded-circle shadow">
                    <i class="ni ni-spaceship"></i>
                  </div>
                </div>
              </div>
                <!--  <p class="mt-3 mb-0 text-sm">
                    <span class="text-white mr-2"><i class="fa fa-arrow-right"></i> </span>
                    <span class="text-nowrap text-light">my e-wallet balance</span>
                  </p> -->
              </div>
            </div>
          </div>
          <div class="col-lg-4">
            <div class="card bg-gradient-default border-0">
                <!-- Card body -->
              <div class="card-body">
                <div class="row">
                  <div class="col">
                    <h5 class="card-title text-uppercase text-muted mb-0 text-white"> MY STOCK</h5>
                    <span class="h2 font-weight-bold mb-0 text-white">{{$token->quantity+$stockQuantity }}</span>
                  </div>
                  <div class="col-auto">
                    <div class="icon icon-shape bg-white text-dark rounded-circle shadow">
                      <i class="ni ni-spaceship"></i>
                    </div>
                  </div>
                </div>
                 <!--  <p class="mt-3 mb-0 text-sm">
                    <span class="text-white mr-2"><i class="fa fa-arrow-right"></i> </span>
                    <span class="text-nowrap text-light"> my stock {{$token->quantity+$stockQuantity }}</span>
                  </p> -->
              </div>
            </div>
          </div>
          <div class="col-lg-4">
            <div class="card bg-gradient-default border-0">
                <!-- Card body -->
              <div class="card-body">
                <div class="row">
                  <div class="col">
                    <h5 class="card-title text-uppercase text-muted mb-0 text-white"> My Cart</h5>
                    <span class="h2 font-weight-bold mb-0 text-white">(<strong>{{ Cart::count() }}</strong>) items</span>
                  </div>
                  <div class="col-auto">
                    <div class="icon icon-shape bg-white text-dark rounded-circle shadow">
                      <i class="ni ni-spaceship"></i>
                    </div>
                  </div>
                </div>
                  <!-- <p class="mt-3 mb-0 text-sm">
                    <span class="text-white mr-2"><i class="fa fa-arrow-right"></i> </span>
                    <span class="text-nowrap text-light">all beutemethod's product</span>
                  </p> -->
              </div>
            </div>
          </div>
        </div>
        <!-- edn -->
        <div class="card">
        <!-- Card header -->
          <div class="card-header border-0">
            <div class="row">
              <div class="col-6">
                <h3 class="mb-0">My Stocks</h3>
              </div>
              <div class="col-6 text-right">
                 <!--   <container :user="user"> </container> -->    
              </div>
            </div>
          </div>
        <!-- Light table -->
          <div class="row">
          @forelse($stocks as $product)
          <div class="col-md-3 col-sm-6">
            <div class="product-grid">
              <div class="product-image">
                <a href="#" class="image">
                  <img   src="{{ asset( 'storage/'.$product->product->image) }}">
                </a>                  
              </div>
              <div class="product-content">
                <span class="product-stock">my stock {{$product->quantity}}</span>
                <h3 class="title">{{$product->product->name}}</h3>
                <div class="row">
                  <div class="col-md-12">
                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                      <div class="panel panel-default">
                        <!-- start -->
                        <form action="{{ url('/cart') }}" method="POST" >
                          {!! csrf_field() !!}

                          <input type="hidden" name="id" value="{{$product->product->id}}">
                          <input type="hidden" name="name" value="{{ $product->product->name }}">
                          <div class="form-group">
                            <label class="form-control-label" for="exampleFormControlSelect1">Quantity</label>
                            <input value="{{$product->product->price->min_quantity ?? 1}}" type="number" min="{{$product->product->price->min_quantity ?? 1}}" name="quantity" class="form-control">RM {{$product->product->price->amount ?? 'No Price' }}
                          </div>
                          @if($product->product->stock_amount>0)
                            <button type="submit" class=" btn btn-sm btn-danger pull-right"><i class="fa fa-shopping-cart"></i> Restock </button>
                          @else
                            <button disabled  class="btn btn-danger btn-sm ">Out of Stock</button>
                          @endif
                          </form>
                        <!-- end -->
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          @empty
          <div class="row">
            <center><h3 style="margin:5em">  You not have stocks yet</h3></center>
          </div>
          @endforelse
        </div>
      </div>
      <br><br>
    </div>
  </div>
</div>
  @endsection

@section('script')
<script src="{{ mix('js/user-stocks.js') }}"></script>
@stop
