
@extends('layouts.app')

@section('content')
  <link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
<input type="hidden" id="routes-data" value='@json($routes)'>
<input type="hidden" id="user-data" value='@json($user)'>
<div id="user-payment">
 <div class="main-content" id="panel">
    <!-- Topnav -->
  
   <div class="header pb-6 d-flex align-items-center" style="min-height: 300px;  background-size: cover; background-position: center top;">
      <!-- Mask -->

      <span class="mask bg-gradient-pink" style="background: linear-gradient(87deg, rgb(0, 0, 0) 0px, rgb(75, 75, 75) 100%)!important;"></span>
      <!-- Header container -->
      
        <div class="col text-right">
      
       
          <container :user="user"> </container>
          

                
                </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--6">
      
    	<!-- stats -->
         <div class="row">
          <div class="col-lg-4">
              <div class="card bg-gradient-default border-0">
                
                <div class="card-body">
                  <div class="row">
                    <div class="col">
                      <h5 class="card-title text-uppercase text-muted mb-0 text-white">CURRENT E-WALLET BALANCE</h5>
                      <span class="h2 font-weight-bold mb-0 text-white">MYR{{$stats['current']}}</span>
                    </div>
                    <div class="col-auto">
                    
                    </div>
                  </div>
                  <p class="mt-3 mb-0 text-sm">
                  
                    <!--span class="text-nowrap text-light">This month wallet transactions</span-->
                  </p>
                </div>
              </div>
            </div>

                <div class="col-lg-4">
              <div class="card bg-gradient-default border-0">
                
                <div class="card-body">
                  <div class="row">
                    <div class="col">
                      <h5 class="card-title text-uppercase text-muted mb-0 text-white">TOTAL MONEY-IN THIS MONTH</h5>
                      <span class="h2 font-weight-bold mb-0 text-white">MYR{{$stats['lastMonth']}}</span>
                    </div>
                    <div class="col-auto">
                    
                    </div>
                  </div>
                  <p class="mt-3 mb-0 text-sm">
                  
                    <!--span class="text-nowrap text-light">Last month wallet transactions</span-->
                  </p>
                </div>
              </div>
            </div>


                <div class="col-lg-4">
              <div class="card bg-gradient-default border-0">
                
                <div class="card-body">
                  <div class="row">
                    <div class="col">
                      <h5 class="card-title text-uppercase text-muted mb-0 text-white">TOTAL MONEY OUT THIS MONTH</h5>
                      <span class="h2 font-weight-bold mb-0 text-white">MYR{{$stats['all']}}</span>
                    </div>
                    <div class="col-auto">
                    
                    </div>
                  </div>
                  <p class="mt-3 mb-0 text-sm">
                  
                    <!--span class="text-nowrap text-light"> All wallet transactions</span-->
                  </p>
                </div>
              </div>
            </div>

           
           
         
          </div>
      <!-- end stats -->
        
      <!-- Table -->
      <div class="row">
        <div class="col">
          <div class="card">
            <!-- Card header -->
            <div class="card-header border-0">
              <h3 class="mb-0">Wallet Transaction</h3>
            </div>
            <!-- Light table -->
            <div class="table-responsive" data-toggle="list" data-list-values='["name", "budget", "status", "completion"]'>
              <table class="table align-items-center table-flush" id="users-table">
                <thead class="thead-light">
                  <tr>
                  	<!-- <th>ID</th> -->
                    <th scope="col" class="sort" data-sort="type">Date, Time</th>
                    <th scope="col" class="sort" data-sort="description">Transaction Details</th>
                    <th scope="col" class="sort" data-sort="amount_in">Money In</th>
                    <th scope="col" class="sort" data-sort="amount_out">Money Out</th>
                 
                      <!-- <th scope="col" class="sort" data-sort="total">Balance</th> -->
                  
                        
                   
                  </tr>
                </thead>
                <tbody class="list">

                </tbody>
              </table>
            </div>
            <!-- Card footer -->
          
          </div>
        </div>
      </div>
 
    <br><br><br>
   
    </div>
 </div>
    

        
          
                  </div>
    

   

         

            

       



        @stack('scripts')

  
@stop

@section('script')
     <script src="{{ mix('js/user-payment.js') }}"></script>    

        <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>


        <script>

        $(function() {

            $('#users-table').DataTable({

                processing: true,

                serverSide: true,

              "ajax": '{{ route('ajax/wallet') }}',

                columns: [
                  { data: 'created_at', name: 'created_at' },
                  { data: 'description', name: 'description' },
                  { data: 'amount_in', name: 'amount_in' },
                  { data: 'amount_out', name: 'amount_out' },

                    // { data: 'total', name: 'total' },

                   

                    


              

                    

                      // { data: "edit_url", name: 'action', render:      function(data){
                      //        return htmlDecode(data) } }

                    
           
                ]

            });

        });

        </script>

@stop

