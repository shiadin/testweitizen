@extends('layouts.app')

@section('content')
<input type="hidden" id="routes-data" value='@json($routes)'>
<input type="hidden" id="user-data" value='@json($user)'>
  <div class="main-content" id="panel">
    <!-- Topnav -->

    <!-- Header -->
    <!-- Header -->
    <div class="header pb-6 d-flex align-items-center" style="min-height: 400px;  background-size: cover; background-position: center top;">
      <!-- Mask -->
      <span class="mask bg-gradient-pink" style="background: linear-gradient(87deg, rgb(0, 0, 0) 0px, rgb(75, 75, 75) 100%)!important;"></span>
      <!-- Header container -->
      <div class="container-fluid d-flex align-items-center">
        <div class="row">
          <div class="col-lg-7 col-md-10">
          @if(auth()->user()->unreadNotifications->count())
                <div class="alert alert-success alert-dismissible fade show" role="alert">
    <span class="alert-icon"><i class="ni ni-like-2"></i></span>
    <span class="alert-text"><strong>Important!</strong> Please check your notification center!</span>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
                @endif
                
            <h1 class="display-2 text-white">Welcome {{auth()->user()->username}},</h1>
            <p class="text-white mt-0 mb-5">This is your dashboard page. You can see the progress you've made with your work and manage your orders or downlines </p>
           <!--  <a href="#!" class="btn btn-neutral">Edit profile</a> -->
          </div>
        </div>
      </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--6">
      <div class="row">
        <div class="col-xl-12 order-xl-1">
                @if (session()->has('success_message'))
    <br><br><br>
        <div class="alert alert-success">
    <h5>{{ session()->get('success_message') }}</h5>
                    </div>
    @endif
                
 @if (session()->has('error_message'))
 <br><br><br>
 <div class="alert alert-danger">
     <h5>{{ session()->get('error_message') }}</h5>
                    </div>
@endif
</div>
         <div class="col-xl-12 order-xl-1">
          <div class="row" id="user-overview">
         
              
        
          <div class="col-md-12">
         <container :user="user"></container>
            </div>
          </div>
        </div>
        <div class="col-xl-4 order-xl-2">
          <div class="card card-profile">
          
          <!-- start -->

          <!-- end -->
            
          
          </div>
          <!-- Progress track -->
        

          <div class="card" style="margin-top: 1em">
            <!-- Card header -->
            <div class="card-header">
              <!-- Title -->
              <h5 class="h3 mb-0">My Stocks</h5>
            </div>
            <!-- Card body -->
            <div class="card-body">
              <!-- List group -->
              <div class="table-responsive">
              <!-- Projects table -->
              <table class="table align-items-center table-flush">
                <thead class="thead-light">
                
                </thead>
                <tbody>

                  @foreach($stocks as $d)
                  <tr>
                    <th scope="row">
                      {{$d->product->name}}
                    </th>
                    
                    <td>
                     {{$d->quantity}}
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
            </div>
          </div>
        </div>
        <div class="col-xl-8 order-xl-1">
          <div class="row" id="user-overview">
          <div class="col-md-12">
         <container :user="user"></container>
            </div>
          </div>
          <div class="card">
            <div class="card-header">
             
            </div>
            <div class="card-body">
             {!! $chart_stock_reloads->html() !!}
            </div>
          </div>

           <div class="card" style="margin-top: 2em">
         
                  <div class="card-header">
              <!-- Title -->
              <h5 class="h3 mb-0">Transactions</h5>
            </div>
            
            <div class="table-responsive">
                  <table class="table align-items-center table-flush">
                    <thead class="thead-light">
                      <tr>
                        <th scope="col" class="sort" data-sort="name">Type</th>
                       
                       
                       <th scope="col" class="sort" data-sort="budget">Description</th>
                        <th scope="col" class="sort" data-sort="budget">Amount</th>
                        <th scope="col" class="sort" data-sort="completion">Paid At</th>
                       
                      </tr>
                    </thead>
                    <tbody class="list">
                      @foreach($wallets->take(5) as $wallet)
                      @if($wallet->accepted)
                      <tr>
                       <td>{{$wallet->type}}  </td>
                       <td>{{$wallet->meta['description']}} </td>
                       <td>{{$wallet->amount}} </td>
                       <td>
                        {{$wallet->created_at}}  </td>
                      
                      </tr>
                      @endif
                     @endforeach
                     
                    </tbody>
                  </table>
                
            </div>
          </div>


          
        </div>
      </div>
      <div style="margin-top: 4em"></div>
      <!-- Footer -->
     
    </div>
  </div>
{!! Charts::scripts() !!}

{!! $chart_stock_reloads->script() !!}
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Credit Reload</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <form action="/creditReload" method="POST">
          @csrf
          <div class="form-group">
          <input  class="form-control" type="number" min="10" name="quantity" placeholder="Enter Credit Amount">
        </div>
          <input type="hidden" name="user_id" value="{{auth()->user()->id}}">
          <button type="submit" class="btn btn-success btn-block">Pay Now</button>
        </form>
      </div>
      
    </div>
  </div>
</div>
@endsection

@section('script')
<script src="{{ mix('js/user-overview.js') }}"></script>

@stop
