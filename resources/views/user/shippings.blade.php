@extends('layouts.app')

@section('content')
<input type="hidden" id="user-data" value='@json($user)'>
<input type="hidden" id="routes-data" value='@json($routes)'>
@if(request()->is('admin/*'))
<input type="hidden" id="couriers-data" value='@json(config('aftership'))'>
@endif
<div id="user-shippings">
    <container :user='user'></container>
</div>
@endsection

@section('script')
<script src="//www.tracking.my/track-button.js"></script>
<script src="{{ mix('js/user-shippings.js') }}"></script>
@endsection
