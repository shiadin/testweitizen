@component('mail::message')
# Hi, {{$user->name}}

 Congratulations! New downline has joined by using your invitation code!

@component('mail::button', ['url' => ''])
Button Text
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
