@component('mail::message')
# Hi, {{ $user->username }}!

Welcome to {{ config('app.name') }}! Thanks so much for joining us. You’re on your way to super-productivity and beyond!

To take your first steps into our system

1. Complete your personal information
@if($user->depositRequired())
2. Upload your deposit ({{ config('app.currency') }} {{ $user->depositRequired() }})
@endif
{{ $user->depositRequired() ? '3.' : '2.' }} Waiting Admin to activate your account.


For your information, you will be notified once the account is activated. Be patient among the activation process.

@component('mail::button', ['url' => config('app.url')])
Visit Our Website
@endcomponent

Cheerfully yours,

The {{ config('app.name') }} Team
@endcomponent
