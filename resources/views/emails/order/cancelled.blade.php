@component('mail::message')
@if($order->strictType === 'deposit')
# A Deposit #{{ $order->id }} Has Been Cancelled

<u>{{ $order->purpose }}</u>

Deposit Paid: **{{ config('app.currency') }} {{ $order->amount }}**

@elseif($order->strictType === 'c-credit')

# A C-Credits #{{ $order->id }} Topup Request Has Been Cancelled

Credits Requested: **{{ config('app.currency') }} {{ $order->amount }}**

@elseif($order->strictType === 'stocks')

# A Stocks #{{ $order->id }} Reload Request Has Been Cancelled

Payment Made: **{{ config('app.currency') }} {{ $order->amount }}**

@component('mail::table')
| Product       | Quantity         | Unit Price ({{ config('app.currency') }})  |
| ------------- |:-------------:| --------:|
@foreach($order->items as $item)
| {{ $item->product->name }}      | {{ $item->quantity }}      | {{ $item->unit_price }}      |
@endforeach
@endcomponent

@elseif($order->strictType === 'shipping')


# A Shipping #{{ $order->id }} Order Has Been Cancelled

@if($order->customer)
@php $customer = json_decode(json_encode(new \App\Http\Resources\CustomerResource($order->customer))) @endphp
Customer: {{ $customer->name }} ({{ $customer->phone_no }})

Address:

{!! $order->customer->address->full !!}
@else
~~Customer~~: SELF PICKUP
@endif

C-Credits Cost: **{{ config('app.currency') }} {{ $order->amount }}**

@component('mail::table')
| Product       | Quantity         | Unit Price ({{ config('app.currency') }})  |
| ------------- |:-------------:| --------:|
@foreach($order->items as $item)
| {{ $item->product->name }}      | {{ $item->quantity }}      | {{ $item->unit_price }}      |
@endforeach
@endcomponent

@endif

Status: {{ strtoupper($order->situation) }}

@component('mail::button', ['url' => config('app.url')])
Enter {{ config('app.name') }} System
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
