@component('mail::message')
@if($order->strictType === 'deposit')
# A New Deposit #{{ $order->id }} was Successfully Submitted

<u>{{ $order->purpose }}</u>

Deposit Paid: **{{ config('app.currency') }} {{ $order->amount }}**

@elseif($order->strictType === 'c-credit')

# A New C-Credits #{{ $order->id }} Topup Request was Successfully Submitted

Credits Requested: **{{ config('app.currency') }} {{ $order->amount }}**

@elseif($order->strictType === 'stocks')

# A New Stocks #{{ $order->id }} Reload Request was Successfully Submitted

Payment Made: **{{ config('app.currency') }} {{ $order->amount }}**

@component('mail::table')
| Product       | Quantity         | Unit Price ({{ config('app.currency') }})  |
| ------------- |:-------------:| --------:|
@foreach($order->items as $item)
| {{ $item->product->name }}      | {{ $item->quantity }}      | {{ $item->unit_price }}      |
@endforeach
@endcomponent

@elseif($order->strictType === 'shipping')


# A New Shipping #{{ $order->id }} Order was Successfully Created

@if($order->customer)
@php $customer = json_decode(json_encode(new \App\Http\Resources\CustomerResource($order->customer))) @endphp
Customer: {{ $customer->name }} ({{ $customer->phone_no }})

Address:

{!! $order->customer->address->full !!}
@else
~~Customer~~: SELF PICKUP
@endif

C-Credits Cost: **{{ config('app.currency') }} {{ $order->amount }}**

@component('mail::table')
| Product       | Quantity         | Unit Price ({{ config('app.currency') }})  |
| ------------- |:-------------:| --------:|
@foreach($order->items as $item)
| {{ $item->product->name }}      | {{ $item->quantity }}      | {{ $item->unit_price }}      |
@endforeach
@endcomponent

@endif

Status: {{ strtoupper($order->situation) }}

@component('mail::button', ['url' => config('app.url')])
Enter {{ config('app.name') }} System
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
