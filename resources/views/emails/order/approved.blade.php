@component('mail::message')
@if($order->strictType === 'deposit')
# A Deposit #{{ $order->id }} Has Been Approved

<u>{{ $order->purpose }}</u>

Deposit Paid: **{{ config('app.currency') }} {{ $order->amount }}**

@elseif($order->strictType === 'c-credit')

# A C-Credits #{{ $order->id }} Topup Request Has Been Approved

Credits Requested: **{{ config('app.currency') }} {{ $order->amount }}**

@elseif($order->strictType === 'stocks')

# A Stocks #{{ $order->id }} Reload Request Has Been Approved

Payment Made: **{{ config('app.currency') }} {{ $order->amount }}**

@component('mail::table')
| Product       | Quantity         | Unit Price ({{ config('app.currency') }})  |
| ------------- |:-------------:| --------:|
@foreach($order->items as $item)
| {{ $item->product->name }}      | {{ $item->quantity }}      | {{ $item->unit_price }}      |
@endforeach
@endcomponent

@elseif($order->strictType === 'shipping')

Something Wrong

@endif

@if(!empty($order->memo))
Memo: {{ $order->memo }}
@endif

@if($order->payment && !empty($order->payment->memo) && $order->payment->memo != $order->memo)
Memo: {{ $order->payment->memo }}
@endif

Status: {{ strtoupper($order->situation) }}

@component('mail::button', ['url' => config('app.url')])
Enter {{ config('app.name') }} System
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
