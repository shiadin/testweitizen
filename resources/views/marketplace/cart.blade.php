@extends('layouts.app')
<style >
    body{margin-top:20px;
    background:#eee;
}
h3 {
    font-size: 16px;
}
.text-navy {
    color: #1ab394;
}
.cart-product-imitation {
  text-align: center;
  padding-top: 0px;
  height: 100px;
  width: 80px;
  background-color: #f8f8f9;
}
.product-imitation.xl {
  padding: 120px 0;
}
.product-desc {
  padding: 20px;
  position: relative;
}
.ecommerce .tag-list {
  padding: 0;
}
.ecommerce .fa-star {
  color: #d1dade;
}
.ecommerce .fa-star.active {
  color: #f8ac59;
}
.ecommerce .note-editor {
  border: 1px solid #e7eaec;
}
table.shoping-cart-table {
  margin-bottom: 0;
}
table.shoping-cart-table tr td {
  border: none;
  text-align: right;
}
table.shoping-cart-table tr td.desc,
table.shoping-cart-table tr td:first-child {
  text-align: left;
}
table.shoping-cart-table tr td:last-child {
  width: 80px;
}
.ibox {
  clear: both;
  margin-bottom: 25px;
  margin-top: 0;
  padding: 0;
}
.ibox.collapsed .ibox-content {
  display: none;
}
.ibox:after,
.ibox:before {
  display: table;
}
.ibox-title {
  -moz-border-bottom-colors: none;
  -moz-border-left-colors: none;
  -moz-border-right-colors: none;
  -moz-border-top-colors: none;
  background-color: #ffffff;
  border-color: #e7eaec;
  border-image: none;
  border-style: solid solid none;
  border-width: 3px 0 0;
  color: inherit;
  margin-bottom: 0;
  padding: 14px 15px 7px;
  min-height: 48px;
}
.ibox-content {
  background-color: #ffffff;
  color: inherit;
  padding: 15px 20px 20px 20px;
  border-color: #e7eaec;
  border-image: none;
  border-style: solid solid none;
  border-width: 1px 0;
}
.ibox-footer {
  color: inherit;
  border-top: 1px solid #e7eaec;
  font-size: 90%;
  background: #ffffff;
  padding: 10px 15px;
}
a:hover,a:focus{
    text-decoration: none;
    outline: none;
}

</style>


@section('content')
  
   


  <div class="header pb-6 d-flex align-items-center" style="min-height: 300px; background-size: cover; background-position: center top;">
      <!-- Mask -->
      <span class="mask bg-gradient-pink" style="background: linear-gradient(87deg, rgb(0, 0, 0) 0px, rgb(75, 75, 75) 100%)!important;"></span>
      <!-- Header container -->
      <div class="container-fluid d-flex align-items-center">
        <div class="row">
          <div class="col-lg-12 col-md-10">
            @if (session()->has('success_message'))
  
        <div class="alert alert-success">
    <h5>{{ session()->get('success_message') }}</h5>
                    </div>
    @endif
                
 @if (session()->has('error_message'))

 <div class="alert alert-danger">
     <h5>{{ session()->get('error_message') }}</h5>
                    </div>
@endif
          </div>
        </div>
      </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--6">
      <div class="row">
        <div class="col-xl-4 order-xl-2">
          
          <!-- Progress track -->
          <div class="card">
            <!-- Card header -->
            <div class="card-header">
              <!-- Title -->
              <h5 class="h3 mb-0">Cart Summary</h5>
            </div>
            <!-- Card body -->
            <div class="card-body">
             <div class="ibox">

<div class="ibox-content">
<span>
Total
</span>
<h2 class="font-bold">
RM {{ Cart::total() }}
</h2>

<hr>

<!--span>
Current e-Wallet Balance 
</span>
<h2 class="font-bold">
RM {{auth()->user()->balance}}
</h2>

<hr>

<div class="m-t-sm">
<div class="btn-group">
 @if(auth()->user()->balance> floatval(preg_replace('/[^\d.]/', '',  Cart::total())))
<a href="/stock_reload_wallet" class="btn btn-primary btn-block"><i class="fa fa-shopping-cart"></i> Checkout</a>
@else
<h2 class="font-bold">
Please Reload you ewallet to checkout or reduce the amount of stock.
</h2>
@endif-->

<div class="m-t-sm">
<div class="btn-group">
 <!--  @if(Cart::total()>0 && (auth()->user()->role_id==5 || auth()->user()->referrer_id==1) ) -->
<!-- <a href="/stockReload" class="btn btn-primary btn-block"><i class="fa fa-shopping-cart"></i> Checkout</a> -->
<!-- @endif -->
<a href="/stockReload" class="btn btn-primary btn-block"><i class="fa fa-shopping-cart"></i> Checkout</a>
</div>
</div>
</div>
</div>
         
            </div>
          </div>
        </div>
        <div class="col-xl-8 order-xl-1">
         
          <div class="card">
            <div class="card-header">
              <div class="row align-items-center">
                <div class="col-8">
                  <h3 class="mb-0">Items in your cart </h3>
                </div>
                <div class="col-4 text-right">
                  <a href="#!" class="btn btn-sm btn-neutral">(<strong>{{ Cart::count() }}</strong>) items</a>
                </div>
              </div>
            </div>
            <div class="card-body">
   <div class="ibox">



<div class="ibox-content">
    @forelse (Cart::content() as $item)  
<div class="table-responsive">
<table class="table shoping-cart-table">
<tbody>
<tr>
<td width="90">
   <img class="cart-product-imitation" src="{{ asset( 'storage/'.$item->model->image) }}">

</td>
<td class="desc">
<h3>
<a href="#" class="text-navy">
{{$item->name}}
</a>
</h3>
<p class="small"> Weight
{{$item->model->weight}} g
</p>
<dl class="small m-b-none">
<dt>SKU {{$item->model->weight}}</dt>
<dd>{{$item->model->description}}.</dd>
</dl>
<div class="m-t-sm">

<form action="{{ url('cart', [$item->rowId]) }}" method="POST" class="side-by-side">
{!! csrf_field() !!}
<input type="hidden" name="_method" value="DELETE">
<br><input type="submit" class="btn btn-warning btn-link btn-sm" style="border-color: #696969; color: #696969;" value="Remove">
</form>
</div>
</td>
<td>
MYR{{$item->price}}
</td>
<td width="65">

  <div class="container">
    <div class="row">
        <div class="col-md-12">
            <form method="POST" action="/cart/{{ $item->rowId }}">


                  @csrf
                  @method('PATCH')
                  <input type="number" min="1" name="quantity" class="form-control" value="{{$item->qty}}">
                 <center> <button type="submit" class="btn btn-success btn-sm">  Update </button>
                   </center>
                 </form>
        </div>
    </div>



</td>
<td>
<h4>
RM {{ $item->subtotal }}
</h4>
</td>
</tr>
</tbody>
</table>
</div>
@empty

@endforelse
</div>
<div class="ibox-content">



@if(sizeof(Cart::content()) > 0) 
<a href="#" class=" pull-right"> <form action="{{ url('/emptyCart') }}" method="POST"> {!! csrf_field() !!} 
<input type="hidden" name="_method" value="DELETE"> 
<input type="submit" class="btn btn-warning btn-round" value="Empty Cart" style="background-color: black!important"> 
 </form> </a>
 @endif


<a href="/stocks" class="btn btn-white"><i class="fa fa-arrow-left"></i> Continue shopping</a>

</div>
            </div>
          </div>
        </div>
      </div>
    

    </div>

@endsection

@section('script')
<script>
        (function(){

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('.quantity').on('change', function() {
                var id = $(this).attr('data-id')
                $.ajax({
                  type: "PATCH",
                  url: '{{ url("/cart") }}' + '/' + id,
                  data: {
                    'quantity': this.value,
                  },
                  success: function(data) {
                    window.location.href = '{{ url('/cart') }}';
                  }
                });

            });

        })();

    </script>
@stop
