@extends('layouts.app')

@section('content')
<input type="hidden" id="routes-data" value='@json($routes)'>
<input type="hidden" id="user-data" value='@json($user)'>
<input type="hidden" id="order-data" value='@json($order)'>
<input type="hidden" id="shipping-data" value='@json($shipping)'>
<div id="user-postpaid-form">
    <container :user="user" :customers="customers" :order="order" :shipping="shipping"></container>
</div>
@endsection

@section('script')
<script src="//www.tracking.my/track-button.js"></script>
<script src="{{ mix('js/user-postpaid-form.js') }}"></script>
@stop
