<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>{{ $title }}</title>

    <style>
        .invoice-box {
            max-width: 800px;
            margin: auto;
            padding: 30px;
            border: 1px solid #eee;
            box-shadow: 0 0 10px rgba(0, 0, 0, .15);
            font-size: 16px;
            line-height: 24px;
            font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
            color: #555;
        }

        .invoice-box table {
            width: 100%;
            text-align: left;
        }

        .invoice-box table td {
            padding: 5px;
            vertical-align: top;
        }

        .invoice-box table tr td:nth-child(2) {
            text-align: right;
        }

        .invoice-box table tr.top table td {
            padding-bottom: 20px;
        }

        .invoice-box table tr.top table td.title {
            font-size: 45px;
            line-height: 45px;
            color: #333;
        }

        .invoice-box table tr.information table td {
            padding-bottom: 40px;
        }

        .invoice-box table tr.heading td {
            background: #eee;
            border-bottom: 1px solid #ddd;
            font-weight: bold;
        }

        .invoice-box table tr.details td {
            padding-bottom: 20px;
        }

        .invoice-box table tr.item td{
            border-bottom: 1px solid #eee;
        }

        .invoice-box table tr.item.last td {
            border-bottom: none;
        }

        .invoice-box table tr.total td {
            border-top: 2px solid #eee;
            font-weight: bold;
        }

        @media only screen and (max-width: 600px) {
            .invoice-box table tr.top table td {
                width: 100%;
                display: block;
                text-align: center;
            }

            .invoice-box table tr.information table td {
                width: 100%;
                display: block;
                text-align: center;
            }
        }

        /** RTL **/
        .rtl {
            direction: rtl;
            font-family: Tahoma, 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
        }

        .rtl table {
            text-align: right;
        }

        .rtl table tr td:nth-child(2) {
            text-align: left;
        }
    </style>
</head>

<body>
    <div class="invoice-box">
        <table cellpadding="0" cellspacing="0">
            <tr class="top">
                <td colspan="2">
                    <table>
                        <tr>
                            <td>
                                <u>{{ env('COMPANY_NAME') }}</u><br>
                                NO PENDAFTARAN: {{ env('COMPANY_REG_NO') }}<br>
                                TEL: {{ env('COMPANY_PHONE_NO') }} <br>
                                {{ env('COMPANY_ADDRESS') }} <br>
                            </td>
                            <td class="title">
                                <img src="{{ asset(config('app.logo')) }}" style="width:100px;">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr><td colspan="2"><hr></td></tr>
            <tr class="information">
                <td colspan="1">
                    Bill To: <br>
                    {{ $order->user->name }}<br>
                    {{ $order->user->phone_no }} <br>
                    {!! $order->user->address->full !!}
                </td>
                <td colspan="1">
                    Invoice #{{ $order->id }}<br>
                    Placed on {{ $order->created_at }}<br>
                    {{-- Customer: <br>
                    @if($order->customer)
                    {{ $order->customer->name }}<br>
                    {{ $order->customer->phone_no }}<br>
                    {!! $order->customer->address->full !!}
                    @else
                    SELF PICKUP
                    @endif --}}
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <table>
                        <tr class="heading">
                            <td>
                                Item
                            </td>
                            <td style="text-align: left;">
                                Quantity
                            </td>
                            <td>
                                Price ({{ $currency }})
                            </td>
                            <td style="text-align: right;">
                                Subtotal ({{ $currency }})
                            </td>
                        </tr>
                        @foreach($order->items as $item)
                        <tr class="item @if($order->type === 'stocks' && $loop->last) last @endif">
                            <td>
                                {{ $item->product->name }}
                            </td>
                            <td style="text-align: left;">{{ $item->quantity }}</td>
                            <td>
                                {{ number_format($item->unit_price, 2) }}
                            </td>
                            <td style="text-align: right;">{{ number_format($item->subtotal, 2) }}</td>
                        </tr>
                        @if($order->type === 'shipping')
                        <tr class="item">
                            <td>
                                Stocks Paid
                            </td>
                            <td style="text-align: left;">{{ $item->quantity }}</td>
                            <td>
                                {{ number_format($item->unit_price, 2) }}
                            </td>
                            <td style="text-align: right;">- {{ number_format($item->subtotal, 2) }}</td>
                        </tr>
                        @endif
                        @endforeach
                        @if(in_array($order->strictType, ['credits', 'deposit']))
                        <tr class="item last">
                            <td>
                                {{ $order->purpose }}
                            </td>
                            <td style="text-align: left;"></td>
                            <td></td>
                            <td style="text-align: right;">{{ number_format($order->amount, 2) }}</td>
                        </tr>
                        @elseif($order->type === 'shipping')
                        <tr class="item last">
                            <td>
                                @if($order->customer) Shipping Fee @else Self Pickup @endif
                            </td>
                            <td style="text-align: left;"></td>
                            <td></td>
                            <td style="text-align: right;">{{ number_format($order->amount, 2) }}</td>
                        </tr>
                        @endif
                        <tr class="total">
                            <td colspan="4" style="text-align: right;">
                               Total: {{ $currency }} {{ number_format($order->amount, 2) }}
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="text-align: center;">
                    <h6>*ALL PRICE ARE SUBJECTED TO SST. <br>
                        THIS IS COMPUTER GENERATED INVOICES AND DOES NOT REQUIRE A SIGNATURE. <br>
                    </h6>
                </td>
            </tr>
        </table>
    </div>
</body>
</html>
