@extends('layouts.app')

@section('content')
   <nav id="navbar-main" class="navbar navbar-horizontal navbar-main navbar-expand-lg navbar-dark bg-primary" style="background: linear-gradient(87deg, rgba(75, 75, 75, 0.72)0, rgb(0, 0, 0) 100% ) !important;">
     <div class="container">
       <a class="navbar-brand" href="/">
         <img src="{{ config('app.logo') }}">
       </a>
       <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-collapse" aria-controls="navbar-collapse" aria-expanded="false" aria-label="Toggle navigation">
         <span class="navbar-toggler-icon"></span>
       </button>
       <div class="navbar-collapse navbar-custom-collapse collapse" id="navbar-collapse">
         <div class="navbar-collapse-header">
           <div class="row">
             <div class="col-6 collapse-brand">
               <a href="/">
                 <img src="{{ config('app.logo') }}">
               </a>
             </div>
             <div class="col-6 collapse-close">
               <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbar-collapse" aria-controls="navbar-collapse" aria-expanded="false" aria-label="Toggle navigation">
                 <span></span>
                 <span></span>
               </button>
             </div>
           </div>
         </div>
         <ul class="navbar-nav mr-auto">
           <li class="nav-item">
             <a href="https://weitizen.com" class="nav-link">
               <span class="nav-link-inner--text">Go To Official Website</span>
             </a>
           </li>
     
           <li class="nav-item">
             <a href="/check" class="nav-link">
               <span class="nav-link-inner--text">Check dealer Certificate</span>
             </a>
           </li>
       

         </ul>
       </div>
     </div>
   </nav>

   <div class="main-content">
     <!-- Header -->
     <div class="header bg-primary pt-5 pb-7" style="background: linear-gradient(87deg, rgba(75, 75, 75, 0.72)0, rgb(0, 0, 0) 100% ) !important;">
       <div class="container">
         <div class="header-body">
           <div class="row align-items-center">
             <div class="col-lg-6">
               <div class="pr-5">
                 <h2 class="display-2 text-white font-weight-bold mb-0">Weitizen Certificate</h2>
                 @if($status==3)  <h3 class="display-4 text-white font-weight-light">Please enter dealer's  phone number or WeChat ID to find out the authenticity of the dealer</h3>
                
                 <div class="mt-5">
                     <form method="get" action="/check">
                               <table>
                        <tr>
                          <td width="100%"> <input name="q" type="text" class="form-control form-control-muted" placeholder="Please enter dealer's WeChat ID / Phone No.">  </td>
                          <td width="100%">  <button type="submit" class="btn btn-neutral my-2" style="color: black!important;"><i class="fas fa-search"></i> Search</button> </td>
                        </tr>
                      </table>
                       
                        
                    </form>
                      @endif
                   @if($status==0)
                   <div class="alert alert-danger" role="alert">
                   <strong> {{$message}}</strong> Not Found in our records

                   </div>
                    <form method="get" action="/check">
                                      <table>
                        <tr>
                          <td width="100%"> <input name="q" type="text" class="form-control form-control-muted" placeholder="Please enter Dealer's WeChat ID / Phone No.">  </td>
                          <td width="100%">  <button type="submit" class="btn btn-neutral my-2"><i class="fas fa-search"></i> Search</button> </td>
                        </tr>
                      </table>

                        
                    </form>
                   @endif
                    @if($status==1)

                   <img  height="600px" src="{{$c}}">
                   @endif
                 </div>
               </div>
             </div>

        
           </div>
         </div>

       </div>
       <div class="separator separator-bottom separator-skew zindex-100">
         <svg x="0" y="0" viewBox="0 0 2560 100" preserveAspectRatio="none" version="1.1" xmlns="http://www.w3.org/2000/svg">
           <polygon class="fill-secondary" points="2560 0 2560 100 0 100"></polygon>
         </svg>
       </div>
     </div>





   </div>
   <footer class="py-5" id="footer-main">
     <div class="container">
       <div class="row align-items-center justify-content-xl-between">
         <div class="col-xl-12">
           <div class="copyright text-center text-xl-center text-muted">
            <ul class="nav nav-footer justify-content-center justify-content-xl-end">
                <li class="nav-item">
               <a href="https://weitizen.com" class="nav-link" target="_blank">Copyright © 2019 Weitizen.com All rights reserved.</a>
             </li>
               <li class="nav-item">
               <a href="https://wrointernational.com/" class="nav-link" target="_blank">Dealership eCommerce System Powered By WRO International Sdn Bhd.</a>
             </li>
             </ul>
             
             
           </div>
         </div>
        
       </div>
     </div>
   </footer>


     



@endsection
