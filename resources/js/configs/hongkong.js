export default [
	      {
           "name" : "Sha Tin ",
            "code" : "HK-01"
	     },
           
        {
            "name" : "Tuen Mun New Town",
            "code" : "HK-02"
        },
        {
            "name" : "Tai Po",
            "code" : "HK-03"
        },
        {
            "name" : "uen Long",
            "code" : "HK-04"
        },
        {
            "name" : "Fanling-Sheung Shui",
            "code" : "HK-05"
        },
        {
            "name" : "Ma On Shan",
            "code" : "HK-06"
        },
        {
            "name" : "Tseung Kwan O New Town",
            "code" : "HK-07"
        },
        {
            "name" : "Tin Shui Wai",
            "code" : "SG-05"
        },
        {
            "name" : "North Lantau ",
            "code" : "SG-05"
        }
]
