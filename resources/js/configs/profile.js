export default {
    deposit: [{
        'role_id': 1,
        'amount': 0
    }, {
        'role_id': 2,
        'amount': 100
    }, {
        'role_id': 3,
        'amount': 200
    }, {
        'role_id': 4,
        'amount': 300
    }, {
        'role_id': 5,
        'amount': 400
    }]
}
