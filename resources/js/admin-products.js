import container from 'components/admin/products/container'

let products = new Vue({
    el: '#admin-products',
    data: {
        routes: assignData(document.querySelector('#routes-data')),
    },
    components: {
        container
    }
})
