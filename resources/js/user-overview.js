import container from 'components/user/overview/container'
let dashboard = new Vue({
    mounted() {
        this.getRoles()
    },
    el: '#user-overview',
    data: {
        user: assignData(document.querySelector('#user-data')),
        routes: assignData(document.querySelector('#routes-data')),
        roles: []
    },
    components: {
        container
    },
    methods: {
        getRoles() {
            // get roles
            return axios(this.$root.routes.getRoles)
                .then(res => {
                    // set data from response
                    this.roles = res.data.data
                }).catch(err => {})
        }
    }
})
