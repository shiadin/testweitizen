import container from 'components/user/stocks/container'
let userStock = new Vue({
    mounted() {
        
    },
    el: '#user-stocks',
    data: {
        products: assignData(document.querySelector('#products-data')),
        stocks: assignData(document.querySelector('#stocks-data')),
        user: assignData(document.querySelector('#user-data')),
        routes: assignData(document.querySelector('#routes-data')),
    
    },
    components: {
        container
    },
    methods: {
       
    }
})
