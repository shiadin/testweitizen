import container from 'components/admin/credits/container'

const credits = new Vue({
    mounted () {
    },
    el: '#admin-credits',
    data: {
        routes: assignData(document.querySelector('#routes-data')),
        users: [],
        roles: [],
        links: {},
        meta: {},
        loading: false,
        filters: {
            keyword: null
        }
    },
    methods: {
        getUsers () {
            // turn into loading state
            this.loading = true
            // get credits
            return axios(this.routes.getUsers, {
                    params: {
                        ...this.filters
                    }
                })
                .then(res => {
                    // set data from response
                    this.users = res.data.data
                    this.links = res.data.links ? res.data.links : null
                    this.meta = res.data.meta ? res.data.meta : null
                    this.situations = res.data.situations ? res.data.situations : {}
                }).catch(err => {})
                .then(() => this.loading = false)
        },
    },
    components: {
        container
    }
})
