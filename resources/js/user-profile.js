import container from 'components/user/profile/container'

const profile = new Vue({
    mounted() {
        this.getRoles()
    },
    el: '#user-profile',
    data: {
        user: assignData(document.querySelector('#user-data')),
        routes: assignData(document.querySelector('#routes-data')),
        roles: []
    },
    methods: {
        getRoles() {
            // get roles
            return axios(this.routes.getRoles)
                .then(res => {
                    // set data from response
                    this.roles = res.data.data
                }).catch(err => {})
        }
    },
    components: {
        container
    }
});
