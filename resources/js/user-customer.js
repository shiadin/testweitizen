import container from 'components/user/customer/container'
let payment = new Vue({
    mounted() {
        this.getRoles()
    },
    el: '#user-customer',
    data: {
       // commission: assignData(document.querySelector('#commision-data')),
         user: assignData(document.querySelector('#user-data')),
         stats: assignData(document.querySelector('#stats-data')),
        routes: assignData(document.querySelector('#routes-data')),
        roles: []
    },
    components: {
        container
    },
    methods: {
        getRoles() {
            // get roles
            return axios(this.$root.routes.getRoles)
                .then(res => {
                    // set data from response
                    this.roles = res.data.data
                }).catch(err => {})
        }
    }
})
