import container from 'components/user/company/container'

const users = new Vue({
    mounted () {
        this.getRoles()
    },
    el: '#admin-downlines',
    data: {
        routes: assignData(document.querySelector('#routes-data')),
        admin: assignData(document.querySelector('#admin-data')),
        roles: [],
    },
    methods: {
        getRoles () {
            // get params from url address
            // let urlParams = new URLSearchParams(window.location.search);
            // let params = {}
            // urlParams.forEach((val, key) => params[key] = val)
            // get roles
            return axios(this.routes.getRoles)
                .then(res => {
                    // set data from response
                    this.roles = res.data.data
                }).catch(err => {})
        }
    },
    components: {
        container
    }
})
