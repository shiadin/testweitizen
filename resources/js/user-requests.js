import container from 'components/user/requests/container'

let requests = new Vue({
    el: '#user-requests',
    data: {
        user: assignData(document.querySelector('#user-data')),
        routes: assignData(document.querySelector('#routes-data')),
    },
    components: {
        container
    },
})
