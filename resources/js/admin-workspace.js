import container from 'components/admin/workspace/container'
let workspace = new Vue({
    el: '#admin-workspace',
    data: {
        routes: assignData(document.querySelector('#routes-data')),
        couriers: assignData(document.querySelector('#couriers-data'))
    },
    components: {
        container
    }
})
