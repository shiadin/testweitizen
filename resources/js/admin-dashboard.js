// get shippings data
let shippings = assignData(document.querySelector('#shippings-data')).months
// extract data to form chart
let data = Object.values(shippings)
// set label format
let labelFormat = 'MMM, Y'
// get labels from shippings and parse to month, year
let labels = Object.keys(shippings).map(date => moment(date, 'YYYY/M').format(labelFormat))
if (labels.length < 6) {
    // get size need to fill
    let size = 6 - labels.length
    // make sure real data is in the middle
    let front = Math.floor(size / 2)
    let back = size - front
    // loop to fill empty data
    for (let i = 0; i < front; i++) {
        labels.unshift(moment(labels[0], labelFormat).subtract(1, 'M').format(labelFormat))
        data.unshift(0)
    }
    for (let i = 0; i < back; i++) {
        labels.push(moment(labels[labels.length - 1], labelFormat).add(1, 'M').format(labelFormat))
        data.push(0)
    }
}

var myLineChart = new Chart($('#stock-sales'), {
    type: 'line',
    data: {
        datasets: [{
            data
        }],
        labels
    },
    options: {
        scales: {
            xAxes: [

            ],
            yAxes: [{
                gridLines: {
                    color: '#212529',
                    zeroLineColor: '#212529'
                },
                ticks: {
                    callback: function(t) {
                        if (!(t % 10))
                            return t
                    }
                }
            }]
        },
    }
})

