import container from 'components/user/shipping/container'

let shipping = new Vue({
    el: '#user-shipping-form',
    data: {
        user: assignData(document.querySelector('#user-data')),
        order: assignData(document.querySelector('#order-data')),
        routes: assignData(document.querySelector('#routes-data')),
        shipping: assignData(document.querySelector('#shipping-data')),
    },
    computed: {
        customers () {
            return this.user ? this.user.customers : []
        }
    },
    components: {
        container
    }
})
