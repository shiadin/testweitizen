/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
require('./bootstrap')

window.assignData = (element) => {
    if (!element) {
        console.warn(`element not found, value unsigned`)
        return false
    }
    let value = JSON.parse(element.value)
    element.remove()
    return value
}

import {
    Pagination,
    Dialog,
    Button,
    Table,
    TableColumn,
    Popover,
    Tooltip,
    Upload,
    Progress,
    Loading,
    Message,
    Form,
    FormItem,
    Tree,
    DatePicker,
    Input
} from 'element-ui'

import 'element-ui/lib/theme-chalk/index.css'
import VueDataTables from 'vue-data-tables'
import lang from 'element-ui/lib/locale/lang/en'
import locale from 'element-ui/lib/locale'
import Multiselect from 'vue-multiselect'

window.Vue = require('vue')
Vue.component('multiselect', Multiselect)

Vue.filter('capitalize', value => _.capitalize(value.toString()))
Vue.filter('uppercase', value => _.upperCase(value.toString()))

locale.use(lang)
Vue.use(Pagination)
Vue.use(Dialog)
Vue.use(Button)
Vue.use(Table)
Vue.use(TableColumn)
Vue.use(Popover)
Vue.use(Tooltip)
Vue.use(Upload)
Vue.use(Progress)
Vue.use(Loading)
Vue.use(Form)
Vue.use(FormItem)
Vue.use(Tree)
Vue.use(DatePicker)
Vue.use(Input)
Vue.use(VueDataTables)

Vue.prototype.$message = Message

window.notice = (content, type = 'info') => {
    if (document.querySelector('#notice-alert')) {
        document.querySelector('#notice-alert').remove()
    }
    let t = `<div id="notice-alert" class="alert alert-${type} alert-dismissible sticky-top m-0 rounded-0 fade show" role="alert">
                <span class="alert-inner--text">${content}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>`
    document.querySelector('header.navbar').insertAdjacentHTML("afterend", t)
}

window.handleValidation = error => {
    let errors = {}
    Object.entries(error.response.data.errors).forEach(error => {
        errors[error[0]] = error[1][0]
    })
    return errors
}

window.copyStringToClipboard = (str) => {
    var el = document.createElement('textarea');
    el.value = str;
    el.setAttribute('readonly', '');
    el.style = {position: 'absolute', left: '-9999px'};
    document.body.appendChild(el);

    if (navigator.userAgent.match(/ipad|ipod|iphone/i)) {
        // save current contentEditable/readOnly status
        var editable = el.contentEditable;
        var readOnly = el.readOnly;

        // convert to editable with readonly to stop iOS keyboard opening
        el.contentEditable = true;
        el.readOnly = true;

        // create a selectable range
        var range = document.createRange();
        range.selectNodeContents(el);

        // select the range
        var selection = window.getSelection();
        selection.removeAllRanges();
        selection.addRange(range);
        el.setSelectionRange(0, 999999);

        // restore contentEditable/readOnly to original state
        el.contentEditable = editable;
        el.readOnly = readOnly;
    } else {
        el.select();
    }

    document.execCommand('copy');
    document.body.removeChild(el);
}

// fix the bug of modal in IOS Safari browser
window.modalAppendToBody = (selector) => {
    $(selector).on('show.bs.modal', function () {
        // if modal parent is not body element
        if (!$(selector).parent().is('body')) {
            // attach modal to body
            $(selector).appendTo('body')
        }
    })
}

window.getQueryStringParams = query => {
    return query
        ? (/^[?#]/.test(query) ? query.slice(1) : query)
            .split('&')
            .reduce((params, param) => {
                    let [key, value] = param.split('=');
                    if (decodeURIComponent(key).match(/\[\]/)) {
                        // remove [] from key
                        let newKey = decodeURIComponent(key).replace('[]', '')
                        // get value
                        let val = value ? decodeURIComponent(value.replace(/\+/g, ' ')) : ''
                        // form a new array value if new key not found
                        if (!params[newKey]) {
                            params[newKey] = [ val ]
                        } else {
                            params[newKey].push(val)
                        }
                    } else {
                        params[key] = value ? decodeURIComponent(value.replace(/\+/g, ' ')) : '';
                    }
                    return params;
                }, {}
            )
        : {}
};
// set default axios error capturer
axios.interceptors.response.use(response => response, error => {
    if (error.response) {
        // if error is validation errors
        if (error.response.status == 422) {
            // alert all error sent
            // Object.keys(error.response.data.errors).forEach(ex => {
            //     new Noty({
            //         type: 'error',
            //         text: error.response.data.errors[ex][0]
            //     }).show();
            // })
        // unknow status, probably cause by logged out account
        } else if (error.response.status == 419) {
            window.location.reload()
        } else {
            // The request was made and the server responded with a status code
            // that falls out of the range of 2xx
            let message = error.hasOwnProperty("response") ? error.response.data.message : "Unexpected Error Occured"
            if (!message.length) {
                message = error.response.statusText
            }
            Message.error(message)
        }

    } else if (error.request) {
        // The request was made but no response was received
        // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
        // http.ClientRequest in node.js
        console.log(error.request);
    } else {
        // Something happened in setting up the request that triggered an Error
        console.log('Error', error.message);
    }
    return Promise.reject(error);
});
