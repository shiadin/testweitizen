import container from 'components/user/shippings/container'

let shippings = new Vue({
    el: '#user-shippings',
    data: {
        user: assignData(document.querySelector('#user-data')),
        routes: assignData(document.querySelector('#routes-data')),
        couriers: window.location.href.match('admin') ? assignData(document.querySelector('#couriers-data')) : []
    },
    components: {
        container
    },
})
