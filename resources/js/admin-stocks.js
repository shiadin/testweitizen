import container from 'components/admin/stocks/container'

const stocks = new Vue({
    mounted () {
    },
    el: '#admin-stocks',
    data: {
        routes: assignData(document.querySelector('#routes-data')),
        stocks: [],
        products: [],
        roles: [],
        links: {},
        meta: {},
        loading: false,
        filters: {
            username: null
        }
    },
    methods: {
        getStocks () {
            // turn into loading state
            this.loading = true
            // get stocks
            return axios(this.routes.getStocks, {
                    params: {
                        ...this.filters
                    }
                })
                .then(res => {
                    // set data from response
                    this.stocks = res.data.data
                    this.links = res.data.links ? res.data.links : null
                    this.meta = res.data.meta ? res.data.meta : null
                    this.products = res.data.products ? res.data.products : {}
                }).catch(err => {})
                .finally(() => this.loading = false)
        },
    },
    components: {
        container
    }
})
