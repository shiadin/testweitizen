<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommisionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('commisions', function (Blueprint $table) {
            $table->increments('id');
             $table->string('name');
            $table->date('start');
            $table->date('end');
            $table->longtext('description')->nullable();
            $table->longtext('meta')->nullable();
            $table->boolean('active')->default(false);
            $table->longtext('eligible_to')->nullable();
            $table->longtext('conditions')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('commisions');
    }
}
