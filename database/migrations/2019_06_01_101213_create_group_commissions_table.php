<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGroupCommissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('group_commissions', function (Blueprint $table) {
            $table->increments('id');
             $table->integer("user_id")->unsigned();
             $table->string("type");
             $table->unsignedDecimal('amount', 8, 2);
             $table->integer("status")->default(0);
             $table->longText("meta")->nullable();
             $table->integer("downline_id")->nullable();
             $table->string("downline")->nullable();
             $table->integer("downline_level")->nullable();
             $table->integer("current_level")->nullable();
             $table->integer("quantity")->nullable();
             $table->unsignedDecimal("unit_price", 8, 2)->nullable();

             $table->timestamps();
             $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('group_commissions');
    }
}
