<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchases', function (Blueprint $table) {
            $table->increments('id');
             $table->integer('user_id');
             $table->string('order_id')->nullable();
             $table->string('name')->nullable();
             $table->string('price')->nullable();
             $table->integer('price_id')->nullable();
             $table->integer('quantity')->nullable();
             $table->string('status')->nullable();
             $table->string('molpay_id')->nullable();
             $table->string('paid')->nullable();
            $table->string('product_id')->nullable()->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchases');
    }
}
