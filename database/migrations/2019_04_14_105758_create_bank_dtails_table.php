<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBankDtailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bank_dtails', function (Blueprint $table) {
            $table->increments('id');
             $table->integer("user_id")->unsigned();
             $table->string("bank");
             $table->string("type_of_withdral")->nullable(); //keep in wallet or withdral monthly
             $table->string("name");
             $table->string("account");
             $table->timestamps();
             $table->softDeletes();
             $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bank_dtails');
    }
}
