<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMolpaysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('molpays', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_id')->nullable();
            $table->integer('quantity')->nullable();
            $table->decimal('amount', 10, 2)->nullable();
            $table->string('order_id');
            $table->string('order')->nullable();
            $table->longText('price')->nullable();
            $table->string('app_code')->nullable();
            $table->integer('transaction_id');
            $table->integer('user_id')->unsigned();
            $table->string('domain');
            $table->string('status', 12);
            $table->string('currency', 12);
            $table->date('paid_at');
            $table->string('channel');
            $table->string('error_code')->nullable();
            $table->string('error_description')->nullable();
            $table->string('security_key', 32)->nullable();
            $table->longText('description')->nullable();
            $table->longText('meta')->nullable();
            $table->longText('content')->nullable();
            $table->string('tax')->nullable();
            $table->string('total')->nullable();
            $table->softDeletes();
            $table->index('transaction_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('molpays');
    }
}
