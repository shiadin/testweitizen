<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("order_id")->unsigned();
            $table->integer("user_id")->unsigned();
            $table->integer("approver_id")->unsigned()->nullable();
            $table->string("gateway");
            $table->unsignedDecimal('amount', 8, 2);
            $table->string("token");
            $table->integer("status")->default(0);
            $table->string("memo")->nullable();
             $table->string("molpay_order_id")->nullable();
            
            $table->timestamps();

            $table->foreign('approver_id')->references('id')->on('users');
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('order_id')->references('id')->on('orders');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
