<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddReputationFieldOnUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::table('users', function (Blueprint $table) {
            $table->double('reputation', 8, 5)->default(0)->after('ic_back');
            $table->boolean('isVerified')->nullable()->default(false)->after('reputation');
            $table->string('otp_code')->nullable()->after('isVerified');
            $table->string('country_code', 4)->nullable()->after('otp_code');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('reputation');
            $table->dropColumn('country_code');
            $table->dropColumn('otp_code');
            $table->dropColumn('isVerified');
        });
    }
}
