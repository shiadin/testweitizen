<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGamifyTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // reputations table
        Schema::create('reputations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->double('point', 8, 5)->default(0);
            $table->integer('subject_id')->nullable();
            $table->integer('order_id')->nullable();
            $table->string('subject_type')->nullable();
            $table->integer('quantity')->nullable();
            $table->text('price')->nullable();
            $table->longText('product')->nullable();
            $table->longText('upline')->nullable();
            $table->longText('downline')->nullable();
            $table->string('last_transaction_compagin')->nullable();
            $table->string('last_transaction_commission')->nullable();
            $table->unsignedInteger('payee_id')->nullable();
            $table->longText('meta')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        // badges table
        Schema::create('badges', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('description')->nullable();
            $table->string('icon')->nullable();
            $table->tinyInteger('level')->default(config('gamify.badge_default_level', 1));
            $table->timestamps();
        });

        // user_badges pivot
        Schema::create('user_badges', function (Blueprint $table) {
            $table->primary(['user_id', 'badge_id']);
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('badge_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_badges');
        Schema::dropIfExists('badges');
        Schema::dropIfExists('reputations');
    }
}
