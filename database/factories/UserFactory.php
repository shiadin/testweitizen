<?php

use Faker\Generator as Faker;
use Illuminate\Support\Facades\Hash;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\User::class, function (Faker $faker) {
    // get gender
    $gender = ['male', 'female'];
    $gender = $gender[array_rand($gender)];
    // get status
    $status = [0, 1, -1];
    $status = $status[array_rand($status)];
    // create MY faker
    $fakerMY = \Faker\Factory::create('ms_MY');
    return [
        'name' => $faker->name,
        'username' => $faker->unique()->username,
        'email' => $faker->unique()->safeEmail,
        'email_verified_at' => now(),
        'remember_token' => str_random(10),
        "gender" => $gender,
        "isVerified" => 1,
        "password" => Hash::make('123456'),
        "phone_no" => $fakerMY->mobileNumber($countryCodePrefix = false, $formatting = true),
        "credits" => rand(0, 500),
        "ic_no" => $fakerMY->myKadNumber($gender),
        "status" => $status
    ];
});
