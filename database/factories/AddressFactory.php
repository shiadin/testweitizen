<?php

use Faker\Generator as Faker;

$factory->define(App\Address::class, function (Faker $faker) {
    // create MY faker
    $fakerMY = \Faker\Factory::create('ms_MY');
    $state = $fakerMY->state();
    return [
        "street" => $fakerMY->streetName(),
        "street_2" => $fakerMY->township(),
        "zipcode" => $fakerMY->postcode(),
        "city" => $fakerMY->city(),
        "state" => $state,
        "country" => "Malaysia"
    ];
});
