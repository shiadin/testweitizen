<?php

use App\User;
use App\Address;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try {
            DB::beginTransaction();
            User::create([
                'role_id' => 999,
                'username' => 'weitizen',
                'password' => Hash::make(123456)
            ]);
            // generate admin
            // factory(User::class, 10)->create([
            //     'role_id' => 950
            // ]);
            // // generate reseller 1
            // factory(User::class, 10)->create([
            //     'role_id' => 1,
            //     'referrer_id' => User::where('role_id', '>', 1)->get()->random()
            // ])->each(function ($user) {
            //     factory(Address::class, 1)->create([
            //         'user_id' => $user->id
            //     ]);
            // });
            // // generate reseller 2
            // factory(User::class, 10)->create([
            //     'role_id' => 2,
            //     'referrer_id' => User::where('role_id', '>', 2)->get()->random()
            // ])->each(function ($user) {
            //     factory(Address::class, 1)->create([
            //         'user_id' => $user->id
            //     ]);
            // });
            // // generate reseller 3
            // factory(User::class, 10)->create([
            //     'role_id' => 3,
            //     'referrer_id' => User::where('role_id', '>', 3)->get()->random()
            // ])->each(function ($user) {
            //     factory(Address::class, 1)->create([
            //         'user_id' => $user->id
            //     ]);
            // });
            // // generate reseller 4
            // factory(User::class, 10)->create([
            //     'role_id' => 4,
            //     'referrer_id' => User::where('role_id', '>', 4)->get()->random()
            // ])->each(function ($user) {
            //     factory(Address::class, 1)->create([
            //         'user_id' => $user->id
            //     ]);
            // });
            // // generate reseller 5
            // factory(User::class, 10)->create([
            //     'role_id' => 5
            // ])->each(function ($user) {
            //     factory(Address::class, 1)->create([
            //         'user_id' => $user->id
            //     ]);
            // });
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            throw new \Exception($e->getMessage(), 500);
        }
    }
}
