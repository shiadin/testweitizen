<?php

use App\Role;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // preset data
        $data = collect([[
            "id" => 999,
            "name" => 'Super Admin',
            "description" => null
        ], [
            "id" => 950,
            "name" => 'Admin',
            "description" => null
        ], [
            "id" => 700,
            "name" => 'Customer',
            "description" => null
        ]])->map(function ($role) {
            $role['created_at'] = $role['updated_at'] = Carbon::now('utc')->toDateTimeString();
            return $role;
        })->toArray();

        $data = array_merge($data, collect(config('resellers'))->map(function ($role) {
            return [
                'id' => $role['role_id'],
                'name' => $role['name'],
                'description' => $role['description'],
                'created_at' => Carbon::now('utc')->toDateTimeString(),
                'updated_at' => Carbon::now('utc')->toDateTimeString()
            ];
        })->toArray());

        Role::insert($data);
    }
}
