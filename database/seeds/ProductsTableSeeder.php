<?php

use App\Price;
use App\Product;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try {
            DB::beginTransaction();
            // generate a product
            $product = Product::create([
                "user_id" => 1,
                "name" => "DEMO PRODUCT",
                "sku" => "demo",
                "description" => null,
                "stock_amount" => null,
                "weight" => 500,
                "status" => 1
            ]);
            // create prices for product
            collect([[
                "role_id" => 1,
                "amount" => 78,
            ], [
                "role_id" => 2,
                "amount" => 68,
            ], [
                "role_id" => 3,
                "amount" => 53,
            ], [
                "role_id" => 4,
                "amount" => 40,
            ], [
                "role_id" => 5,
                "amount" => 30,
            ], [
                "role_id" => 700,
                "amount" => 138,
            ], [
                "role_id" => 950,
                "amount" => 0,
            ], [
                "role_id" => 999,
                "amount" => 0,
            ]])->each(function ($params) use ($product) {
                Price::create(array_merge($params, [
                    'user_id' => $product->user_id,
                    'product_id' => $product->id
                ]));
            });
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            throw new \Exception($e->getMessage(), 500);
        }
    }
}
