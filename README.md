# Reseller Management Software

<p align="right">V1.0.2</p>
<p align="right">Revised on 19 January 2019</p>

-----

## Prerequisite

- The developer should familiar with Laravel Framework, or any PHP framework.
- The developer should familiar with MYSQL.
- The developer should familiar with NPM and Vue.js.
- The developer should familiar with SSH connection, and console commands.
- The developer should familiar with cron jobs of Linux OS.
- The developer should familiar with GIT operations.

---

## Installation

1. Git clone source code `git@github.com:lostncg/reseller-management.git` to new server [1]
2. Copy contents of .env.example file to .env file, refer to *ENV Section*
3. Check the .env file in application root, ensure the system configuration is correct
4. Copy contents of \*.php.example in folder **configs** to \*.php, refer to *Config Section*
5. Update the vendor packages by command `composer update` [2]
6. Install node_modules by command `npm install` [3]

    i.) Fix issues found on "creativetimofficial/argon-dashboard", refer to *Issues Sections*

7. Run `npm run prod` to generate required js and css assets
8. Set up the cron job, refer to *Cron Jobs Section*
9. Initialize the database, refer to *Database Section*
10. Move required image assets into **storage/app/public/images**, refer to *Assets Section*
11. Run the command `php artisan storage:link` to generate a symbolic link point to storage folder



#### Reminder

The documentation assumes the developer able to explore and learn themselves the knowledges required from **Prerequisite**, Do not ask for this section since it is too broad.

1. The private key must set up in OS before the git clone process
2. Ensure the system able to support requirements of Laravel
3. Ensure the system has npm and nodejs installed

---

## Database

### Create / Refresh the System Database

Please make sure the config files in **Installation step 3** has been copied before proceed to this section.

By default, the system will only generate initial data for:

1. First user (super admin), **remember to modify the password after login**

   username: admin

   password: 123456

2. Roles table

   The roles data includes superadmin, admin, customer, and resellers.

   To customize the levels and settings of reseller, please modify in **config/resellers.php** before migrate the data.

In the command console, enter the command `php artisan migrate:fresh --seed` to create the tables into a database. The argument '--seed' is used to seed some initial data into the system.

---

## Cron Jobs

`php artisan schedule:run`

example: `* * * * * php /var/www/canale/artisan schedule:run >> /dev/null 2>&1`

- Tracking the order status every day, turn order into complete stage if parcel delivered or last update over 20 days
- Queue Listener `php artisan queue:work --tries=3` - listen to order events, send email to reseller if necessary.

---
## Config

### Deposit Instructions (config/deposits.php)

*Instructions shown in deposit form when user request to upgrade or first time login*

The upload guideline can be modified through **config\deposits.php**

### Resellers Configuration (config/resellers.php)

*Settings for different level of reseller*

1. role id of reseller (use in initialize database)
2. name of reseller (use in initialize database)
3. description of reseller (use in initialize database)
4. deposit need to pay by of reseller (set 0 if not necessary)

The configuration can be modified through **config\resellers.php**

### Aftership Configuration (config\aftership.php)

*Courier code required in tracking parcel through hacking the service from https://track.aftership.com/*

The configuration will shown as the courier options when admin process the order.

`[code] => [courier name]`

To add more courier options into system, please refer to courier list in https://www.aftership.com/couriers

The configuration can be modified through **config\aftership.php**

### Certificate Configuration (config\certificate.php)

*To insert the reseller data into reseller certificate image*

The certificate draft is stored at **storage/app/public/certificate-draft.png**

Refer to documentation in config file to customize the position, size, color, and etc options of data inserted into image.

The configuration can be modified through **config\certificate.php**

### Countries Configuration

*Countries and its states list*

The configuration can be modified through
1. **config\resellers.php**
2. **resources/js/configs/malaysia.js**
3. **resources/js/configs/singapore.js**

PS: The name of state must be the same in js file and php file above

### Shipping Fee Configuration (config\shipping.php)

*A list of price for shipping fee based on weight of parcel*

The configuration can be modified through **config\shipping.php**

Current Supported Countries: Malaysia, Singapore

---

## Assets Section (storage/app/public/images/)

Image resources must stored in folder **storage/app/public/images**/

1. bg.png - background for login and register page
2. certificate-draft.png - reseller certificate image template
3. favicon.png - favicon website
4. logo.png - logo website

---

## ENV

*The sensitive information need to provided*

1. APP_NAME - application name
2. APP_URL - application url
3. DB_DATABASE - database name
4. DB_USERNAME - database username
5. DB_PASSWORD - database password
6. MAIL_HOST - mail server host
7. MAIL_PORT - mail server port
8. MAIL_USERNAME - mail server username
9. MAIL_PASSWORD - mail server password
10. MAIL_ENCRYPTION - email encrypt protocol
11. MAIL_FROM_ADDRESS - sender email address shown in receiver mailbox
12. MAIL_FROM_NAME - sender name shown in receiver mailbox
13. COMPANY_NAME - company name
14. COMPANY_REG_NO - company register number
15. COMPANY_PHONE_NO - company contact number
16. COMPANY_ADDRESS - company address
17. HIGHEST_RESELLER_LEVEL - the highest reseller role id
18. CREDIT_NAME - the name of credits used for shipping parcel
19. MODULES - system modules enabled, default is "shipping,stocks,credits,deposit"
20. AUTH_TITLE - some title shown in left hand side of login/register form
21. AUTH_BODY - some description shown in left hand side of login/register form
22. AUTH_BUTTON - button content shown in left hand side of login/register form
23. AUTH_LINK - button link in left hand side of login/register form

---

## Issues
#### Issue 1
Compiling errors caused by npm package "creativetimofficial/argon-dashboard"

a. "File to import not found or unreadable" shown on importing file 'custom/components.scss' [ISSUE LINK](https://github.com/creativetimofficial/argon-dashboard/pull/5)

**Solution:**

i. open the file in path "node_modules/@creative-tim-official/argon-dashboard-free/assets/scss/custom/_components.scss"
ii. replace "custom/" with "./"

b. "JisonLexerError: Lexical error on line 1: Unrecognized text.
" shown on css syntax "calc" [ISSUE LINK](https://github.com/creativetimofficial/argon-dashboard/pull/12)

**Solution:**

i.open the file in path "node_modules/@creative-tim-official/argon-dashboard-free/assets/scss/core/icons/_icon.scss"
ii. replace "\$icon-size-\*" with "\#\{$icon-size-\*}" in all calc function

> Direct modification in npm package is not a good pratice, but I'm not going to fork a package just for a tiny modification, there is pull requests for issues mentioned already, just waiting for owner to merge it. For current solution, please direct modify in package content.

---

## Recommended

Maintainer are strongly recommended to improve and enhance the maintenance documentation
any matters not mentioned in the file.
